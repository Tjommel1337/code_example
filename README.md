# Start KeyCloak-Docker

## Start Reverse Proxy

`docker run -d --name el_reverseproxy -p 443:443 registry.gitlab.com/hr-hosting/myapp/dev-infra/nginx:latest`

Test env:
https://auth.myapp.dev (Centralized KeyCloak instance)
https://www.myapp.dev (Local MyApp UI)
https://api.myapp.dev (Local MyApp API)

## Normal start with DB-Connection

`docker run -d -p 8080:8080 -e KEYCLOAK_ADMIN=admin -e KEYCLOAK_ADMIN_PASSWORD=admin quay.io/keycloak/keycloak:22.0.1 start-dev --db mariadb --db-url jdbc:mariadb://host.docker.internal:63307/keycloak --db-username keycloak --db-password keycloak --features=declarative-user-profile`

## With mapped theme

`docker run -p 8080:8080 -d -v /mnt/d/workspace/MyApp/v3/dist/apps/keycloak:/opt/keycloak/themes/mytheme -e KEYCLOAK_ADMIN_PASSWORD=admin quay.io/keycloak/keycloak:20.0.5 start-dev --db mariadb --db-url jdbc:mariadb://host.docker.internal:63307/keycloak --db-username keycloak --db-password keycloak --features=declarative-user-profile`

### Users in default DB

`user1` with role `user` with password `user1`
`user2` with role `admin` with password `user2`
`user3` with roles `user` and `admin` with password `user3`
