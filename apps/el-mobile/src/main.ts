import { enableProdMode } from '@angular/core';
import { platformNativeScript, runNativeScriptAngularApp } from '@nativescript/angular';
import { environment } from './environments/environment';
import { AppModule } from './app/app.module';
import { configureTnsOAuth } from 'nativescript-oauth2';
import { configureOAuthProviderMyApp } from './app/provider/myapp.provider';
import { install } from '@nativescript-community/ui-drawer';

if (environment.production) {
    enableProdMode();
}

configureTnsOAuth([configureOAuthProviderMyApp()]);

install();

runNativeScriptAngularApp({
    appModuleBootstrap: () => platformNativeScript().bootstrapModule(AppModule),
});
