import { Component } from "@angular/core";

@Component({
    selector: "layout-login",
    template: `<GridLayout><router-outlet></router-outlet></GridLayout>`,
    styleUrls: []
})
export class LayoutLoginComponent {
}
