import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Drawer } from '@nativescript-community/ui-drawer';
import { Application, GridLayout } from '@nativescript/core';
import { AuthService } from '../../service/auth.service';
import { RouterExtensions } from '@nativescript/angular';
import { NavigationEnd } from '@angular/router';
import { RouteService } from '@myapp/ngservices';

@Component({
    selector: 'layout-basic',
    templateUrl: './basic.component.html',
    styleUrls: ['./basic.component.scss'],
})
export class LayoutBasicComponent implements OnInit {
    @ViewChild('drawer', { static: true }) drawerElementRef?: ElementRef;
    @ViewChild('drawercontent', { static: true }) drawerContentElementRef?: ElementRef;
    drawer?: Drawer;
    drawerContent?: GridLayout;

    public title: string = '';

    constructor(private authService: AuthService, private routerExtensions: RouterExtensions, private readonly routeService: RouteService) {
        Application.on(Application.systemAppearanceChangedEvent, () => {
            this.setDrawerStyle();
        });
    }

    async ngOnInit() {
        try {
            const isLogin = await this.authService.isLogin();
            if (!isLogin) {
                this.routerExtensions.navigate(['/login'], { clearHistory: true });
            }
        } catch (error) {
            console.error(error);
        }

        this.setTitle();

        this.checkIfDrawerIsSet();
    }

    private setTitle(): void {
        this.title = this.routeService.activeRoute()?.data?.['title'] || '';
        this.routerExtensions.router.events.subscribe((event): void => {
            if (event instanceof NavigationEnd) {
                this.title = this.routeService.activeRoute()?.data?.['title'] || '';
            }
        });
    }

    private checkIfDrawerIsSet(): void {
        if (!this.drawer) {
            if (this.drawerElementRef) {
                this.drawer = this.drawerElementRef.nativeElement;
            }
        }

        if (!this.drawerContent) {
            if (this.drawerContentElementRef) {
                this.drawerContent = this.drawerContentElementRef.nativeElement;
            }
        }
    }

    private setDrawerStyle(): void {
        this.checkIfDrawerIsSet();
        if (this.drawerContent) {
            this.drawerContent.className = 'drawer-' + Application.systemAppearance();
        }
    }

    onOpenDrawer() {
        this.checkIfDrawerIsSet();
        if (this.drawer) {
            this.drawer.open();
        }
    }

    onCloseDrawer() {
        this.checkIfDrawerIsSet();
        if (this.drawer) {
            this.drawer.close();
        }
    }

    toggleDrawer() {
        this.checkIfDrawerIsSet();
        this.setDrawerStyle();
        if (this.drawer) {
            this.drawer.toggle();
        }
    }

    public async onTapDashboard(): Promise<void> {
        this.onCloseDrawer();
        await this.routerExtensions.navigate(['/dashboard']);
    }

    public async onTapProfile(): Promise<void> {
        this.onCloseDrawer();
        await this.routerExtensions.navigate(['/profile']);
    }

    public async onTapLogout(): Promise<void> {
        try {
            this.onCloseDrawer();
            await this.authService.tnsOauthLogout();
            await this.routerExtensions.navigate(['/login'], { clearHistory: true });
        } catch (error) {
            console.error(error);
        }
    }
}
