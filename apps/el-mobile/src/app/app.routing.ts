// angular
import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';

// nativescript
import { NativeScriptRouterModule } from '@nativescript/angular';

// app
import { SharedModule } from './pages/shared/shared.module';
import { LoginComponent } from './pages/login/login.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LayoutLoginComponent } from './layouts/login/login.component';
import { LayoutBasicComponent } from './layouts/basic/basic.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: '', component: LayoutBasicComponent, children: [
    { path: 'dashboard', component: DashboardComponent, data: { title: 'Dashboard' } },
    { path: 'profile', component: ProfileComponent, data: { title: 'Profile' } },
  ] },
  { path: '', component: LayoutLoginComponent, children: [
    { path: 'login', component: LoginComponent }
  ] },
];

@NgModule({
  imports: [SharedModule, NativeScriptRouterModule.forRoot(routes)],
  exports: [NativeScriptRouterModule]
})
export class AppRoutingModule {}
