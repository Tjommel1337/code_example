import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptModule } from '@nativescript/angular';
import { DrawerModule } from "@nativescript-community/ui-drawer/angular";
import { CoreModule } from './core/core.module';
import { SharedModule } from './pages/shared/shared.module';
import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { AuthService } from './service/auth.service';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LoginComponent } from './pages/login/login.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { LayoutLoginComponent } from './layouts/login/login.component';
import { LayoutBasicComponent } from './layouts/basic/basic.component';

@NgModule({
  bootstrap: [AppComponent],
  imports: [
    CoreModule,
    SharedModule,
    AppRoutingModule,
    DrawerModule,
    NativeScriptModule,
  ],
  declarations: [
    AppComponent,
    LayoutBasicComponent,
    DashboardComponent,
    LayoutLoginComponent,
    LoginComponent,
    ProfileComponent,
  ],
  providers: [AuthService],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {}
