import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { RouterExtensions } from '@nativescript/angular';
import { AuthService } from '../../service/auth.service';

@Component({
  moduleId: module.id,
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent
{
  constructor(
    private authService: AuthService,
    private routerExtensions: RouterExtensions
  ) {}

  public async onTapLogout(): Promise<void> {
    try {
      await this.authService.tnsOauthLogout();
      await this.routerExtensions.navigate(["/login"], { clearHistory: true });
    } catch (error) {
      console.error(error);
    }
  }
}
