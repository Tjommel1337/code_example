import { Component } from '@angular/core';
import { setStatusBarColor } from '../../utils';
import { ITnsOAuthTokenResult } from 'nativescript-oauth2';
import { RouterExtensions } from '@nativescript/angular';
import { AuthService } from '../../service/auth.service';

@Component({
    moduleId: module.id,
    selector: 'app-login',
    templateUrl: './login.component.html'
})
export class LoginComponent {

    constructor(
        private authService: AuthService,
        private routerExtensions: RouterExtensions
    ) { }

    async ngOnInit() {
        setStatusBarColor('dark', '#97d9e9');
        if ((await this.authService.isLogin())) {
            this.routerExtensions.navigate(["/dashboard"], { clearHistory: true });
        }
    }

    public async onTapLogin(): Promise<void> {
        try {
            //   const result: ITnsOAuthTokenResult = await this.authService.tnsOauthLogin('myapp');
            //   if (result) {
            this.routerExtensions.navigate(["/dashboard"], { clearHistory: true });
            //     return;
            //   }
        } catch (error) {
            console.error(error);
        }
    }
}
