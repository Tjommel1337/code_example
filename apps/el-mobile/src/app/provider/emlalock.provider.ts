import { TnsOaOpenIdProviderOptions, TnsOaProvider, TnsOaProviderOptions, TnsOaProviderType } from 'nativescript-oauth2/providers';
import { ITnsOAuthTokenResult } from 'nativescript-oauth2';

export interface MyAppProviderOptions extends TnsOaOpenIdProviderOptions {
    realm: string;
    kcBaseUrl: string;
}

export class MyAppProvider implements TnsOaProvider {
    authority: string;
    authorizeEndpoint: string;
    cookieDomains: string[];
    providerType: TnsOaProviderType;
    tokenEndpoint: string;
    tokenEndpointBase: string;
    revokeEndpoint: string;
    endSessionEndpoint: string;

    openIdSupport: string;

    constructor(public options: MyAppProviderOptions) {
        const realm = options.realm;

        this.openIdSupport = "oid-full";
        this.providerType = "myapp";
        this.authority = options.kcBaseUrl;
        this.tokenEndpointBase = options.kcBaseUrl;
        this.authorizeEndpoint = '/realms/' + realm + '/protocol/openid-connect/auth';
        this.tokenEndpoint = '/realms/' + realm + '/protocol/openid-connect/token';
        this.revokeEndpoint = '/realms/' + realm + '/protocol/openid-connect/revoke';
        this.endSessionEndpoint = '/realms/' + realm + '/protocol/openid-connect/logout';
        this.cookieDomains = [realm];
    }

    parseTokenResult(jsonData: ITnsOAuthTokenResult): ITnsOAuthTokenResult {
        return jsonData;
    }
}

export function configureOAuthProviderMyApp(): TnsOaProvider {
    return new MyAppProvider(
        {
            openIdSupport: "oid-full",
            clientId: "mobile",
            redirectUri: "myapp://login",
            urlScheme: "myapp",
            kcBaseUrl: 'https://auth.myapp.dev',
            realm: 'myapp-dev',
            scopes: ["openid", "email"],
        }
    );
}
