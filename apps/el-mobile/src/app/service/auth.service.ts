import { Injectable } from '@angular/core';

import { TnsOAuthClient, ITnsOAuthTokenResult } from 'nativescript-oauth2';

@Injectable()
export class AuthService {
    private client: TnsOAuthClient;
    public tokenResult?: ITnsOAuthTokenResult;

    constructor() {
        this.client = new TnsOAuthClient('enlalock');
    }

    public tnsOauthLogin(providerType: string): Promise<ITnsOAuthTokenResult> {
        this.client = new TnsOAuthClient(providerType);

        return new Promise<ITnsOAuthTokenResult>((resolve, reject) => {
            this.client.loginWithCompletion((tokenResult: ITnsOAuthTokenResult, error) => {
                if (error) {
                    console.error('back to main page with error: ');
                    console.error(error);
                    reject(error);
                } else {
                    this.tokenResult = tokenResult;
                    console.log('back to main page with access token: ');
                    resolve(tokenResult);
                }
            });
        });
    }

    public tnsOauthLogout(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (this.client) {
                this.client.logoutWithCompletion((error) => {
                    if (error) {
                        console.error('back to main page with error: ');
                        console.error(error);
                        reject(error);
                    } else {
                        console.log('back to main page with success');
                        resolve(undefined);
                    }
                });
            } else {
                console.log('back to main page with success');
                resolve(undefined);
            }
        });
    }

    public async isLogin(): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            if (this.client?.tokenResult) {
                this.client.refreshTokenWithCompletion((tokenResult: ITnsOAuthTokenResult, error) => {
                    if (error) {
                        return reject(error);
                    }

                    this.tokenResult = tokenResult;
                    return resolve(true);
                });
            } else {
                resolve(false);
            }
        });
    }
}
