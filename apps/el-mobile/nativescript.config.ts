import { NativeScriptConfig } from '@nativescript/core';

export default {
    displayName: 'MyApp',
    id: 'com.myapp.mobile',
    appResourcesPath: 'App_Resources',
    android: {
        v8Flags: '--expose_gc',
        markingMode: 'none',
        codeCache: true,
        suppressCallJSMethodExceptions: false
    },
    ios: {
        discardUncaughtJsExceptions: false
    },
    appPath: 'src',
} as NativeScriptConfig;
