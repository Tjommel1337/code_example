import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app/app.module';
import { BrowserTracing, Replay, init, routingInstrumentation } from '@sentry/angular-ivy';
// import { Integrations } from "@sentry/tracing";

if (process.env['NODE_ENV'] === 'production') {
    enableProdMode();
}

init({
    dsn: "https://298f4e07d84e4079884380decf184ad8@glitch.my-key.cloud/1",
    integrations: [

        // Registers and configures the Tracing integration,
        // which automatically instruments your application to monitor its
        // performance, including custom Angular routing instrumentation
        new BrowserTracing({
            routingInstrumentation: routingInstrumentation,
        }),

        // Registers the Replay integration,
        // which automatically captures Session Replays
        new Replay(),
    ],

    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1,

    // Capture Replay for 10% of all sessions,
    // plus for 100% of sessions with an error
    replaysSessionSampleRate: 0.1,
    replaysOnErrorSampleRate: 1.0,
});

platformBrowserDynamic()
    .bootstrapModule(AppModule)
    .catch((err) => console.error(err));
