import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, } from '@angular/router';
import { KeycloakService, KeycloakAuthGuard } from '@myapp/ngkeycloak';

@Injectable({
    providedIn: "root"
})
export class OnlyGuestsGuard extends KeycloakAuthGuard {
    constructor(
        protected override router: Router,
        protected override keycloakAngular: KeycloakService
    ) {
        super(router, keycloakAngular);
    }

    isAccessAllowed(
        _route: ActivatedRouteSnapshot,
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        _state: RouterStateSnapshot
    ): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // check if logged in
            if (this.authenticated) {
                this.router.navigate(['/dashboard']);
                return reject(false);
            }
            return resolve(true);
        });

    }
}
