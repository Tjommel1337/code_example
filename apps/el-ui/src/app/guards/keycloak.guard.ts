import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, } from '@angular/router';
import { KeycloakService, KeycloakAuthGuard } from '@myapp/ngkeycloak';

@Injectable({
    providedIn: "root"
})
export class KeycloakGuard extends KeycloakAuthGuard {
    constructor(
        protected override router: Router,
        protected override keycloakAngular: KeycloakService
    ) {
        super(router, keycloakAngular);
    }

    isAccessAllowed(
        route: ActivatedRouteSnapshot,
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        _state: RouterStateSnapshot
    ): Promise<boolean> {
        return new Promise((resolve, reject) => {
            const requiredRoles: string[] = route.data['roles'];

            // check if logged in
            if (!this.authenticated) {
                this.router.navigate(['/']);
                return reject(false);
            }

            // allow route if no roles are required
            if (!requiredRoles || requiredRoles.length === 0) {
                return resolve(true);
            }

            // deny route if roles are required but user don't have any role assigned
            if (!this.roles || this.roles.length === 0) {
                this.router.navigate(['/']);
                return reject(false);
            }

            // check if role is assigned to user
            if (requiredRoles.every((role) => this.roles.indexOf(role) > -1)) {
                return resolve(true);
            }
            this.router.navigate(['/']);
            return reject(false);
        });

    }
}
