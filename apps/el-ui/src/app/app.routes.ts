import { Route } from '@angular/router';

export const appRoutes: Route[] = [
    { path: "", pathMatch: "full", redirectTo: "/landing" },
    {
        path: "",
        loadComponent: () => import('./layouts/empty/empty.component').then(mod => mod.LayoutEmptyComponent),
        loadChildren: () => import('./pages/guest.routes').then(mod => mod.ROUTES)
    },
    {
        path: "",
        loadComponent: () => import('./layouts/basic/basic.component').then(mod => mod.LayoutBasicComponent),
        loadChildren: () => import('./pages/authenticated.routes').then(mod => mod.ROUTES)
    },
    { path: "**", pathMatch: "full", redirectTo: "/landing" }

];
