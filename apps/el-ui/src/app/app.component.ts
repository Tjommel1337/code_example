import { Component } from '@angular/core';
import { TranslateService } from '@myapp/ngservices';

@Component({
    selector: 'myapp-root',
    template: `<p-toast></p-toast><router-outlet></router-outlet>`,
    styleUrls: [],
})
export class AppComponent {
    constructor(private translateService: TranslateService) {
        translateService.setDefaultLang('en');
    }
}
