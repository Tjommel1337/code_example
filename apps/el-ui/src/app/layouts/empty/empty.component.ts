import { Component } from "@angular/core";
import { RouterModule } from '@angular/router';

@Component({
    standalone: true,
    imports: [
        RouterModule
    ],
    selector: "layout-empty",
    template: `<router-outlet></router-outlet>`,
    styleUrls: []
})
export class LayoutEmptyComponent {

}
