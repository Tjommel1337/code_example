import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Component } from "@angular/core";
import { BadgeModule } from 'primeng/badge';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { MenuModule } from 'primeng/menu';
import { RippleModule } from 'primeng/ripple';
import { StyleClassModule } from 'primeng/styleclass';
import { SessionService, ThemeService } from '@myapp/ngservices';
import { MenuItem } from 'primeng/api';
import { Router } from '@angular/router';

@Component({
    standalone: true,
    imports: [
        CommonModule,
        InputTextModule,
        BadgeModule,
        StyleClassModule,
        ButtonModule,
        RippleModule,
        MenuModule,
        HttpClientModule
    ],
    selector: "layout-basic-menu",
    templateUrl: './menu.component.html',
    styleUrls: []
})
export class LayoutBasicMenuComponent {

    public menu: MenuItem[] = [
        {
            label: 'Home',
            icon: 'pi pi-fw pi-home',
            routerLink: ['/']
        },
        {
            label: 'Sessions',
            icon: 'pi pi-fw pi-lock',
            routerLink: ['/sessions']
        },
        {
            label: 'Logout',
            icon: 'pi pi-fw pi-sign-out',
            command: () => this.sessionService.logout()
        }
    ];

    constructor(
        private router: Router,
        public readonly sessionService: SessionService,
        public readonly themeService: ThemeService
    ) { }

    public logoClick(): void {
        this.router.navigate(['/']);
    }

}
