import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Component } from '@angular/core';
import { BadgeModule } from 'primeng/badge';
import { ButtonModule } from 'primeng/button';
import { MenuModule } from 'primeng/menu';
import { RippleModule } from 'primeng/ripple';
import { StyleClassModule } from 'primeng/styleclass';
import { SessionService, ThemeService, UserService } from '@myapp/ngservices';
import { MenuItem } from 'primeng/api';
import { Router } from '@angular/router';
import { AvatarComponent } from '@myapp/ngcomponents';

@Component({
    standalone: true,
    imports: [CommonModule, BadgeModule, StyleClassModule, ButtonModule, RippleModule, MenuModule, HttpClientModule, AvatarComponent],
    selector: 'layout-basic-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: [],
})
export class LayoutBasicNavbarComponent {
    public items: MenuItem[] = [
        {
            label: 'Profil',
            items: [
                {
                    label: 'Account',
                    icon: 'pi pi-refresh',
                    command: () => {
                        this.router.navigate(['/profile']);
                    },
                },
                {
                    label: 'Settings',
                    icon: 'pi pi-times',
                    command: () => {
                        this.router.navigate(['/settings/general']);
                    },
                },
            ],
        },
        {
            separator: true,
            items: [
                {
                    label: 'Logout',
                    icon: 'pi pi-times',
                    command: () => {
                        this.sessionService.logout();
                    },
                },
            ],
        },
    ];

    constructor(
        public readonly sessionService: SessionService,
        public readonly userService: UserService,
        public readonly themeService: ThemeService,
        public readonly router: Router,
    ) { }
}
