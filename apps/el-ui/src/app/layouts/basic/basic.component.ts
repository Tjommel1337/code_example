import { Component } from "@angular/core";
import { RouterModule } from '@angular/router';
import { LayoutBasicMenuComponent } from './menu/menu.component';
import { LayoutBasicNavbarComponent } from './navbar/navbar.component';

@Component({
    standalone: true,
    imports: [
        LayoutBasicMenuComponent,
        LayoutBasicNavbarComponent,
        RouterModule,
    ],
    selector: "layout-basic",
    templateUrl: './basic.component.html',
    styleUrls: []
})
export class LayoutBasicComponent {

}
