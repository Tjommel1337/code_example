import { NgModule, APP_INITIALIZER, ErrorHandler } from '@angular/core';
import { BrowserModule, Meta } from '@angular/platform-browser';
import { Event, NavigationEnd, Router, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { appRoutes } from './app.routes';

import { KeycloakAngularModule, KeycloakService } from '@myapp/ngkeycloak';

import { SessionService, ThemeService, TranslateService, UserService } from '@myapp/ngservices';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { APIInterceptor } from './middleware/api.middleware';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';

import { createErrorHandler, TraceService } from "@sentry/angular-ivy";

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        ToastModule,

        RouterModule.forRoot(appRoutes, { initialNavigation: 'enabledNonBlocking' }),

        KeycloakAngularModule
    ],
    providers: [
        TranslateService,
        UserService,
        MessageService,
        {
            provide: ErrorHandler,
            useValue: createErrorHandler({
                showDialog: false,
            }),
        },
        {
            provide: TraceService,
            deps: [Router]
        },
        {
            provide: APP_INITIALIZER,
            deps: [
                KeycloakService,
                SessionService,
                ThemeService,
                TranslateService,
                Meta,
                Router,
                TraceService
            ],
            useFactory: (
                keycloakService: KeycloakService,
                sessionService: SessionService,
                themeService: ThemeService,
                translateService: TranslateService,
                metaService: Meta,
                router: Router,
            ): (() => Promise<void>) => {
                const fnCall: (() => Promise<void>) = async (): Promise<void> => {
                    metaService.addTag({
                        name: 'viewport',
                        content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
                    });

                    // init keycloak
                    await keycloakService.init({
                        config: {
                            url: process.env['PUBLIC_KEYCLOAK_URL'],
                            realm: String(process.env['PUBLIC_KEYCLOAK_REALM']),
                            clientId: String(process.env['PUBLIC_KEYCLOAK_CLIENT_ID'])
                        },
                        initOptions: {
                            onLoad: 'check-sso',
                            silentCheckSsoRedirectUri:
                                window.location.origin + '/assets/verify-sso.html',
                            pkceMethod: 'S256',
                            token: localStorage.getItem("kc_token") || undefined,
                            refreshToken: localStorage.getItem("kc_refreshtoken") || undefined,
                        }
                    });

                    // init theme
                    themeService.watchPrefersColorScheme();

                    // init language
                    translateService.use(translateService.getBrowserLang() || "en");

                    // load session
                    await sessionService.init();

                    // route triggers
                    router.events.subscribe(async (event: Event): Promise<void> => {
                        if (event instanceof NavigationEnd) {
                            await sessionService.refreshToken();
                        }
                    });
                };
                return fnCall;
            },
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: APIInterceptor,
            multi: true,
        },
    ],
    bootstrap: [AppComponent],
})
export class AppModule { }
