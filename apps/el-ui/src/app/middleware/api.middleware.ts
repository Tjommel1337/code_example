import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class APIInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
        if (req.url.indexOf("assets") === -1) {
            const apiReq = req.clone({ url: `${process.env['PUBLIC_API_URL']}${req.url}` });
            return next.handle(apiReq);
        }
        return next.handle(req);
    }
}
