import { Route } from '@angular/router';
import { KeycloakGuard } from '../guards/keycloak.guard';

export const ROUTES: Route[] = [
    {
        path: 'dashboard',
        loadComponent: () => import('./dashboard/dashboard.component').then((mod) => mod.PageDashboardComponent),
        canActivate: [KeycloakGuard],
    },
    {
        path: 'sessions',
        loadComponent: () => import('./sessions/sessions.component').then((mod) => mod.PageSessionsComponent),
        canActivate: [KeycloakGuard],
    },
    {
        path: 'settings',
        loadChildren: () => {
            return import('./settings/settings.routes').then((mod) => mod.ROUTES);
        },
    },
];
