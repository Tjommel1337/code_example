import { Route } from '@angular/router';

import { OnlyGuestsGuard } from '../guards/onlyGuests.guard';

export const ROUTES: Route[] = [
    {
        path: "landing",
        loadComponent: () => import('./landing/landing.component').then(mod => mod.PageLandingComponent),
        canActivate: [OnlyGuestsGuard]
    },
    {
        path: "register",
        loadComponent: () => import('./register/register.component').then(mod => mod.PageRegisterComponent),
        canActivate: [OnlyGuestsGuard]
    },
];
