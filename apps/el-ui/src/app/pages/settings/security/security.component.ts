import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { TranslationModule } from '@myapp/ngpipes';

@Component({
    standalone: true,
    imports: [
        CommonModule,
        FormsModule,
        DropdownModule,
        InputTextModule,
        InputTextareaModule,
        TranslationModule
    ],
    selector: 'page-settings-security',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './security.component.html',
    styleUrls: []
})
export class PageSettingsSecurityComponent {
}
