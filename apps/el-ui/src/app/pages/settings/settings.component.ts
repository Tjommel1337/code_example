import { CommonModule } from '@angular/common';
import { Component } from "@angular/core";
import { FormsModule } from '@angular/forms';
import { Router, RouterModule } from '@angular/router';
import { DividerModule } from 'primeng/divider';
import { StyleClassModule } from 'primeng/styleclass';
import { TranslationModule } from '@myapp/ngpipes';

interface LinkItem {
    target: string;
    icon: string;
    active: boolean;
    label: string;
}

@Component({
    standalone: true,
    imports: [
        CommonModule,
        RouterModule,
        DividerModule,
        TranslationModule,
        FormsModule,
        StyleClassModule
    ],
    selector: "layout-settings",
    templateUrl: './settings.component.html',
    styleUrls: []
})
export class LayoutSettingsComponent {

    public links: LinkItem[] = [
        {
            target: '/settings/general',
            icon: "pi pi-user",
            active: false,
            label: "General"
        },
        {
            target: '/settings/security',
            icon: "pi pi-user",
            active: false,
            label: "Security"
        },
        {
            target: '/settings/notifications',
            icon: "pi pi-user",
            active: false,
            label: "Notifications"
        },
        {
            target: '/settings/socials',
            icon: "pi pi-user",
            active: false,
            label: "Socials"
        },
        {
            target: '/settings/api',
            icon: "pi pi-user",
            active: false,
            label: "API"
        },
    ];

    constructor(private router: Router) {
        this.setActiveLink();
    }

    public async navigate(url: string): Promise<void> {
        await this.router.navigate([url]);
        this.setActiveLink();
    }

    private setActiveLink() {
        this.links.forEach(link => link.active = link.target === this.router.url);
    }
}
