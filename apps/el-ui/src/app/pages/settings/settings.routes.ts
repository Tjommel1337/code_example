import { Route } from '@angular/router';
import { KeycloakGuard } from '../../guards/keycloak.guard';

export const ROUTES: Route[] = [
    {
        path: "",
        loadComponent: () => import('./settings.component').then(mod => mod.LayoutSettingsComponent),
        children: [
            {
                path: "api",
                loadComponent: () => import('./api/api.component').then(mod => mod.PageSettingsAPIComponent),
                canActivate: [KeycloakGuard],
                data: { roles: ['user'] },
            },
            {
                path: "general",
                loadComponent: () => import('./general/general.component').then(mod => mod.PageSettingsGeneralComponent),
                canActivate: [KeycloakGuard],
                data: { roles: ['user'] },
            },
            {
                path: "notifications",
                loadComponent: () => import('./notifications/notifications.component').then(mod => mod.PageSettingsNotificationsComponent),
                canActivate: [KeycloakGuard],
                data: { roles: ['user'] },
            },
            {
                path: "security",
                loadComponent: () => import('./security/security.component').then(mod => mod.PageSettingsSecurityComponent),
                canActivate: [KeycloakGuard],
                data: { roles: ['user'] },
            },
            {
                path: "socials",
                loadComponent: () => import('./socials/socials.component').then(mod => mod.PageSettingsSocialsComponent),
                canActivate: [KeycloakGuard],
                data: { roles: ['user'] },
            },
        ]
    }
];
