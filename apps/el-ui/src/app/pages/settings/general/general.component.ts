import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { EnumOptionsPipe, TranslationModule } from '@myapp/ngpipes';
import {
    SkeletonTextareaComponent,
    AvatarComponent,
    UploadAreaComponent,
    SkeletonDateComponent,
    SkeletonSelectButtonComponent,
    SkeletonDropdownComponent,
} from '@myapp/ngcomponents';
import { UserService } from '@myapp/ngservices';
import { ProjectRole, DateTimeFormat, Gender, GenderType, LanguageLabel, Theme, UploadType } from '@myapp/enums';
import { UserInterface } from '@myapp/interfaces';

@Component({
    standalone: true,
    imports: [
        CommonModule,
        FormsModule,
        DropdownModule,
        InputTextModule,
        InputTextareaModule,
        TranslationModule,
        UploadAreaComponent,
        AvatarComponent,
        SkeletonTextareaComponent,
        SkeletonDateComponent,
        SkeletonSelectButtonComponent,
        EnumOptionsPipe,
        SkeletonDropdownComponent,
    ],
    selector: 'page-settings-general',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './general.component.html',
    styleUrls: [],
})
export class PageSettingsGeneralComponent {
    public uploadType: typeof UploadType = UploadType;

    public genderEnum = Gender;
    public genderTypeEnum = GenderType;
    public themeEnum = Theme;
    public dateTimeFormatEnum = DateTimeFormat;
    public languageLabelEnum = LanguageLabel;
    public projectRoleEnum = ProjectRole;

    constructor(public readonly userService: UserService) { }

    public async submitData(data: string | number | Date, field: string): Promise<void> {
        if (this.userService.userData$.value) {
            const current: UserInterface = this.userService.userData$.value;
            current[field] = data;
            this.userService.saveUser(current).subscribe(() => this.userService.reloadUser());
        }
    }
}
