import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';
import { ButtonModule } from 'primeng/button';
import { StyleClassModule } from 'primeng/styleclass';
import { SessionService, ThemeService } from '@myapp/ngservices';

@Component({
    standalone: true,
    imports: [CommonModule, StyleClassModule, ButtonModule],
    selector: 'page-landing',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './landing.component.html',
    styleUrls: []
})
export class PageLandingComponent {

    constructor(
        public readonly sessionService: SessionService,
        public readonly theme: ThemeService,
        public readonly router: Router
    ) { }

    public scrollTo(anchorElement: HTMLElement): void {
        anchorElement.scrollIntoView({
            behavior: "smooth",
            block: "start",
            inline: "nearest"
        });
    }

}
