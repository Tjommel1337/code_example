import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ButtonModule } from 'primeng/button';
import { StyleClassModule } from 'primeng/styleclass';
import { SessionService, ThemeService } from '@myapp/ngservices';

@Component({
    standalone: true,
    imports: [CommonModule, StyleClassModule, ButtonModule],
    selector: 'page-register',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class PageRegisterComponent {

    constructor(
        public readonly sessionService: SessionService,
        public readonly theme: ThemeService
    ) { }

    public register(): void {
        console.log("jetzt register")
    }

}
