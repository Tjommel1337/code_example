import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';
import { StyleClassModule } from 'primeng/styleclass';
import { SessionService, ThemeService, UserService } from '@myapp/ngservices';
import { InputTextModule } from 'primeng/inputtext';
import { BadgeModule } from 'primeng/badge';
import { MenuModule } from 'primeng/menu';
import { HttpClientModule } from '@angular/common/http';
import { AvatarComponent, UploadAreaComponent, HeadlineComponent } from '@myapp/ngcomponents';
import { TranslationModule } from '@myapp/ngpipes';
import { SkeletonModule } from 'primeng/skeleton';

@Component({
    standalone: true,
    imports: [
        CommonModule,
        InputTextModule,
        BadgeModule,
        StyleClassModule,
        ButtonModule,
        RippleModule,
        MenuModule,
        HttpClientModule,
        UploadAreaComponent,
        TranslationModule,
        AvatarComponent,
        HeadlineComponent,
        SkeletonModule,
    ],
    selector: 'page-dashboard',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './dashboard.component.html',
    styleUrls: [],
})
export class PageDashboardComponent {
    constructor(
        public readonly sessionService: SessionService,
        public readonly themeService: ThemeService,
        public readonly userService: UserService,
    ) { }
}
