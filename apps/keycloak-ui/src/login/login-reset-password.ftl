<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=true; section>
    <#if section = "header">
        ${msg("emailForgotTitle")}
    <#elseif section = "form">
        <form action="${url.loginAction}" method="post">
            <span class="p-float-label mb-5">
                <input
                    id="username"
                    name="username"
                    type="text"
                    placeholder=""
                    class="p-inputtext p-component p-element w-full"
                    tabindex="1"
                    autofocus
                    autocomplete="off"
                />
                <label for="username">
                    <#if !realm.loginWithEmailAllowed>
                        ${msg("username")}
                    <#elseif !realm.registrationEmailAsUsername>
                        ${msg("usernameOrEmail")}
                    <#else>
                        ${msg("email")}
                    </#if>
                </label>
            </span>

            <div class="flex align-items-center justify-content-between mb-6">
                <div class="flex align-items-center"></div>
                <a
                    tabindex="5"
                    class="font-medium no-underline ml-2 text-blue-500 text-right cursor-pointer"
                    href="${url.loginUrl}"
                >
                    ${kcSanitize(msg("backToLogin"))?no_esc}
                </a>
            </div>

            <button
                type="submit"
                class="p-element p-ripple w-full p-button p-component"
                tabindex="4"
            >
                <span class="p-button-label">${msg("doSubmit")}</span>
            </button>
        </form>
    <#elseif section = "info" >
        ${msg("emailInstruction")}
    </#if>
</@layout.registrationLayout>
