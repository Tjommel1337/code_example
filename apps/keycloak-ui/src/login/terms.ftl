<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=false; section>
    <#if section = "header">
        ${msg("termsTitle")}
    <#elseif section = "form">
    <div>
        ${kcSanitize(msg("termsText"))?no_esc}
    </div>
    <form class="form-actions" action="${url.loginAction}" method="POST">
        <div class="flex align-items-center justify-content-between mb-6">
            <input
                name="accept"
                type="submit"
                class="flex p-element p-ripple w-full p-button p-button-success p-component mr-3"
                tabindex="4"
                value="${msg("doAccept")}"
            />
            <input
                name="cancel"
                type="submit"
                class="flex p-element p-ripple w-full p-button p-button-danger p-component ml-3"
                tabindex="4"
                value="${msg("doDecline")}"
            />
        </div>
    </form>
    <div class="clearfix"></div>
    </#if>
</@layout.registrationLayout>
