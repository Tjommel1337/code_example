<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=social.displayInfo displayWide=(realm.password && social.providers??); section>
    <#if section = "header">
        ${msg("doLogIn")}
    <#elseif section = "form">
        <form onsubmit="login.disabled = true; return true;" action="${url.loginAction}" method="post">
            <div class="p-field p-col-12 p-md-4">
                <span class="p-float-label mb-5 labelschnickschnack">
                    <#if usernameEditDisabled??>
                        <input
                            id="username"
                            name="username"
                            type="text"
                            placeholder=""
                            class="p-inputtext p-component p-element w-full"
                            tabindex="1"
                            disabled
                        />
                    <#else>
                        <input
                            id="username"
                            name="username"
                            type="text"
                            placeholder=""
                            class="p-inputtext p-component p-element w-full"
                            tabindex="1"
                            autofocus
                            autocomplete="off"
                        />
                    </#if>
                    <label for="username">
                        <#if !realm.loginWithEmailAllowed>
                            ${msg("username")}
                        <#elseif !realm.registrationEmailAsUsername>
                            ${msg("usernameOrEmail")}
                        <#else>
                            ${msg("email")}
                        </#if>
                    </label>
                </span>
            </div>

            <span class="p-float-label mb-5">
                <input
                    id="password"
                    name="password"
                    type="password"
                    placeholder=""
                    class="p-inputtext p-component p-element w-full"
                    tabindex="2"
                    autocomplete="off"
                />
                <label for="password">${msg("password")}</label>
            </span>

            <div class="flex align-items-center justify-content-between mb-6">
                <#if realm.rememberMe && !usernameEditDisabled??>
                    <div class="flex align-items-center">
                        <label>
                            <#if login.rememberMe??>
                                <input class="p-checkbox" tabindex="3" id="rememberMe" name="rememberMe" type="checkbox" checked> ${msg("rememberMe")}
                            <#else>
                                <input class="p-checkbox" tabindex="3" id="rememberMe" name="rememberMe" type="checkbox"> ${msg("rememberMe")}
                            </#if>
                        </label>
                    </div>
                </#if>
                <a
                    tabindex="5"
                    class="font-medium no-underline ml-2 text-blue-500 text-right cursor-pointer"
                    href="${url.loginResetCredentialsUrl}"
                >
                    ${msg("doForgotPassword")}
                </a>
            </div>

            <button
                type="submit"
                class="p-element p-ripple w-full p-button p-component"
                tabindex="4"
            >
                <span class="p-button-label">${msg("doLogIn")}</span>
            </button>
        </form>

        <#if realm.password && social.providers??>
            <div id="kc-social-providers" class="${properties.kcFormSocialAccountContentClass!} ${properties.kcFormSocialAccountClass!}">
                <ul class="${properties.kcFormSocialAccountListClass!} <#if social.providers?size gt 4>${properties.kcFormSocialAccountDoubleListClass!}</#if>">
                    <#list social.providers as p>
                        <li class="${properties.kcFormSocialAccountListLinkClass!}"><a href="${p.loginUrl}" id="zocial-${p.alias}" class="zocial ${p.providerId}"> <span>${p.displayName}</span></a></li>
                    </#list>
                </ul>
            </div>
        </#if>
    </#if>
</@layout.registrationLayout>
