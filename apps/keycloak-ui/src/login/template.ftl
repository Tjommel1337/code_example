<#macro registrationLayout bodyClass="" displayInfo=false displayMessage=true displayWide=false displayRequiredFields=false>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <meta name="robots" content="noindex, nofollow">

            <#if properties.meta?has_content>
                <#list properties.meta?split(' ') as meta>
                    <meta name="${meta?split('==')[0]}" content="${meta?split('==')[1]}"/>
                </#list>
            </#if>

            <title>${msg("loginTitle") + realm.displayName}</title>
            <link rel="icon" href="${url.resourcesPath}/img/favicon.ico" />

            <#if properties.styles?has_content>
                <#list properties.styles?split(' ') as style>
                    <link href="${url.resourcesPath}/${style}" rel="stylesheet" />
                </#list>
            </#if>

            <#if properties.scripts?has_content>
                <#list properties.scripts?split(' ') as script>
                    <script src="${url.resourcesPath}/${script}" type="text/javascript"></script>
                </#list>
            </#if>

            <#if scripts??>
                <#list scripts as script>
                    <script src="${script}" type="text/javascript"></script>
                </#list>
            </#if>
        </head>

        <body>
            <div class="gradient-box full-height align-items-center flex justify-content-center lg:px-8 md:px-6 px-4 py-8 surface-ground ng-star-inserted">
                <div class="surface-card p-4 shadow-6 border-round w-full lg:w-4">
                    <div class="text-center mb-5">
                        <img
                            src="${url.resourcesPath}/images/logo.svg"
                            alt="MyApp"
                            class="mb-3"
                            height="75"
                        />
                        <div class="text-900 text-3xl font-medium mb-3">${kcSanitize(msg("loginTitleHtml",(realm.displayNameHtml!'')))?no_esc}</div>
                        <div><#nested "header"></div>
                        <div class="mt-2"><#nested "info"></div>
                    </div>
                    <div>
                        <#if displayMessage && message?has_content>
                            <#if message.type = 'success'>
                                <div class="border-solid border-round-sm bg-green-50 border-green-400 p-2 mb-4">
                                    ${kcSanitize(message.summary)?no_esc}
                                </div>
                            </#if>
                            <#if message.type = 'warning'>
                                <div class="border-solid border-round-sm bg-yellow-50 border-yellow-400 p-2 mb-4">
                                    ${kcSanitize(message.summary)?no_esc}
                                </div>
                            </#if>
                            <#if message.type = 'error'>
                                <div class="border-solid border-round-sm bg-red-50 border-red-400 p-2 mb-4">
                                    ${kcSanitize(message.summary)?no_esc}
                                </div>
                            </#if>
                            <#if message.type = 'info'>
                                <div class="border-solid border-round-sm bg-blue-50 border-blue-400 p-2 mb-4">
                                    ${kcSanitize(message.summary)?no_esc}
                                </div>
                            </#if>
                        </#if>
                        <div><#nested "form"></div>
                    </div>
                </div>
            </div>
        </body>
    </html>
</#macro>
