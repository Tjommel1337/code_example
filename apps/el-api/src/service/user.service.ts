import { wait } from '@myapp/helper';
import UserRepresentation from '@keycloak/keycloak-admin-client/lib/defs/userRepresentation';
import { Injectable, Logger } from '@nestjs/common';
import { UserUpdateRequest, ChangePasswordRequest } from '../dtos/user.dto';
import { User } from '../entities/user.entity';
import { UserId } from '../interfaces/keycloak.interface';
import { KeyCloakService } from './keycloak.service';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UserService {
    constructor(
        private readonly logger: Logger,
        private readonly keyCloakService: KeyCloakService,
        @InjectRepository(User)
        private usersRepository: Repository<User>,
    ) { }

    public async updateUser(reqData: UserUpdateRequest, userId: string | undefined = undefined): Promise<void> {
        const user: User | null = await this.getUser(userId ?? '');
        if (user === null) {
            throw new Error('User not found!');
        }

        if (reqData.language !== undefined) {
            user.language = reqData.language;
        }
        if (reqData.theme !== undefined) {
            user.theme = reqData.theme;
        }
        if (reqData.birthdate !== undefined) {
            user.birthdate = reqData.birthdate;
        }
        if (reqData.gender !== undefined) {
            user.gender = reqData.gender;
        }
        if (reqData.gender_type !== undefined) {
            user.gender_type = reqData.gender_type;
        }
        if (reqData.project_role !== undefined) {
            user.project_role = reqData.project_role;
        }
        if (reqData.datetime_format !== undefined) {
            user.datetime_format = reqData.datetime_format;
        }
        if (reqData.description !== undefined) {
            user.description = reqData.description;
        }


        this.usersRepository.save(user);
    }

    public async setAvatar(filename: string, userId: string): Promise<void> {
        const user: User | null = await this.getUser(userId);
        if (user === null) {
            throw new Error('User not found!');
        }

        user.avatar = filename;
        this.usersRepository.save(user);
    }

    public async changePassword(reqData: ChangePasswordRequest, userId: string | undefined = undefined): Promise<void> {
        if (reqData.password !== reqData.passwordConfirm) {
            throw new Error('Passwords do not match!');
        }

        await this.keyCloakService.setUserPassword(userId ?? '', reqData.password);
    }

    public async createUser(username: string, email: string, password: string): Promise<User | boolean | undefined> {
        try {
            if ((await this.keyCloakService.getUserByUsernameOrEmail(username, email)).length > 0) {
                return false;
            }

            const kcUser: UserRepresentation | undefined = await this.keyCloakService.createUser(username, email, password);
            if (kcUser === undefined) {
                return undefined;
            }

            return await this.createDbUser(kcUser);
        } catch (error) {
            this.logger.error(error, 'createUser');
            return undefined;
        }
    }

    public async getUser(userId: string): Promise<User | null> {
        let dbUser: User | null = null;
        try {
            let kcUser: UserRepresentation | undefined = await this.keyCloakService.getUserById(userId);
            if (kcUser === undefined) {
                dbUser = await this.usersRepository.findOne({ where: { id: userId } });
                if (dbUser === null) {
                    return null;
                }
                kcUser = await this.keyCloakService.getUserById(dbUser.auth_id ?? '');
            }

            if (dbUser === null && kcUser !== undefined) {
                dbUser = await this.getDbUser(kcUser.id ?? '', kcUser);
            }

            if (dbUser === null) {
                return null;
            }
        } catch (error) {
            this.logger.error(error, 'getUser');
            return null;
        }

        return dbUser;
    }

    private async getDbUser(id: string, kcUser?: UserRepresentation): Promise<User | null> {
        const dbUser: User | null = await this.usersRepository.findOne({
            where: [
                { id: id },
                { auth_id: id }
            ],
        });
        if (dbUser !== null) {
            return dbUser;
        }

        if (kcUser === undefined) {
            return null;
        }

        return await this.createDbUser(kcUser);
    }

    private async createDbUser(kcUser: UserRepresentation): Promise<User> {
        let dbUser = new User();
        dbUser.auth_id = kcUser.id ?? '';
        dbUser.username = kcUser.username ?? '';
        dbUser.email = kcUser.email ?? '';

        dbUser = await this.usersRepository.save(dbUser);

        if (dbUser.id !== undefined) {
            await this.setKeyCloakUserId(dbUser);
        }

        return dbUser;
    }

    private async setKeyCloakUserId(dbUser: User): Promise<void> {
        const keyCloakId: string = dbUser.auth_id ?? '';
        const attributes: Record<string, any> = await this.keyCloakService.getUserAtrributes(keyCloakId ?? '');
        (attributes as unknown as Record<string, unknown>)['userid'] = [dbUser.id];

        await this.keyCloakService.updateUser(
            keyCloakId ?? '',
            {
                attributes: attributes,
                email: dbUser.email ?? '',
            }
        );
    }
}
