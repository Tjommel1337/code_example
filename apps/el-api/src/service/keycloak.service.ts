import { type KeycloakAdminClient } from '@keycloak/keycloak-admin-client/lib/client';
import UserRepresentation from '@keycloak/keycloak-admin-client/lib/defs/userRepresentation';
import { Credentials } from '@keycloak/keycloak-admin-client/lib/utils/auth';
import { HttpService } from '@nestjs/axios';
import { Injectable, Logger } from '@nestjs/common';
import { UserId } from '../interfaces/keycloak.interface';

@Injectable()
export class KeyCloakService {
    private kcAdminClient: KeycloakAdminClient | undefined;

    constructor(private readonly httpService: HttpService, private readonly logger: Logger) {}

    public async getUserById(userId: string): Promise<UserRepresentation | undefined> {
        const kcAdminClient = await this.getAdminClient();
        try {
            return await kcAdminClient.users.findOne({ id: userId });
        } catch (error) {
            this.logger.error(error, 'KeyCloakService.getUserById');
            return undefined;
        }
    }

    public async getUserByUsername(username: string, maxResults: number = 1, exactSearch: boolean = true): Promise<UserRepresentation[]> {
        const kcAdminClient = await this.getAdminClient();
        try {
            return await kcAdminClient.users.find({ username: username, max: maxResults, exact: exactSearch });
        } catch (error) {
            this.logger.error(error, 'KeyCloakService.getUserByUsername');
            return [];
        }
    }

    public async getUserByEmail(email: string, maxResults: number = 1, exactSearch: boolean = true): Promise<UserRepresentation[]> {
        const kcAdminClient = await this.getAdminClient();
        try {
            return await kcAdminClient.users.find({ email: email, max: maxResults, exact: exactSearch });
        } catch (error) {
            this.logger.error(error, 'KeyCloakService.getUserByEmail');
            return [];
        }
    }

    public async getUserByUsernameAndEmail(
        username: string,
        email: string,
        maxResults: number = 1,
        exactSearch: boolean = true,
    ): Promise<UserRepresentation[]> {
        const kcAdminClient = await this.getAdminClient();
        try {
            return await kcAdminClient.users.find({ username: username, email: email, max: maxResults, exact: exactSearch });
        } catch (error) {
            this.logger.error(error, 'KeyCloakService.getUserByUsernameAndEmail');
            return [];
        }
    }

    public async getUserByUsernameOrEmail(username: string, email: string): Promise<UserRepresentation[]> {
        let getUser: UserRepresentation[] = await this.getUserByUsername(username);
        if (getUser.length > 0) {
            return getUser;
        }

        getUser = await this.getUserByEmail(email);
        if (getUser.length > 0) {
            return getUser;
        }

        return [];
    }

    public async getUserAtrributes(userId: string): Promise<Record<string, any>> {
        const user: UserRepresentation | undefined = await this.getUserById(userId);

        if (user === undefined) {
            return {};
        }

        return user.attributes ?? {};
    }

    public async setUserAttributes(userId: string, attributes: Record<string, any>): Promise<boolean> {
        const kcAdminClient = await this.getAdminClient();

        const user: UserRepresentation | undefined = await this.getUserById(userId);

        if (user === undefined) {
            return false;
        }

        await kcAdminClient.users.update({ id: userId }, { attributes: attributes });

        return true;
    }

    public async updateUser(userId: string, payload: UserRepresentation): Promise<boolean> {
        const kcAdminClient = await this.getAdminClient();

        const user: UserRepresentation | undefined = await this.getUserById(userId);

        if (user === undefined) {
            return false;
        }

        await kcAdminClient.users.update({ id: userId }, payload);

        return true;
    }

    public async setUserPassword(userId: string, password: string): Promise<void> {
        const kcAdminClient = await this.getAdminClient();

        kcAdminClient.users.resetPassword({ id: userId, credential: { type: 'password', value: password, temporary: false } });
    }

    public async createUser(
        username: string,
        email: string,
        password: string,
        attributes?: Record<string, any>,
    ): Promise<UserRepresentation | undefined> {
        if (attributes === undefined) {
            attributes = {
                userid: [''],
            };
        }
        this.logger.debug(email, 'KeyCloakService.createUser');
        const kcAdminClient = await this.getAdminClient();

        try {
            const createdUser: UserId = await kcAdminClient.users.create({
                username: username,
                email: email,
                enabled: true,
                credentials: [
                    {
                        type: 'password',
                        value: password,
                        temporary: false,
                    },
                ],
                attributes: attributes,
            });
            return await this.getUserById(createdUser.id);
        } catch (error) {
            this.logger.error(error, 'KeyCloakService.createUser');
            return undefined;
        }
    }

    private async getAdminClient(): Promise<KeycloakAdminClient> {
        if (this.kcAdminClient !== undefined) {
            return this.kcAdminClient;
        }

        // eslint-disable-next-line security/detect-eval-with-expression
        let kcAdminClient: KeycloakAdminClient = new (await eval(`import('@keycloak/keycloak-admin-client')`)).default();

        const kcAuthOptions: Credentials = {
            username: process.env['KEYCLOAK_ADMIN_USER'],
            password: process.env['KEYCLOAK_ADMIN_PASSWORD'],
            grantType: 'password',
            clientId: 'admin-cli',
        };

        if (process.env['KEYCLOAK_ADMIN_TOTP_SECRET'] !== undefined && process.env['KEYCLOAK_ADMIN_TOTP_SECRET'] !== '') {
            /**
             * @todo: Add support for TOTP
             */
            // kcAuthOptions['totp'] = process.env['KEYCLOAK_ADMIN_TOTP_SECRET'];
        }

        kcAdminClient.setConfig({
            baseUrl: process.env['KEYCLOAK_URL'],
        });

        try {
            await kcAdminClient.auth(kcAuthOptions); // Authenticate with Keycloak
        } catch (error) {
            this.logger.error(error, 'KeyCloakService.getAdminClient');
        }

        kcAdminClient.setConfig({
            realmName: process.env['KEYCLOAK_REALM'],
        });

        this.kcAdminClient = kcAdminClient;

        return kcAdminClient;
    }
}
