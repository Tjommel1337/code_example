import Jimp from "jimp";
import { v4 as uuidv4 } from 'uuid';
import convert from 'heic-convert';
import imagemin from 'imagemin';
import imageminPngquant from 'imagemin-pngquant';
import { writeFile, mkdir, stat } from 'fs/promises';
import { Injectable, Logger, LoggerService } from "@nestjs/common";
import { UserService } from "./user.service";

@Injectable()
export class UploadService {
    public convertFiles: string[] = [Jimp.MIME_PNG, Jimp.MIME_JPEG, 'image/heic'];
    
    // TODO: Move to config
    private readonly basePath: string = 'userdata'; // Base path for all files
    private readonly profilePath: string = 'profile'; // Location for profile pictures
    private readonly sessionPath: string = 'session'; // Location for session pictures
    private readonly userPath: string = 'user'; // Location for user pictures (picture galery in profile)
    private readonly communityPath: string = 'community'; // Location for community pictures (pictures in Forum, etc.)

    constructor(private readonly logger: Logger, private readonly userService: UserService) {}

    public async uploadProfileFile(file: Express.Multer.File, userId: string): Promise<string> {
        if (!this.convertFiles.includes(file.mimetype)) {
            throw new Error('File type not supported. (' + file.mimetype + ')');
        }
        const filename: string = uuidv4() + '.png';
        let fileBuffer: Buffer | undefined;
        if (file.mimetype === 'image/heic') {
            const pngBuffer = await convert({
                buffer: file.buffer,
                format: 'JPEG',
                quality: 0.7
            });

            fileBuffer = await this.convertFileToPng(Buffer.from(pngBuffer));
        } else {
            fileBuffer = await this.convertFileToPng(file.buffer);
        }

        if (!fileBuffer) {
            throw new Error('File could not be converted.');
        }

        fileBuffer = await this.minifyImage(fileBuffer);

        if (!fileBuffer) {
            throw new Error('File could not be minified.');
        }

        try {
            await this.writeFile(fileBuffer, this.basePath + '/' + this.profilePath, filename);
            await this.userService.setAvatar('/files/avatar/' + filename, userId);
        } catch (e) {
            throw e;
        }

        return filename;
    }

    private async convertFileToPng(fileBuffer: Buffer): Promise<Buffer | undefined> {
        try {
            return (await Jimp.read(fileBuffer)).getBufferAsync(Jimp.MIME_PNG);
        } catch (e) {
            this.logger.error(e, 'UploadService.convertFileToPng');
        }

        return undefined;
    }

    private async minifyImage(fileBuffer: Buffer): Promise<Buffer | undefined> {
        try {
            return await imagemin.buffer(fileBuffer, {
                plugins: [
                    imageminPngquant({
                        speed: 11,
                        strip: true,
                        quality: [0.6, 0.8]
                    })
                ]
            });
        } catch (e) {
            this.logger.error(e, 'UploadService.minifyImage');
        }

        return undefined;
    }

    private async writeFile(file: Buffer, destDir: string, filename: string): Promise<string> {
        if (!await this.checkDestDir(destDir, true)) {
            throw new Error('Destination directory does not exist.');
        }
        
        const destPath: string = destDir + '/' + filename;
        try {
            await writeFile(destPath, file);
        } catch (e) {
            throw new Error('File could not be saved.');
        }

        return filename;
    }

    private async checkDestDir(dir: string, create: boolean = true): Promise<boolean> {
        try {
            const fileStat = await stat(dir);
            if (fileStat.isDirectory()) {
                return true;
            }
        } catch (e) {
            if (create) {
                try {
                    await mkdir(dir);
                    return true;
                } catch (e) {
                    this.logger.error(e, 'UploadService.checkDestDir');
                }
                return false;
            }
        }

        return false;
    }
}
