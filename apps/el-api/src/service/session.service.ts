import { Injectable, Logger } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Session } from '../entities/session.entity';
import { CreateSessionRequest } from '../dtos/session.dto';

@Injectable()
export class SessionService {
    constructor(
        private readonly logger: Logger,
        @InjectRepository(Session)
        private sessionsRepository: Repository<Session>,
    ) { }

    public async createSession(data: CreateSessionRequest): Promise<Session | undefined> {
        const session: Session = new Session();

        session.start = data.start;
        session.duration = data.duration;
        session.emergency = data.emergency;

        return await this.sessionsRepository.save(session);
    }

    public async getSession(sessionId: string): Promise<Session | null> {
        return await this.sessionsRepository.findOne({
            where: [
                { id: sessionId },
            ],
        });
    }

    public async getSessions(): Promise<Session[]> {
        return await this.sessionsRepository.find();
    }
}
