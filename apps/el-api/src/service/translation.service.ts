import { Injectable } from '@nestjs/common';
import { join, parse } from 'path';
import { readFile, readdirSync } from 'graceful-fs';
import { of, Observable, bindCallback, map, catchError } from 'rxjs';
import { BaseTranslateService, i18nFile } from '@myapp/i18n';
@Injectable()
export class TranslateService extends BaseTranslateService {

    private languagesAvailable: string[] = [];
    private languageCache: Record<string, i18nFile> = {};

    public loadLanguages(): void {
        this.loadLanguagesAvailable();

        // Load all language in cache
        this.languagesAvailable.forEach((language: string) => {
            this.readLanguageFromFile(language);
        });
    }

    public override loadLanguage(languages: string[]): Observable<i18nFile> {
        this.loadLanguagesAvailable();

        // Search for matching languages
        const matchedLanguage: string = this.getMatchingLanguage(languages);

        // No matching language found
        if (!matchedLanguage) {
            return of({});
        }
        this.currentLang = matchedLanguage;

        // Load and return matching language
        return this.readLanguageFromFile(matchedLanguage);
    }

    private loadLanguagesAvailable(): void {
        const i18nPath: string = join(__dirname, "/assets", "/i18n");

        // Scan folder for all languages
        if (!this.languagesAvailable || this.languagesAvailable.length === 0) {
            this.languagesAvailable = readdirSync(i18nPath).map((filename: string) => parse(filename).name);
        }
    }

    public getMatchingLanguage(languages: string[]): string {
        let matchedLanguage: string = this.getDefaultLang();

        const matchResult: string | undefined = languages.find((language: string): boolean => {
            let index: number = this.languagesAvailable.indexOf(language);
            if (index !== -1) {
                matchedLanguage = this.languagesAvailable[index];
                return true;
            }

            // maybe we have to load the base version
            if (language.indexOf("-") !== -1) {
                const baseLanguage: string = language.split("-")[0];
                let index: number = this.languagesAvailable.indexOf(baseLanguage);
                if (index !== -1) {
                    matchedLanguage = this.languagesAvailable[index];
                    return true
                }
            }

            return false;
        });

        return matchResult ? matchedLanguage : this.getDefaultLang();
    }

    public getLanguageFromCache(language: string): i18nFile {
        if (this.languageCache[language]) {
            return this.languageCache[language];
        }
        return {};
    }

    private readLanguageFromFile(language: string): Observable<i18nFile> {
        const i18nPath: string = join(__dirname, "/assets", "/i18n");
        if (this.languageCache && this.languageCache[language]) {
            return of(this.languageCache[language]);
        } else {
            return bindCallback(readFile)(join(i18nPath, language + ".json")).pipe(
                map((callBackReturn: [err: NodeJS.ErrnoException | null, data: Buffer]): i18nFile => {
                    if (!Array.isArray(callBackReturn)) {
                        throw callBackReturn;
                    }
                    this.languageCache[language] = JSON.parse(String(callBackReturn[1]));
                    return this.languageCache[language];
                }),
                catchError((error: any) => {
                    console.log(error);
                    return of({})
                })
            );
        }
    }
}

