import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { SessionStatus, Time } from '@myapp/enums';
import { CreateSessionRequestInterface, SessionInterface } from '@myapp/interfaces';
import { SessionEntityInterface } from '../interfaces/entities/session.interface';


export class CreateSessionRequest implements CreateSessionRequestInterface {
    @ApiProperty({
        type: Date,
        example: 'start',
    })
    @IsNotEmpty()
    readonly start!: Date;

    @ApiProperty({
        type: Number,
        example: Time.Day,
    })
    @IsNotEmpty()
    readonly duration!: number;

    @ApiProperty({
        type: String,
        example: "emergency",
    })
    @IsNotEmpty()
    readonly emergency!: string;
}

export class SessionResponse implements SessionInterface {
    // Quickfix
    [key: string]: any; // index signature to access fields dynamicly

    constructor(session: SessionEntityInterface) {
        Object.keys(session).forEach((key: string): void => {
            if (Object.keys(this).includes(key)) {
                this[key] = session[key];
            }
        });
    }


    @ApiProperty({
        type: Date,
        example: '2023-10-28T20:28:32.529Z',
    })
    readonly createdAt!: Date;

    @ApiProperty({
        type: Date,
        example: '2023-10-28T20:28:32.529Z',
    })
    readonly updatedAt!: Date;

    @ApiProperty({
        type: String,
        example: '4ba00864-98ee-4a4f-a52c-f975ed7a4fd7',
    })
    readonly id!: string;

    @ApiProperty({
        type: String,
        example: SessionStatus.DRAFT,
    })
    readonly status: SessionStatus = SessionStatus.DRAFT;

    @ApiProperty({
        type: Date,
        example: new Date(),
    })
    readonly start!: Date;

    @ApiProperty({
        type: Number,
        example: Time.Day,
    })
    readonly duration?: number;

    @ApiProperty({
        type: String,
        example: "emergency",
    })
    readonly emergency?: string;

}
