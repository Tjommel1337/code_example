import { UploadType } from '@myapp/enums';
import { UploadRequestInterface, UploadResponseInterface } from '@myapp/interfaces';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum } from 'class-validator';

export class UploadRequest implements UploadRequestInterface {
    @ApiProperty({
        type: 'string',
        example: UploadType.session,
        enum: UploadType,
    })
    @IsEnum(UploadType)
    readonly type!: UploadType;

    @ApiProperty({
        type: 'string',
        format: 'binary',
        required: true
    })
    readonly file!: Express.Multer.File;

    @ApiPropertyOptional({
        type: String,
        example: "571e1852-ae92-49d9-a966-65c610f6b44f",
        description: `required for type '${UploadType.session}'`,
    })
    readonly session_id?: string;
}

export class UploadResponse implements UploadResponseInterface {
    @ApiProperty({
        type: String,
        example: "571e1852-ae92-49d9-a966-65c610f6b44f",
    })
    readonly id: string = "571e1852-ae92-49d9-a966-65c610f6b44f";
}
