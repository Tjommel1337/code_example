import { ErrorResponseInterface } from '@myapp/interfaces';
import { HttpStatus } from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';

export class ErrorResponse implements ErrorResponseInterface {
    @ApiProperty({
        type: Number,
        example: HttpStatus.BAD_REQUEST,
    })
    statusCode: number = HttpStatus.BAD_REQUEST;

    @ApiProperty({
        type: String,
        isArray: true,
    })
    message: string | string[] = [];

    @ApiProperty({
        type: String,
        example: 'Bad Request',
    })
    error: string = 'Bad Request';
}

export class UnauthorizedResponse extends ErrorResponse {
    @ApiProperty({
        type: Number,
        example: HttpStatus.UNAUTHORIZED,
    })
    override statusCode: number = HttpStatus.UNAUTHORIZED;

    @ApiProperty({
        type: String,
        example: 'Unauthorized',
    })
    override error: string = 'Unauthorized';
}

export class ForbiddenResponse extends ErrorResponse {
    @ApiProperty({
        type: Number,
        example: HttpStatus.FORBIDDEN,
    })
    override statusCode: number = HttpStatus.FORBIDDEN;

    @ApiProperty({
        type: String,
        example: 'Forbidden',
    })
    override error: string = 'Forbidden';
}

export class NotFoundResponse extends ErrorResponse {
    @ApiProperty({
        type: Number,
        example: HttpStatus.NOT_FOUND,
    })
    override statusCode: number = HttpStatus.NOT_FOUND;

    @ApiProperty({
        type: String,
        example: 'Not Found',
    })
    override error: string = 'Not Found';
}

export class InternalServerErrorResponse extends ErrorResponse {
    @ApiProperty({
        type: Number,
        example: HttpStatus.INTERNAL_SERVER_ERROR,
    })
    override statusCode: number = HttpStatus.INTERNAL_SERVER_ERROR;

    @ApiProperty({
        type: String,
        example: 'Internal Server Error',
    })
    override error: string = 'Internal Server Error';
}
