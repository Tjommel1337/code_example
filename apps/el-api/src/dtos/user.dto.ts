import { ProjectRole, Gender, GenderType, Language, Theme } from '@myapp/enums';
import {
    UpdateUserRequestInterface,
    ChangePasswordRequestInterface,
    CreateUserRequestInterface,
    UserInterface,
} from '@myapp/interfaces';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsDateString, IsEmail, IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { UserEntityInterface } from '../interfaces/entities/user.interface';
import { LooseObject } from '../interfaces/loose-object.interface';

export class UserUpdateRequest implements UpdateUserRequestInterface {
    @ApiPropertyOptional({
        type: String,
        example: Language.EN,
    })
    @IsOptional()
    @IsEnum(Language)
    readonly language?: Language = Language.EN;

    @ApiPropertyOptional({
        type: String,
        example: Theme.System,
    })
    @IsOptional()
    @IsEnum(Theme)
    readonly theme?: Theme = Theme.System;

    @ApiPropertyOptional({
        type: Date,
        example: new Date('2000-10-30'),
    })
    @IsOptional()
    @IsDateString()
    readonly birthdate?: Date;

    @ApiPropertyOptional({
        type: String,
        example: Gender.MAN,
    })
    @IsOptional()
    @IsEnum(Gender)
    readonly gender?: Gender = Gender.MAN;

    @ApiPropertyOptional({
        type: String,
        example: GenderType.EMPTY,
    })
    @IsOptional()
    @IsEnum(GenderType)
    readonly gender_type?: GenderType = GenderType.EMPTY;

    @ApiPropertyOptional({
        type: String,
        example: ProjectRole.WEARER,
    })
    @IsOptional()
    @IsEnum(ProjectRole)
    readonly project_role?: ProjectRole = ProjectRole.WEARER;

    @ApiPropertyOptional({
        type: String,
        example: 'dd.MM.yyyy HH:mm',
    })
    @IsOptional()
    @IsString()
    // @Matches(/^(dd\.MM\.yyyy\ HH\:mm)|(foobar)$/)
    readonly datetime_format?: string = 'dd.MM.yyyy HH:mm';

    @ApiPropertyOptional({
        type: String,
        example: 'A sample profile description.',
    })
    @IsOptional()
    @IsString()
    readonly description?: string = 'A sample profile description.';
}

export class CreateUserRequest implements CreateUserRequestInterface {
    @ApiProperty({
        type: String,
        example: 'username',
    })
    @IsNotEmpty()
    readonly username!: string;

    @ApiProperty({
        type: String,
        example: 'S3cr3tP@ssW0rd',
    })
    @IsNotEmpty()
    readonly password!: string;

    @ApiProperty({
        type: String,
        example: 'mail@example.com',
    })
    @IsNotEmpty()
    @IsEmail()
    readonly email!: string;
}

export class ChangePasswordRequest implements ChangePasswordRequestInterface {
    @ApiProperty({
        type: String,
        example: 'oldPassword',
    })
    @IsNotEmpty({
        groups: ['user.change_password'],
    })
    readonly oldPassword: string = 'oldPassword';

    @ApiProperty({
        type: String,
        example: 'S3cr3tP@ssW0rd',
    })
    @IsNotEmpty({
        groups: ['user.change_password'],
    })
    readonly password: string = 'S3cr3tP@ssW0rd';

    @ApiProperty({
        type: String,
        example: 'S3cr3tP@ssW0rd',
    })
    @IsNotEmpty({
        groups: ['user.change_password'],
    })
    readonly passwordConfirm: string = 'S3cr3tP@ssW0rd';
}

export class UserResponse implements UserInterface {
    // Quickfix
    [key: string]: any; // index signature to access fields dynamicly

    constructor(user: UserEntityInterface) {
        Object.keys(user).forEach((key: string): void => {
            if (Object.keys(this).includes(key)) {
                this[key] = user[key];
            }
        });
    }

    @ApiProperty({
        type: String,
        example: '4ba00864-98ee-4a4f-a52c-f975ed7a4fd7',
    })
    readonly id!: string;

    @ApiProperty({
        type: Date,
        example: '2023-10-28T20:28:32.529Z',
    })
    readonly createdAt!: Date;

    @ApiProperty({
        type: Date,
        example: '2023-10-28T20:28:32.529Z',
    })
    readonly updatedAt!: Date;

    @ApiProperty({
        type: String,
        example: '4ba00864-98ee-4a4f-a52c-f975ed7a4fd7',
    })
    readonly auth_id!: string;

    @ApiProperty({
        type: String,
        example: 'userXYZ',
    })
    readonly username!: string;

    @ApiProperty({
        type: String,
        example: 'mail@example.com',
    })
    readonly email?: string;

    @ApiProperty({
        type: String,
        example: '/files/avatar/4988fd37-7aa0-416d-835b-6114c67d9661.png',
    })
    readonly avatar?: string;

    @ApiProperty({
        type: String,
        example: Language.EN,
    })
    readonly language?: Language = Language.EN;

    @ApiProperty({
        type: String,
        example: Theme.System,
    })
    readonly theme?: Theme = Theme.System;

    @ApiProperty({
        type: Date,
        example: new Date('2000-10-30'),
    })
    readonly birthdate?: Date;

    @ApiProperty({
        type: String,
        example: Gender.WOMAN,
    })
    readonly gender?: Gender = Gender.WOMAN;

    @ApiProperty({
        type: String,
        example: GenderType.EMPTY,
    })
    readonly gender_type?: GenderType = GenderType.EMPTY;

    @ApiProperty({
        type: String,
        example: ProjectRole.WEARER,
    })
    readonly project_role?: ProjectRole = ProjectRole.WEARER;

    @ApiProperty({
        type: String,
        example: 'dd.MM.yyyy HH:mm',
    })
    readonly datetime_format?: string = 'dd.MM.yyyy HH:mm';

    @ApiProperty({
        type: String,
        example: 'A sample profile description.',
    })
    readonly description?: string = 'A sample profile description.';
}
