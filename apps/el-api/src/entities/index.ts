import { Session } from './session.entity';
import { User } from './user.entity';

export * from './session.entity';
export * from './user.entity';

export const allEntities = [User, Session];
