import { SessionStatus } from "@myapp/enums";
import { BaseEntity } from "@myapp/nest-db";
import { Entity, Column } from 'typeorm';
import { SessionEntityInterface } from "../interfaces/entities/session.interface";


@Entity()
export class Session extends BaseEntity implements SessionEntityInterface {
    @Column({ name: "start", type: "datetime", nullable: true })
    public start: Date = new Date();

    @Column({ name: "duration", type: "int", nullable: true })
    public duration: number = 0;

    @Column({ name: "status", type: "varchar", nullable: true })
    public status: SessionStatus = SessionStatus.DRAFT;

    @Column({ name: "emergency", type: "varchar", nullable: true })
    public emergency: string = "";
}
