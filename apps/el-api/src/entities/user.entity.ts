import { ProjectRole, Gender, GenderType, Language, Theme } from '@myapp/enums';
import { BaseEntity } from '@myapp/nest-db';
import { Entity, Column } from 'typeorm';
import { UserEntityInterface } from '../interfaces/entities/user.interface';

@Entity()
export class User extends BaseEntity implements UserEntityInterface {
    @Column({ name: 'auth_id', type: 'varchar', unique: true })
    public auth_id: string = '';

    @Column({ name: 'username', type: 'varchar', unique: true })
    public username: string = '';

    @Column({ name: 'email', type: 'varchar', unique: true })
    public email: string = '';

    @Column({ name: 'avatar', type: 'varchar', nullable: true })
    public avatar?: string;

    @Column({ name: 'language', type: 'varchar', default: Language.EN })
    public language: Language = Language.EN;

    @Column({ name: 'theme', type: 'varchar', default: Theme.System })
    public theme: Theme = Theme.System;

    @Column({ name: 'birthdate', type: 'datetime', nullable: true })
    public birthdate?: Date;

    @Column({ name: 'gender', type: 'varchar', nullable: true })
    public gender?: Gender;

    @Column({ name: 'gender_type', type: 'varchar', nullable: true })
    public gender_type?: GenderType;

    @Column({ name: 'project_role', type: 'varchar', nullable: true })
    public project_role?: ProjectRole;

    @Column({ name: 'datetime_format', type: 'varchar', nullable: true })
    public datetime_format?: string;

    @Column({ name: 'description', type: 'text', nullable: true })
    public description?: string;
}
