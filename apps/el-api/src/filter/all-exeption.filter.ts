import { ExceptionFilter, Catch, ArgumentsHost, HttpException, HttpStatus } from '@nestjs/common';
import { HttpAdapterHost } from '@nestjs/core';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
    constructor(private readonly httpAdapterHost: HttpAdapterHost) {}

    catch(exception: unknown, host: ArgumentsHost): void {
        const { httpAdapter } = this.httpAdapterHost;
        const ctx = host.switchToHttp();
        if (exception instanceof HttpException) {
            httpAdapter.reply(ctx.getResponse(), exception.getResponse(), exception.getStatus());
            return;
        }

        httpAdapter.reply(
            ctx.getResponse(),
            {
                statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
                message: 'Internal Server Error',
            },
            HttpStatus.INTERNAL_SERVER_ERROR,
        );
    }
}
