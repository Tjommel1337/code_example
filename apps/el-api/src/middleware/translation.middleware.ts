import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import * as accepts from 'accepts';
import { TranslateService } from '../service/translation.service';

/*
TODO:
O> Keycloak sprache laden wenn user eingelogt ist
*/

@Injectable()
export class TranslationMiddleware implements NestMiddleware {

    constructor(public readonly translateService: TranslateService) { }

    use(req: Request, _res: Response, next: NextFunction) {
        const accept: accepts.Accepts = accepts.default(req);
        this.translateService.use(accept.languages()).subscribe(() => next());
    }
}
