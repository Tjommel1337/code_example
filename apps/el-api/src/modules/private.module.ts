import { HttpModule } from '@nestjs/axios';
import { Logger, MiddlewareConsumer, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FileController } from '../controller/private/file.controller';
import { UserController } from '../controller/private/user.controller';
import { allEntities } from '../entities';
import { TranslationMiddleware } from '../middleware/translation.middleware';
import { KeyCloakService } from '../service/keycloak.service';
import { TranslateService } from '../service/translation.service';
import { UploadService } from '../service/upload.service';
import { UserService } from '../service/user.service';
import { SessionService } from '../service/session.service';
import { SessionController } from '../controller/private/session.controller';

@Module({
    imports: [HttpModule, TypeOrmModule.forFeature(allEntities)],
    controllers: [FileController, UserController, SessionController],
    providers: [HttpModule, TranslateService, KeyCloakService, Logger, UserService, SessionService, UploadService],
    exports: [UploadService],
})
export class PrivateModule {
    configure(consumer: MiddlewareConsumer) {
        consumer.apply(TranslationMiddleware).forRoutes('*');
    }
}
