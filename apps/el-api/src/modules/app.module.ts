import { KeycloakGuard, NestKeycloakModule } from '@myapp/nest-keycloak';
import { HttpModule } from '@nestjs/axios';
import { Logger, MiddlewareConsumer, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { APP_GUARD } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NestjsFormDataModule } from 'nestjs-form-data';
import { TranslationMiddleware } from '../middleware/translation.middleware';
import { KeyCloakService } from '../service/keycloak.service';
import { HealthModule } from './health.module';
import { PrivateModule } from './private.module';
import { PublicModule } from './public.module';
import { TranslateService } from '../service/translation.service';

@Module({
    imports: [
        ConfigModule.forRoot({
            envFilePath: ['.env.local', '.env'],
            isGlobal: true,
        }),
        HealthModule,
        HttpModule,
        NestjsFormDataModule,
        NestKeycloakModule,
        PublicModule,
        PrivateModule,
        TypeOrmModule.forRootAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: (configService: ConfigService) => ({
                type: 'mariadb',
                url: configService.get<string>('DATABASE_URL'),
                debug:
                    configService.get<string>('DATABASE_DEBUG', 'false') === 'true' ||
                    configService.get<string>('DATABASE_DEBUG', 'false') === '1',
                trace:
                    configService.get<string>('DATABASE_DEBUG', 'false') === 'true' ||
                    configService.get<string>('DATABASE_DEBUG', 'false') === '1',
                autoLoadEntities: true,
                subscribers: ['../subscriber/*.js', '../subscriber/*.ts'],
                migrations: ['../migration/*.js', '../migration/*.ts'],
                logging: ['error'],
                synchronize: true,
            }),
        }),
    ],
    controllers: [],
    providers: [
        {
            provide: APP_GUARD,
            useClass: KeycloakGuard,
        },
        KeyCloakService,
        Logger,
        TranslateService,
    ],
    exports: [KeyCloakService, Logger, TypeOrmModule],
})
export class AppModule {
    configure(consumer: MiddlewareConsumer) {
        consumer.apply(TranslationMiddleware).forRoutes('*');
    }
}
