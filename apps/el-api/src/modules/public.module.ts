import { HttpModule } from '@nestjs/axios';
import { Logger, MiddlewareConsumer, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AppController } from '../controller/public/app.controller';
import { FileController } from '../controller/public/file.controller';
import { allEntities, User } from '../entities';
import { TranslationMiddleware } from '../middleware/translation.middleware';
import { KeyCloakService } from '../service/keycloak.service';
import { UserService } from '../service/user.service';
import { TranslateService } from '../service/translation.service';

@Module({
    imports: [HttpModule, TypeOrmModule.forFeature(allEntities)],
    controllers: [AppController, FileController],
    providers: [Repository<User>, KeyCloakService, Logger, UserService, TranslateService],
    exports: [],
})
export class PublicModule {
    configure(consumer: MiddlewareConsumer) {
        consumer.apply(TranslationMiddleware).forRoutes('*');
    }
}
