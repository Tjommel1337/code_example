import { Request } from 'express';

export interface RequestWithUser extends Request {
    user: {
        preferred_username?: string;
        sub?: string;
    };
}
