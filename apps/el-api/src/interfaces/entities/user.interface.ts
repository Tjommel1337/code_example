import { ProjectRole, Gender, GenderType, Language, Theme } from '@myapp/enums';
import { BaseInterface } from '@myapp/nest-db';

export interface UserEntityInterface extends BaseInterface {
    auth_id: string;
    username: string;
    email?: string;
    avatar?: string;
    language?: Language;
    theme?: Theme;
    birthdate?: Date;
    gender?: Gender;
    gender_type?: GenderType;
    project_role?: ProjectRole;
    datetime_format?: string;
    description?: string;

    [key: string]: any; // index signature to access fields dynamicly
}
