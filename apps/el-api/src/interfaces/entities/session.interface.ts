import { BaseInterface } from "@myapp/nest-db";

export interface SessionEntityInterface extends BaseInterface {
    start: Date;
    duration: number;
    status: string;
    emergency: string;

    [key: string]: any; // index signature to access fields dynamicly
}
