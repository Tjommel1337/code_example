import { HttpAdapterHost, NestFactory } from '@nestjs/core';
import { Logger, ValidationPipe, VersioningType } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import helmet from 'helmet';
import { AllExceptionsFilter } from './filter/all-exeption.filter';
import { AppModule } from './modules/app.module';
import { PublicModule } from './modules/public.module';

async function bootstrap() {
    // create (API) Logger
    const logger = new Logger('API');

    // create NestJS Application
    const app = await NestFactory.create(AppModule, {
        // uncomment this line to disable debug logging
        // logger: ['error', 'warn'],
        cors: {
            origin: '*',
        },
    });

    const configService = app.get(ConfigService);

    // basic Security
    app.use(
        helmet({
            crossOriginResourcePolicy: false,
        }),
    );

    // enable gloabl ValidationPipe
    app.useGlobalPipes(new ValidationPipe());

    // default API Version
    app.enableVersioning({
        type: VersioningType.URI,
        defaultVersion: '1',
    });

    // private API Docs
    const privateConfig = new DocumentBuilder()
        .setTitle(configService.get<string>('APP_NAME', ''))
        .setDescription('All API Endpoints')
        .setVersion('1.0')
        .addBearerAuth()
        .build();
    const privateDocument = SwaggerModule.createDocument(app, privateConfig);
    SwaggerModule.setup('/docs', app, privateDocument);

    // public API Docs
    const publicConfig = new DocumentBuilder()
        .setTitle(configService.get<string>('APP_NAME', ''))
        .setDescription('API Doczumentation')
        .setVersion('1.0')
        .addBearerAuth()
        .addOAuth2({
            type: 'oauth2',
            flows: {
                authorizationCode: {
                    authorizationUrl: configService.get<string>('KEYCLOAK_AUTH_URL', ''),
                    tokenUrl: configService.get<string>('KEYCLOAK_TOKEN_URL', ''),
                    refreshUrl: configService.get<string>('KEYCLOAK_TOKEN_URL', ''),
                    scopes: {
                        profile: 'User profile',
                    },
                },
            },
        })
        .build();
    const publicDocument = SwaggerModule.createDocument(app, publicConfig, {
        include: [PublicModule],
    });
    SwaggerModule.setup('/', app, publicDocument);

    // "Catch" all Exceptions
    app.useGlobalFilters(new AllExceptionsFilter(app.get(HttpAdapterHost)));

    // start the server
    const port = configService.get<number>('WEB_PORT', 3000);
    await app.listen(port);
    logger.log(`Listening on Port: ${port.toString()}`);
}
bootstrap();
