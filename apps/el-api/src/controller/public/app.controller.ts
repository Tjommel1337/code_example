import { i18nFile } from '@myapp/i18n';
import { HasScope, Unprotected } from '@myapp/nest-keycloak';
import { Controller, Get, Param, VERSION_NEUTRAL, Version } from '@nestjs/common';
import { ApiSecurity, ApiTags } from '@nestjs/swagger';
import { TranslateService } from '../../service/translation.service';

@Controller()
export class AppController {
    constructor(private readonly translateService: TranslateService) { }

    @ApiTags('Public')
    @Version(['1'])
    @ApiSecurity('bearer')
    @Get('/user')
    @HasScope('profile')
    getUser(): string {
        return `Hello from user`;
    }

    @ApiTags('Public')
    @Version([VERSION_NEUTRAL])
    @Get('/i18n/:language')
    @Unprotected()
    getLanguage(@Param('language') language: string): Record<string, string | i18nFile> {
        const matchingLanguage: string = this.translateService.getMatchingLanguage(language.split(','));
        return {
            language: matchingLanguage,
            translation: this.translateService.getLanguageFromCache(matchingLanguage),
        };
    }
}
