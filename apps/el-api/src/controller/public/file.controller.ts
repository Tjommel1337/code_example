import { Unprotected } from '@myapp/nest-keycloak';
import { Controller, Get, HttpStatus, Param, Res, Version, VERSION_NEUTRAL } from '@nestjs/common';
import { ApiNotFoundResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { stat } from 'fs/promises';
import { InternalServerErrorResponse, NotFoundResponse } from '../../dtos/base.dto';
import { UserService } from '../../service/user.service';

@Controller('/files')
export class FileController {
    constructor(private readonly userService: UserService) { }

    @ApiTags('Public')
    @Version([VERSION_NEUTRAL])
    @ApiOkResponse({
        description: 'The user Avatar',
    })
    @ApiNotFoundResponse({
        description: 'The user Avatar was not found',
    })
    @Unprotected()
    @Get('/avatar/:filename')
    async getAvatar(@Param('filename') filename: string, @Res() res: Response) {
        // 4988fd37-7aa0-416d-835b-6114c67d9661.png
        if (!filename.match(/^[a-z0-9-.]+$/i)) {
            res.status(HttpStatus.NOT_FOUND).json(new NotFoundResponse());
            return;
        }
        try {
            await stat('userdata/profile/' + decodeURI(filename));
        } catch (e) {
            res.status(HttpStatus.NOT_FOUND).json(new NotFoundResponse());
            return;
        }
        try {
            res.status(HttpStatus.OK).sendFile(decodeURI(filename), { root: 'userdata/profile' });
            return;
        } catch (e) {
            res.status(HttpStatus.INTERNAL_SERVER_ERROR).send(new InternalServerErrorResponse());
            return;
        }
    }
}
