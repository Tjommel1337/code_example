/**
 * Decorator Documentation:
 *
 * --> Before class:
 * =================
 * @Controller()
 * Set the path (prefix) of the controller to "/"
 *
 * @Controller('XYZ')
 * Set the path (prefix) of the controller to "/XYZ"
 *
 * --> Before method:
 * ==================
 * @ApiTags('XYZ')
 * Group endpoints by tag with name 'XYZ'
 *
 * @Version([VERSION_NEUTRAL])
 * Set the version of the endpoint
 * Possible options: VERSION_NEUTRAL, "1"
 * VERSION_NEUTRAL: Endpoint has no version
 *
 * @Get('/XYZ')
 * Set the path of the endpoint to "/XYZ" (as GET request)
 *
 * @Post('/XYZ')
 * Set the path of the endpoint to "/XYZ" (as POST request)
 *
 * @Put('/XYZ')
 * Set the path of the endpoint to "/XYZ" (as PUT request)
 *
 * @Unprotected()
 * Endpoint is unprotected (no login required)
 *
 * @Unprotected()
 * Endpoint is protected (a valid JWT is required)
 *
 * @HasRole(['user']) / @HasRole('user')
 * Endpoint is protected (role 'user' required)
 *
 * @HasScope(['profile']) / @HasScope('profile')
 * Endpoint is protected (scope 'profile' required)
 *
 * @ApiSecurity('bearer')
 * API documentation: Endpoint is protected (login with bearertoken is required)
 *
 * @ApiCreatedResponse({
 *   description: 'The file has been successfully uploaded.',
 *   type: UploadResponse,
 * })
 * API documentation: Endpoint returns status code 201 (created) and the UploadResponse object
 *
 * @ApiBadRequestResponse({
 *   description: 'The data are invalid',
 *   type: ErrorResponse,
 * })
 * API documentation: Endpoint returns status code 400 (bad request) and the ErrorResponse object
 *
 * @ApiUnauthorizedResponse({
 *   description: 'The user is not authorized.',
 *   type: UnauthorizedResponse,
 * })
 * API documentation: Endpoint returns status code 401 (unauthorized) and the UnauthorizedResponse object
 *
 * @ApiConsumes('multipart/form-data')
 * API documentation: Endpoint consumes multipart/form-data
 *
 * @UseInterceptors(FileInterceptor('file'))
 * Use the FileInterceptor to parse the file with the name 'file'
 *
 * --> In method:
 * ==============
 * @Req() req: Request
 * Get the request object
 *
 * @Res() res: Response
 * Get the response object
 *
 * @Body() body: UploadRequest,
 * Get the body object (parsed with the UploadRequest class)
 *
 * @Param('id') id: string
 * Get the parameter 'id' from the path
 *
 * @Query('id') id: string
 * Get the query parameter 'id' from the path
 *
 * @Headers('id') id: string
 * Get the header parameter 'id' from the path
 *
 * @UploadedFile(
 *   new ParseFilePipe()
 * ) file: Express.Multer.File
 * Parse the file with the name 'file' (and validate it)
 *
 * @UploadedFiles(
 *   new ParseFilePipe()
 * ) file: Express.Multer.File
 * Parse the files (multiple) with the name 'file' (and validate them)
 */
