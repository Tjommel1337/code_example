import { HasRole, HasScope, Unprotected } from '@myapp/nest-keycloak';
import { Controller, Get, Version, VERSION_NEUTRAL } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

@Controller()
export class DummyController {
    @ApiTags('Hello')
    @Version([VERSION_NEUTRAL])
    @Get('/private')
    @Unprotected()
    getpublic(): string {
        return `Hello from private`;
    }

    @ApiTags('Hello')
    @Version([VERSION_NEUTRAL])
    @Get('/scope')
    @HasScope(['profile'])
    getScope(): string {
        return `Hello from scope`;
    }

    @ApiTags('Hello')
    @Version([VERSION_NEUTRAL])
    @Get('/user')
    @HasRole(['user'])
    getUser(): string {
        return `Hello from user`;
    }

    @ApiTags('Hello')
    @Version([VERSION_NEUTRAL])
    @Get('/admin')
    @HasRole(['admin'])
    getAdmin(): string {
        return `Hello from user`;
    }

    @ApiTags('Hello')
    @Version([VERSION_NEUTRAL])
    @Get('/all')
    @HasRole(['user', 'admin'])
    getAll(): string {
        return `Hello from all`;
    }
}
