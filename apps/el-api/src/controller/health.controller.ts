import { Unprotected } from '@myapp/nest-keycloak';
import { Controller, Get, Version, VERSION_NEUTRAL } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { HealthCheckService, HealthCheck } from '@nestjs/terminus';

@Controller('health')
export class HealthController {
    constructor(private readonly health: HealthCheckService) { }

    @ApiTags('System')
    @Version([VERSION_NEUTRAL])
    @Get()
    @Unprotected()
    @HealthCheck()
    check() {
        return this.health.check([]);
    }
}
