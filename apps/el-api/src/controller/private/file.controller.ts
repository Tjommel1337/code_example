import { UploadType } from '@myapp/enums';
import { HasRole } from '@myapp/nest-keycloak';
import {
    Body,
    Controller,
    HttpStatus,
    ParseFilePipe,
    Post,
    Req,
    Res,
    UploadedFile,
    UseInterceptors,
    Version,
    VERSION_NEUTRAL,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBadRequestResponse, ApiConsumes, ApiCreatedResponse, ApiSecurity, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { Response } from 'express';
import Jimp from 'jimp';
import 'multer';
import { ErrorResponse, InternalServerErrorResponse, UnauthorizedResponse } from '../../dtos/base.dto';
import { UploadRequest, UploadResponse } from '../../dtos/upload.dto';
import { RequestWithUser } from '../../interfaces/request.interface';
import { UploadService } from '../../service/upload.service';

@Controller('/files')
export class FileController {
    constructor(private readonly uploadService: UploadService) { }

    @Post('/upload')
    @ApiTags('File')
    @Version(VERSION_NEUTRAL)
    @ApiCreatedResponse({
        description: 'The file has been successfully uploaded.',
        type: UploadResponse,
    })
    @ApiBadRequestResponse({
        description: 'The data are invalid',
        type: ErrorResponse,
    })
    @ApiUnauthorizedResponse({
        description: 'The user is not authorized.',
        type: UnauthorizedResponse,
    })
    @ApiConsumes('multipart/form-data')
    @ApiSecurity('bearer')
    @HasRole('user')
    @UseInterceptors(FileInterceptor('file'))
    async uploadFile(
        @Body() body: UploadRequest,
        @UploadedFile(new ParseFilePipe()) file: Express.Multer.File,
        @Req() req: RequestWithUser,
        @Res() res: Response,
    ) {
        let filename: string = '';
        switch (body.type) {
            case UploadType.session:
                if (!body.session_id) {
                    const errorResponse: ErrorResponse = new ErrorResponse();
                    errorResponse.message = 'session_id is required.';
                    res.status(HttpStatus.BAD_REQUEST).json(errorResponse);
                    return;
                }

                res.status(HttpStatus.NOT_IMPLEMENTED).json({ message: 'Not implemented' });
                return;
                // do something
                break;
            case UploadType.profile:
                if (req.user.sub === undefined) {
                    const errorResponse: ErrorResponse = new ErrorResponse();
                    errorResponse.message = 'User not found.';
                    res.status(HttpStatus.BAD_REQUEST).json(errorResponse);
                    return;
                }
                if (![Jimp.MIME_PNG, Jimp.MIME_JPEG, 'image/heic'].includes(file.mimetype)) {
                    const errorResponse: ErrorResponse = new ErrorResponse();
                    errorResponse.message = 'File type not supported.';
                    res.status(HttpStatus.BAD_REQUEST).json(errorResponse);
                    return;
                }

                try {
                    filename = await this.uploadService.uploadProfileFile(file, req.user.sub);
                } catch (error) {
                    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(new InternalServerErrorResponse());
                    return;
                }
                break;
            default:
                const errorResponse: ErrorResponse = new ErrorResponse();
                errorResponse.message = 'type is not supported.';
                res.status(HttpStatus.BAD_REQUEST).json(errorResponse);
                return;
        }

        res.status(HttpStatus.CREATED).json({
            filename: filename,
        });
    }
}
