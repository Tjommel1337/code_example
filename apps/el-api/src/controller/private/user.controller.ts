import { HasRole, Unprotected } from '@myapp/nest-keycloak';
import { Body, Controller, Get, HttpStatus, Param, Post, Put, Req, Res, Version, VERSION_NEUTRAL } from '@nestjs/common';
import {
    ApiBadRequestResponse,
    ApiForbiddenResponse,
    ApiNoContentResponse,
    ApiNotFoundResponse,
    ApiOkResponse,
    ApiSecurity,
    ApiTags,
    ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Response } from 'express';
import { ErrorResponse, ForbiddenResponse, InternalServerErrorResponse, NotFoundResponse, UnauthorizedResponse } from '../../dtos/base.dto';
import { UserUpdateRequest, ChangePasswordRequest, CreateUserRequest, UserResponse } from '../../dtos/user.dto';
import { User } from '../../entities/user.entity';
import { RequestWithUser } from '../../interfaces/request.interface';
import { UserService } from '../../service/user.service';

@Controller('/user')
export class UserController {
    constructor(private readonly userService: UserService) { }

    @ApiTags('User')
    @Version([VERSION_NEUTRAL])
    @ApiNoContentResponse({
        description: 'Data saved successfully',
    })
    @ApiBadRequestResponse({
        description: 'The data are invalid',
        type: ErrorResponse,
    })
    @ApiForbiddenResponse({
        description: 'The user is not allowed to change the password',
        type: ForbiddenResponse,
    })
    @Post('/')
    @Unprotected()
    async addUser(@Body() data: CreateUserRequest, @Req() req: RequestWithUser, @Res() res: Response) {
        const user: User | boolean | undefined = await this.userService.createUser(data.username, data.email, data.password);
        if (user === undefined) {
            res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(new InternalServerErrorResponse());
            return;
        }
        if (!user) {
            const badRequest: ErrorResponse = new ErrorResponse();
            badRequest.message = 'Username or Email already exists!';
            res.status(HttpStatus.BAD_REQUEST).json(badRequest);
            return;
        }
        res.json(user);
    }

    @ApiTags('User')
    @Version([VERSION_NEUTRAL])
    @ApiOkResponse({
        description: 'Data saved successfully',
        type: UserResponse,
    })
    @ApiBadRequestResponse({
        description: 'The data are invalid',
        type: ErrorResponse,
    })
    @ApiUnauthorizedResponse({
        description: 'The user is not authorized.',
        type: UnauthorizedResponse,
    })
    @ApiForbiddenResponse({
        description: 'The user is not allowed to change the password',
        type: ForbiddenResponse,
    })
    @ApiNotFoundResponse({
        description: 'The user was not found',
        type: NotFoundResponse,
    })
    @ApiSecurity('bearer')
    @Get('/:id')
    @HasRole('user')
    async getUser(@Param('id') id: string, @Req() req: RequestWithUser, @Res() res: Response) {
        const user: User | null = await this.userService.getUser(id);
        if (user === null) {
            res.status(HttpStatus.NOT_FOUND).json(new NotFoundResponse());
            return;
        }

        const response: UserResponse = new UserResponse(user);

        res.json(response);
    }

    @ApiTags('User')
    @Version([VERSION_NEUTRAL])
    @ApiNoContentResponse({
        description: 'Data saved successfully',
    })
    @ApiBadRequestResponse({
        description: 'The data are invalid',
        type: ErrorResponse,
    })
    @ApiUnauthorizedResponse({
        description: 'The user is not authorized.',
        type: UnauthorizedResponse,
    })
    @ApiForbiddenResponse({
        description: 'The user is not allowed to change the password',
        type: ForbiddenResponse,
    })
    @ApiSecurity('bearer')
    @Put('/:id')
    @HasRole('user')
    async changeAttributes(@Param('id') id: string, @Body() data: UserUpdateRequest, @Req() req: RequestWithUser, @Res() res: Response) {
        console.log(req.user);
        if (id !== req.user.sub) {
            res.status(HttpStatus.FORBIDDEN).send(new ForbiddenResponse());
            return;
        }
        await this.userService.updateUser(data, req.user.sub);
        res.status(HttpStatus.NO_CONTENT).send();
    }

    @ApiTags('User')
    @Version([VERSION_NEUTRAL])
    @ApiNoContentResponse({
        description: 'The password changed successfully',
    })
    @ApiBadRequestResponse({
        description: 'The data are invalid',
        type: ErrorResponse,
    })
    @ApiUnauthorizedResponse({
        description: 'The user is not authorized.',
        type: UnauthorizedResponse,
    })
    @ApiForbiddenResponse({
        description: 'The user is not allowed to change the password',
        type: ForbiddenResponse,
    })
    @ApiSecurity('bearer')
    @Put('/:id/change-password')
    @HasRole('user')
    async putChangePassword(
        @Param('id') id: string,
        @Body() data: ChangePasswordRequest,
        @Req() req: RequestWithUser,
        @Res() res: Response,
    ) {
        if (id !== req.user.sub) {
            res.status(HttpStatus.FORBIDDEN).send(new ForbiddenResponse());
            return;
        }
        await this.userService.changePassword(data, req.user.sub);
        res.status(HttpStatus.NO_CONTENT).send();
    }
}
