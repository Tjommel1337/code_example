import { HasRole, Unprotected } from '@myapp/nest-keycloak';
import { Body, Controller, Get, HttpStatus, Param, Post, Req, Res, Version, VERSION_NEUTRAL } from '@nestjs/common';
import {
    ApiBadRequestResponse,
    ApiForbiddenResponse,
    ApiNoContentResponse,
    ApiNotFoundResponse,
    ApiOkResponse,
    ApiSecurity,
    ApiTags,
    ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Response } from 'express';
import { ErrorResponse, ForbiddenResponse, InternalServerErrorResponse, NotFoundResponse, UnauthorizedResponse } from '../../dtos/base.dto';
import { RequestWithUser } from '../../interfaces/request.interface';
import { SessionService } from '../../service/session.service';
import { CreateSessionRequest, SessionResponse } from '../../dtos/session.dto';
import { Session } from '../../entities/session.entity';

@Controller('/session')
export class SessionController {
    constructor(private readonly sessionService: SessionService) { }

    @ApiTags('Session')
    @Version([VERSION_NEUTRAL])
    @ApiNoContentResponse({
        description: 'Data saved successfully',
    })
    @ApiBadRequestResponse({
        description: 'The data are invalid',
        type: ErrorResponse,
    })
    @ApiForbiddenResponse({
        description: 'The user is not allowed to change the password',
        type: ForbiddenResponse,
    })
    @Post('/')
    @Unprotected()
    async postSession(@Body() data: CreateSessionRequest, @Req() req: RequestWithUser, @Res() res: Response) {
        const session: Session | undefined = await this.sessionService.createSession(data);
        if (session === undefined) {
            res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(new InternalServerErrorResponse());
            return;
        }
        res.json(session);
    }


    @ApiTags('Session')
    @Version([VERSION_NEUTRAL])
    @ApiOkResponse({
        description: 'Data saved successfully',
        type: SessionResponse,
    })
    @ApiBadRequestResponse({
        description: 'The data are invalid',
        type: ErrorResponse,
    })
    @ApiUnauthorizedResponse({
        description: 'The user is not authorized.',
        type: UnauthorizedResponse,
    })
    @ApiForbiddenResponse({
        description: 'The user is not allowed to change the password',
        type: ForbiddenResponse,
    })
    @ApiNotFoundResponse({
        description: 'The user was not found',
        type: NotFoundResponse,
    })
    @ApiSecurity('bearer')
    @Get('/:id')
    @HasRole('user')
    async getSession(@Param('id') id: string, @Req() req: RequestWithUser, @Res() res: Response) {
        const session: Session | null = await this.sessionService.getSession(id);
        if (session === null) {
            res.status(HttpStatus.NOT_FOUND).json(new NotFoundResponse());
            return;
        }

        const response: SessionResponse = new SessionResponse(session);

        res.json(response);
    }


    @ApiTags('Session')
    @Version([VERSION_NEUTRAL])
    @ApiOkResponse({
        description: 'Data saved successfully',
        type: Array<SessionResponse>,
    })
    @ApiBadRequestResponse({
        description: 'The data are invalid',
        type: ErrorResponse,
    })
    @ApiUnauthorizedResponse({
        description: 'The user is not authorized.',
        type: UnauthorizedResponse,
    })
    @ApiForbiddenResponse({
        description: 'The user is not allowed to change the password',
        type: ForbiddenResponse,
    })
    @ApiNotFoundResponse({
        description: 'The user was not found',
        type: NotFoundResponse,
    })
    @ApiSecurity('bearer')
    @Get('/')
    @HasRole('user')
    async getSessions(@Req() req: RequestWithUser, @Res() res: Response) {
        const sessions: Session[] | null = await this.sessionService.getSessions();
        if (sessions === null) {
            res.status(HttpStatus.NOT_FOUND).json(new NotFoundResponse());
            return;
        }

        const response: SessionResponse[] = sessions.map((session: Session): SessionResponse => new SessionResponse(session));

        res.json(response);
    }
}
