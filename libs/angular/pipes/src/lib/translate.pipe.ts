import { CommonModule } from '@angular/common';
import { NgModule, Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@myapp/ngservices';

@Pipe({
    name: 'translate',
})
export class TranslationPipe implements PipeTransform {
    constructor(private readonly translateService: TranslateService) { }

    public transform(key: string, interpolateParams?: any): string {
        return this.translateService.get(key, interpolateParams);
    }
}

@NgModule({
    imports: [CommonModule],
    declarations: [TranslationPipe],
    exports: [CommonModule, TranslationPipe],
})
export class TranslationModule { }
