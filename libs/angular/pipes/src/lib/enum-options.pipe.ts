import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'enumOptions',
    standalone: true
})
export class EnumOptionsPipe implements PipeTransform {
    transform(_value: any, enumType: any): any[] {
        const options = [];
        for (const key of Object.keys(enumType)) {
            options.push({ value: enumType[key], label: key });
        }
        return options;
    }
}
