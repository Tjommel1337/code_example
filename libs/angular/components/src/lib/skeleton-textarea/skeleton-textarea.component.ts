import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SkeletonModule } from 'primeng/skeleton';

@Component({
    standalone: true,
    imports: [CommonModule, FormsModule, SkeletonModule, ReactiveFormsModule],
    selector: 'skeleton-textarea',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './skeleton-textarea.component.html',
    styleUrls: []
})
export class SkeletonTextareaComponent implements OnChanges {
    @Input('floatLabel') public floatLabel: string | undefined;
    @Input('model') public value: string | undefined;
    @Input('rows') public rows: number = 5;
    @Output() public valueChange: EventEmitter<string> = new EventEmitter<string>();
    @Output() public modelChange: EventEmitter<string> = new EventEmitter<string>();

    public myForm: FormGroup;

    constructor(private readonly formBuilder: FormBuilder) {
        this.myForm = this.formBuilder.group({
            myTextarea: [this.value || '']
        });
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (changes['value']) {
            this.myForm.get('myTextarea')?.patchValue(this.value);
        }
    }

    public onTextAreaBlur(): void {
        const newValue: string = this.myForm.get('myTextarea')?.value || '';
        if (this.value !== newValue) {
            this.value = newValue;
            this.valueChange.emit(this.value);
            this.modelChange.emit(this.value);
        }
    }
}
