import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SkeletonModule } from 'primeng/skeleton';
import { CalendarModule } from 'primeng/calendar';

type mode = 'date' | 'unix';

@Component({
    standalone: true,
    imports: [CommonModule, FormsModule, SkeletonModule, ReactiveFormsModule, CalendarModule],
    selector: 'skeleton-date',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './skeleton-date.component.html',
    styleUrls: []
})
export class SkeletonDateComponent implements OnChanges {
    @Input('floatLabel') public floatLabel: string | undefined;
    @Input('model') public value: Date | undefined;
    @Input('mode') public mode: mode = 'date';
    @Output() public valueChange: EventEmitter<Date | number> = new EventEmitter<Date | number>();
    @Output() public modelChange: EventEmitter<Date | number> = new EventEmitter<Date | number>();

    public myForm: FormGroup;

    constructor(private readonly formBuilder: FormBuilder) {
        this.myForm = this.formBuilder.group({
            myDatepicker: ['']
        });
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (!changes['value'] || changes['value'].currentValue <= 0) {
            this.value = new Date(0);
            this.myForm.get('myDatepicker')?.patchValue('');
            return;
        }

        this.value = new Date(changes['value'].currentValue);
        this.myForm.get('myDatepicker')?.patchValue(this.value);
    }

    public onDatepickerBlur(): void {
        const newValue: Date = this.myForm.get('myDatepicker')?.value || null;
        if (this.value !== newValue) {
            this.value = newValue;
            this.emitValue();
        }
    }

    public onDatepickerClear(): void {
        // on clear set date to 01.01.1970
        this.value = new Date(0);
        this.myForm.get('myDatepicker')?.patchValue('');
        this.emitValue();
    }

    private emitValue(): void {
        if (!this.value) {
            this.value = new Date(0);
        }
        switch (this.mode) {
            case 'date': {
                this.valueChange.emit(this.value);
                this.modelChange.emit(this.value);
                break;
            }
            case 'unix': {
                this.valueChange.emit(this.value.getTime());
                this.modelChange.emit(this.value.getTime());
            }
        }
    }

}
