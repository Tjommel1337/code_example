import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { SkeletonModule } from 'primeng/skeleton';
import { EditorModule } from 'primeng/editor';
import { FormsModule } from '@angular/forms';

@Component({
    standalone: true,
    imports: [CommonModule, SkeletonModule, FormsModule, EditorModule],
    selector: 'skeleton-wysiwyg',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './skeleton-wysiwyg.component.html',
    styleUrls: []
})
export class SkeletonWYSIWYGComponent implements OnChanges {
    public test: string = 'test';
    @Input('model') public model: string | undefined;
    @Output() public modelChange: EventEmitter<string> = new EventEmitter<string>();

    public ngOnChanges(changes: SimpleChanges): void {
        if (changes['value']) {
            this.model = this.model || '';
        }
    }

    public onWYSIWYGBlur(event: any): void {
        console.log(event);
        const newValue: string = event.htmlValue;
        if (this.model !== newValue) {
            this.model = newValue;
            this.modelChange.emit(this.model);
        }
    }
}
