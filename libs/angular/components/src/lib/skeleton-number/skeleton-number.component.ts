import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SkeletonModule } from 'primeng/skeleton';
import { InputNumberModule } from 'primeng/inputnumber';

@Component({
    standalone: true,
    imports: [CommonModule, FormsModule, SkeletonModule, ReactiveFormsModule, InputNumberModule],
    selector: 'skeleton-number',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './skeleton-number.component.html',
    styleUrls: []
})
export class SkeletonNumberComponent implements OnChanges {
    @Input('floatLabel') public floatLabel: string | undefined;
    @Input('model') public value: string | undefined;
    @Input('disabled') public disabled: boolean = false;
    @Input('type') public type: 'int' | 'float' = 'int';
    @Input('steps') public steps: number = 0.1;
    @Input('alignment') public alignment: 'right' | 'left' = 'left';
    @Output() public valueChange: EventEmitter<string> = new EventEmitter<string>();
    @Output() public modelChange: EventEmitter<string> = new EventEmitter<string>();


    public myForm: FormGroup;

    constructor(private readonly formBuilder: FormBuilder) {
        this.myForm = this.formBuilder.group({
            myInput: {
                value: this.value || '',
                disabled: this.disabled
            }
        });
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (changes['value']) {
            this.myForm.get('myInput')?.patchValue(this.value);
            if (this.disabled) {
                this.myForm.get('myInput')?.disable();
            } else {
                this.myForm.get('myInput')?.enable();
            }
        }
    }

    public onTextAreaBlur(): void {
        const newValue: string = this.myForm.get('myInput')?.value || '';
        if (this.value !== newValue) {
            this.value = newValue;
            this.valueChange.emit(this.value);
            this.modelChange.emit(this.value);
        }
    }
}
