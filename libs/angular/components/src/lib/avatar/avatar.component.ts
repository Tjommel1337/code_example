import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UserInterface } from '@myapp/interfaces';
import { SkeletonModule } from 'primeng/skeleton';

@Component({
    standalone: true,
    imports: [
        CommonModule,
        SkeletonModule
    ],
    selector: 'avatar',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './avatar.component.html',
    styleUrls: ['./avatar.component.scss']
})
export class AvatarComponent {
    @Input() public size?: number;
    @Input() public userData: BehaviorSubject<UserInterface | undefined> | undefined;

    public apiUrl: string = '';

    public getImageStyles(): { [key: string]: string } {
        return {
            'height.px': String(this.size) ?? 'auto',
            'width.px': String(this.size) ?? 'auto'
        };
    }
}
