import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SkeletonModule } from 'primeng/skeleton';
import { DropdownModule } from 'primeng/dropdown';

export interface DropdownOption {
    label: string;
    value: any;
}

@Component({
    selector: 'skeleton-dropdown',
    templateUrl: './skeleton-dropdown.component.html',
    styleUrls: [],
    standalone: true,
    imports: [CommonModule, FormsModule, ReactiveFormsModule, SkeletonModule, DropdownModule],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SkeletonDropdownComponent implements OnInit, OnChanges {
    @Input('floatLabel') public floatLabel: string | undefined;
    @Input('options') public options: DropdownOption[] | undefined;
    @Input('model') public value: any;
    @Input('waitForValue') public waitForValue: boolean = true;
    @Input('disabled') public disabled: boolean = false;
    @Output() public valueChange: EventEmitter<any> = new EventEmitter<any>();
    @Output() public modelChange: EventEmitter<any> = new EventEmitter<any>();
    @Output() public onChange: EventEmitter<any> = new EventEmitter<any>();

    public ngOnInit(): void {
        this.checkForValue();
    }

    public ngOnChanges(): void {
        this.checkForValue();
        console.log('ngonChanges');
    }

    public onDropdownChange(event: any): void {
        this.value = event.value;
        this.valueChange.emit(this.value);
        this.modelChange.emit(this.value);
        this.onChange.emit(this.value);
    }

    private checkForValue(): void {
        // if value does not exists in options, add new options with value as value and blank label
        if (this.options?.find((x: DropdownOption): boolean => x.value === this.value) === undefined) {
            this.options?.unshift({ label: '', value: this.value });
        }
    }
}
