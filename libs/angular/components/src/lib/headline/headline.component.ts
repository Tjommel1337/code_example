import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UserInterface } from '@myapp/interfaces';
import { SkeletonModule } from 'primeng/skeleton';

@Component({
    standalone: true,
    imports: [
        CommonModule,
        SkeletonModule
    ],
    selector: 'headline',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './headline.component.html',
    styleUrls: []
})
export class HeadlineComponent {
    @Input() public userData: BehaviorSubject<UserInterface | undefined> | undefined;
}
