import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SkeletonModule } from 'primeng/skeleton';
import { InputTextModule } from 'primeng/inputtext';

@Component({
    standalone: true,
    imports: [CommonModule, FormsModule, SkeletonModule, ReactiveFormsModule, InputTextModule],
    selector: 'skeleton-input',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './skeleton-input.component.html',
    styleUrls: []
})
export class SkeletonInputComponent implements OnChanges {
    @Input('floatLabel') public floatLabel: string | undefined;
    @Input('model') public value: string | undefined;
    @Output() public valueChange: EventEmitter<string> = new EventEmitter<string>();
    @Output() public modelChange: EventEmitter<string> = new EventEmitter<string>();

    public myForm: FormGroup;

    constructor(private readonly formBuilder: FormBuilder) {
        this.myForm = this.formBuilder.group({
            myInput: [this.value || '']
        });
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (changes['value']) {
            this.myForm.get('myInput')?.patchValue(this.value);
        }
    }

    public onTextAreaBlur(): void {
        const newValue: string = this.myForm.get('myInput')?.value || '';
        if (this.value !== newValue) {
            this.value = newValue;
            this.valueChange.emit(this.value);
            this.modelChange.emit(this.value);
        }
    }
}
