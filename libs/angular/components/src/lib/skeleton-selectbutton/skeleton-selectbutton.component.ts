import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SkeletonModule } from 'primeng/skeleton';
import { SelectButtonModule } from 'primeng/selectbutton';

interface SelectButtonOption {
    label: string;
    value: any;
}

@Component({
    selector: 'skeleton-select-button',
    templateUrl: './skeleton-selectbutton.component.html',
    styleUrls: [],
    standalone: true,
    imports: [CommonModule, FormsModule, SkeletonModule, SelectButtonModule],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SkeletonSelectButtonComponent {
    @Input() public options: SelectButtonOption[] = [];
    @Input('model') public value: string | undefined = undefined;
    @Output() public valueChange: EventEmitter<string> = new EventEmitter<string>();

    public onSelectButtonChange(event: any): void {
        this.value = event.value;
        this.valueChange.emit(this.value);
    }
}
