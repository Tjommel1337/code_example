import { HttpClient, HttpErrorResponse, HttpEvent, HttpEventType } from '@angular/common/http';
import { ChangeDetectorRef, Component, EventEmitter, Input, Output, inject } from '@angular/core';
import { UploadType } from '@myapp/enums';
import { ErrorResponseInterface, UploadResponseInterface } from '@myapp/interfaces';
import { Observable, catchError, map, of } from 'rxjs';
import { TranslateService } from '@myapp/ngservices';

@Component({
    standalone: true,
    template: ''
})
export abstract class UploadComponent {
    @Input() public uploadType: UploadType | undefined;

    @Output() public onSuccess: EventEmitter<string> = new EventEmitter<string>();

    public progress: number = 0;

    public http: HttpClient = inject(HttpClient);
    public changeDetection: ChangeDetectorRef = inject(ChangeDetectorRef);
    public translateService: TranslateService = inject(TranslateService);

    public abstract uploadStarted(): void;
    public abstract uploadFinished(error?: string): void;

    public addFile(event: any): void {
        if (event.currentTarget) {
            this.upload(event.currentTarget.files[event.currentTarget.files.length - 1]);
        }
    }

    public upload(file: File): void {
        this.uploadStarted();

        if (!this.uploadType) {
            this.uploadFinished(this.translateService.get('error.missingUploadType'));
            this.changeDetection.detectChanges();
            return;
        }

        const formData: FormData = new FormData();
        formData.append('type', this.uploadType);
        formData.append('file', file);
        if (this.uploadType === UploadType.session) {
            formData.append('session_id', 'foo');
        }

        this.http
            .post('/files/upload', formData, {
                observe: 'events',
                reportProgress: true
            })
            .pipe(
                map((event: HttpEvent<Object>): Observable<void> => {
                    console.log(event);
                    if (event.type === HttpEventType.UploadProgress) {
                        if (event.total) {
                            const progress: number = Math.round(100 * (event.loaded / event.total));
                            this.progress = progress > 99 ? 99 : progress;
                            this.changeDetection.detectChanges();
                        }
                    }
                    if (event.type === HttpEventType.Response) {
                        const uploadResponse: UploadResponseInterface = event.body as UploadResponseInterface;
                        this.onSuccess.emit(uploadResponse.id);
                        this.uploadFinished();
                        this.changeDetection.detectChanges();
                    }
                    return of();
                }),
                catchError((response: HttpErrorResponse): Observable<void> => {
                    const errorResponse: ErrorResponseInterface = response.error as ErrorResponseInterface;
                    console.log(this.translateService);
                    let errorMesage: string = this.translateService.get('error.unknownErrorOccurred');
                    if (typeof errorResponse.message === 'string') {
                        errorMesage = errorResponse.message;
                    } else if (errorResponse.message.length > 0) {
                        errorMesage = errorResponse.message[0];
                    }
                    this.uploadFinished(errorMesage);
                    this.changeDetection.detectChanges();
                    return of();
                })
            )
            .subscribe();
    }
}
