import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FileUploadModule } from 'primeng/fileupload';
import { ProgressBarModule } from 'primeng/progressbar';
import { UploadComponent } from '../upload.component';

@Component({
    standalone: true,
    imports: [CommonModule, FileUploadModule, HttpClientModule, ProgressBarModule],
    selector: 'upload-area',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: 'upload.component.html'
})
export class UploadAreaComponent extends UploadComponent {
    public fileSelect: boolean = true;
    public dragging: boolean = false;
    public uploading: boolean = false;
    public uploaded: boolean = false;

    public error: string = '';

    public uploadAreaButton: string = '';
    public uploadAreaButtonSubtext: string = '';

    constructor() {
        super();
        this.uploadAreaButton = this.translateService.get('components.uploadAreaButton');
        this.uploadAreaButtonSubtext = this.translateService.get('components.uploadAreaButtonSubtext');
    }

    public dragenter(event: DragEvent): void {
        event.preventDefault();
        this.dragging = true;
    }

    public dragleave(event: DragEvent): void {
        event.preventDefault();
        this.dragging = false;
    }

    public drop(event: DragEvent): void {
        event.preventDefault();
        event.stopPropagation();
        this.dragging = false;
        if (event.dataTransfer) {
            // in case the files array fucked up somehow... we always return the newest element
            this.upload(event.dataTransfer.files[event.dataTransfer.files.length - 1]);
        }
    }

    public reset(): void {
        this.uploaded = false;
        this.fileSelect = true;
        this.uploading = false;
    }

    public uploadStarted(): void {
        this.progress = 0;
        this.error = '';
        this.uploaded = false;
        this.fileSelect = false;
        this.uploading = true;
    }

    public uploadFinished(error?: string): void {
        if (error) {
            this.error = error;
        }
        this.uploaded = true;
        this.uploading = false;
    }
}
