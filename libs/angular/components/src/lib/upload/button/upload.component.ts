import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FileUploadModule } from 'primeng/fileupload';
import { ProgressBarModule } from 'primeng/progressbar';

@Component({
    standalone: true,
    imports: [CommonModule, FileUploadModule, HttpClientModule, ProgressBarModule],
    selector: 'upload-button',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: 'upload.component.html'
})
export class UploadButtonComponent {
    public chooseIcon: string = 'pi pi-plus';
    public label: string = 'Browse';
    public progress: number = 20;

    constructor(public readonly http: HttpClient) {}

    public onProgress(event: any): void {
        this.label = 'Uploading . . .';
        console.log('onProgress', event);
    }

    public onUpload(event: any): void {
        this.label = 'Uploaded';
        console.log('onUpload', event);
    }

    public onError(event: any): void {
        this.label = 'Error while uploading';
        console.log('onError', event);
    }
}
