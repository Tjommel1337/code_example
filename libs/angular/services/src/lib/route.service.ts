import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';

@Injectable({
    providedIn: 'root',
})
export class RouteService {
    constructor(private readonly activatedRoute: ActivatedRoute) {}

    public activeRoute(): ActivatedRouteSnapshot | undefined {
        let route: ActivatedRoute = this.activatedRoute;
        while (route.firstChild) {
            route = route.firstChild;
        }
        if (route.outlet === 'primary') {
            return route.snapshot;
        }
        return undefined;
    }
}
