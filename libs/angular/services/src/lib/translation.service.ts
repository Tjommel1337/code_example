import { Injectable } from '@angular/core';
import { of, Observable, map } from 'rxjs';
import { BaseTranslateService, i18nFile } from '@myapp/i18n';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class TranslateService extends BaseTranslateService {
    constructor(private readonly http: HttpClient) {
        super();
    }

    public override loadLanguage(languages: string[]): Observable<i18nFile> {
        if (this.http && languages[0]) {
            return this.http.get('/i18n/' + languages[0]).pipe(
                map((response: any): any => {
                    this.currentLang = response.language;
                    return response.translation;
                })
            );
        }
        return of({});
    }

    public getBrowserLang(): string[] {
        return navigator.languages as string[];
    }
}
