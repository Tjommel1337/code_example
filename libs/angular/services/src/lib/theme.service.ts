import { Injectable } from '@angular/core';
import { fromEvent, map, Observable, startWith } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ThemeService {
    private auto: boolean = false;
    private scale: number = 16;
    public activeTheme: string = '';

    // should be called in factory in app.module.ts
    public watchPrefersColorScheme(): void {
        // subscripte to media to watch changes on the fly
        this.media('(prefers-color-scheme: dark)').subscribe((matches: boolean): void => {
            if (this.auto) {
                if (matches) {
                    this.setTheme('dark', false);
                } else {
                    this.setTheme('light', false);
                }
            } else {
                this.setTheme('light', false);
            }
        });
    }

    public setAuto(auto: boolean): void {
        this.auto = auto;
        this.watchPrefersColorScheme();
    }

    public setTheme(theme: string, overwriteAuto: boolean = true): void {
        // disable auto system theme selection
        if (overwriteAuto) {
            this.auto = false;
        }

        // Check if theme-node exists or else create theme-node
        let themeElement: HTMLLinkElement | null = document.getElementById('theme-link') as HTMLLinkElement | null;
        if (!themeElement) {
            themeElement = document.createElement('link');
            themeElement.type = 'text/css';
            themeElement.rel = 'stylesheet';
            document.head.appendChild(themeElement);
        }

        // Set theme
        themeElement.setAttribute('href', theme + '.css');
        this.activeTheme = theme;
    }

    public decrementScale(): void {
        this.scale -= 1;
        this.applyScale();
    }

    public incrementScale(): void {
        this.scale += 1;
        this.applyScale();
    }

    private applyScale(): void {
        document.documentElement.style.fontSize = String(this.scale) + 'px';
    }

    private media(query: string): Observable<boolean> {
        const mediaQuery: MediaQueryList = window.matchMedia(query);
        return fromEvent<MediaQueryList>(mediaQuery, 'change').pipe(
            startWith(mediaQuery),
            map((list: MediaQueryList): boolean => list.matches)
        );
    }
}
