import { Injectable } from '@angular/core';
import { KeycloakService } from '@myapp/ngkeycloak';
import { KeycloakProfile } from 'keycloak-js';
import { MsTime, Time } from '@myapp/enums';
import { IntervalTimer } from '@myapp/helper';
import { UserService } from './user.service';

@Injectable({
    providedIn: 'root'
})
export class SessionService {
    public isLoggedIn: boolean = false;
    public userProfile: KeycloakProfile = {};

    private readonly intervals: IntervalTimer = new IntervalTimer();

    constructor(public readonly keycloakService: KeycloakService, public readonly userService: UserService) { }

    public async init(): Promise<void> {
        this.isLoggedIn = await this.keycloakService.isLoggedIn();

        if (!this.isLoggedIn) {
            return;
        }

        // set token
        await this.setToken();

        // start refresh interval
        this.intervals.clearAll();
        this.intervals.startInterval('kc_refresh', async (): Promise<void> => this.refreshToken(), MsTime.Minute * 2);

        // load user
        if (this.userProfile.id) {
            this.userService.loadUser(this.userProfile.id);
        }
    }

    public async register(): Promise<void> {
        await this.keycloakService.register();
    }

    public async login(): Promise<void> {
        await this.keycloakService.login();
        if (this.userProfile.id) {
            this.userService.loadUser(this.userProfile.id);
        }
    }

    public async logout(): Promise<void> {
        localStorage.clear();
        await this.keycloakService.logout();
    }

    public async refreshToken(): Promise<void> {
        if (!this.isLoggedIn) {
            return;
        }

        const refreshed: boolean = await this.keycloakService.updateToken(Time.Minute * 5);
        if (refreshed) {
            await this.setToken();
        }

        const debugInfo: Record<string, unknown> = {
            debug: true,
            type: 'refreshToken',
            userid: this.keycloakService.getKeycloakInstance().tokenParsed?.sub,
            token: await this.keycloakService.getToken()
        };

        console.log(debugInfo);
    }

    private async setToken(): Promise<void> {
        // Set token in local storage
        const token: string | undefined = await this.keycloakService.getToken();
        if (token !== undefined) {
            localStorage.setItem('kc_token', token);
            localStorage.setItem('kc_refreshtoken', String(this.keycloakService.getKeycloakInstance().refreshToken));

            // load userprofile in service
            this.userProfile = await this.keycloakService.loadUserProfile();
        }
    }
}
