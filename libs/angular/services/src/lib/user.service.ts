import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpEvent, HttpEventType } from '@angular/common/http';
import { BehaviorSubject, Observable, catchError, distinctUntilChanged, last, map, of } from 'rxjs';
import { UserInterface } from '@myapp/interfaces';
import { MessageService } from 'primeng/api';
import { ThemeService } from './theme.service';
import { Theme } from '@myapp/enums';

@Injectable()
export class UserService {
    public userData$: BehaviorSubject<UserInterface | undefined> = new BehaviorSubject<UserInterface | undefined>(undefined);

    constructor(
        private readonly http: HttpClient,
        private readonly messageService: MessageService,
        private readonly themeService: ThemeService
    ) { }

    public loadUser(id: string): void {
        this.http
            .get('/user/' + id)
            .pipe(
                distinctUntilChanged(),
                map((response: any): void => {
                    this.userData$.next(response);
                    this.setTheme();
                })
            )
            .subscribe();
    }

    public reloadUser(): void {
        if (!this.userData$.value?.auth_id) {
            return;
        }
        this.loadUser(this.userData$.value?.auth_id);
    }

    public saveUser(data: UserInterface): Observable<void | Observable<void>> {
        if (!this.userData$.value?.auth_id) {
            return of();
        }

        return this.http
            .put('/user/' + String(this.userData$.value?.auth_id), data, {
                observe: 'events'
            })
            .pipe(
                map((event: HttpEvent<Object>): Observable<void> => {
                    if (event.type === HttpEventType.Response) {
                        this.messageService.add({
                            severity: 'success',
                            summary: 'Save settings',
                            detail: 'Successfully saved'
                        });
                        this.setTheme();
                    }
                    return of();
                }),
                last(),
                catchError((_response: HttpErrorResponse): Observable<void> => {
                    this.messageService.add({
                        severity: 'error',
                        summary: 'Save settings',
                        detail: 'Error while saving'
                    });
                    return of();
                })
            );
    }

    private setTheme(): void {
        if (this.userData$.value?.theme) {
            if (this.userData$.value?.theme === Theme.System) {
                this.themeService.setAuto(true);
                return;
            }
            this.themeService.setTheme(this.userData$.value?.theme);
        }
    }
}
