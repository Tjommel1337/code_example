export enum Language {
    EN = 'en',
    DE = 'de',
}

export enum LanguageLabel {
    'English' = 'en',
    'Deutsch' = 'de',

}
