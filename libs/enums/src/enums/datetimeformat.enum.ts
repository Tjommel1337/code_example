export enum DateTimeFormat {
    'dd.MM.yyyy HH:mm:ss' = 'dd.MM.yyyy HH:mm:ss',
    'yyyy.MM.dd HH:mm:ss' = 'yyyy.MM.dd HH:mm:ss',
    'MM.dd.yyyy HH:mm:ss' = 'MM.dd.yyyy HH:mm:ss',
}
