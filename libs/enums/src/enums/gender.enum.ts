export enum Gender {
    MAN = 'man',
    WOMAN = 'woman',
    NONBBINARY = 'nonbinary',
}

export enum GenderType {
    EMPTY = 'empty',
    CIS = 'cis',
    TRANS = 'trans',
}
