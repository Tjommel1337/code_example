export enum SessionStatus {
    DRAFT = 'draft',
    RUNNING = 'running',
    FINISHED = 'finished',
    ABORTED = 'aborted',
    FAILED = 'failed',
    KEY = 'key',
    PENDING = 'pending',
}
