export enum ProjectRole {
    WEARER = 'wearer',
    HOLDER = 'holder',
    SWITCH = 'switch',
}
