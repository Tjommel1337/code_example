import { createIntl } from '@formatjs/intl'
import { switchMap, of, Observable, map, BehaviorSubject } from 'rxjs';

export type i18nFile = Record<string, Record<string, unknown>>;

export class BaseTranslateService {

    public currentLang: string = "en";
    private defaultLang: BehaviorSubject<string> = new BehaviorSubject("en");

    private i18nCurrent: i18nFile = {};
    private i18nDefault: i18nFile = {};

    constructor() {
        this.defaultLang.pipe(
            switchMap((lang: string) => this.loadLanguage([lang])),
            map((languageFile: i18nFile) => this.i18nDefault = languageFile)
        ).subscribe();
    }

    public setDefaultLang(lang: string): void {
        this.defaultLang.next(lang);
    }

    public getDefaultLang(): string {
        return this.defaultLang.getValue();
    }

    public use(lang: string | string[]): Observable<void> {
        let selectedLanguage: string[] = [];

        // set browserselection from user
        if (typeof lang === "string") {
            selectedLanguage = [lang];
        } else {
            selectedLanguage = lang;
        }

        const $loadlanguage: Observable<void> = this.loadLanguage(selectedLanguage).pipe(
            map((languageFile: i18nFile) => {
                this.i18nCurrent = languageFile
            })
        );
        $loadlanguage.subscribe();
        return $loadlanguage;
    }

    public get(key: string, interpolateParams?: any): string {
        return createIntl(
            {
                locale: this.currentLang,
                messages: this.flatteningTranslations(this.i18nCurrent)
            }
        ).formatMessage(
            {
                id: key,
                defaultMessage: this.flatteningTranslations(this.i18nDefault)[key]
            },
            interpolateParams
        );
    }

    // only API
    public loadLanguage(_languages: string[]): Observable<i18nFile> {
        return of({});
    };

    private flatteningTranslations(obj: any = {}, res: any = {}, extraKey: string = ''): Record<string, string> {
        Object.keys(obj).forEach((key: string): void => {
            if (typeof obj[key] !== 'object') {
                res[extraKey + key] = obj[key];
            } else {
                this.flatteningTranslations(obj[key], res, `${extraKey}${key}.`);
            };
        });
        return res;
    }
}

