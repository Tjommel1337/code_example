/* eslint-disable security/detect-object-injection */

export class IntervalTimer {
    private readonly oIntervals: Record<string, ReturnType<typeof setTimeout> | boolean> = {};

    public startInterval(
        identifier: string,
        action: () => void,
        timeout: number,
    ): void {
        // cleanup identifier
        const cleanIdentifier: string = identifier.replace(/[^A-Za-z0-9]/gu, '');

        // start action
        action();

        // reset the interval
        if (this.oIntervals[cleanIdentifier] !== false) {
            clearInterval(this.oIntervals[cleanIdentifier] as ReturnType<typeof setTimeout>);
        }

        // start interval
        this.oIntervals[cleanIdentifier] = setInterval(action, timeout);
    }

    public stopInterval(identifier: string): void {
        // cleanup identifier
        const cleanIdentifier: string = identifier.replace(/[^A-Za-z0-9]/gu, '');

        // clear interval
        clearInterval(this.oIntervals[cleanIdentifier] as ReturnType<typeof setTimeout>);
    }

    public clearAll(): void {
        Object.keys(this.oIntervals).forEach((sKey: string): void => {
            clearInterval(this.oIntervals[sKey] as ReturnType<typeof setTimeout>);
        });
    }
}
