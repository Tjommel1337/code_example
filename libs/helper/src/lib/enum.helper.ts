export function getEnumValue<T extends Record<string, unknown>, K extends keyof T>(enumObj: T, key: K): T[K] {
    // Return the value of the enum
    // e.g. getEnumValue(Theme, "System") returns "Systemeee"
    // cause Theme.System = "Systemeee"
    return enumObj[key];
}
