export async function sleep(seconds: number): Promise<void> {
    return new Promise(resolve => setTimeout(resolve, 1000 * seconds));
}

export async function wait(seconds: number): Promise<void> {
    return sleep(seconds);
}
