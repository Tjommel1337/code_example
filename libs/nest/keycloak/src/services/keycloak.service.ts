import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as Keycloak from 'keycloak-connect';

@Injectable()
export class KeycloakService {
    private readonly clientsCache = new Map<string, Keycloak.Keycloak>();
    private readonly clientId: string;
    private readonly clientSecret: string;
    private readonly authorizationServerUrl: string;

    constructor(configService: ConfigService) {
        this.clientId = configService.get('KEYCLOAK_CLIENT_ID') ?? '';
        this.clientSecret = configService.get('KEYCLOAK_CLIENT_SECRET') ?? '';
        this.authorizationServerUrl = configService.get('KEYCLOAK_URL') ?? '';
    }

    async validateAccessToken(realm: string, token: string): Promise<Record<string, unknown> | undefined> {
        const tokenResult = await this.clientForRealm(realm).grantManager.validateAccessToken(token);

        if (typeof tokenResult === 'string') {
            const grant = await this.clientForRealm(realm).grantManager.createGrant({
                access_token: token as unknown as Keycloak.Token,
            });
            const accessToken: any = grant.access_token as any;

            return accessToken.content;
        }

        throw new Error('Invalid access token');
    }

    async hasScope(realm: string, token: string, scopes: string[]): Promise<Record<string, unknown> | undefined> {
        const grant = await this.clientForRealm(realm).grantManager.createGrant({
            access_token: token as unknown as Keycloak.Token,
        });

        try {
            const accessToken: any = grant.access_token as any;
            const hasScope: boolean = scopes.some((scope: string) => accessToken.content.scope.split(' ').includes(scope));

            if (hasScope) {
                return accessToken.content;
            }
        } catch (e) {
            throw new Error('Invalid access token');
        }

        throw new Error('Missing required role');
    }

    async hasRole(realm: string, token: string, roles: string[]): Promise<Record<string, unknown> | undefined> {
        const grant = await this.clientForRealm(realm).grantManager.createGrant({
            access_token: token as unknown as Keycloak.Token,
        });

        const accessToken: any = grant.access_token as any;
        const hasRole: boolean = roles.some((role: string) => accessToken.hasRole(role));

        if (hasRole) {
            return accessToken.content;
        }

        throw new Error('Missing required role');
    }

    private clientForRealm(realm: string): Keycloak.Keycloak {
        if (!this.clientsCache.has(realm)) {
            this.clientsCache.set(
                realm,
                new Keycloak.default({}, {
                    resource: this.clientId,
                    realm,
                    authServerUrl: this.authorizationServerUrl,
                    secret: this.clientSecret,
                } as any),
            );
        }

        const keycloak: Keycloak.Keycloak | undefined = this.clientsCache.get(realm);

        if (keycloak) {
            return keycloak;
        }

        throw new Error(`No client found for realm ${realm}`);
    }
}
