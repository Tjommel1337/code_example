import { Injectable, Logger } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-http-bearer';
import { HAS_ROLE, HAS_SCOPE, PROTECTED, UNPROTECTED } from '../constants';
import { KeycloakService } from '../services';

@Injectable()
export class KeycloakStrategy extends PassportStrategy(Strategy, 'keycloak') {
    constructor(private readonly keycloakService: KeycloakService) {
        super({ passReqToCallback: true });
    }

    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    async validate(req: any, token: string): Promise<boolean | Record<string, unknown>> {
        try {
            const type = req.protectionType as string;
            const realm = this.realmFromToken(token);

            switch (type) {
                case UNPROTECTED:
                    return {};
                case PROTECTED:
                    return await this.keycloakService.validateAccessToken(realm, token) || false;
                case HAS_SCOPE:
                    return await this.keycloakService.hasScope(realm, token, req.scopes) || false;
                case HAS_ROLE:
                    return await this.keycloakService.hasRole(realm, token, req.roles) || false;
                default:
                    Logger.warn('No protection type defined denying access', KeycloakStrategy.name);
                    return false;
            }
        } catch (error: any) {
            Logger.debug(`Invalid token message=${error.message}`, KeycloakStrategy.name);

            return false;
        }
    }

    private realmFromToken(token: string): string {
        try {
            const [, payload] = token.split('.');

            const data = JSON.parse(Buffer.from(payload, 'base64').toString());

            return data.iss.substring(data.iss.lastIndexOf('/') + 1);
        } catch (error) {
            throw new Error('Invalid token');
        }
    }
}
