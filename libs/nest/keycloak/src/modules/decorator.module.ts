import { Module } from '@nestjs/common';
import { KeycloakGuard } from '../guards';
import { KeycloakService } from '../services';
import { KeycloakStrategy } from '../strategies';

@Module({ providers: [KeycloakService, KeycloakStrategy, KeycloakGuard] })
export class DecoratorModule {}
