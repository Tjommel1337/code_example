import { applyDecorators, SetMetadata, UseGuards } from '@nestjs/common';
import { KeycloakGuard } from '../guards/keycloak.guard';
import { UNPROTECTED } from '../constants';

export const Unprotected = (): any => applyDecorators(SetMetadata(UNPROTECTED, true), UseGuards(KeycloakGuard));
