import { applyDecorators, SetMetadata, UseGuards } from '@nestjs/common';
import { KeycloakGuard } from '../guards/keycloak.guard';
import { HAS_SCOPE } from '../constants';

export const HasScope = (scopes: string | string[]): any => applyDecorators(SetMetadata(HAS_SCOPE, scopes), UseGuards(KeycloakGuard));
