import { applyDecorators, SetMetadata, UseGuards } from '@nestjs/common';
import { KeycloakGuard } from '../guards/keycloak.guard';
import { PROTECTED } from '../constants';

export const Protected = (): any => applyDecorators(SetMetadata(PROTECTED, true), UseGuards(KeycloakGuard));
