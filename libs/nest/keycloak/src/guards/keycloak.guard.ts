import { ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { HAS_ROLE, HAS_SCOPE, PROTECTED, UNPROTECTED } from '../constants';

@Injectable()
export class KeycloakGuard extends AuthGuard('keycloak') {
    constructor(private readonly reflector: Reflector) {
        super();
    }

    override getRequest<T>(context: ExecutionContext): T {
        const request = context.switchToHttp().getRequest();
        const handler = context.getHandler();

        const hasUnprotectedDecorator = this.reflector.get<boolean>(UNPROTECTED, handler);
        if (hasUnprotectedDecorator) {
            request.protectionType = UNPROTECTED;
            return request;
        }

        const hasProtectedDecorator = this.reflector.get<boolean>(PROTECTED, handler);
        if (hasProtectedDecorator) {
            request.protectionType = PROTECTED;
            return request;
        }

        const scopeDecorator = this.reflector.get<string | string[]>(HAS_SCOPE, handler);
        if (scopeDecorator) {
            request.protectionType = HAS_SCOPE;
            request.scopes = Array.isArray(scopeDecorator) ? scopeDecorator : [scopeDecorator];
            return request;
        }

        const roleDecorator = this.reflector.get<string | string[]>(HAS_ROLE, handler);
        if (roleDecorator) {
            request.protectionType = HAS_ROLE;
            request.roles = Array.isArray(roleDecorator) ? roleDecorator : [roleDecorator];
            return request;
        }

        return request;
    }
}
