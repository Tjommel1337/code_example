import { BaseInterface } from '../interfaces';
import { PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, DeleteDateColumn } from 'typeorm';

export abstract class BaseEntity implements BaseInterface {
    @PrimaryGeneratedColumn('uuid', { name: 'id' })
    public id?: string;

    @CreateDateColumn({ name: 'created_at', type: 'datetime', nullable: false })
    public created_at?: Date;

    @UpdateDateColumn({ name: 'updated_at', type: 'datetime', nullable: true })
    public updated_at?: Date;

    @DeleteDateColumn({ name: 'deleted_at', type: 'datetime', nullable: true })
    public deleted_at?: Date;
}
