import { ProjectRole, Gender, GenderType, Language, Theme } from '@myapp/enums';

export interface UserInterface {
    createdAt: Date;
    updatedAt: Date;
    auth_id: string;
    username: string;
    email?: string;
    avatar?: string;
    language?: Language;
    theme?: Theme;
    birthdate?: Date;
    gender?: Gender;
    gender_type?: GenderType;
    project_role?: ProjectRole;
    description?: string;
    [key: string]: any; // index signature to access fields dynamicly
}

export interface CreateUserRequestInterface {
    username: string;
    password: string;
    email: string;
}

export interface UpdateUserRequestInterface {
    language?: Language;
    theme?: Theme;
    birthdate?: Date;
    gender?: Gender;
    gender_type?: GenderType;
    project_role?: ProjectRole;
    description?: string;
}

export interface ChangePasswordRequestInterface {
    oldPassword: string;
    password: string;
    passwordConfirm: string;
}
