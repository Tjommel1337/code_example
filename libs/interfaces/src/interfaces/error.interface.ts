export interface ErrorResponseInterface {
    statusCode: number;
    message: string | string[];
    error: string;
}
