export interface UploadRequestInterface {
    type: string;
    session_id?: string;
}

export interface UploadResponseInterface {
    id: string;
}
