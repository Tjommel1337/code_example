import { SessionStatus } from '@myapp/enums';

export interface SessionInterface {
    createdAt: Date;
    updatedAt: Date;
    status: SessionStatus;
    start?: Date;
    duration?: number;
    emergency?: string;
    [key: string]: any; // index signature to access fields dynamicly
}

export interface CreateSessionRequestInterface {
    start: Date;
    duration: number;
    emergency: string;
}
