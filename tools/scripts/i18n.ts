import { mkdirSync, readdirSync, lstatSync, copyFileSync, existsSync, readFileSync, writeFileSync } from 'graceful-fs';
import { join } from 'path';

try {
    // copy i18n files
    const srcDir = "./libs/i18n/src/translations";
    const destDir = "./dist/libs/i18n";
    copyFolderSync(srcDir, destDir);

    // load default language (english)
    const defaultLanguage: Record<string, Record<string, string>> = JSON.parse(String(readFileSync(join(destDir, "en.json"))));

    // go through each non-default language and add missing translations
    const files: string[] = readdirSync(destDir).filter((filename: string): boolean => filename !== "en.json");
    files.forEach((filename: string): void => {
        // load file
        const language: Record<string, Record<string, string>> = JSON.parse(String(readFileSync(join(destDir, filename))));

        // get through file
        Object.keys(defaultLanguage).forEach((key: string): void => {
            if (!language[key]) {
                language[key] = defaultLanguage[key];
            } else {
                Object.keys(defaultLanguage[key]).forEach((subkey: string): void => {
                    if (!language[key][subkey]) {
                        language[key][subkey] = defaultLanguage[key][subkey];
                    }
                });
            }
        });

        // save file
        writeFileSync(join(destDir, filename), JSON.stringify(language, null, 2), { encoding: "utf-8" });
    });
} catch (err) {
    console.error(err);
}

function copyFolderSync(from: string, to: string) {
    if (!existsSync(to)) {
        mkdirSync(to, {
            recursive: true
        });
    }
    readdirSync(from).forEach(element => {
        if (lstatSync(join(from, element)).isFile()) {
            copyFileSync(join(from, element), join(to, element));
        } else {
            copyFolderSync(join(from, element), join(to, element));
        }
    });
}
