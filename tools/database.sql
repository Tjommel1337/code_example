-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server Version:               10.5.9-MariaDB - mariadb.org binary distribution
-- Server Betriebssystem:        Win64
-- HeidiSQL Version:             12.3.0.6589
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Exportiere Datenbank Struktur für keycloak
CREATE DATABASE IF NOT EXISTS `keycloak` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;
USE `keycloak`;

-- Exportiere Struktur von Tabelle keycloak.admin_event_entity
CREATE TABLE IF NOT EXISTS `admin_event_entity` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `ADMIN_EVENT_TIME` bigint(20) DEFAULT NULL,
  `REALM_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `OPERATION_TYPE` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `AUTH_REALM_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `AUTH_CLIENT_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `AUTH_USER_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `IP_ADDRESS` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `RESOURCE_PATH` text COLLATE utf8mb4_bin DEFAULT NULL,
  `REPRESENTATION` text COLLATE utf8mb4_bin DEFAULT NULL,
  `ERROR` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `RESOURCE_TYPE` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_ADMIN_EVENT_TIME` (`REALM_ID`,`ADMIN_EVENT_TIME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.admin_event_entity: ~0 rows (ungefähr)
DELETE FROM `admin_event_entity`;

-- Exportiere Struktur von Tabelle keycloak.associated_policy
CREATE TABLE IF NOT EXISTS `associated_policy` (
  `POLICY_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `ASSOCIATED_POLICY_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`POLICY_ID`,`ASSOCIATED_POLICY_ID`),
  KEY `IDX_ASSOC_POL_ASSOC_POL_ID` (`ASSOCIATED_POLICY_ID`),
  CONSTRAINT `FK_FRSR5S213XCX4WNKOG82SSRFY` FOREIGN KEY (`ASSOCIATED_POLICY_ID`) REFERENCES `resource_server_policy` (`ID`),
  CONSTRAINT `FK_FRSRPAS14XCX4WNKOG82SSRFY` FOREIGN KEY (`POLICY_ID`) REFERENCES `resource_server_policy` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.associated_policy: ~0 rows (ungefähr)
DELETE FROM `associated_policy`;

-- Exportiere Struktur von Tabelle keycloak.authentication_execution
CREATE TABLE IF NOT EXISTS `authentication_execution` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `ALIAS` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `AUTHENTICATOR` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `FLOW_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `REQUIREMENT` int(11) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `AUTHENTICATOR_FLOW` bit(1) NOT NULL DEFAULT b'0',
  `AUTH_FLOW_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `AUTH_CONFIG` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_AUTH_EXEC_REALM_FLOW` (`REALM_ID`,`FLOW_ID`),
  KEY `IDX_AUTH_EXEC_FLOW` (`FLOW_ID`),
  CONSTRAINT `FK_AUTH_EXEC_FLOW` FOREIGN KEY (`FLOW_ID`) REFERENCES `authentication_flow` (`ID`),
  CONSTRAINT `FK_AUTH_EXEC_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `realm` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.authentication_execution: ~94 rows (ungefähr)
DELETE FROM `authentication_execution`;
INSERT INTO `authentication_execution` (`ID`, `ALIAS`, `AUTHENTICATOR`, `REALM_ID`, `FLOW_ID`, `REQUIREMENT`, `PRIORITY`, `AUTHENTICATOR_FLOW`, `AUTH_FLOW_ID`, `AUTH_CONFIG`) VALUES
	('014cb9a6-cebc-4d02-8545-24acdcfc1f5b', NULL, 'basic-auth', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'a4b992c7-8330-4f18-91fe-db2af7f4d57b', 0, 10, b'0', NULL, NULL),
	('01924a8d-ea76-4129-9eaf-c29f5521f62b', NULL, 'registration-profile-action', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '562daa2e-c8ac-4e0e-a96a-224644c6d218', 0, 40, b'0', NULL, NULL),
	('06f78bc6-ada1-4cad-8905-53dae07dd35b', NULL, 'client-jwt', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '86ea6c61-8a23-4508-bfd8-aee426bb5202', 2, 20, b'0', NULL, NULL),
	('09198d8f-f524-4531-97d8-8e3ce9e78c14', NULL, 'reset-otp', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '642e6a18-c8da-48e2-b765-1cc34bb369e0', 0, 20, b'0', NULL, NULL),
	('12ec9616-efae-491b-a308-c0544e415470', NULL, 'registration-profile-action', '47958335-5c70-4603-abe7-b887490d7b1c', '12eca568-0ec3-4859-a6d6-219ba89e89a7', 0, 40, b'0', NULL, NULL),
	('1374fb9d-bc1a-4e11-8d9f-aa84c67c57ca', NULL, 'conditional-user-configured', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '1bf0031d-81f3-4fe3-a857-755472a2615d', 0, 10, b'0', NULL, NULL),
	('16341107-132b-4778-acec-df4c7fd1458b', NULL, 'reset-otp', '47958335-5c70-4603-abe7-b887490d7b1c', '150628ff-9e5e-4e0d-98e2-2f02db1217cf', 0, 20, b'0', NULL, NULL),
	('16ca3147-abf8-41a8-885a-b9550a22c875', NULL, 'reset-password', '47958335-5c70-4603-abe7-b887490d7b1c', '5bd4b3d8-c015-4fd6-9252-0ce4f1695222', 0, 30, b'0', NULL, NULL),
	('1994d3c2-5b9e-448c-8ef7-3f1d3aa77bc4', NULL, 'registration-page-form', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '59d2dcfc-f1cc-4b71-90d0-e4708adfc1b8', 0, 10, b'1', '562daa2e-c8ac-4e0e-a96a-224644c6d218', NULL),
	('1c1378b4-6bce-4d44-b2fd-af3c4c723396', NULL, 'auth-spnego', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '47b8216a-ea64-46a0-b859-e3fac25a40b4', 3, 20, b'0', NULL, NULL),
	('1fe9f607-c9da-4d64-a178-218afc496357', NULL, 'no-cookie-redirect', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '8f844ccf-d860-4144-97bb-4d365e399d5d', 0, 10, b'0', NULL, NULL),
	('208e2c5f-38b2-47e5-8c95-7aa519ce74c1', NULL, 'registration-user-creation', '47958335-5c70-4603-abe7-b887490d7b1c', '12eca568-0ec3-4859-a6d6-219ba89e89a7', 0, 20, b'0', NULL, NULL),
	('21e64458-90ab-43d3-94ee-2700fe4ea814', NULL, 'client-secret', '47958335-5c70-4603-abe7-b887490d7b1c', '75beb864-f5e1-468a-a008-ebbd4999acdc', 2, 10, b'0', NULL, NULL),
	('238e011a-7980-47b2-98e8-48745a7792a0', NULL, NULL, '47958335-5c70-4603-abe7-b887490d7b1c', '54afa048-7406-48b9-9a1a-67654de9344e', 0, 20, b'1', 'f62f45b8-79e1-4da1-b5ec-5126143d7a78', NULL),
	('2702aeae-d6b4-4696-aa9c-eec3037cbf58', NULL, 'auth-otp-form', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'de91cf71-98fb-4089-8308-50ca57de0d93', 0, 20, b'0', NULL, NULL),
	('2d717124-d89c-4c93-b629-0d81e797f3f1', NULL, 'direct-grant-validate-otp', '47958335-5c70-4603-abe7-b887490d7b1c', '03f6e58e-b1af-4195-b7f3-f2e54eac4d13', 0, 20, b'0', NULL, NULL),
	('3155f7d0-ed04-4a3f-a876-ab47bef4f843', NULL, 'idp-email-verification', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'f948e961-824b-49b4-9540-da6321278e35', 2, 10, b'0', NULL, NULL),
	('35588740-be49-4890-aaec-602a469fd7bc', NULL, 'http-basic-authenticator', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '0f96bfa4-0a91-49cc-ab21-b01c37368905', 0, 10, b'0', NULL, NULL),
	('3850ee26-7dd0-4d99-8370-df67a1c6f6d6', NULL, 'reset-credentials-choose-user', '47958335-5c70-4603-abe7-b887490d7b1c', '5bd4b3d8-c015-4fd6-9252-0ce4f1695222', 0, 10, b'0', NULL, NULL),
	('39d9931b-0fd9-45ae-a039-5b811ef98e9a', NULL, 'direct-grant-validate-password', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '3d666503-5c58-4640-b2ac-1afc4e2492b6', 0, 20, b'0', NULL, NULL),
	('3a8a41e4-8191-43de-a7ab-8c14bd3d14e8', NULL, 'auth-username-password-form', '47958335-5c70-4603-abe7-b887490d7b1c', 'a86c66c1-3d59-4c0c-8b76-be9c6c99dfda', 0, 10, b'0', NULL, NULL),
	('3b87ee68-0ae1-4dc1-b662-931a9a3f6973', NULL, NULL, '23d8cb0f-53b6-4402-a9f9-072c402d5927', '8f844ccf-d860-4144-97bb-4d365e399d5d', 0, 20, b'1', 'a4b992c7-8330-4f18-91fe-db2af7f4d57b', NULL),
	('3bd49330-d3c0-4a3b-b7e1-01de6cc69cc2', NULL, 'client-x509', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '86ea6c61-8a23-4508-bfd8-aee426bb5202', 2, 40, b'0', NULL, NULL),
	('3eb2b2e1-405b-451a-b52d-270464d4199f', NULL, NULL, '47958335-5c70-4603-abe7-b887490d7b1c', '587b86c0-7d27-4915-8a5e-2e1b6fe0ae66', 2, 20, b'1', '9c1d6183-dc1c-4f66-88e8-ef9317e96f87', NULL),
	('3edee62b-72be-493a-a3e2-cfe88c518c10', NULL, 'auth-otp-form', '47958335-5c70-4603-abe7-b887490d7b1c', '0618c0b7-2caa-423f-9813-d03d73f59283', 0, 20, b'0', NULL, NULL),
	('3fadea17-a3f2-44ec-a851-17ea3ede6083', NULL, 'idp-review-profile', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '3b280654-ef09-4231-b744-4ae13faec8d8', 0, 10, b'0', NULL, 'd346b2ce-cf0f-40d6-86da-a54708acd13b'),
	('418526aa-f7bf-4365-b649-f2bd763d3733', NULL, 'conditional-user-configured', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'c5655568-ddf1-4895-8da8-5a3c70d29b96', 0, 10, b'0', NULL, NULL),
	('418b0c9e-354c-481f-b469-56498a67aa82', NULL, 'registration-user-creation', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '562daa2e-c8ac-4e0e-a96a-224644c6d218', 0, 20, b'0', NULL, NULL),
	('42c1d397-e7d0-4fb1-8750-05e5ed1be073', NULL, NULL, '47958335-5c70-4603-abe7-b887490d7b1c', 'a86c66c1-3d59-4c0c-8b76-be9c6c99dfda', 1, 20, b'1', '5f1bc262-5fbc-4c50-b406-feb027ad5935', NULL),
	('448f2eab-9c05-4d3e-919c-b85c2949ce60', NULL, 'auth-spnego', '47958335-5c70-4603-abe7-b887490d7b1c', '6c9cf822-831b-48f4-a8f3-5c0db558db8e', 3, 20, b'0', NULL, NULL),
	('48fe2258-14f0-4584-9d07-6bc6b5e680f6', NULL, 'identity-provider-redirector', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '47b8216a-ea64-46a0-b859-e3fac25a40b4', 2, 25, b'0', NULL, NULL),
	('4a53ebb9-b79c-49e5-acb7-a4a11e08dee6', NULL, 'docker-http-basic-authenticator', '47958335-5c70-4603-abe7-b887490d7b1c', '4cd27323-0e9c-4156-b355-46e3dbe96609', 0, 10, b'0', NULL, NULL),
	('4bf400e0-cf8e-43f0-bb13-eb4dc3bc8e4e', NULL, 'direct-grant-validate-username', '47958335-5c70-4603-abe7-b887490d7b1c', 'e647ed12-8f0e-4095-ab6c-53ca78508944', 0, 10, b'0', NULL, NULL),
	('4c28ef73-a591-445c-8a9f-8da02d54b539', NULL, 'direct-grant-validate-otp', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '1bf0031d-81f3-4fe3-a857-755472a2615d', 0, 20, b'0', NULL, NULL),
	('4d2dde8a-0c1f-456d-b590-b2e9da62ba1b', NULL, 'http-basic-authenticator', '47958335-5c70-4603-abe7-b887490d7b1c', '1fde688b-9db7-4dfb-99d7-a5fdc79ff026', 0, 10, b'0', NULL, NULL),
	('4ed87228-3fe9-42e0-98a7-6273f8bfe355', NULL, 'auth-cookie', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '47b8216a-ea64-46a0-b859-e3fac25a40b4', 2, 10, b'0', NULL, NULL),
	('50c78b34-a1d9-4804-8d50-e9a617686dc0', NULL, 'client-secret-jwt', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '86ea6c61-8a23-4508-bfd8-aee426bb5202', 2, 30, b'0', NULL, NULL),
	('5562aac6-8fc1-43cc-a9e7-4cd79227d5c5', NULL, 'reset-password', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '3876b1cf-8d8b-4e46-b7e6-b155694e07d4', 0, 30, b'0', NULL, NULL),
	('59546471-59bd-4e5a-94f4-808299911f42', NULL, 'auth-username-password-form', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '5654ad5a-2736-4e07-a0ba-aa70af819693', 0, 10, b'0', NULL, NULL),
	('5b14d51a-8ed8-4013-8189-f4bbfe16c47e', NULL, 'direct-grant-validate-password', '47958335-5c70-4603-abe7-b887490d7b1c', 'e647ed12-8f0e-4095-ab6c-53ca78508944', 0, 20, b'0', NULL, NULL),
	('5ca1e235-1db8-4cbb-9ce1-057f12f804ae', NULL, 'client-secret', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '86ea6c61-8a23-4508-bfd8-aee426bb5202', 2, 10, b'0', NULL, NULL),
	('61ba14bb-663b-41a9-bd41-6a573d7aa127', NULL, NULL, '47958335-5c70-4603-abe7-b887490d7b1c', 'e612bbd7-1b36-40da-ac53-b6dbf6b298fc', 2, 20, b'1', '4e1a727e-fa24-480e-b5a7-c72fcc96f0ec', NULL),
	('6357bbd2-90d8-420d-8d41-2012c1e5ee06', NULL, 'auth-spnego', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'a4b992c7-8330-4f18-91fe-db2af7f4d57b', 3, 30, b'0', NULL, NULL),
	('671991dc-eb2e-4593-bed9-fc9d388d21c6', NULL, 'idp-email-verification', '47958335-5c70-4603-abe7-b887490d7b1c', '587b86c0-7d27-4915-8a5e-2e1b6fe0ae66', 2, 10, b'0', NULL, NULL),
	('69f2c618-b7c0-48f7-bb8c-d90583797665', NULL, 'docker-http-basic-authenticator', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '321b21ab-42cc-4507-84f9-5e3e5de52bb1', 0, 10, b'0', NULL, NULL),
	('72a316e5-1c7d-47c4-8bec-15c135eecf1c', NULL, 'idp-create-user-if-unique', '47958335-5c70-4603-abe7-b887490d7b1c', 'e612bbd7-1b36-40da-ac53-b6dbf6b298fc', 2, 10, b'0', NULL, '4049ac7f-bc4f-43fe-9108-c8d4867d90e8'),
	('746c7a5e-3d37-4980-947a-8c894347b74e', NULL, 'client-secret-jwt', '47958335-5c70-4603-abe7-b887490d7b1c', '75beb864-f5e1-468a-a008-ebbd4999acdc', 2, 30, b'0', NULL, NULL),
	('779113dc-3818-42f7-b5ec-171b122e90c3', NULL, 'idp-username-password-form', '47958335-5c70-4603-abe7-b887490d7b1c', '9c1d6183-dc1c-4f66-88e8-ef9317e96f87', 0, 10, b'0', NULL, NULL),
	('7a48370b-29c3-440c-b8ed-c6da4a515f18', NULL, 'conditional-user-configured', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'de91cf71-98fb-4089-8308-50ca57de0d93', 0, 10, b'0', NULL, NULL),
	('7de6fb2c-3c2e-4860-baa2-e75883d68ad1', NULL, 'registration-password-action', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '562daa2e-c8ac-4e0e-a96a-224644c6d218', 0, 50, b'0', NULL, NULL),
	('849e5639-23e6-4493-95a6-0c54c79e758f', NULL, NULL, '23d8cb0f-53b6-4402-a9f9-072c402d5927', '442963de-94d9-46b7-8dde-7094d010989d', 2, 20, b'1', 'fec37fa5-44aa-4ad6-b1b8-7771bb081997', NULL),
	('86050bf8-eb7f-4914-8f4b-509ac243e2d8', NULL, 'auth-spnego', '47958335-5c70-4603-abe7-b887490d7b1c', 'f62f45b8-79e1-4da1-b5ec-5126143d7a78', 3, 30, b'0', NULL, NULL),
	('86a53cfa-7830-44d7-b6aa-0b75d0742cf0', NULL, NULL, '47958335-5c70-4603-abe7-b887490d7b1c', '6c9cf822-831b-48f4-a8f3-5c0db558db8e', 2, 30, b'1', 'a86c66c1-3d59-4c0c-8b76-be9c6c99dfda', NULL),
	('8b3f4af4-267d-4d3b-b03a-6beec6101adc', NULL, 'conditional-user-configured', '47958335-5c70-4603-abe7-b887490d7b1c', '150628ff-9e5e-4e0d-98e2-2f02db1217cf', 0, 10, b'0', NULL, NULL),
	('8b45f9d4-d093-409b-a3cb-5f240a3462c8', NULL, NULL, '23d8cb0f-53b6-4402-a9f9-072c402d5927', '3d666503-5c58-4640-b2ac-1afc4e2492b6', 1, 30, b'1', '1bf0031d-81f3-4fe3-a857-755472a2615d', NULL),
	('8c509b0d-ec18-4850-82c3-1a436571fc99', NULL, 'registration-recaptcha-action', '47958335-5c70-4603-abe7-b887490d7b1c', '12eca568-0ec3-4859-a6d6-219ba89e89a7', 3, 60, b'0', NULL, NULL),
	('8dbc343a-7714-446c-a34b-4a198481f7bd', NULL, NULL, '47958335-5c70-4603-abe7-b887490d7b1c', '9c1d6183-dc1c-4f66-88e8-ef9317e96f87', 1, 20, b'1', '0618c0b7-2caa-423f-9813-d03d73f59283', NULL),
	('8de4c328-ad35-4342-8f58-fdc3330e4ab3', NULL, NULL, '47958335-5c70-4603-abe7-b887490d7b1c', '5bd4b3d8-c015-4fd6-9252-0ce4f1695222', 1, 40, b'1', '150628ff-9e5e-4e0d-98e2-2f02db1217cf', NULL),
	('96d6cf56-e3a7-436a-b9d7-9d0e08e9372b', NULL, 'basic-auth-otp', '47958335-5c70-4603-abe7-b887490d7b1c', 'f62f45b8-79e1-4da1-b5ec-5126143d7a78', 3, 20, b'0', NULL, NULL),
	('9caf9023-2a73-4284-9675-fcb837543d18', NULL, 'basic-auth-otp', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'a4b992c7-8330-4f18-91fe-db2af7f4d57b', 3, 20, b'0', NULL, NULL),
	('9e44a867-f52f-4877-a050-c03f905739d7', NULL, 'reset-credential-email', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '3876b1cf-8d8b-4e46-b7e6-b155694e07d4', 0, 20, b'0', NULL, NULL),
	('a67ff785-06c0-4187-9c68-483b23d01fbe', NULL, 'idp-username-password-form', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '64a9c475-61e5-44b6-abed-555992725d96', 0, 10, b'0', NULL, NULL),
	('a77c3ef5-8ce8-45bb-9dc6-fa6f404a818a', NULL, NULL, '23d8cb0f-53b6-4402-a9f9-072c402d5927', '3b280654-ef09-4231-b744-4ae13faec8d8', 0, 20, b'1', '442963de-94d9-46b7-8dde-7094d010989d', NULL),
	('a8b9a39e-3ba0-4bac-9ae2-f9e93ca419d1', NULL, 'direct-grant-validate-username', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '3d666503-5c58-4640-b2ac-1afc4e2492b6', 0, 10, b'0', NULL, NULL),
	('a9498f12-ac99-4abd-b2da-b5ba38b94577', NULL, 'basic-auth', '47958335-5c70-4603-abe7-b887490d7b1c', 'f62f45b8-79e1-4da1-b5ec-5126143d7a78', 0, 10, b'0', NULL, NULL),
	('a990f27e-0807-47c0-8a2f-8d86ebb6c285', NULL, NULL, '23d8cb0f-53b6-4402-a9f9-072c402d5927', '5654ad5a-2736-4e07-a0ba-aa70af819693', 1, 20, b'1', 'c5655568-ddf1-4895-8da8-5a3c70d29b96', NULL),
	('aae6af20-0155-4271-8f5f-4f2d60e7af35', NULL, 'registration-recaptcha-action', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '562daa2e-c8ac-4e0e-a96a-224644c6d218', 3, 60, b'0', NULL, NULL),
	('b90921df-84cf-46ff-afe3-00f81c258dde', NULL, 'idp-create-user-if-unique', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '442963de-94d9-46b7-8dde-7094d010989d', 2, 10, b'0', NULL, '1e783779-b286-429c-8f4d-2067be88c35e'),
	('b94cc2ba-255e-40d5-869e-226f718866b4', NULL, 'idp-review-profile', '47958335-5c70-4603-abe7-b887490d7b1c', 'ddc4a5d7-fb2e-4b04-9ab5-3c2a044bb7c6', 0, 10, b'0', NULL, 'aa5365a0-7ea4-493b-bb77-801e0dd85c89'),
	('ba112e28-b520-4a72-8187-0881c1a96521', NULL, 'registration-password-action', '47958335-5c70-4603-abe7-b887490d7b1c', '12eca568-0ec3-4859-a6d6-219ba89e89a7', 0, 50, b'0', NULL, NULL),
	('ba4d153d-d27c-4ed8-884e-2dda10b0066a', NULL, 'idp-confirm-link', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'fec37fa5-44aa-4ad6-b1b8-7771bb081997', 0, 10, b'0', NULL, NULL),
	('ba78c686-eb4d-474f-91bc-32891d884b8b', NULL, 'auth-otp-form', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'c5655568-ddf1-4895-8da8-5a3c70d29b96', 0, 20, b'0', NULL, NULL),
	('bbfd23a5-f665-4351-9528-00d273ad90e4', NULL, 'identity-provider-redirector', '47958335-5c70-4603-abe7-b887490d7b1c', '6c9cf822-831b-48f4-a8f3-5c0db558db8e', 2, 25, b'0', NULL, NULL),
	('bc02a71b-df8f-4d56-9f73-882f26fd4619', NULL, 'conditional-user-configured', '47958335-5c70-4603-abe7-b887490d7b1c', '03f6e58e-b1af-4195-b7f3-f2e54eac4d13', 0, 10, b'0', NULL, NULL),
	('bf501753-23b9-4bcc-a6fc-26ebe4d663d0', NULL, 'client-jwt', '47958335-5c70-4603-abe7-b887490d7b1c', '75beb864-f5e1-468a-a008-ebbd4999acdc', 2, 20, b'0', NULL, NULL),
	('c534f587-ff5b-4017-882e-acd0b6997198', NULL, 'auth-otp-form', '47958335-5c70-4603-abe7-b887490d7b1c', '5f1bc262-5fbc-4c50-b406-feb027ad5935', 0, 20, b'0', NULL, NULL),
	('c6ce2903-c449-4399-ae03-ca5970692247', NULL, 'auth-cookie', '47958335-5c70-4603-abe7-b887490d7b1c', '6c9cf822-831b-48f4-a8f3-5c0db558db8e', 2, 10, b'0', NULL, NULL),
	('cbd11c4e-f5f9-420d-a028-54daca04625b', NULL, NULL, '23d8cb0f-53b6-4402-a9f9-072c402d5927', '64a9c475-61e5-44b6-abed-555992725d96', 1, 20, b'1', 'de91cf71-98fb-4089-8308-50ca57de0d93', NULL),
	('cc8a06ac-4241-46ff-bd5d-5b449af37b93', NULL, NULL, '23d8cb0f-53b6-4402-a9f9-072c402d5927', '3876b1cf-8d8b-4e46-b7e6-b155694e07d4', 1, 40, b'1', '642e6a18-c8da-48e2-b765-1cc34bb369e0', NULL),
	('ce5fbb4c-ff4e-4062-a54c-86097c029451', NULL, NULL, '47958335-5c70-4603-abe7-b887490d7b1c', 'ddc4a5d7-fb2e-4b04-9ab5-3c2a044bb7c6', 0, 20, b'1', 'e612bbd7-1b36-40da-ac53-b6dbf6b298fc', NULL),
	('d24291c5-866f-438a-bfbb-06eca410d1dc', NULL, NULL, '47958335-5c70-4603-abe7-b887490d7b1c', 'e647ed12-8f0e-4095-ab6c-53ca78508944', 1, 30, b'1', '03f6e58e-b1af-4195-b7f3-f2e54eac4d13', NULL),
	('d2930089-9eba-4010-9b4c-c83efe25c9d2', NULL, 'registration-page-form', '47958335-5c70-4603-abe7-b887490d7b1c', '51e69285-0fde-4d24-8c52-7f5394e04f24', 0, 10, b'1', '12eca568-0ec3-4859-a6d6-219ba89e89a7', NULL),
	('d2b6b1c0-60e1-413a-aae7-f026ccdd0d2c', NULL, NULL, '47958335-5c70-4603-abe7-b887490d7b1c', '4e1a727e-fa24-480e-b5a7-c72fcc96f0ec', 0, 20, b'1', '587b86c0-7d27-4915-8a5e-2e1b6fe0ae66', NULL),
	('d3e51a21-32a7-472c-95c8-a69e935ebf0f', NULL, 'idp-confirm-link', '47958335-5c70-4603-abe7-b887490d7b1c', '4e1a727e-fa24-480e-b5a7-c72fcc96f0ec', 0, 10, b'0', NULL, NULL),
	('daf7a630-aeda-4523-818a-9d0f7fc2d738', NULL, 'client-x509', '47958335-5c70-4603-abe7-b887490d7b1c', '75beb864-f5e1-468a-a008-ebbd4999acdc', 2, 40, b'0', NULL, NULL),
	('dd0b8bb7-1851-42a0-8b0a-804cc27a40a1', NULL, NULL, '23d8cb0f-53b6-4402-a9f9-072c402d5927', '47b8216a-ea64-46a0-b859-e3fac25a40b4', 2, 30, b'1', '5654ad5a-2736-4e07-a0ba-aa70af819693', NULL),
	('dd29db96-360e-418e-8b0f-b552e56fcb73', NULL, 'no-cookie-redirect', '47958335-5c70-4603-abe7-b887490d7b1c', '54afa048-7406-48b9-9a1a-67654de9344e', 0, 10, b'0', NULL, NULL),
	('e07bbc90-eaf8-4dd2-b882-0103a6d79354', NULL, NULL, '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'fec37fa5-44aa-4ad6-b1b8-7771bb081997', 0, 20, b'1', 'f948e961-824b-49b4-9540-da6321278e35', NULL),
	('e2ad52bc-4d4e-4824-a21e-964ffa79ef60', NULL, 'conditional-user-configured', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '642e6a18-c8da-48e2-b765-1cc34bb369e0', 0, 10, b'0', NULL, NULL),
	('e7434be1-9eca-45e2-b465-38b2dd8a575a', NULL, 'reset-credentials-choose-user', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '3876b1cf-8d8b-4e46-b7e6-b155694e07d4', 0, 10, b'0', NULL, NULL),
	('ee84c7ce-0c2f-4ec8-a569-47a64d30a342', NULL, NULL, '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'f948e961-824b-49b4-9540-da6321278e35', 2, 20, b'1', '64a9c475-61e5-44b6-abed-555992725d96', NULL),
	('f877cc89-eb70-4887-804e-c8779631dc5d', NULL, 'conditional-user-configured', '47958335-5c70-4603-abe7-b887490d7b1c', '5f1bc262-5fbc-4c50-b406-feb027ad5935', 0, 10, b'0', NULL, NULL),
	('f9b85681-6d69-4b4b-a758-df6db3079769', NULL, 'conditional-user-configured', '47958335-5c70-4603-abe7-b887490d7b1c', '0618c0b7-2caa-423f-9813-d03d73f59283', 0, 10, b'0', NULL, NULL),
	('ffdaa577-dda6-4034-af0b-e1805ea00ce9', NULL, 'reset-credential-email', '47958335-5c70-4603-abe7-b887490d7b1c', '5bd4b3d8-c015-4fd6-9252-0ce4f1695222', 0, 20, b'0', NULL, NULL);

-- Exportiere Struktur von Tabelle keycloak.authentication_flow
CREATE TABLE IF NOT EXISTS `authentication_flow` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `ALIAS` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `PROVIDER_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL DEFAULT 'basic-flow',
  `TOP_LEVEL` bit(1) NOT NULL DEFAULT b'0',
  `BUILT_IN` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`ID`),
  KEY `IDX_AUTH_FLOW_REALM` (`REALM_ID`),
  CONSTRAINT `FK_AUTH_FLOW_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `realm` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.authentication_flow: ~40 rows (ungefähr)
DELETE FROM `authentication_flow`;
INSERT INTO `authentication_flow` (`ID`, `ALIAS`, `DESCRIPTION`, `REALM_ID`, `PROVIDER_ID`, `TOP_LEVEL`, `BUILT_IN`) VALUES
	('03f6e58e-b1af-4195-b7f3-f2e54eac4d13', 'Direct Grant - Conditional OTP', 'Flow to determine if the OTP is required for the authentication', '47958335-5c70-4603-abe7-b887490d7b1c', 'basic-flow', b'0', b'1'),
	('0618c0b7-2caa-423f-9813-d03d73f59283', 'First broker login - Conditional OTP', 'Flow to determine if the OTP is required for the authentication', '47958335-5c70-4603-abe7-b887490d7b1c', 'basic-flow', b'0', b'1'),
	('0f96bfa4-0a91-49cc-ab21-b01c37368905', 'saml ecp', 'SAML ECP Profile Authentication Flow', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'basic-flow', b'1', b'1'),
	('12eca568-0ec3-4859-a6d6-219ba89e89a7', 'registration form', 'registration form', '47958335-5c70-4603-abe7-b887490d7b1c', 'form-flow', b'0', b'1'),
	('150628ff-9e5e-4e0d-98e2-2f02db1217cf', 'Reset - Conditional OTP', 'Flow to determine if the OTP should be reset or not. Set to REQUIRED to force.', '47958335-5c70-4603-abe7-b887490d7b1c', 'basic-flow', b'0', b'1'),
	('1bf0031d-81f3-4fe3-a857-755472a2615d', 'Direct Grant - Conditional OTP', 'Flow to determine if the OTP is required for the authentication', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'basic-flow', b'0', b'1'),
	('1fde688b-9db7-4dfb-99d7-a5fdc79ff026', 'saml ecp', 'SAML ECP Profile Authentication Flow', '47958335-5c70-4603-abe7-b887490d7b1c', 'basic-flow', b'1', b'1'),
	('321b21ab-42cc-4507-84f9-5e3e5de52bb1', 'docker auth', 'Used by Docker clients to authenticate against the IDP', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'basic-flow', b'1', b'1'),
	('3876b1cf-8d8b-4e46-b7e6-b155694e07d4', 'reset credentials', 'Reset credentials for a user if they forgot their password or something', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'basic-flow', b'1', b'1'),
	('3b280654-ef09-4231-b744-4ae13faec8d8', 'first broker login', 'Actions taken after first broker login with identity provider account, which is not yet linked to any Keycloak account', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'basic-flow', b'1', b'1'),
	('3d666503-5c58-4640-b2ac-1afc4e2492b6', 'direct grant', 'OpenID Connect Resource Owner Grant', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'basic-flow', b'1', b'1'),
	('442963de-94d9-46b7-8dde-7094d010989d', 'User creation or linking', 'Flow for the existing/non-existing user alternatives', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'basic-flow', b'0', b'1'),
	('47b8216a-ea64-46a0-b859-e3fac25a40b4', 'browser', 'browser based authentication', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'basic-flow', b'1', b'1'),
	('4cd27323-0e9c-4156-b355-46e3dbe96609', 'docker auth', 'Used by Docker clients to authenticate against the IDP', '47958335-5c70-4603-abe7-b887490d7b1c', 'basic-flow', b'1', b'1'),
	('4e1a727e-fa24-480e-b5a7-c72fcc96f0ec', 'Handle Existing Account', 'Handle what to do if there is existing account with same email/username like authenticated identity provider', '47958335-5c70-4603-abe7-b887490d7b1c', 'basic-flow', b'0', b'1'),
	('51e69285-0fde-4d24-8c52-7f5394e04f24', 'registration', 'registration flow', '47958335-5c70-4603-abe7-b887490d7b1c', 'basic-flow', b'1', b'1'),
	('54afa048-7406-48b9-9a1a-67654de9344e', 'http challenge', 'An authentication flow based on challenge-response HTTP Authentication Schemes', '47958335-5c70-4603-abe7-b887490d7b1c', 'basic-flow', b'1', b'1'),
	('562daa2e-c8ac-4e0e-a96a-224644c6d218', 'registration form', 'registration form', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'form-flow', b'0', b'1'),
	('5654ad5a-2736-4e07-a0ba-aa70af819693', 'forms', 'Username, password, otp and other auth forms.', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'basic-flow', b'0', b'1'),
	('587b86c0-7d27-4915-8a5e-2e1b6fe0ae66', 'Account verification options', 'Method with which to verity the existing account', '47958335-5c70-4603-abe7-b887490d7b1c', 'basic-flow', b'0', b'1'),
	('59d2dcfc-f1cc-4b71-90d0-e4708adfc1b8', 'registration', 'registration flow', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'basic-flow', b'1', b'1'),
	('5bd4b3d8-c015-4fd6-9252-0ce4f1695222', 'reset credentials', 'Reset credentials for a user if they forgot their password or something', '47958335-5c70-4603-abe7-b887490d7b1c', 'basic-flow', b'1', b'1'),
	('5f1bc262-5fbc-4c50-b406-feb027ad5935', 'Browser - Conditional OTP', 'Flow to determine if the OTP is required for the authentication', '47958335-5c70-4603-abe7-b887490d7b1c', 'basic-flow', b'0', b'1'),
	('642e6a18-c8da-48e2-b765-1cc34bb369e0', 'Reset - Conditional OTP', 'Flow to determine if the OTP should be reset or not. Set to REQUIRED to force.', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'basic-flow', b'0', b'1'),
	('64a9c475-61e5-44b6-abed-555992725d96', 'Verify Existing Account by Re-authentication', 'Reauthentication of existing account', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'basic-flow', b'0', b'1'),
	('6c9cf822-831b-48f4-a8f3-5c0db558db8e', 'browser', 'browser based authentication', '47958335-5c70-4603-abe7-b887490d7b1c', 'basic-flow', b'1', b'1'),
	('75beb864-f5e1-468a-a008-ebbd4999acdc', 'clients', 'Base authentication for clients', '47958335-5c70-4603-abe7-b887490d7b1c', 'client-flow', b'1', b'1'),
	('86ea6c61-8a23-4508-bfd8-aee426bb5202', 'clients', 'Base authentication for clients', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'client-flow', b'1', b'1'),
	('8f844ccf-d860-4144-97bb-4d365e399d5d', 'http challenge', 'An authentication flow based on challenge-response HTTP Authentication Schemes', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'basic-flow', b'1', b'1'),
	('9c1d6183-dc1c-4f66-88e8-ef9317e96f87', 'Verify Existing Account by Re-authentication', 'Reauthentication of existing account', '47958335-5c70-4603-abe7-b887490d7b1c', 'basic-flow', b'0', b'1'),
	('a4b992c7-8330-4f18-91fe-db2af7f4d57b', 'Authentication Options', 'Authentication options.', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'basic-flow', b'0', b'1'),
	('a86c66c1-3d59-4c0c-8b76-be9c6c99dfda', 'forms', 'Username, password, otp and other auth forms.', '47958335-5c70-4603-abe7-b887490d7b1c', 'basic-flow', b'0', b'1'),
	('c5655568-ddf1-4895-8da8-5a3c70d29b96', 'Browser - Conditional OTP', 'Flow to determine if the OTP is required for the authentication', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'basic-flow', b'0', b'1'),
	('ddc4a5d7-fb2e-4b04-9ab5-3c2a044bb7c6', 'first broker login', 'Actions taken after first broker login with identity provider account, which is not yet linked to any Keycloak account', '47958335-5c70-4603-abe7-b887490d7b1c', 'basic-flow', b'1', b'1'),
	('de91cf71-98fb-4089-8308-50ca57de0d93', 'First broker login - Conditional OTP', 'Flow to determine if the OTP is required for the authentication', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'basic-flow', b'0', b'1'),
	('e612bbd7-1b36-40da-ac53-b6dbf6b298fc', 'User creation or linking', 'Flow for the existing/non-existing user alternatives', '47958335-5c70-4603-abe7-b887490d7b1c', 'basic-flow', b'0', b'1'),
	('e647ed12-8f0e-4095-ab6c-53ca78508944', 'direct grant', 'OpenID Connect Resource Owner Grant', '47958335-5c70-4603-abe7-b887490d7b1c', 'basic-flow', b'1', b'1'),
	('f62f45b8-79e1-4da1-b5ec-5126143d7a78', 'Authentication Options', 'Authentication options.', '47958335-5c70-4603-abe7-b887490d7b1c', 'basic-flow', b'0', b'1'),
	('f948e961-824b-49b4-9540-da6321278e35', 'Account verification options', 'Method with which to verity the existing account', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'basic-flow', b'0', b'1'),
	('fec37fa5-44aa-4ad6-b1b8-7771bb081997', 'Handle Existing Account', 'Handle what to do if there is existing account with same email/username like authenticated identity provider', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'basic-flow', b'0', b'1');

-- Exportiere Struktur von Tabelle keycloak.authenticator_config
CREATE TABLE IF NOT EXISTS `authenticator_config` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `ALIAS` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_AUTH_CONFIG_REALM` (`REALM_ID`),
  CONSTRAINT `FK_AUTH_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `realm` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.authenticator_config: ~4 rows (ungefähr)
DELETE FROM `authenticator_config`;
INSERT INTO `authenticator_config` (`ID`, `ALIAS`, `REALM_ID`) VALUES
	('1e783779-b286-429c-8f4d-2067be88c35e', 'create unique user config', '23d8cb0f-53b6-4402-a9f9-072c402d5927'),
	('4049ac7f-bc4f-43fe-9108-c8d4867d90e8', 'create unique user config', '47958335-5c70-4603-abe7-b887490d7b1c'),
	('aa5365a0-7ea4-493b-bb77-801e0dd85c89', 'review profile config', '47958335-5c70-4603-abe7-b887490d7b1c'),
	('d346b2ce-cf0f-40d6-86da-a54708acd13b', 'review profile config', '23d8cb0f-53b6-4402-a9f9-072c402d5927');

-- Exportiere Struktur von Tabelle keycloak.authenticator_config_entry
CREATE TABLE IF NOT EXISTS `authenticator_config_entry` (
  `AUTHENTICATOR_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`AUTHENTICATOR_ID`,`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.authenticator_config_entry: ~4 rows (ungefähr)
DELETE FROM `authenticator_config_entry`;
INSERT INTO `authenticator_config_entry` (`AUTHENTICATOR_ID`, `VALUE`, `NAME`) VALUES
	('1e783779-b286-429c-8f4d-2067be88c35e', 'false', 'require.password.update.after.registration'),
	('4049ac7f-bc4f-43fe-9108-c8d4867d90e8', 'false', 'require.password.update.after.registration'),
	('aa5365a0-7ea4-493b-bb77-801e0dd85c89', 'missing', 'update.profile.on.first.login'),
	('d346b2ce-cf0f-40d6-86da-a54708acd13b', 'missing', 'update.profile.on.first.login');

-- Exportiere Struktur von Tabelle keycloak.broker_link
CREATE TABLE IF NOT EXISTS `broker_link` (
  `IDENTITY_PROVIDER` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `STORAGE_PROVIDER_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `BROKER_USER_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `BROKER_USERNAME` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `TOKEN` text COLLATE utf8mb4_bin DEFAULT NULL,
  `USER_ID` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`IDENTITY_PROVIDER`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.broker_link: ~0 rows (ungefähr)
DELETE FROM `broker_link`;

-- Exportiere Struktur von Tabelle keycloak.client
CREATE TABLE IF NOT EXISTS `client` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `FULL_SCOPE_ALLOWED` bit(1) NOT NULL DEFAULT b'0',
  `CLIENT_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `NOT_BEFORE` int(11) DEFAULT NULL,
  `PUBLIC_CLIENT` bit(1) NOT NULL DEFAULT b'0',
  `SECRET` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `BASE_URL` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `BEARER_ONLY` bit(1) NOT NULL DEFAULT b'0',
  `MANAGEMENT_URL` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `SURROGATE_AUTH_REQUIRED` bit(1) NOT NULL DEFAULT b'0',
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `PROTOCOL` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `NODE_REREG_TIMEOUT` int(11) DEFAULT 0,
  `FRONTCHANNEL_LOGOUT` bit(1) NOT NULL DEFAULT b'0',
  `CONSENT_REQUIRED` bit(1) NOT NULL DEFAULT b'0',
  `NAME` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `SERVICE_ACCOUNTS_ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `CLIENT_AUTHENTICATOR_TYPE` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `ROOT_URL` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `REGISTRATION_TOKEN` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `STANDARD_FLOW_ENABLED` bit(1) NOT NULL DEFAULT b'1',
  `IMPLICIT_FLOW_ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `DIRECT_ACCESS_GRANTS_ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `ALWAYS_DISPLAY_IN_CONSOLE` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_B71CJLBENV945RB6GCON438AT` (`REALM_ID`,`CLIENT_ID`),
  KEY `IDX_CLIENT_ID` (`CLIENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.client: ~15 rows (ungefähr)
DELETE FROM `client`;
INSERT INTO `client` (`ID`, `ENABLED`, `FULL_SCOPE_ALLOWED`, `CLIENT_ID`, `NOT_BEFORE`, `PUBLIC_CLIENT`, `SECRET`, `BASE_URL`, `BEARER_ONLY`, `MANAGEMENT_URL`, `SURROGATE_AUTH_REQUIRED`, `REALM_ID`, `PROTOCOL`, `NODE_REREG_TIMEOUT`, `FRONTCHANNEL_LOGOUT`, `CONSENT_REQUIRED`, `NAME`, `SERVICE_ACCOUNTS_ENABLED`, `CLIENT_AUTHENTICATOR_TYPE`, `ROOT_URL`, `DESCRIPTION`, `REGISTRATION_TOKEN`, `STANDARD_FLOW_ENABLED`, `IMPLICIT_FLOW_ENABLED`, `DIRECT_ACCESS_GRANTS_ENABLED`, `ALWAYS_DISPLAY_IN_CONSOLE`) VALUES
	('035989c0-3119-47ee-98ca-db933a226945', b'1', b'1', 'wurstwasser-app', 0, b'1', NULL, 'http://localhost:4200/', b'0', '', b'0', '47958335-5c70-4603-abe7-b887490d7b1c', 'openid-connect', -1, b'1', b'0', '', b'0', 'client-secret', 'http://localhost:4200/', '', NULL, b'1', b'0', b'1', b'0'),
	('0c372b1f-3961-4907-83de-1153d2af2d57', b'1', b'0', 'account-console', 0, b'1', NULL, '/realms/wurstwasser/account/', b'0', NULL, b'0', '47958335-5c70-4603-abe7-b887490d7b1c', 'openid-connect', 0, b'0', b'0', '${client_account-console}', b'0', 'client-secret', '${authBaseUrl}', NULL, NULL, b'1', b'0', b'0', b'0'),
	('2c77769b-4aab-4622-9e44-3a8433637f36', b'1', b'1', 'wurstwasser-nest', 0, b'0', 'o4pEDqMdioKxBbQsuN8WSAECOPrJxRIO', '', b'0', '', b'0', '47958335-5c70-4603-abe7-b887490d7b1c', 'openid-connect', -1, b'1', b'0', '', b'0', 'client-secret', '', '', NULL, b'0', b'0', b'1', b'0'),
	('2ef3bfdd-049a-4228-af7b-de4403fe6d04', b'1', b'0', 'account', 0, b'1', NULL, '/realms/master/account/', b'0', NULL, b'0', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'openid-connect', 0, b'0', b'0', '${client_account}', b'0', 'client-secret', '${authBaseUrl}', NULL, NULL, b'1', b'0', b'0', b'0'),
	('2fcff3fa-9001-46ca-87f1-b2a78917962c', b'1', b'0', 'admin-cli', 0, b'1', NULL, NULL, b'0', NULL, b'0', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'openid-connect', 0, b'0', b'0', '${client_admin-cli}', b'0', 'client-secret', NULL, NULL, NULL, b'0', b'0', b'1', b'0'),
	('47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', b'1', b'0', 'master-realm', 0, b'0', NULL, NULL, b'1', NULL, b'0', '23d8cb0f-53b6-4402-a9f9-072c402d5927', NULL, 0, b'0', b'0', 'master Realm', b'0', 'client-secret', NULL, NULL, NULL, b'1', b'0', b'0', b'0'),
	('4ea4fda8-0d35-4644-9adf-24236632f114', b'1', b'0', 'account', 0, b'1', NULL, '/realms/wurstwasser/account/', b'0', NULL, b'0', '47958335-5c70-4603-abe7-b887490d7b1c', 'openid-connect', 0, b'0', b'0', '${client_account}', b'0', 'client-secret', '${authBaseUrl}', NULL, NULL, b'1', b'0', b'0', b'0'),
	('58be2cf6-f202-4a7e-9cc9-a94bb28cec01', b'1', b'0', 'wurstwasser-realm', 0, b'0', NULL, NULL, b'1', NULL, b'0', '23d8cb0f-53b6-4402-a9f9-072c402d5927', NULL, 0, b'0', b'0', 'wurstwasser Realm', b'0', 'client-secret', NULL, NULL, NULL, b'1', b'0', b'0', b'0'),
	('66506671-3f2a-4b67-8f53-f2752ca989fd', b'1', b'0', 'broker', 0, b'0', NULL, NULL, b'1', NULL, b'0', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'openid-connect', 0, b'0', b'0', '${client_broker}', b'0', 'client-secret', NULL, NULL, NULL, b'1', b'0', b'0', b'0'),
	('808458f5-33ba-4e81-9601-019ae159abb3', b'1', b'0', 'security-admin-console', 0, b'1', NULL, '/admin/master/console/', b'0', NULL, b'0', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'openid-connect', 0, b'0', b'0', '${client_security-admin-console}', b'0', 'client-secret', '${authAdminUrl}', NULL, NULL, b'1', b'0', b'0', b'0'),
	('8d03d66e-c122-4357-b8ac-647ff79ac3be', b'1', b'0', 'security-admin-console', 0, b'1', NULL, '/admin/wurstwasser/console/', b'0', NULL, b'0', '47958335-5c70-4603-abe7-b887490d7b1c', 'openid-connect', 0, b'0', b'0', '${client_security-admin-console}', b'0', 'client-secret', '${authAdminUrl}', NULL, NULL, b'1', b'0', b'0', b'0'),
	('c3e3ce52-d510-4517-bc39-7a8d0e300bd9', b'1', b'0', 'realm-management', 0, b'0', NULL, NULL, b'1', NULL, b'0', '47958335-5c70-4603-abe7-b887490d7b1c', 'openid-connect', 0, b'0', b'0', '${client_realm-management}', b'0', 'client-secret', NULL, NULL, NULL, b'1', b'0', b'0', b'0'),
	('cc4616ba-1805-413c-8edb-1f5315bb8df5', b'1', b'0', 'account-console', 0, b'1', NULL, '/realms/master/account/', b'0', NULL, b'0', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'openid-connect', 0, b'0', b'0', '${client_account-console}', b'0', 'client-secret', '${authBaseUrl}', NULL, NULL, b'1', b'0', b'0', b'0'),
	('d6802727-17f1-48c8-915a-e26735047ce2', b'1', b'0', 'admin-cli', 0, b'1', NULL, NULL, b'0', NULL, b'0', '47958335-5c70-4603-abe7-b887490d7b1c', 'openid-connect', 0, b'0', b'0', '${client_admin-cli}', b'0', 'client-secret', NULL, NULL, NULL, b'0', b'0', b'1', b'0'),
	('e300dbf0-8156-400d-a394-21bc9ca441c3', b'1', b'0', 'broker', 0, b'0', NULL, NULL, b'1', NULL, b'0', '47958335-5c70-4603-abe7-b887490d7b1c', 'openid-connect', 0, b'0', b'0', '${client_broker}', b'0', 'client-secret', NULL, NULL, NULL, b'1', b'0', b'0', b'0');

-- Exportiere Struktur von Tabelle keycloak.client_attributes
CREATE TABLE IF NOT EXISTS `client_attributes` (
  `CLIENT_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` longtext CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`CLIENT_ID`,`NAME`),
  KEY `IDX_CLIENT_ATT_BY_NAME_VALUE` (`NAME`),
  CONSTRAINT `FK3C47C64BEACCA966` FOREIGN KEY (`CLIENT_ID`) REFERENCES `client` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.client_attributes: ~29 rows (ungefähr)
DELETE FROM `client_attributes`;
INSERT INTO `client_attributes` (`CLIENT_ID`, `NAME`, `VALUE`) VALUES
	('035989c0-3119-47ee-98ca-db933a226945', 'acr.loa.map', '{}'),
	('035989c0-3119-47ee-98ca-db933a226945', 'backchannel.logout.revoke.offline.tokens', 'false'),
	('035989c0-3119-47ee-98ca-db933a226945', 'backchannel.logout.session.required', 'true'),
	('035989c0-3119-47ee-98ca-db933a226945', 'client_credentials.use_refresh_token', 'false'),
	('035989c0-3119-47ee-98ca-db933a226945', 'display.on.consent.screen', 'false'),
	('035989c0-3119-47ee-98ca-db933a226945', 'oauth2.device.authorization.grant.enabled', 'false'),
	('035989c0-3119-47ee-98ca-db933a226945', 'oidc.ciba.grant.enabled', 'false'),
	('035989c0-3119-47ee-98ca-db933a226945', 'post.logout.redirect.uris', 'http://localhost:4200/*'),
	('035989c0-3119-47ee-98ca-db933a226945', 'require.pushed.authorization.requests', 'false'),
	('035989c0-3119-47ee-98ca-db933a226945', 'tls-client-certificate-bound-access-tokens', 'false'),
	('035989c0-3119-47ee-98ca-db933a226945', 'token.response.type.bearer.lower-case', 'false'),
	('035989c0-3119-47ee-98ca-db933a226945', 'use.refresh.tokens', 'true'),
	('0c372b1f-3961-4907-83de-1153d2af2d57', 'pkce.code.challenge.method', 'S256'),
	('0c372b1f-3961-4907-83de-1153d2af2d57', 'post.logout.redirect.uris', '+'),
	('2c77769b-4aab-4622-9e44-3a8433637f36', 'backchannel.logout.revoke.offline.tokens', 'false'),
	('2c77769b-4aab-4622-9e44-3a8433637f36', 'backchannel.logout.session.required', 'true'),
	('2c77769b-4aab-4622-9e44-3a8433637f36', 'client.secret.creation.time', '1675264988'),
	('2c77769b-4aab-4622-9e44-3a8433637f36', 'display.on.consent.screen', 'false'),
	('2c77769b-4aab-4622-9e44-3a8433637f36', 'oauth2.device.authorization.grant.enabled', 'false'),
	('2c77769b-4aab-4622-9e44-3a8433637f36', 'oidc.ciba.grant.enabled', 'false'),
	('2c77769b-4aab-4622-9e44-3a8433637f36', 'post.logout.redirect.uris', 'http://localhost:3333/*'),
	('2ef3bfdd-049a-4228-af7b-de4403fe6d04', 'post.logout.redirect.uris', '+'),
	('4ea4fda8-0d35-4644-9adf-24236632f114', 'post.logout.redirect.uris', '+'),
	('808458f5-33ba-4e81-9601-019ae159abb3', 'pkce.code.challenge.method', 'S256'),
	('808458f5-33ba-4e81-9601-019ae159abb3', 'post.logout.redirect.uris', '+'),
	('8d03d66e-c122-4357-b8ac-647ff79ac3be', 'pkce.code.challenge.method', 'S256'),
	('8d03d66e-c122-4357-b8ac-647ff79ac3be', 'post.logout.redirect.uris', '+'),
	('cc4616ba-1805-413c-8edb-1f5315bb8df5', 'pkce.code.challenge.method', 'S256'),
	('cc4616ba-1805-413c-8edb-1f5315bb8df5', 'post.logout.redirect.uris', '+');

-- Exportiere Struktur von Tabelle keycloak.client_auth_flow_bindings
CREATE TABLE IF NOT EXISTS `client_auth_flow_bindings` (
  `CLIENT_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `FLOW_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `BINDING_NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`CLIENT_ID`,`BINDING_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.client_auth_flow_bindings: ~0 rows (ungefähr)
DELETE FROM `client_auth_flow_bindings`;

-- Exportiere Struktur von Tabelle keycloak.client_initial_access
CREATE TABLE IF NOT EXISTS `client_initial_access` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `TIMESTAMP` int(11) DEFAULT NULL,
  `EXPIRATION` int(11) DEFAULT NULL,
  `COUNT` int(11) DEFAULT NULL,
  `REMAINING_COUNT` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_CLIENT_INIT_ACC_REALM` (`REALM_ID`),
  CONSTRAINT `FK_CLIENT_INIT_ACC_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `realm` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.client_initial_access: ~1 rows (ungefähr)
DELETE FROM `client_initial_access`;
INSERT INTO `client_initial_access` (`ID`, `REALM_ID`, `TIMESTAMP`, `EXPIRATION`, `COUNT`, `REMAINING_COUNT`) VALUES
	('a570454f-95f4-4be9-90b1-11fa24ec5f4e', '47958335-5c70-4603-abe7-b887490d7b1c', 1675264868, 86400, 1, 1);

-- Exportiere Struktur von Tabelle keycloak.client_node_registrations
CREATE TABLE IF NOT EXISTS `client_node_registrations` (
  `CLIENT_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` int(11) DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`CLIENT_ID`,`NAME`),
  CONSTRAINT `FK4129723BA992F594` FOREIGN KEY (`CLIENT_ID`) REFERENCES `client` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.client_node_registrations: ~0 rows (ungefähr)
DELETE FROM `client_node_registrations`;

-- Exportiere Struktur von Tabelle keycloak.client_scope
CREATE TABLE IF NOT EXISTS `client_scope` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `PROTOCOL` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_CLI_SCOPE` (`REALM_ID`,`NAME`),
  KEY `IDX_REALM_CLSCOPE` (`REALM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.client_scope: ~20 rows (ungefähr)
DELETE FROM `client_scope`;
INSERT INTO `client_scope` (`ID`, `NAME`, `REALM_ID`, `DESCRIPTION`, `PROTOCOL`) VALUES
	('1326cc85-b925-4654-a935-74af8ef758eb', 'roles', '47958335-5c70-4603-abe7-b887490d7b1c', 'OpenID Connect scope for add user roles to the access token', 'openid-connect'),
	('1e109bdc-2125-41fc-a9cd-988e4926ebba', 'email', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'OpenID Connect built-in scope: email', 'openid-connect'),
	('272fc934-f3a7-4268-83ac-2acc745d87f1', 'address', '47958335-5c70-4603-abe7-b887490d7b1c', 'OpenID Connect built-in scope: address', 'openid-connect'),
	('381dfb0c-222f-4c9a-b3bb-336e4987f292', 'profile', '47958335-5c70-4603-abe7-b887490d7b1c', 'OpenID Connect built-in scope: profile', 'openid-connect'),
	('4e92b233-7271-4ccf-a9b7-bd5b43c9a573', 'profile', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'OpenID Connect built-in scope: profile', 'openid-connect'),
	('51444a9d-afea-400e-b37a-8624ac52ff3d', 'phone', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'OpenID Connect built-in scope: phone', 'openid-connect'),
	('5e5faf95-a740-4ecd-b7ff-757299ed729b', 'email', '47958335-5c70-4603-abe7-b887490d7b1c', 'OpenID Connect built-in scope: email', 'openid-connect'),
	('6d92611e-48b4-4641-9191-de1cd49b50cd', 'address', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'OpenID Connect built-in scope: address', 'openid-connect'),
	('79ae1850-0614-45c7-beed-41698ef668ee', 'acr', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'OpenID Connect scope for add acr (authentication context class reference) to the token', 'openid-connect'),
	('7d79994b-6c92-452b-9e2f-83840e62c837', 'offline_access', '47958335-5c70-4603-abe7-b887490d7b1c', 'OpenID Connect built-in scope: offline_access', 'openid-connect'),
	('81fb9c52-5475-4410-86d5-55dee623e213', 'web-origins', '47958335-5c70-4603-abe7-b887490d7b1c', 'OpenID Connect scope for add allowed web origins to the access token', 'openid-connect'),
	('8afddcb9-4bba-4dc3-a54f-fa313a2fcdf7', 'offline_access', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'OpenID Connect built-in scope: offline_access', 'openid-connect'),
	('ac77881e-2cf5-4db1-8ee9-bef2ac8dcb52', 'roles', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'OpenID Connect scope for add user roles to the access token', 'openid-connect'),
	('ae70759a-d8f4-4c19-9ed4-0fa8bdb796fd', 'web-origins', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'OpenID Connect scope for add allowed web origins to the access token', 'openid-connect'),
	('b032d9af-b13c-4a76-9069-6b687cc7ac6c', 'role_list', '47958335-5c70-4603-abe7-b887490d7b1c', 'SAML role list', 'saml'),
	('b15a69e0-6f2e-4948-8feb-b7fbd0e4e36c', 'role_list', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'SAML role list', 'saml'),
	('b880b5f7-bb9c-4fbf-930b-db668b9907ab', 'phone', '47958335-5c70-4603-abe7-b887490d7b1c', 'OpenID Connect built-in scope: phone', 'openid-connect'),
	('c74d2f87-1571-40ec-b831-04c17af5ec75', 'microprofile-jwt', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'Microprofile - JWT built-in scope', 'openid-connect'),
	('cd2bd538-424e-4b02-884e-26620e48ce31', 'acr', '47958335-5c70-4603-abe7-b887490d7b1c', 'OpenID Connect scope for add acr (authentication context class reference) to the token', 'openid-connect'),
	('f558df31-2459-4a39-b429-fc9e8d1edfb6', 'microprofile-jwt', '47958335-5c70-4603-abe7-b887490d7b1c', 'Microprofile - JWT built-in scope', 'openid-connect');

-- Exportiere Struktur von Tabelle keycloak.client_scope_attributes
CREATE TABLE IF NOT EXISTS `client_scope_attributes` (
  `SCOPE_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` text COLLATE utf8mb4_bin DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`SCOPE_ID`,`NAME`),
  KEY `IDX_CLSCOPE_ATTRS` (`SCOPE_ID`),
  CONSTRAINT `FK_CL_SCOPE_ATTR_SCOPE` FOREIGN KEY (`SCOPE_ID`) REFERENCES `client_scope` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.client_scope_attributes: ~52 rows (ungefähr)
DELETE FROM `client_scope_attributes`;
INSERT INTO `client_scope_attributes` (`SCOPE_ID`, `VALUE`, `NAME`) VALUES
	('1326cc85-b925-4654-a935-74af8ef758eb', '${rolesScopeConsentText}', 'consent.screen.text'),
	('1326cc85-b925-4654-a935-74af8ef758eb', 'true', 'display.on.consent.screen'),
	('1326cc85-b925-4654-a935-74af8ef758eb', 'false', 'include.in.token.scope'),
	('1e109bdc-2125-41fc-a9cd-988e4926ebba', '${emailScopeConsentText}', 'consent.screen.text'),
	('1e109bdc-2125-41fc-a9cd-988e4926ebba', 'true', 'display.on.consent.screen'),
	('1e109bdc-2125-41fc-a9cd-988e4926ebba', 'true', 'include.in.token.scope'),
	('272fc934-f3a7-4268-83ac-2acc745d87f1', '${addressScopeConsentText}', 'consent.screen.text'),
	('272fc934-f3a7-4268-83ac-2acc745d87f1', 'true', 'display.on.consent.screen'),
	('272fc934-f3a7-4268-83ac-2acc745d87f1', 'true', 'include.in.token.scope'),
	('381dfb0c-222f-4c9a-b3bb-336e4987f292', '${profileScopeConsentText}', 'consent.screen.text'),
	('381dfb0c-222f-4c9a-b3bb-336e4987f292', 'true', 'display.on.consent.screen'),
	('381dfb0c-222f-4c9a-b3bb-336e4987f292', 'true', 'include.in.token.scope'),
	('4e92b233-7271-4ccf-a9b7-bd5b43c9a573', '${profileScopeConsentText}', 'consent.screen.text'),
	('4e92b233-7271-4ccf-a9b7-bd5b43c9a573', 'true', 'display.on.consent.screen'),
	('4e92b233-7271-4ccf-a9b7-bd5b43c9a573', 'true', 'include.in.token.scope'),
	('51444a9d-afea-400e-b37a-8624ac52ff3d', '${phoneScopeConsentText}', 'consent.screen.text'),
	('51444a9d-afea-400e-b37a-8624ac52ff3d', 'true', 'display.on.consent.screen'),
	('51444a9d-afea-400e-b37a-8624ac52ff3d', 'true', 'include.in.token.scope'),
	('5e5faf95-a740-4ecd-b7ff-757299ed729b', '${emailScopeConsentText}', 'consent.screen.text'),
	('5e5faf95-a740-4ecd-b7ff-757299ed729b', 'true', 'display.on.consent.screen'),
	('5e5faf95-a740-4ecd-b7ff-757299ed729b', 'true', 'include.in.token.scope'),
	('6d92611e-48b4-4641-9191-de1cd49b50cd', '${addressScopeConsentText}', 'consent.screen.text'),
	('6d92611e-48b4-4641-9191-de1cd49b50cd', 'true', 'display.on.consent.screen'),
	('6d92611e-48b4-4641-9191-de1cd49b50cd', 'true', 'include.in.token.scope'),
	('79ae1850-0614-45c7-beed-41698ef668ee', 'false', 'display.on.consent.screen'),
	('79ae1850-0614-45c7-beed-41698ef668ee', 'false', 'include.in.token.scope'),
	('7d79994b-6c92-452b-9e2f-83840e62c837', '${offlineAccessScopeConsentText}', 'consent.screen.text'),
	('7d79994b-6c92-452b-9e2f-83840e62c837', 'true', 'display.on.consent.screen'),
	('81fb9c52-5475-4410-86d5-55dee623e213', '', 'consent.screen.text'),
	('81fb9c52-5475-4410-86d5-55dee623e213', 'false', 'display.on.consent.screen'),
	('81fb9c52-5475-4410-86d5-55dee623e213', 'false', 'include.in.token.scope'),
	('8afddcb9-4bba-4dc3-a54f-fa313a2fcdf7', '${offlineAccessScopeConsentText}', 'consent.screen.text'),
	('8afddcb9-4bba-4dc3-a54f-fa313a2fcdf7', 'true', 'display.on.consent.screen'),
	('ac77881e-2cf5-4db1-8ee9-bef2ac8dcb52', '${rolesScopeConsentText}', 'consent.screen.text'),
	('ac77881e-2cf5-4db1-8ee9-bef2ac8dcb52', 'true', 'display.on.consent.screen'),
	('ac77881e-2cf5-4db1-8ee9-bef2ac8dcb52', 'false', 'include.in.token.scope'),
	('ae70759a-d8f4-4c19-9ed4-0fa8bdb796fd', '', 'consent.screen.text'),
	('ae70759a-d8f4-4c19-9ed4-0fa8bdb796fd', 'false', 'display.on.consent.screen'),
	('ae70759a-d8f4-4c19-9ed4-0fa8bdb796fd', 'false', 'include.in.token.scope'),
	('b032d9af-b13c-4a76-9069-6b687cc7ac6c', '${samlRoleListScopeConsentText}', 'consent.screen.text'),
	('b032d9af-b13c-4a76-9069-6b687cc7ac6c', 'true', 'display.on.consent.screen'),
	('b15a69e0-6f2e-4948-8feb-b7fbd0e4e36c', '${samlRoleListScopeConsentText}', 'consent.screen.text'),
	('b15a69e0-6f2e-4948-8feb-b7fbd0e4e36c', 'true', 'display.on.consent.screen'),
	('b880b5f7-bb9c-4fbf-930b-db668b9907ab', '${phoneScopeConsentText}', 'consent.screen.text'),
	('b880b5f7-bb9c-4fbf-930b-db668b9907ab', 'true', 'display.on.consent.screen'),
	('b880b5f7-bb9c-4fbf-930b-db668b9907ab', 'true', 'include.in.token.scope'),
	('c74d2f87-1571-40ec-b831-04c17af5ec75', 'false', 'display.on.consent.screen'),
	('c74d2f87-1571-40ec-b831-04c17af5ec75', 'true', 'include.in.token.scope'),
	('cd2bd538-424e-4b02-884e-26620e48ce31', 'false', 'display.on.consent.screen'),
	('cd2bd538-424e-4b02-884e-26620e48ce31', 'false', 'include.in.token.scope'),
	('f558df31-2459-4a39-b429-fc9e8d1edfb6', 'false', 'display.on.consent.screen'),
	('f558df31-2459-4a39-b429-fc9e8d1edfb6', 'true', 'include.in.token.scope');

-- Exportiere Struktur von Tabelle keycloak.client_scope_client
CREATE TABLE IF NOT EXISTS `client_scope_client` (
  `CLIENT_ID` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `SCOPE_ID` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `DEFAULT_SCOPE` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`CLIENT_ID`,`SCOPE_ID`),
  KEY `IDX_CLSCOPE_CL` (`CLIENT_ID`),
  KEY `IDX_CL_CLSCOPE` (`SCOPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.client_scope_client: ~126 rows (ungefähr)
DELETE FROM `client_scope_client`;
INSERT INTO `client_scope_client` (`CLIENT_ID`, `SCOPE_ID`, `DEFAULT_SCOPE`) VALUES
	('035989c0-3119-47ee-98ca-db933a226945', '1326cc85-b925-4654-a935-74af8ef758eb', b'1'),
	('035989c0-3119-47ee-98ca-db933a226945', '272fc934-f3a7-4268-83ac-2acc745d87f1', b'0'),
	('035989c0-3119-47ee-98ca-db933a226945', '381dfb0c-222f-4c9a-b3bb-336e4987f292', b'1'),
	('035989c0-3119-47ee-98ca-db933a226945', '5e5faf95-a740-4ecd-b7ff-757299ed729b', b'1'),
	('035989c0-3119-47ee-98ca-db933a226945', '7d79994b-6c92-452b-9e2f-83840e62c837', b'0'),
	('035989c0-3119-47ee-98ca-db933a226945', '81fb9c52-5475-4410-86d5-55dee623e213', b'1'),
	('035989c0-3119-47ee-98ca-db933a226945', 'b880b5f7-bb9c-4fbf-930b-db668b9907ab', b'0'),
	('035989c0-3119-47ee-98ca-db933a226945', 'cd2bd538-424e-4b02-884e-26620e48ce31', b'1'),
	('035989c0-3119-47ee-98ca-db933a226945', 'f558df31-2459-4a39-b429-fc9e8d1edfb6', b'0'),
	('0c372b1f-3961-4907-83de-1153d2af2d57', '1326cc85-b925-4654-a935-74af8ef758eb', b'1'),
	('0c372b1f-3961-4907-83de-1153d2af2d57', '272fc934-f3a7-4268-83ac-2acc745d87f1', b'0'),
	('0c372b1f-3961-4907-83de-1153d2af2d57', '381dfb0c-222f-4c9a-b3bb-336e4987f292', b'1'),
	('0c372b1f-3961-4907-83de-1153d2af2d57', '5e5faf95-a740-4ecd-b7ff-757299ed729b', b'1'),
	('0c372b1f-3961-4907-83de-1153d2af2d57', '7d79994b-6c92-452b-9e2f-83840e62c837', b'0'),
	('0c372b1f-3961-4907-83de-1153d2af2d57', '81fb9c52-5475-4410-86d5-55dee623e213', b'1'),
	('0c372b1f-3961-4907-83de-1153d2af2d57', 'b880b5f7-bb9c-4fbf-930b-db668b9907ab', b'0'),
	('0c372b1f-3961-4907-83de-1153d2af2d57', 'cd2bd538-424e-4b02-884e-26620e48ce31', b'1'),
	('0c372b1f-3961-4907-83de-1153d2af2d57', 'f558df31-2459-4a39-b429-fc9e8d1edfb6', b'0'),
	('2c77769b-4aab-4622-9e44-3a8433637f36', '1326cc85-b925-4654-a935-74af8ef758eb', b'1'),
	('2c77769b-4aab-4622-9e44-3a8433637f36', '272fc934-f3a7-4268-83ac-2acc745d87f1', b'0'),
	('2c77769b-4aab-4622-9e44-3a8433637f36', '381dfb0c-222f-4c9a-b3bb-336e4987f292', b'1'),
	('2c77769b-4aab-4622-9e44-3a8433637f36', '5e5faf95-a740-4ecd-b7ff-757299ed729b', b'1'),
	('2c77769b-4aab-4622-9e44-3a8433637f36', '7d79994b-6c92-452b-9e2f-83840e62c837', b'0'),
	('2c77769b-4aab-4622-9e44-3a8433637f36', '81fb9c52-5475-4410-86d5-55dee623e213', b'1'),
	('2c77769b-4aab-4622-9e44-3a8433637f36', 'b880b5f7-bb9c-4fbf-930b-db668b9907ab', b'0'),
	('2c77769b-4aab-4622-9e44-3a8433637f36', 'cd2bd538-424e-4b02-884e-26620e48ce31', b'1'),
	('2c77769b-4aab-4622-9e44-3a8433637f36', 'f558df31-2459-4a39-b429-fc9e8d1edfb6', b'0'),
	('2ef3bfdd-049a-4228-af7b-de4403fe6d04', '1e109bdc-2125-41fc-a9cd-988e4926ebba', b'1'),
	('2ef3bfdd-049a-4228-af7b-de4403fe6d04', '4e92b233-7271-4ccf-a9b7-bd5b43c9a573', b'1'),
	('2ef3bfdd-049a-4228-af7b-de4403fe6d04', '51444a9d-afea-400e-b37a-8624ac52ff3d', b'0'),
	('2ef3bfdd-049a-4228-af7b-de4403fe6d04', '6d92611e-48b4-4641-9191-de1cd49b50cd', b'0'),
	('2ef3bfdd-049a-4228-af7b-de4403fe6d04', '79ae1850-0614-45c7-beed-41698ef668ee', b'1'),
	('2ef3bfdd-049a-4228-af7b-de4403fe6d04', '8afddcb9-4bba-4dc3-a54f-fa313a2fcdf7', b'0'),
	('2ef3bfdd-049a-4228-af7b-de4403fe6d04', 'ac77881e-2cf5-4db1-8ee9-bef2ac8dcb52', b'1'),
	('2ef3bfdd-049a-4228-af7b-de4403fe6d04', 'ae70759a-d8f4-4c19-9ed4-0fa8bdb796fd', b'1'),
	('2ef3bfdd-049a-4228-af7b-de4403fe6d04', 'c74d2f87-1571-40ec-b831-04c17af5ec75', b'0'),
	('2fcff3fa-9001-46ca-87f1-b2a78917962c', '1e109bdc-2125-41fc-a9cd-988e4926ebba', b'1'),
	('2fcff3fa-9001-46ca-87f1-b2a78917962c', '4e92b233-7271-4ccf-a9b7-bd5b43c9a573', b'1'),
	('2fcff3fa-9001-46ca-87f1-b2a78917962c', '51444a9d-afea-400e-b37a-8624ac52ff3d', b'0'),
	('2fcff3fa-9001-46ca-87f1-b2a78917962c', '6d92611e-48b4-4641-9191-de1cd49b50cd', b'0'),
	('2fcff3fa-9001-46ca-87f1-b2a78917962c', '79ae1850-0614-45c7-beed-41698ef668ee', b'1'),
	('2fcff3fa-9001-46ca-87f1-b2a78917962c', '8afddcb9-4bba-4dc3-a54f-fa313a2fcdf7', b'0'),
	('2fcff3fa-9001-46ca-87f1-b2a78917962c', 'ac77881e-2cf5-4db1-8ee9-bef2ac8dcb52', b'1'),
	('2fcff3fa-9001-46ca-87f1-b2a78917962c', 'ae70759a-d8f4-4c19-9ed4-0fa8bdb796fd', b'1'),
	('2fcff3fa-9001-46ca-87f1-b2a78917962c', 'c74d2f87-1571-40ec-b831-04c17af5ec75', b'0'),
	('47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', '1e109bdc-2125-41fc-a9cd-988e4926ebba', b'1'),
	('47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', '4e92b233-7271-4ccf-a9b7-bd5b43c9a573', b'1'),
	('47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', '51444a9d-afea-400e-b37a-8624ac52ff3d', b'0'),
	('47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', '6d92611e-48b4-4641-9191-de1cd49b50cd', b'0'),
	('47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', '79ae1850-0614-45c7-beed-41698ef668ee', b'1'),
	('47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', '8afddcb9-4bba-4dc3-a54f-fa313a2fcdf7', b'0'),
	('47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', 'ac77881e-2cf5-4db1-8ee9-bef2ac8dcb52', b'1'),
	('47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', 'ae70759a-d8f4-4c19-9ed4-0fa8bdb796fd', b'1'),
	('47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', 'c74d2f87-1571-40ec-b831-04c17af5ec75', b'0'),
	('4ea4fda8-0d35-4644-9adf-24236632f114', '1326cc85-b925-4654-a935-74af8ef758eb', b'1'),
	('4ea4fda8-0d35-4644-9adf-24236632f114', '272fc934-f3a7-4268-83ac-2acc745d87f1', b'0'),
	('4ea4fda8-0d35-4644-9adf-24236632f114', '381dfb0c-222f-4c9a-b3bb-336e4987f292', b'1'),
	('4ea4fda8-0d35-4644-9adf-24236632f114', '5e5faf95-a740-4ecd-b7ff-757299ed729b', b'1'),
	('4ea4fda8-0d35-4644-9adf-24236632f114', '7d79994b-6c92-452b-9e2f-83840e62c837', b'0'),
	('4ea4fda8-0d35-4644-9adf-24236632f114', '81fb9c52-5475-4410-86d5-55dee623e213', b'1'),
	('4ea4fda8-0d35-4644-9adf-24236632f114', 'b880b5f7-bb9c-4fbf-930b-db668b9907ab', b'0'),
	('4ea4fda8-0d35-4644-9adf-24236632f114', 'cd2bd538-424e-4b02-884e-26620e48ce31', b'1'),
	('4ea4fda8-0d35-4644-9adf-24236632f114', 'f558df31-2459-4a39-b429-fc9e8d1edfb6', b'0'),
	('66506671-3f2a-4b67-8f53-f2752ca989fd', '1e109bdc-2125-41fc-a9cd-988e4926ebba', b'1'),
	('66506671-3f2a-4b67-8f53-f2752ca989fd', '4e92b233-7271-4ccf-a9b7-bd5b43c9a573', b'1'),
	('66506671-3f2a-4b67-8f53-f2752ca989fd', '51444a9d-afea-400e-b37a-8624ac52ff3d', b'0'),
	('66506671-3f2a-4b67-8f53-f2752ca989fd', '6d92611e-48b4-4641-9191-de1cd49b50cd', b'0'),
	('66506671-3f2a-4b67-8f53-f2752ca989fd', '79ae1850-0614-45c7-beed-41698ef668ee', b'1'),
	('66506671-3f2a-4b67-8f53-f2752ca989fd', '8afddcb9-4bba-4dc3-a54f-fa313a2fcdf7', b'0'),
	('66506671-3f2a-4b67-8f53-f2752ca989fd', 'ac77881e-2cf5-4db1-8ee9-bef2ac8dcb52', b'1'),
	('66506671-3f2a-4b67-8f53-f2752ca989fd', 'ae70759a-d8f4-4c19-9ed4-0fa8bdb796fd', b'1'),
	('66506671-3f2a-4b67-8f53-f2752ca989fd', 'c74d2f87-1571-40ec-b831-04c17af5ec75', b'0'),
	('808458f5-33ba-4e81-9601-019ae159abb3', '1e109bdc-2125-41fc-a9cd-988e4926ebba', b'1'),
	('808458f5-33ba-4e81-9601-019ae159abb3', '4e92b233-7271-4ccf-a9b7-bd5b43c9a573', b'1'),
	('808458f5-33ba-4e81-9601-019ae159abb3', '51444a9d-afea-400e-b37a-8624ac52ff3d', b'0'),
	('808458f5-33ba-4e81-9601-019ae159abb3', '6d92611e-48b4-4641-9191-de1cd49b50cd', b'0'),
	('808458f5-33ba-4e81-9601-019ae159abb3', '79ae1850-0614-45c7-beed-41698ef668ee', b'1'),
	('808458f5-33ba-4e81-9601-019ae159abb3', '8afddcb9-4bba-4dc3-a54f-fa313a2fcdf7', b'0'),
	('808458f5-33ba-4e81-9601-019ae159abb3', 'ac77881e-2cf5-4db1-8ee9-bef2ac8dcb52', b'1'),
	('808458f5-33ba-4e81-9601-019ae159abb3', 'ae70759a-d8f4-4c19-9ed4-0fa8bdb796fd', b'1'),
	('808458f5-33ba-4e81-9601-019ae159abb3', 'c74d2f87-1571-40ec-b831-04c17af5ec75', b'0'),
	('8d03d66e-c122-4357-b8ac-647ff79ac3be', '1326cc85-b925-4654-a935-74af8ef758eb', b'1'),
	('8d03d66e-c122-4357-b8ac-647ff79ac3be', '272fc934-f3a7-4268-83ac-2acc745d87f1', b'0'),
	('8d03d66e-c122-4357-b8ac-647ff79ac3be', '381dfb0c-222f-4c9a-b3bb-336e4987f292', b'1'),
	('8d03d66e-c122-4357-b8ac-647ff79ac3be', '5e5faf95-a740-4ecd-b7ff-757299ed729b', b'1'),
	('8d03d66e-c122-4357-b8ac-647ff79ac3be', '7d79994b-6c92-452b-9e2f-83840e62c837', b'0'),
	('8d03d66e-c122-4357-b8ac-647ff79ac3be', '81fb9c52-5475-4410-86d5-55dee623e213', b'1'),
	('8d03d66e-c122-4357-b8ac-647ff79ac3be', 'b880b5f7-bb9c-4fbf-930b-db668b9907ab', b'0'),
	('8d03d66e-c122-4357-b8ac-647ff79ac3be', 'cd2bd538-424e-4b02-884e-26620e48ce31', b'1'),
	('8d03d66e-c122-4357-b8ac-647ff79ac3be', 'f558df31-2459-4a39-b429-fc9e8d1edfb6', b'0'),
	('c3e3ce52-d510-4517-bc39-7a8d0e300bd9', '1326cc85-b925-4654-a935-74af8ef758eb', b'1'),
	('c3e3ce52-d510-4517-bc39-7a8d0e300bd9', '272fc934-f3a7-4268-83ac-2acc745d87f1', b'0'),
	('c3e3ce52-d510-4517-bc39-7a8d0e300bd9', '381dfb0c-222f-4c9a-b3bb-336e4987f292', b'1'),
	('c3e3ce52-d510-4517-bc39-7a8d0e300bd9', '5e5faf95-a740-4ecd-b7ff-757299ed729b', b'1'),
	('c3e3ce52-d510-4517-bc39-7a8d0e300bd9', '7d79994b-6c92-452b-9e2f-83840e62c837', b'0'),
	('c3e3ce52-d510-4517-bc39-7a8d0e300bd9', '81fb9c52-5475-4410-86d5-55dee623e213', b'1'),
	('c3e3ce52-d510-4517-bc39-7a8d0e300bd9', 'b880b5f7-bb9c-4fbf-930b-db668b9907ab', b'0'),
	('c3e3ce52-d510-4517-bc39-7a8d0e300bd9', 'cd2bd538-424e-4b02-884e-26620e48ce31', b'1'),
	('c3e3ce52-d510-4517-bc39-7a8d0e300bd9', 'f558df31-2459-4a39-b429-fc9e8d1edfb6', b'0'),
	('cc4616ba-1805-413c-8edb-1f5315bb8df5', '1e109bdc-2125-41fc-a9cd-988e4926ebba', b'1'),
	('cc4616ba-1805-413c-8edb-1f5315bb8df5', '4e92b233-7271-4ccf-a9b7-bd5b43c9a573', b'1'),
	('cc4616ba-1805-413c-8edb-1f5315bb8df5', '51444a9d-afea-400e-b37a-8624ac52ff3d', b'0'),
	('cc4616ba-1805-413c-8edb-1f5315bb8df5', '6d92611e-48b4-4641-9191-de1cd49b50cd', b'0'),
	('cc4616ba-1805-413c-8edb-1f5315bb8df5', '79ae1850-0614-45c7-beed-41698ef668ee', b'1'),
	('cc4616ba-1805-413c-8edb-1f5315bb8df5', '8afddcb9-4bba-4dc3-a54f-fa313a2fcdf7', b'0'),
	('cc4616ba-1805-413c-8edb-1f5315bb8df5', 'ac77881e-2cf5-4db1-8ee9-bef2ac8dcb52', b'1'),
	('cc4616ba-1805-413c-8edb-1f5315bb8df5', 'ae70759a-d8f4-4c19-9ed4-0fa8bdb796fd', b'1'),
	('cc4616ba-1805-413c-8edb-1f5315bb8df5', 'c74d2f87-1571-40ec-b831-04c17af5ec75', b'0'),
	('d6802727-17f1-48c8-915a-e26735047ce2', '1326cc85-b925-4654-a935-74af8ef758eb', b'1'),
	('d6802727-17f1-48c8-915a-e26735047ce2', '272fc934-f3a7-4268-83ac-2acc745d87f1', b'0'),
	('d6802727-17f1-48c8-915a-e26735047ce2', '381dfb0c-222f-4c9a-b3bb-336e4987f292', b'1'),
	('d6802727-17f1-48c8-915a-e26735047ce2', '5e5faf95-a740-4ecd-b7ff-757299ed729b', b'1'),
	('d6802727-17f1-48c8-915a-e26735047ce2', '7d79994b-6c92-452b-9e2f-83840e62c837', b'0'),
	('d6802727-17f1-48c8-915a-e26735047ce2', '81fb9c52-5475-4410-86d5-55dee623e213', b'1'),
	('d6802727-17f1-48c8-915a-e26735047ce2', 'b880b5f7-bb9c-4fbf-930b-db668b9907ab', b'0'),
	('d6802727-17f1-48c8-915a-e26735047ce2', 'cd2bd538-424e-4b02-884e-26620e48ce31', b'1'),
	('d6802727-17f1-48c8-915a-e26735047ce2', 'f558df31-2459-4a39-b429-fc9e8d1edfb6', b'0'),
	('e300dbf0-8156-400d-a394-21bc9ca441c3', '1326cc85-b925-4654-a935-74af8ef758eb', b'1'),
	('e300dbf0-8156-400d-a394-21bc9ca441c3', '272fc934-f3a7-4268-83ac-2acc745d87f1', b'0'),
	('e300dbf0-8156-400d-a394-21bc9ca441c3', '381dfb0c-222f-4c9a-b3bb-336e4987f292', b'1'),
	('e300dbf0-8156-400d-a394-21bc9ca441c3', '5e5faf95-a740-4ecd-b7ff-757299ed729b', b'1'),
	('e300dbf0-8156-400d-a394-21bc9ca441c3', '7d79994b-6c92-452b-9e2f-83840e62c837', b'0'),
	('e300dbf0-8156-400d-a394-21bc9ca441c3', '81fb9c52-5475-4410-86d5-55dee623e213', b'1'),
	('e300dbf0-8156-400d-a394-21bc9ca441c3', 'b880b5f7-bb9c-4fbf-930b-db668b9907ab', b'0'),
	('e300dbf0-8156-400d-a394-21bc9ca441c3', 'cd2bd538-424e-4b02-884e-26620e48ce31', b'1'),
	('e300dbf0-8156-400d-a394-21bc9ca441c3', 'f558df31-2459-4a39-b429-fc9e8d1edfb6', b'0');

-- Exportiere Struktur von Tabelle keycloak.client_scope_role_mapping
CREATE TABLE IF NOT EXISTS `client_scope_role_mapping` (
  `SCOPE_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `ROLE_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`SCOPE_ID`,`ROLE_ID`),
  KEY `IDX_CLSCOPE_ROLE` (`SCOPE_ID`),
  KEY `IDX_ROLE_CLSCOPE` (`ROLE_ID`),
  CONSTRAINT `FK_CL_SCOPE_RM_SCOPE` FOREIGN KEY (`SCOPE_ID`) REFERENCES `client_scope` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.client_scope_role_mapping: ~2 rows (ungefähr)
DELETE FROM `client_scope_role_mapping`;
INSERT INTO `client_scope_role_mapping` (`SCOPE_ID`, `ROLE_ID`) VALUES
	('7d79994b-6c92-452b-9e2f-83840e62c837', 'b1c58ee6-4ce1-4e99-8eb2-031740c8e54e'),
	('8afddcb9-4bba-4dc3-a54f-fa313a2fcdf7', '8f230571-7d8b-4885-a9ac-4ec5da2b7979');

-- Exportiere Struktur von Tabelle keycloak.client_session
CREATE TABLE IF NOT EXISTS `client_session` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `CLIENT_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `REDIRECT_URI` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `STATE` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `TIMESTAMP` int(11) DEFAULT NULL,
  `SESSION_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `AUTH_METHOD` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `REALM_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `AUTH_USER_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `CURRENT_ACTION` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_CLIENT_SESSION_SESSION` (`SESSION_ID`),
  CONSTRAINT `FK_B4AO2VCVAT6UKAU74WBWTFQO1` FOREIGN KEY (`SESSION_ID`) REFERENCES `user_session` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.client_session: ~0 rows (ungefähr)
DELETE FROM `client_session`;

-- Exportiere Struktur von Tabelle keycloak.client_session_auth_status
CREATE TABLE IF NOT EXISTS `client_session_auth_status` (
  `AUTHENTICATOR` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `CLIENT_SESSION` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`CLIENT_SESSION`,`AUTHENTICATOR`),
  CONSTRAINT `AUTH_STATUS_CONSTRAINT` FOREIGN KEY (`CLIENT_SESSION`) REFERENCES `client_session` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.client_session_auth_status: ~0 rows (ungefähr)
DELETE FROM `client_session_auth_status`;

-- Exportiere Struktur von Tabelle keycloak.client_session_note
CREATE TABLE IF NOT EXISTS `client_session_note` (
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `CLIENT_SESSION` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`CLIENT_SESSION`,`NAME`),
  CONSTRAINT `FK5EDFB00FF51C2736` FOREIGN KEY (`CLIENT_SESSION`) REFERENCES `client_session` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.client_session_note: ~0 rows (ungefähr)
DELETE FROM `client_session_note`;

-- Exportiere Struktur von Tabelle keycloak.client_session_prot_mapper
CREATE TABLE IF NOT EXISTS `client_session_prot_mapper` (
  `PROTOCOL_MAPPER_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `CLIENT_SESSION` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`CLIENT_SESSION`,`PROTOCOL_MAPPER_ID`),
  CONSTRAINT `FK_33A8SGQW18I532811V7O2DK89` FOREIGN KEY (`CLIENT_SESSION`) REFERENCES `client_session` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.client_session_prot_mapper: ~0 rows (ungefähr)
DELETE FROM `client_session_prot_mapper`;

-- Exportiere Struktur von Tabelle keycloak.client_session_role
CREATE TABLE IF NOT EXISTS `client_session_role` (
  `ROLE_ID` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `CLIENT_SESSION` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`CLIENT_SESSION`,`ROLE_ID`),
  CONSTRAINT `FK_11B7SGQW18I532811V7O2DV76` FOREIGN KEY (`CLIENT_SESSION`) REFERENCES `client_session` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.client_session_role: ~0 rows (ungefähr)
DELETE FROM `client_session_role`;

-- Exportiere Struktur von Tabelle keycloak.client_user_session_note
CREATE TABLE IF NOT EXISTS `client_user_session_note` (
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` text COLLATE utf8mb4_bin DEFAULT NULL,
  `CLIENT_SESSION` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`CLIENT_SESSION`,`NAME`),
  CONSTRAINT `FK_CL_USR_SES_NOTE` FOREIGN KEY (`CLIENT_SESSION`) REFERENCES `client_session` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.client_user_session_note: ~0 rows (ungefähr)
DELETE FROM `client_user_session_note`;

-- Exportiere Struktur von Tabelle keycloak.component
CREATE TABLE IF NOT EXISTS `component` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `PARENT_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `PROVIDER_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `PROVIDER_TYPE` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `SUB_TYPE` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_COMPONENT_REALM` (`REALM_ID`),
  KEY `IDX_COMPONENT_PROVIDER_TYPE` (`PROVIDER_TYPE`),
  CONSTRAINT `FK_COMPONENT_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `realm` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.component: ~24 rows (ungefähr)
DELETE FROM `component`;
INSERT INTO `component` (`ID`, `NAME`, `PARENT_ID`, `PROVIDER_ID`, `PROVIDER_TYPE`, `REALM_ID`, `SUB_TYPE`) VALUES
	('13219257-4ba7-4c22-a321-14df865ceaba', 'Allowed Protocol Mapper Types', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'allowed-protocol-mappers', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'anonymous'),
	('197f5ce9-1f48-4e95-aa5e-1a0adefe17e7', 'rsa-enc-generated', '47958335-5c70-4603-abe7-b887490d7b1c', 'rsa-enc-generated', 'org.keycloak.keys.KeyProvider', '47958335-5c70-4603-abe7-b887490d7b1c', NULL),
	('24572056-69d9-43f4-b6ee-aec26ee47f35', 'Max Clients Limit', '47958335-5c70-4603-abe7-b887490d7b1c', 'max-clients', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', '47958335-5c70-4603-abe7-b887490d7b1c', 'anonymous'),
	('26bff43e-7f8e-43fb-a7ed-44b62f46b13d', 'hmac-generated', '47958335-5c70-4603-abe7-b887490d7b1c', 'hmac-generated', 'org.keycloak.keys.KeyProvider', '47958335-5c70-4603-abe7-b887490d7b1c', NULL),
	('3ec23b42-934a-4698-b5b1-49f422078190', 'Trusted Hosts', '47958335-5c70-4603-abe7-b887490d7b1c', 'trusted-hosts', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', '47958335-5c70-4603-abe7-b887490d7b1c', 'anonymous'),
	('5f62b71f-79e2-4fbd-8720-0ac8a605f944', 'Consent Required', '47958335-5c70-4603-abe7-b887490d7b1c', 'consent-required', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', '47958335-5c70-4603-abe7-b887490d7b1c', 'anonymous'),
	('69580512-0721-404b-af40-41106865158e', 'Full Scope Disabled', '47958335-5c70-4603-abe7-b887490d7b1c', 'scope', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', '47958335-5c70-4603-abe7-b887490d7b1c', 'anonymous'),
	('7a130231-485f-42ce-bfdd-2634b0581298', 'hmac-generated', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'hmac-generated', 'org.keycloak.keys.KeyProvider', '23d8cb0f-53b6-4402-a9f9-072c402d5927', NULL),
	('852472e4-f3d5-4793-807f-08fb0947fcda', 'Allowed Protocol Mapper Types', '47958335-5c70-4603-abe7-b887490d7b1c', 'allowed-protocol-mappers', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', '47958335-5c70-4603-abe7-b887490d7b1c', 'authenticated'),
	('8940756a-b212-439e-87a8-edfdb7db3e64', 'Max Clients Limit', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'max-clients', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'anonymous'),
	('94198eb4-f969-4b93-bfe8-29d10fb20520', 'Allowed Protocol Mapper Types', '47958335-5c70-4603-abe7-b887490d7b1c', 'allowed-protocol-mappers', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', '47958335-5c70-4603-abe7-b887490d7b1c', 'anonymous'),
	('99190c3e-8a7d-418f-9bd6-73bdebd2a1ec', 'Allowed Client Scopes', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'allowed-client-templates', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'anonymous'),
	('a3ceb2eb-5667-424b-967f-7570ca1a0dbb', 'aes-generated', '47958335-5c70-4603-abe7-b887490d7b1c', 'aes-generated', 'org.keycloak.keys.KeyProvider', '47958335-5c70-4603-abe7-b887490d7b1c', NULL),
	('a7a895de-c679-4af4-915c-600559331172', 'rsa-generated', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'rsa-generated', 'org.keycloak.keys.KeyProvider', '23d8cb0f-53b6-4402-a9f9-072c402d5927', NULL),
	('b38a18bb-404e-4887-a173-af5d89f8b4b9', 'Consent Required', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'consent-required', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'anonymous'),
	('b5334acf-d175-4bd2-ad8a-23af9e0d8b04', 'Allowed Client Scopes', '47958335-5c70-4603-abe7-b887490d7b1c', 'allowed-client-templates', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', '47958335-5c70-4603-abe7-b887490d7b1c', 'authenticated'),
	('c53215dd-5112-4ff3-b8c9-c7a067e93dcb', 'Full Scope Disabled', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'scope', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'anonymous'),
	('c9cc1407-e164-4e85-9fc6-131f28669446', 'rsa-generated', '47958335-5c70-4603-abe7-b887490d7b1c', 'rsa-generated', 'org.keycloak.keys.KeyProvider', '47958335-5c70-4603-abe7-b887490d7b1c', NULL),
	('cf445cf1-e777-4491-b09f-8566fdad114f', 'Trusted Hosts', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'trusted-hosts', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'anonymous'),
	('d3558fc3-72bb-4bde-b8ba-ab25c5ee82b9', 'Allowed Client Scopes', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'allowed-client-templates', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'authenticated'),
	('d36df399-354d-4419-8b45-dcf42977d1d1', 'Allowed Protocol Mapper Types', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'allowed-protocol-mappers', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'authenticated'),
	('e6fe2a24-74de-440f-8eb8-d1cb71c93f61', 'Allowed Client Scopes', '47958335-5c70-4603-abe7-b887490d7b1c', 'allowed-client-templates', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', '47958335-5c70-4603-abe7-b887490d7b1c', 'anonymous'),
	('ec7efbb9-1b6e-4e6c-ad63-e8462048f58b', 'rsa-enc-generated', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'rsa-enc-generated', 'org.keycloak.keys.KeyProvider', '23d8cb0f-53b6-4402-a9f9-072c402d5927', NULL),
	('f3813659-b17b-4e01-b0c0-609093f59490', 'aes-generated', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'aes-generated', 'org.keycloak.keys.KeyProvider', '23d8cb0f-53b6-4402-a9f9-072c402d5927', NULL);

-- Exportiere Struktur von Tabelle keycloak.component_config
CREATE TABLE IF NOT EXISTS `component_config` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `COMPONENT_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` varchar(4000) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_COMPO_CONFIG_COMPO` (`COMPONENT_ID`),
  CONSTRAINT `FK_COMPONENT_CONFIG` FOREIGN KEY (`COMPONENT_ID`) REFERENCES `component` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.component_config: ~74 rows (ungefähr)
DELETE FROM `component_config`;
INSERT INTO `component_config` (`ID`, `COMPONENT_ID`, `NAME`, `VALUE`) VALUES
	('04da42da-2ab7-4c44-97d7-62fca360b6f3', '197f5ce9-1f48-4e95-aa5e-1a0adefe17e7', 'certificate', 'MIICpTCCAY0CBgGF9ZbFJzANBgkqhkiG9w0BAQsFADAWMRQwEgYDVQQDDAt3dXJzdHdhc3NlcjAeFw0yMzAxMjcyMzM0MzZaFw0zMzAxMjcyMzM2MTZaMBYxFDASBgNVBAMMC3d1cnN0d2Fzc2VyMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1qMrHvJE7Uy+Jq8sVcgJV8WZEnAmEwCKTJAMQikFcnM8kI+VxyXJ5ddfak2QXdmQfcbaZ4NOy9W+dLJW8/tKMR/LR2JzNv6qFoi5AjcbpgAfF6JzuHLfJux/RLSGASYHtK9YaZwpo90iofshGCu6z36UjK8hUGjVfqUehp79za8t8meUrLTNP5trxF9XArAbNcODNPlqcq5jih0TsL0eco4wM+YpxnoMP+3a0a+/1NKacUNlMEZPJXxkhKYT131sKyD8VIZGMl3dCSlJMCPrhykuvxroMux76ScFwW+Nm9fTFlV8Bp7LbBi1xk8bkYrBhcFDloFFEAieZXFYYngkzwIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQBwnDlsBJGkTtfaaj4Ebq+J1+61hLjD+LP5NWrdNLm0k0UVm+okQqWYmkU1bTrTVqZ1m18lSNz5Jpih3u+oVPYILe2Dbn9q35Ac68BTy3Oui/vcQQ9mV++k4fseFO0eTePTuX+Y88MhVW4NG7qk11Cd7Cf6FNBtyrxQcYQvKnAGngvv/AdDz91AuVy2Yd5CnlD5Wsz4TW4ruahwPAgbFrQtCEX27H0HgeF/Cc0j5PFHIjN/lDYHMgJD3/t77LyQKXxl9sRwk4Ik1gIt4QOnm5lfJrbC0ulmrUZByOdIOIXXDNB4FyTDQuX8h1a2YL9IistoKGoGLwEYQPTDirFAaOlc'),
	('056b6b6b-346d-407a-9c2c-a45f90065965', 'ec7efbb9-1b6e-4e6c-ad63-e8462048f58b', 'keyUse', 'ENC'),
	('0727c074-3275-4aa2-8427-158e58e14495', 'd36df399-354d-4419-8b45-dcf42977d1d1', 'allowed-protocol-mapper-types', 'oidc-address-mapper'),
	('07895613-2e60-4358-a469-cc3fd1fe1940', '26bff43e-7f8e-43fb-a7ed-44b62f46b13d', 'priority', '100'),
	('0a8ae6bb-7bbd-4cbc-bd7f-e185f0df1e0d', 'd36df399-354d-4419-8b45-dcf42977d1d1', 'allowed-protocol-mapper-types', 'saml-user-attribute-mapper'),
	('0ae8f3bc-6b2c-4936-b707-21967acde260', 'a7a895de-c679-4af4-915c-600559331172', 'priority', '100'),
	('0d61520e-49e1-4589-9616-b5e389a926ba', 'f3813659-b17b-4e01-b0c0-609093f59490', 'secret', 'cr8fF469X0SbzQSjCe48bg'),
	('11b107eb-9eff-49f1-8071-adf9acc28226', 'a7a895de-c679-4af4-915c-600559331172', 'certificate', 'MIICmzCCAYMCBgGF9ZLsFDANBgkqhkiG9w0BAQsFADARMQ8wDQYDVQQDDAZtYXN0ZXIwHhcNMjMwMTI3MjMzMDI0WhcNMzMwMTI3MjMzMjA0WjARMQ8wDQYDVQQDDAZtYXN0ZXIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDWDm3J5AgeRDxcdr3bw2ZHFR1P2QI8d6mWGgEKKR2VX/miEO7quhhV+yVm9pKAhLNjnJTmlNmgzYD4iRzUO+3740bznVeYtiqwuvLmoWgLYbBjoXhaNl5svKlxdzBtlbck5F77DomdWJ9i/56ZnxBrHQ3OtccpGZyI7HU+ViL5DqVtLHn7snhrDBMPGShhrqoKo+k//kEW6nsiV/6LJteChsp60QBGgBsxjeQFM1UdxFdVyU0IFvyCw7udW+9lyM31QhxZ0QW1r0HsjD2MpNSB/+uC0psP8AQvkUUXeHjZvTH9i9/0Dy/Bb5PFFdHhZMHJLvViKvnhPVIxp5L0cFgtAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAHdAh14at58dlArvkjGmBJRRM4rWZm8HLf7LEOBxLCkSBtZa5jXme87VkDVBCm6ofQrcDfyWKPeEJ6NUpZXJpUVPgR4CI0ol+zORVy+M+MqFg3UOX4dvqGQXP5hNO/WTcWecH+gyu0gWJuFaL8t8kpN0/exO2Ow/1c6/LmP2qcET/gIIH67lmpzmavBCoDZTu3MqrXXyj1EV2vowXPTXfxzlC4saEovblCMyehpXaJN2c8l73LG2QRbR5UdW8dMsiOlcQsiFxCen0SLfULHFOBKkKJgflZwqfBrpQSznakTIE44KWwKvmhqi0iBKahjxsPrJCPGSaKGR6K4c0hIKiV4='),
	('2c5ff898-621b-46dc-99f0-b754431582ab', 'a7a895de-c679-4af4-915c-600559331172', 'privateKey', 'MIIEowIBAAKCAQEA1g5tyeQIHkQ8XHa928NmRxUdT9kCPHeplhoBCikdlV/5ohDu6roYVfslZvaSgISzY5yU5pTZoM2A+Ikc1Dvt++NG851XmLYqsLry5qFoC2GwY6F4WjZebLypcXcwbZW3JORe+w6JnVifYv+emZ8Qax0NzrXHKRmciOx1PlYi+Q6lbSx5+7J4awwTDxkoYa6qCqPpP/5BFup7Ilf+iybXgobKetEARoAbMY3kBTNVHcRXVclNCBb8gsO7nVvvZcjN9UIcWdEFta9B7Iw9jKTUgf/rgtKbD/AEL5FFF3h42b0x/Yvf9A8vwW+TxRXR4WTByS71Yir54T1SMaeS9HBYLQIDAQABAoIBACicNS0L05R2aqM6LdvvLszGjziSWa/CHgzpow/b7A5mVnRuVP14T9ykGJz3o6S5SFVaJA+45q7wvgqo7KzBL+3NWS0nyTIiQWglF6c0sQoTvwSv4ibvUKu9gUITFD9+G3Bm2TQ1NluBMD/2cg8AE/vWV8PDTeUYxE9f2QB6FAl+6lmrJUTsfG9fI3JBiMBZmroA8Fo/kTPEZ0pnYj3gn1uifEgmlxBDS+SPVwfrUi+VfaL57+k/VCSOHArqSnC7+tjQn+ox56Tp2gHpylrlLGKVsGA/krcjmXnWxmKbehdS1hkL0TxWdKnRISW8RMressZ7Sp5shtd1sicI0LyHndsCgYEA+SICwz6hjQkspEoiXcXdWlOCLKQf9/WzuS+qAIA9Yj6aXwNmM9EIsIj5telFm4nDHE8mnv6Rf+vwfWdxNoVmDWfTq7cEg4aTXjGlwV2fEAxGP+KMyqt/frXFtz6EmLMxNgDzn4Nn9PTuykQTkbOLJlBwDNUXoKZDarRm40Bh01cCgYEA2/TnNf2hLtYpNXCj1gcnYmXsWd4ttfgtQRznu5MAVASqVuJp0PXwEW/MYz7PciJooG4Yq6UfNd3Sr2FnyTa6x/LGEHdGuziEuQricas9FfKr3a42fwSj5nQoooBOppi85Xl2M3xtMBDqg993C1VMvbK0MyR878AeHnLDUkVuohsCgYEAvqXwR0leh3dF2lOBCRGp2r2dGFxgungkGMsI5Wk9kDkoGR2Y0H5ej/nCXhstUWGTFc7cVz+oSKRdXRoT4Q0kk42oxfdAVQZfL+3+UoM6fiVmfOz5oP6simK/8wK8pb47IoCH7sG4hBQLyG1gLFehXKihlrBbbGzPmpCDS9SxB/ECgYBFaBotZ0MdTGbkfg600r0Sx6a1FO7HpPBiw4Q8JD7OREqb8AYD8hc26VhNWNyM3160gQk34HrgvIZm4Q2m6KY3wdvOBlVDbqoy2cRqqeY0rsacxQRdQFGXvr+zx1kKKNgycQljZbfumuflhPZDlT8J/QHvwWvLG4xlf0EzDMXEUQKBgFjQpbuFgIaDKbij4+8psFcOwno6XTx/xXu4NYVARcUfkkpsu3/q48Qu3YbrfkUpD2EU+zeJaRvMDgrFlezZ4FV5ZPZG8hifjqQAkqSFyUVXiwognR01ZizSn+42cGzMfBX8QjsOXaDw3tnVJFwOoVh2MVnF5LRNrkdckuP7khVb'),
	('2c895e1e-f3ab-468f-95ca-63462ebde6d4', '852472e4-f3d5-4793-807f-08fb0947fcda', 'allowed-protocol-mapper-types', 'saml-user-attribute-mapper'),
	('2cb42b9f-d6ec-41b6-a2a7-316105832ee3', '852472e4-f3d5-4793-807f-08fb0947fcda', 'allowed-protocol-mapper-types', 'saml-user-property-mapper'),
	('2cbbc01e-24d7-4ff8-b69f-6d3b7fa8a8b1', '94198eb4-f969-4b93-bfe8-29d10fb20520', 'allowed-protocol-mapper-types', 'saml-user-attribute-mapper'),
	('3003dd64-96dc-4ea1-ab48-d27d18c612b9', 'b5334acf-d175-4bd2-ad8a-23af9e0d8b04', 'allow-default-scopes', 'true'),
	('32232c5e-c84b-4cf2-9ace-4c4db12dfa87', '852472e4-f3d5-4793-807f-08fb0947fcda', 'allowed-protocol-mapper-types', 'oidc-address-mapper'),
	('3488acf0-f250-46bc-b730-666de1eeabb5', '3ec23b42-934a-4698-b5b1-49f422078190', 'client-uris-must-match', 'true'),
	('371d2720-be33-41b4-bcd0-6df44c2a06d6', 'a3ceb2eb-5667-424b-967f-7570ca1a0dbb', 'kid', '99b861eb-02ef-4891-86ec-21967a270bc8'),
	('3b2e3360-ca79-4846-be03-77aefba6b33e', 'a3ceb2eb-5667-424b-967f-7570ca1a0dbb', 'secret', 'kS3yQ-6o2p72_oP6S124ng'),
	('3c2d3722-a299-4515-8318-a74652af794e', '852472e4-f3d5-4793-807f-08fb0947fcda', 'allowed-protocol-mapper-types', 'oidc-sha256-pairwise-sub-mapper'),
	('3cfaeaa1-e35e-4331-b974-5623d213096f', 'ec7efbb9-1b6e-4e6c-ad63-e8462048f58b', 'privateKey', 'MIIEowIBAAKCAQEAjgKnI5xELnmTA4H4xzNG1Ya0xMlPH8la1oeMLl4P+WZRQ7lfPGEk2l9A3DaqilhrANzEUVt+RiMhw7vTQlYu2IS63hQ+rHTILaPWNyiDVEclBrE+YCp2jIbN16j5luX8HkCL2gEAcO5pAiAM+VMleRoKZfHGIr3WSvZrUgZwvcaWnLbCJ3MPGW3ZRkCGuRVcfzgsOssclPZ4yakyeA8UUoX7BEqHpHfKMWbuWX42wfet/t58REtE7f24L87AQ3T1zuJeMLEmTNgThDzPGMyMVkg2i6yIn8ye8PdwTkaVzmVyRmjwjKJXot0vfvDb5QHbEcKp+VeEjRR7R/sPwyvZXwIDAQABAoIBAB3jzQ4fu/IUQK9CUfyLfGNBARdST8ce+HGjAgxYXABgWsZoY/UvdTRJTDuNfgO0bIE39lIxcbiNeHPcOrEBtsZ32WUY2cAurFt+2n2tl0bL2q037SsMhPs6/GRh+naaXaB4cXcnMtKSO6Po3xIzP/XnVzzx0IRLdKk+iIQxOtWWgJga7SS51O4jN53sA9sI2idcutfO5GMhIbkwwFtFNChF7/EXB4jWY7B0z+qN8cTgzVHBu0DH91T43M8qZs+gat+K5YhmA/VkeczU90DP/f0kTQ9TUF4cGUbEPvj6MugNnsswE843MZF+Q8DwoPO32FD0kZKOh/Fr8LSMZVHg2HECgYEAxaM6BudC1ebo2Sk6SGb5pHHqqii6TfyXcMfSP3v8mbBiAw2tuLp1YtUBGe6OMCEP6sTfh4vsNACo8hqskE05c8wYp30nXNz+64342HG46YsP5DOXmiu6OukZ1ZBD3OTtkpS14LZ617EtJvDRjYBNFms8fEI+5zuCE+wedbxZudsCgYEAt/IvGGweFE2TVRdtLLUf9yXbs6ZRdlRE9XrCivsUAvBYcuoyfe0N8AMe5CFWBN5mCHDDEQgKSZaVb8icmgGa0Qj0J/9NuVa/q9qGQQDXThII9LQNXXuZGhORCSTz/SEihlZMv1Kzzurl8S+Ton7SditG/qEVYELWn566KLOZn80CgYBtwIk20DIzavmfrt9o2ZaElaZFPDzJlh3bSXweORI7ntGu9GemkBUj/zmPVwSrAZE/RHkFYtSWxE8Ns3Zk5B2lio+eY0f6r9b4pGyudejD+K+ogGYN/X57vHrWODkXcCHmJqMh2PQ6129FkrIsLDWi34ZFUtxvuIIx4gvAQgppxQKBgFCVqeoyG29bmOsTS0Hly64ziK+IcnG7c0Zz1FycqGStNKKLVTlzq8J1K4hXzl5trBeQhvSkKBWbAbaqFWkmwNNI7+XB5CQJDAlWv7AklS5fmkrRwnUs26Lf5Y8zACzp4j0l+5NpwNoqcDgcVro+gyc/yGdu3HWCcpIVMIr6O5gVAoGBAKtKiWo2W24L0apB27W/nEhtn8SzV6KPVoAjXHNjVimWBuWmTpiLegdjAbdDcZ0/nK9W1euxPQ6d+XOZ1W6fk4mi8WjOC94lVYR9CfHQQux6Sxe3dckWGHBg/BaWiNxA+f4Ohp1bgtE4vXvoHQCGG7uywS5vawLJlj9+syCIJ9Lk'),
	('3d8d6a4e-d05a-4e33-857d-9ec615a2c08d', '13219257-4ba7-4c22-a321-14df865ceaba', 'allowed-protocol-mapper-types', 'oidc-address-mapper'),
	('40a18768-756d-46c3-b357-760f2b71742c', '13219257-4ba7-4c22-a321-14df865ceaba', 'allowed-protocol-mapper-types', 'saml-user-property-mapper'),
	('47750283-6aa0-4ee9-881b-503c6dfacb18', 'f3813659-b17b-4e01-b0c0-609093f59490', 'kid', '40355b4a-a4cd-4109-90e1-eb64e2d945fc'),
	('4aa4277f-1002-40b8-b053-8c8e2bd1b06b', '852472e4-f3d5-4793-807f-08fb0947fcda', 'allowed-protocol-mapper-types', 'oidc-full-name-mapper'),
	('4d16fb17-779f-4a38-9cc7-f935b0e8b1d4', '13219257-4ba7-4c22-a321-14df865ceaba', 'allowed-protocol-mapper-types', 'saml-role-list-mapper'),
	('5b39d070-26ec-44f1-90f8-25991f11f689', '3ec23b42-934a-4698-b5b1-49f422078190', 'host-sending-registration-request-must-match', 'true'),
	('5c67ad92-ddf8-47ae-917d-4e86578533a4', '13219257-4ba7-4c22-a321-14df865ceaba', 'allowed-protocol-mapper-types', 'oidc-full-name-mapper'),
	('5dfe9f7d-a75d-4181-86b5-7267bddb1e45', '94198eb4-f969-4b93-bfe8-29d10fb20520', 'allowed-protocol-mapper-types', 'oidc-address-mapper'),
	('5f98afaa-fecc-4f7a-9f64-8e387f2e1709', '8940756a-b212-439e-87a8-edfdb7db3e64', 'max-clients', '200'),
	('6ab33884-0ddf-4b9a-8777-ae8d51895764', '94198eb4-f969-4b93-bfe8-29d10fb20520', 'allowed-protocol-mapper-types', 'saml-role-list-mapper'),
	('6acbd8ff-b722-43ea-8a71-61fea62bc1f9', '7a130231-485f-42ce-bfdd-2634b0581298', 'priority', '100'),
	('6b7e43d1-8c4b-409d-a796-54712d320f85', '94198eb4-f969-4b93-bfe8-29d10fb20520', 'allowed-protocol-mapper-types', 'oidc-full-name-mapper'),
	('6c323fdc-bb01-44bb-9192-3f588071207d', '94198eb4-f969-4b93-bfe8-29d10fb20520', 'allowed-protocol-mapper-types', 'oidc-usermodel-attribute-mapper'),
	('6da9db2d-23f7-4652-83c1-61d54d4497e2', 'd3558fc3-72bb-4bde-b8ba-ab25c5ee82b9', 'allow-default-scopes', 'true'),
	('6ecc42d0-70c5-4b4b-a3d0-a4755df09544', '13219257-4ba7-4c22-a321-14df865ceaba', 'allowed-protocol-mapper-types', 'oidc-usermodel-attribute-mapper'),
	('7603019c-8c0a-4aed-911c-a6aa126a7565', 'ec7efbb9-1b6e-4e6c-ad63-e8462048f58b', 'certificate', 'MIICmzCCAYMCBgGF9ZLtXzANBgkqhkiG9w0BAQsFADARMQ8wDQYDVQQDDAZtYXN0ZXIwHhcNMjMwMTI3MjMzMDI1WhcNMzMwMTI3MjMzMjA1WjARMQ8wDQYDVQQDDAZtYXN0ZXIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCOAqcjnEQueZMDgfjHM0bVhrTEyU8fyVrWh4wuXg/5ZlFDuV88YSTaX0DcNqqKWGsA3MRRW35GIyHDu9NCVi7YhLreFD6sdMgto9Y3KINURyUGsT5gKnaMhs3XqPmW5fweQIvaAQBw7mkCIAz5UyV5Ggpl8cYivdZK9mtSBnC9xpactsIncw8ZbdlGQIa5FVx/OCw6yxyU9njJqTJ4DxRShfsESoekd8oxZu5ZfjbB963+3nxES0Tt/bgvzsBDdPXO4l4wsSZM2BOEPM8YzIxWSDaLrIifzJ7w93BORpXOZXJGaPCMolei3S9+8NvlAdsRwqn5V4SNFHtH+w/DK9lfAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAFZUsyGq5PfsJ7QMHBU4AWCDrRpK2l62SGvTsnWMKN1+7ixZJnvPJDJ9BKUPZwKMhENgV3PofdJPq/K7Hi4wNCAu0uVDwZ69epjbNz2bhWJZIlgp1BLUBVM0rJdQ27/wU3B4BHq7FaEEdnEZec5b9OwqYmUlIJ6IjYExUfoSeqZ2J1C3aiz+OlTWVQTT8KyyUgnXbFRDNFiosrLJX9Rs0AktWEEqzb7vTo42x9DNw2zbMO7La6QQ0Fu1XTZ5rum1Prf+4H73iIca4wdMbrO5mFuAspclqEhVLv3YBiU3vCTO35AtQvepOBuXs1wDI7DF4VLOstpmfY90m+BUeqQ2C0U='),
	('77d2db82-18fb-428d-bcd4-4830745b9c7e', 'cf445cf1-e777-4491-b09f-8566fdad114f', 'client-uris-must-match', 'true'),
	('867f5af0-b210-461f-9b27-c93bc6300453', '26bff43e-7f8e-43fb-a7ed-44b62f46b13d', 'kid', 'd0e77f64-397a-4c6c-b4f4-fab70532d315'),
	('8a992e34-a101-4ce7-9c41-5a5486c91366', 'd36df399-354d-4419-8b45-dcf42977d1d1', 'allowed-protocol-mapper-types', 'saml-user-property-mapper'),
	('8da26cfa-d2db-476c-9fe9-9e0bf0ba5211', '7a130231-485f-42ce-bfdd-2634b0581298', 'secret', 'vH44znwCzajMpRJzKENkVbjv84eofX9jyhQVyFk9X0gWsjaiDwGutnh0tAzUPA0Eo4I4CNhiTrT6tS1sBOJ2ug'),
	('91841b0d-b818-4a87-aed8-f7cf7b4cbe01', '852472e4-f3d5-4793-807f-08fb0947fcda', 'allowed-protocol-mapper-types', 'oidc-usermodel-property-mapper'),
	('930eb708-836b-45d9-a8da-dafa813c5427', 'd36df399-354d-4419-8b45-dcf42977d1d1', 'allowed-protocol-mapper-types', 'oidc-sha256-pairwise-sub-mapper'),
	('93cd77ce-8601-4383-ae2f-c8bb92761ed4', '197f5ce9-1f48-4e95-aa5e-1a0adefe17e7', 'priority', '100'),
	('9590d650-db9d-417a-bac1-60913e9c2087', 'd36df399-354d-4419-8b45-dcf42977d1d1', 'allowed-protocol-mapper-types', 'oidc-full-name-mapper'),
	('96eea216-3e5a-498f-a396-80016a19feb5', '852472e4-f3d5-4793-807f-08fb0947fcda', 'allowed-protocol-mapper-types', 'saml-role-list-mapper'),
	('9ecbe23d-5ee2-4198-ac71-c3ee4a9b7a73', '24572056-69d9-43f4-b6ee-aec26ee47f35', 'max-clients', '200'),
	('a14af2b5-375f-4e5f-b9c7-07240ee86ed5', '13219257-4ba7-4c22-a321-14df865ceaba', 'allowed-protocol-mapper-types', 'oidc-sha256-pairwise-sub-mapper'),
	('a22c5ade-a5bd-4b34-ad4d-18b829785265', '94198eb4-f969-4b93-bfe8-29d10fb20520', 'allowed-protocol-mapper-types', 'oidc-sha256-pairwise-sub-mapper'),
	('b011d138-7c78-46de-b611-dd6c81926af8', '94198eb4-f969-4b93-bfe8-29d10fb20520', 'allowed-protocol-mapper-types', 'saml-user-property-mapper'),
	('b1f0fec7-fe94-4a86-a649-1c37b761d672', 'c9cc1407-e164-4e85-9fc6-131f28669446', 'privateKey', 'MIIEpAIBAAKCAQEArwCiIhJ9feOmESQZq52W/mfyEGe3IVorrByp2LuIB9dQYFaVHfpVcaCAau05ya0/2KiQ01xgeZ8XWj5bPTIezOIGFL4c03BQgUPZPCiVTCughaQoyz0inCRAMP1J8hw969aIobiEX8DhmLrHyu7UT6CJ119fXXmTgvQOizVLcdLzAclTLgU8y3sYhvvbfb8Wd4jfCiwsWeD8pGgrfSGKABFYH3xWeQzDAh+22n9QQ/ys+Mv0FOHqI+hoqyp/DWPfnXOlbhFj79zaDMskLKXWoteiDLkyvMmjBDiEDzmB4drKJUW5Midpl8R/eRjl1f7Tt6MYNNrkjSFvEENmh/VRkQIDAQABAoIBAAjeNlkVqMhNVef+dVKN/M3NhKWyK8SD5jylgzHZZu5Gyern2EyZl+TXJkrE1K3Umqym9hfP0Y+4MR3AlS0HsindtkMs0xcf1OSIBKUYxWADwfGjvPF3mp5g8+PHogyjw7uEto3DZOnZQ1VS+bbd3WJflzyTt6hr9b6oEWiafcU5vsdt34gidomoNd0JRZCjXUrXhOtFjXT5/3jyIaz3J8JQj4MMn7YEU2LnCmWFYIJj48pbta7qS0p1c+y+DUAUS+i3wBUpYGH/n1FLfqISN60yWWi26E4jpxm0iLlODRLRL45Bh2vbLOIWfbBqsbdXaRCWGh7wTtH4Ixm3WtgPyEECgYEA6tOq3DfC7opk4V81RmP2Y2KN3i+gks938gl6IpH2jGbnpd9/BpqFcTsrrN25G3vyH0jha3mSIKisWpu+KupJe3iLS6Qu4DOwkVKpMStaUvbaWZD18dNoJIXOCc2pwBbqcjC5dW4+xfEbbZZ97wJDl+Jr88DVhOAF48ujY7iXXckCgYEAvsgVwc+jxEMv/7ZLvUThS4plUoDIIfDSTiLckM/4Si9M3Eqd5iLSwTmLy/e0PjQpOC44YvDgxLidOtTq03jHmkHDhPaPEQDbAwxlorc6REaezptX31Fy+VcNtgJyNEZBVNZ6Z+N3usGchaydup7rEoP0aL9glYmTGQoBU5AvmYkCgYEAoQZ8h34FQaXM/25WDerGLeiK9+0Y4xM5YNN22PXWZvi5qFjVtiWHk9nLXUK9iEc6BJHpYdV/ir2fUpKxmKQHt481la0EJ+HKFhzkwD7GNVXo7LomrKkXJNRMMRTCbmJIaf1Xb1sl2H45HLCxso+O4jv1YrS8td0YrNgbNmqCAWECgYATf4uJtVj9dAhx0AXJoEKOX+SfG/5YSa6UwcddHYQ4wMDQOugMXz6+9Myu2fAX82jLDx5ALnIqaNvY6W3ekxeDUnlTG4ffqwC3PQseKC/STo1ofEPs1Pw1M4p5MaLOaOfAbfMwrvTLp4IBv3SXYCqSR94w3m5WmirogjdEfRYAUQKBgQDqe7jPDZ382sOeJR7BppL2TVEGuZq4kljbGn5/SbY29488/L3aU8qAxRdjsgs6N1o/vma1gyQlonwk+5uwmoeq04N5ByoA5H7qbuCHg1mK8LgXgO+/Oq4Vvo7mNt/L0cVDMutDZSZGSTOLUyptwbpBGRZsxRKxmmSPqWYXr65TiA=='),
	('b7ef89af-9a21-4297-9aee-9e5d4ca8a5b2', '26bff43e-7f8e-43fb-a7ed-44b62f46b13d', 'secret', 'xd5m-gn37moZMYLBxAXDUt9dbLa-m7wgA8j7lGFaTUfPkMMvHbYmPoYjZwAVRZjwW5OjvaeskCIRyFXPcr_gtA'),
	('b83fb4f7-69f9-431f-97c3-9493dd81b618', 'e6fe2a24-74de-440f-8eb8-d1cb71c93f61', 'allow-default-scopes', 'true'),
	('bc7a4ea3-99e7-495e-84aa-e087ed9158f1', '197f5ce9-1f48-4e95-aa5e-1a0adefe17e7', 'keyUse', 'ENC'),
	('bd861b17-2b61-4c6e-9936-6b504853abf6', '197f5ce9-1f48-4e95-aa5e-1a0adefe17e7', 'privateKey', 'MIIEowIBAAKCAQEA1qMrHvJE7Uy+Jq8sVcgJV8WZEnAmEwCKTJAMQikFcnM8kI+VxyXJ5ddfak2QXdmQfcbaZ4NOy9W+dLJW8/tKMR/LR2JzNv6qFoi5AjcbpgAfF6JzuHLfJux/RLSGASYHtK9YaZwpo90iofshGCu6z36UjK8hUGjVfqUehp79za8t8meUrLTNP5trxF9XArAbNcODNPlqcq5jih0TsL0eco4wM+YpxnoMP+3a0a+/1NKacUNlMEZPJXxkhKYT131sKyD8VIZGMl3dCSlJMCPrhykuvxroMux76ScFwW+Nm9fTFlV8Bp7LbBi1xk8bkYrBhcFDloFFEAieZXFYYngkzwIDAQABAoIBABX/fX0Q+IEtAzTExwb4VwN2FDUAZdk/DhhAZTk8qhBrq3KSJ21/O9JA1h3M2vXQxWSBW8NjK6bBfovITBglEkVhoacXJIF87U1yRX/qylHOTmF7kV+p9Pawzhm/uheRCxYjfddpHW1sIyS12n5MhmYbbjnS/deXaauEBye7UNONJ4Y/6aqnaNXxOiQaBAzPB8ZCKiqg0dCzRZAoFOK/POQXj4QlbfMlSZHXSoduD3e2zHBstzIcMwrAC/UWJC9Qmpq919h1YEDFE3m1L4SpLg//H/I3VV1rLDMN0sFYmHtEHfyXs8+qBMMOb0WnGsPethXsrH2C16bqs1Aud+r3pnECgYEA7N9wNsY21DhmhdX7a4b/m5VoSRuNQk3OKeto9DVqrKTbHkxbjE3NOsGaMMgF//Cc7pPW3rNxEkvQe5NdJApKpxZD8LSbtthS3Kb9rqIWNvptWoNzSvwpNbjBpBy0D12WMQatgADZR4iG0Fwmcm0d+gLTLXpD/2gTra/xXtRE73ECgYEA5/gWDrYXfj1LEG13aLKz6bDWFnPKFsUgOEUHyuPA/JLkhiePnfOHwFtP/CGqJbaNdzJacUCkvTyL5k7A9sJGNE08IM96u/BJDZqy4l9TV59/UFXt8u1f2FTyGkUjMWmJWJpJJliSlgjJSzsTHf+SAuL6ZNHZSEHH63kRKcvouD8CgYEA0z5YxqBymURLOiQg+j4Eim3t+PTXP0BAEeZdTRyuK5VBzH1wT2m7EyFBLyvTHfMpISM7RpRkzFpMbUh4kgGTnfsfT/ThwWieTSWnqyvl/9XJ3CTKlx7WaZCBMKhgdCuUG9jGthcLrt20zPDBYN6rEyGdzzrwlT/t/MGh0l4ftCECgYAQjP7l5dAfTbhBX84ZRkbX8aoUyAYvQbleym4X59ApNUHg4Im6t1mEO/+k+bHuJuxJCOqzEe2vItbIg+34rTvdF3Wm0absKeTMd7EzvaFwqwymarsjjhQ8lzL+9Nfh+qm2DKwTdFZhJhdWqsxeY5HDavZ6ow5hvW/KOUUkOkxMeQKBgGjAysgvE/gnaHR8gkEUC1TappTupHopMAvPCOi1y+6RxPJ9u6zJoDkbuv2O4INVpl+QRDiL8VBNZ4WcHxsLlWrT2OHqZXLW7bAnQ7C+/Bcm63w94WQyDiCFcuT2nv+Bz0zBoJwgrMbjymjw7SmiblPBA9s4+FUFzYjueaXjh5O6'),
	('becc94ce-b0a8-427b-8af1-24d04602e6ff', 'a3ceb2eb-5667-424b-967f-7570ca1a0dbb', 'priority', '100'),
	('c12b2d32-99f0-470a-9c24-653b5dba2263', 'cf445cf1-e777-4491-b09f-8566fdad114f', 'host-sending-registration-request-must-match', 'true'),
	('c9f2d94c-f6e7-4421-b338-3f24b9403c4b', 'f3813659-b17b-4e01-b0c0-609093f59490', 'priority', '100'),
	('cb66278b-ebc3-4c83-9d52-9f6bfead0796', '852472e4-f3d5-4793-807f-08fb0947fcda', 'allowed-protocol-mapper-types', 'oidc-usermodel-attribute-mapper'),
	('d02f7768-ce14-46d2-a4f3-b603475485ce', 'd36df399-354d-4419-8b45-dcf42977d1d1', 'allowed-protocol-mapper-types', 'oidc-usermodel-property-mapper'),
	('d39d0921-74a0-4bd9-91fe-eb8ec658440a', 'ec7efbb9-1b6e-4e6c-ad63-e8462048f58b', 'algorithm', 'RSA-OAEP'),
	('d94f403f-1270-4ee1-be37-0f26110ae14d', '26bff43e-7f8e-43fb-a7ed-44b62f46b13d', 'algorithm', 'HS256'),
	('dc0c5a37-9da7-4f39-9c12-cff0ba3e6ef9', 'c9cc1407-e164-4e85-9fc6-131f28669446', 'certificate', 'MIICpTCCAY0CBgGF9ZbA0TANBgkqhkiG9w0BAQsFADAWMRQwEgYDVQQDDAt3dXJzdHdhc3NlcjAeFw0yMzAxMjcyMzM0MzVaFw0zMzAxMjcyMzM2MTVaMBYxFDASBgNVBAMMC3d1cnN0d2Fzc2VyMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArwCiIhJ9feOmESQZq52W/mfyEGe3IVorrByp2LuIB9dQYFaVHfpVcaCAau05ya0/2KiQ01xgeZ8XWj5bPTIezOIGFL4c03BQgUPZPCiVTCughaQoyz0inCRAMP1J8hw969aIobiEX8DhmLrHyu7UT6CJ119fXXmTgvQOizVLcdLzAclTLgU8y3sYhvvbfb8Wd4jfCiwsWeD8pGgrfSGKABFYH3xWeQzDAh+22n9QQ/ys+Mv0FOHqI+hoqyp/DWPfnXOlbhFj79zaDMskLKXWoteiDLkyvMmjBDiEDzmB4drKJUW5Midpl8R/eRjl1f7Tt6MYNNrkjSFvEENmh/VRkQIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQCsUuCoRO8b2He2C8saThSKSb2Zafrj53dvTsEl8Ib2ReBoblCO+3n+znS5NIGKzgCcJ3iCgNJ0OE6LrtHxH7O07DRfoN5LU7LMma3LcRFvW2nQ+rkKrIz6B1jJoj4IgOwqkBa6EtDq0WjnkdMzh4qNTn6/0XtEy14ayycpzHq0SNp6m9mdj6u8Rx8fu+b9A4ohD9gjOs1W/r2KEKhuEttbju5gtMhUARFGRwaWn0MEnMKx76/qILyDiWCDsVjflORpNNomYZcg1Q499/2aGIVK3sJtx/Rjq2buC72QCzDLrsOBr31BVzo8cZZlwLUUcnn/LwCy0qjC3dFu0N7rVXxk'),
	('e1c950ea-24be-4a6e-a85b-b4e360740686', '7a130231-485f-42ce-bfdd-2634b0581298', 'algorithm', 'HS256'),
	('e5553aaf-b005-4cbb-a2c0-400faf2ca9c6', '13219257-4ba7-4c22-a321-14df865ceaba', 'allowed-protocol-mapper-types', 'saml-user-attribute-mapper'),
	('e79f86c7-855b-453a-8a86-b9ed419987c9', 'ec7efbb9-1b6e-4e6c-ad63-e8462048f58b', 'priority', '100'),
	('e7b04508-1c6a-49ec-950d-53e7f7113804', 'd36df399-354d-4419-8b45-dcf42977d1d1', 'allowed-protocol-mapper-types', 'oidc-usermodel-attribute-mapper'),
	('e8196686-bf0d-4a2f-83df-b315238fb57f', '94198eb4-f969-4b93-bfe8-29d10fb20520', 'allowed-protocol-mapper-types', 'oidc-usermodel-property-mapper'),
	('e8bba2c0-df70-421c-9237-0111d7b2c1f0', 'c9cc1407-e164-4e85-9fc6-131f28669446', 'keyUse', 'SIG'),
	('ea6bb60c-9b74-46ac-86b5-b65700de0fb3', '7a130231-485f-42ce-bfdd-2634b0581298', 'kid', 'd38497fa-c6af-4e02-952d-d9da74478ba6'),
	('ead6a5ba-d333-42fd-bc56-b5c78938e50f', '13219257-4ba7-4c22-a321-14df865ceaba', 'allowed-protocol-mapper-types', 'oidc-usermodel-property-mapper'),
	('eb8a29da-607e-4c0d-9e71-dfb5f85319d3', '99190c3e-8a7d-418f-9bd6-73bdebd2a1ec', 'allow-default-scopes', 'true'),
	('ec428860-e64a-4255-a7ee-bf5c0a7cf8d4', '197f5ce9-1f48-4e95-aa5e-1a0adefe17e7', 'algorithm', 'RSA-OAEP'),
	('eee64e98-2c53-41f7-ba12-518e5dc84cf4', 'd36df399-354d-4419-8b45-dcf42977d1d1', 'allowed-protocol-mapper-types', 'saml-role-list-mapper'),
	('f3a4a5cc-f421-4947-b43c-9b6b198e6357', 'c9cc1407-e164-4e85-9fc6-131f28669446', 'priority', '100'),
	('f89a869e-07eb-418a-9280-fee1f0b59efb', 'a7a895de-c679-4af4-915c-600559331172', 'keyUse', 'SIG');

-- Exportiere Struktur von Tabelle keycloak.composite_role
CREATE TABLE IF NOT EXISTS `composite_role` (
  `COMPOSITE` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `CHILD_ROLE` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`COMPOSITE`,`CHILD_ROLE`),
  KEY `IDX_COMPOSITE` (`COMPOSITE`),
  KEY `IDX_COMPOSITE_CHILD` (`CHILD_ROLE`),
  CONSTRAINT `FK_A63WVEKFTU8JO1PNJ81E7MCE2` FOREIGN KEY (`COMPOSITE`) REFERENCES `keycloak_role` (`ID`),
  CONSTRAINT `FK_GR7THLLB9LU8Q4VQA4524JJY8` FOREIGN KEY (`CHILD_ROLE`) REFERENCES `keycloak_role` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.composite_role: ~80 rows (ungefähr)
DELETE FROM `composite_role`;
INSERT INTO `composite_role` (`COMPOSITE`, `CHILD_ROLE`) VALUES
	('1421012b-7fdd-4ca9-a9b2-4b453246a06c', '07004d4f-f090-405f-9165-7c4a78cf9948'),
	('1421012b-7fdd-4ca9-a9b2-4b453246a06c', '3808c986-eab6-4ad6-b05f-2e3866dfb81e'),
	('23eb6694-49b2-4fb4-b1d0-1bce51c5eb74', '0d39cea7-99be-4bae-9b6c-441c62409f95'),
	('23eb6694-49b2-4fb4-b1d0-1bce51c5eb74', '5afd0771-9254-44c6-9132-2e0892992254'),
	('23eb6694-49b2-4fb4-b1d0-1bce51c5eb74', '89a002c1-82f9-4999-8ea9-8a084fc5b406'),
	('23eb6694-49b2-4fb4-b1d0-1bce51c5eb74', '8f230571-7d8b-4885-a9ac-4ec5da2b7979'),
	('24ed7f0c-5282-4cb3-88b7-c14e8d3602d2', '03e4737e-0972-42f8-96f0-f9754cff01a9'),
	('24ed7f0c-5282-4cb3-88b7-c14e8d3602d2', '0e117675-56ad-4573-ae32-e041aeaa563e'),
	('24ed7f0c-5282-4cb3-88b7-c14e8d3602d2', '0e27cff3-02fa-456c-a077-5ad6c89a0c3f'),
	('24ed7f0c-5282-4cb3-88b7-c14e8d3602d2', '183a780a-478c-40f9-b639-63bb2f9fa188'),
	('24ed7f0c-5282-4cb3-88b7-c14e8d3602d2', '23652cea-4a07-49b1-b4f0-a1196c7ddbbe'),
	('24ed7f0c-5282-4cb3-88b7-c14e8d3602d2', '245c4cde-bbe6-41b4-8068-84ade85b620c'),
	('24ed7f0c-5282-4cb3-88b7-c14e8d3602d2', '2afb513c-3028-430a-9b35-f0ce5d11d717'),
	('24ed7f0c-5282-4cb3-88b7-c14e8d3602d2', '36798338-061a-4e0e-ac68-d3afd4534c75'),
	('24ed7f0c-5282-4cb3-88b7-c14e8d3602d2', '3d9637f8-77df-4a4c-a38c-6bd9ea7276c1'),
	('24ed7f0c-5282-4cb3-88b7-c14e8d3602d2', '58a329c0-e493-4554-8a16-bb10b0305c28'),
	('24ed7f0c-5282-4cb3-88b7-c14e8d3602d2', '925b7df8-67ba-415b-8d3c-e07f0f4cb06e'),
	('24ed7f0c-5282-4cb3-88b7-c14e8d3602d2', '985095f2-9e54-43cd-9e3c-c507c7218479'),
	('24ed7f0c-5282-4cb3-88b7-c14e8d3602d2', 'ac823ecf-f797-437c-9600-2c1adf768f86'),
	('24ed7f0c-5282-4cb3-88b7-c14e8d3602d2', 'b3e09f35-8853-4276-b194-ec28c2531fcb'),
	('24ed7f0c-5282-4cb3-88b7-c14e8d3602d2', 'b3eddf3d-250b-482a-88d4-20add1ad0002'),
	('24ed7f0c-5282-4cb3-88b7-c14e8d3602d2', 'dfe03b0b-d4b1-427b-b451-38eb681ee0ed'),
	('24ed7f0c-5282-4cb3-88b7-c14e8d3602d2', 'ec0c3c3a-6bf5-4258-9db6-3b39975c2bb6'),
	('24ed7f0c-5282-4cb3-88b7-c14e8d3602d2', 'fdc98d84-25a5-4975-bba3-17540c9e99a2'),
	('3043dd87-953b-4a47-8cfd-d8b5f2a8932c', '9d7fc4b7-71da-4b4d-a364-b869218bbd88'),
	('3583d5e6-1173-432c-981c-b7815cee20b9', '0b008376-b0d1-4241-9452-de351cd88717'),
	('3583d5e6-1173-432c-981c-b7815cee20b9', '24d1868a-7671-4224-9d11-41655a7aab27'),
	('5afd0771-9254-44c6-9132-2e0892992254', 'f6ea3bab-9428-4573-946c-2fc101660be1'),
	('925b7df8-67ba-415b-8d3c-e07f0f4cb06e', '0e117675-56ad-4573-ae32-e041aeaa563e'),
	('925b7df8-67ba-415b-8d3c-e07f0f4cb06e', '58a329c0-e493-4554-8a16-bb10b0305c28'),
	('93d5e01a-236c-4aa0-80d2-735ce5235fb9', '2da535f5-229f-48a7-a939-6f96351f4fd2'),
	('ace7c196-2f0c-4a12-9b17-88f4a21b918d', 'a64a81ce-b5d2-4275-854b-e3679edef93a'),
	('b3eddf3d-250b-482a-88d4-20add1ad0002', '2afb513c-3028-430a-9b35-f0ce5d11d717'),
	('c222fc2d-aa3a-4fbd-9e3c-27894f821c16', '7e7ea688-c340-4493-a173-5a56727114e8'),
	('ddad37c9-58a3-4aa4-bdf6-509c4065822f', '3043dd87-953b-4a47-8cfd-d8b5f2a8932c'),
	('ddad37c9-58a3-4aa4-bdf6-509c4065822f', '55f5e036-e10c-41d3-a3fa-a033e92b251b'),
	('ddad37c9-58a3-4aa4-bdf6-509c4065822f', 'b1c58ee6-4ce1-4e99-8eb2-031740c8e54e'),
	('ddad37c9-58a3-4aa4-bdf6-509c4065822f', 'f5b6cbfd-7a24-4020-a51b-2f36c0bdd0d9'),
	('de0b2758-4063-41aa-9f51-7e0114080e3a', '9a75a1c2-56c7-427d-82da-5d25228fae81'),
	('de0b2758-4063-41aa-9f51-7e0114080e3a', 'd05f01e5-497e-4724-8169-13ba9de8915c'),
	('e0733acb-438f-4ee9-8adc-20304869393b', '13d7527e-3038-47ad-95b7-07bea4469d5d'),
	('f2c9e482-411f-41f7-8c55-8bc5da6f1ab3', '89b619dc-2ed1-494a-84d2-e1510d9ab22e'),
	('f2c9e482-411f-41f7-8c55-8bc5da6f1ab3', 'f037a6b3-bab8-44a9-bcf4-16ad5aeab248'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', '081fe6ab-f9c8-4068-a0a0-402a09d1f3b8'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', '0b008376-b0d1-4241-9452-de351cd88717'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', '0d070596-f17e-4097-982a-f6321ab1fe9e'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', '24d1868a-7671-4224-9d11-41655a7aab27'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', '27a4cc8c-bafb-4a93-b174-87e032c421ee'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', '29673115-8376-45bb-9e7b-595ddfd3b57e'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', '2aea6f01-bb16-493e-bdc0-52b83bae5051'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', '2f8f1984-8e52-4cc4-ad84-f6b4d0275042'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', '34e6bc32-5596-4c61-8696-8525ce761e59'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', '3583d5e6-1173-432c-981c-b7815cee20b9'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', '417b3cc0-e882-4198-bf3a-9ef52104ae13'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', '477ab853-069f-4331-85f2-74f4ec6ed03e'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', '514e1be8-470f-4a41-8787-3fa664cd7910'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', '51e31922-bb2a-4129-8a82-c17a48a793f0'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', '536620cd-042d-4c61-b1ce-e779f8d00b28'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', '542b0798-98eb-4ce0-be4d-ccf5cef75513'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', '54abcfa3-4770-4bbe-a5d1-d2d68c04d7e4'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', '606ad618-f0a8-4543-b526-971af5b5448e'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', '63bcb921-b0b6-4df5-aabf-0aa91b11596a'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', '79093fb1-0093-47d2-97f4-572ff8cc2420'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', '79710760-aac2-4971-acc8-8d28154b28c7'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', '7e7ea688-c340-4493-a173-5a56727114e8'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', '8883719d-2359-4401-97c7-376e7aa6aa45'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', '9a75a1c2-56c7-427d-82da-5d25228fae81'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', 'a64a81ce-b5d2-4275-854b-e3679edef93a'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', 'ace7c196-2f0c-4a12-9b17-88f4a21b918d'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', 'ad565401-754f-4f0e-bf5f-e307c36783a5'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', 'b3e6fd4f-5175-42a4-b7c7-f7aa4fab418a'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', 'b715f2d0-3222-4ecd-a0ee-19ad64a204b1'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', 'c0a95923-ba7a-4457-9e86-c054bd15016a'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', 'c222fc2d-aa3a-4fbd-9e3c-27894f821c16'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', 'c295fe04-6fea-4e31-ada8-7cac547508bc'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', 'd05f01e5-497e-4724-8169-13ba9de8915c'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', 'd0b80bc7-6aaf-4c2f-906e-0deede603ef8'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', 'de0b2758-4063-41aa-9f51-7e0114080e3a'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', 'ea4cfb54-d2cd-4ff7-a046-61241163cfae'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', 'f65cc882-6e5d-43e1-a8de-6d7d8b3543b5');

-- Exportiere Struktur von Tabelle keycloak.credential
CREATE TABLE IF NOT EXISTS `credential` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `SALT` tinyblob DEFAULT NULL,
  `TYPE` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `USER_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `CREATED_DATE` bigint(20) DEFAULT NULL,
  `USER_LABEL` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `SECRET_DATA` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `CREDENTIAL_DATA` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_USER_CREDENTIAL` (`USER_ID`),
  CONSTRAINT `FK_PFYR0GLASQYL0DEI3KL69R6V0` FOREIGN KEY (`USER_ID`) REFERENCES `user_entity` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.credential: ~4 rows (ungefähr)
DELETE FROM `credential`;
INSERT INTO `credential` (`ID`, `SALT`, `TYPE`, `USER_ID`, `CREATED_DATE`, `USER_LABEL`, `SECRET_DATA`, `CREDENTIAL_DATA`, `PRIORITY`) VALUES
	('7246a64e-1896-473f-b4ac-b92fba42dff0', NULL, 'password', '305b4f46-733f-4d60-8fb3-c46731ffcb31', 1674993525977, 'My password', '{"value":"rXfWsAfDGP9S5xq7qNZSGNV9iTcgi081XwyAed5Drx7kOaTAsiiOhscr5ULG0ukO/gjnKSfRnefzRQnI9ZbDUg==","salt":"L0qsuQ2CyQehXHRX5fcaxA==","additionalParameters":{}}', '{"hashIterations":27500,"algorithm":"pbkdf2-sha256","additionalParameters":{}}', 10),
	('7477508c-35c5-4d5f-aa7b-6990ab62e964', NULL, 'password', '4ba00864-98ee-4a4f-a52c-f975ed7a4fd7', 1674993469960, 'My password', '{"value":"JlCt8rOiZ2+L1XdYtxb9ssQDWepBtFAHZqEew/KRV9tYS9L5q9xQhuBEQYJed5tzlSFE1wS2LSxPU3jyXtvNVg==","salt":"ucF2UjwJS/l0xGrbDDbE4g==","additionalParameters":{}}', '{"hashIterations":27500,"algorithm":"pbkdf2-sha256","additionalParameters":{}}', 10),
	('b5f275e9-1cff-440a-9f35-ecdecfc48602', NULL, 'password', '7356dc37-ad48-405e-8285-7fad444f8274', 1674993565930, 'My password', '{"value":"cwgAPYuYnVMFd1bGZfAqpEOH4zB26cZ5nBFGz8+IzPyY+v+vh5cLHVp6/gx6PYU0kvxfTZvLHiT/oiOIxcicXA==","salt":"vUMr/bwGdbWkMweGvZgmKw==","additionalParameters":{}}', '{"hashIterations":27500,"algorithm":"pbkdf2-sha256","additionalParameters":{}}', 10),
	('cd74e910-3398-4d84-97c5-42d1245cf8cc', NULL, 'password', 'e27e025c-ec23-4c15-9594-c6cee010598f', 1674862326472, NULL, '{"value":"2Wc8Qbheh3DZsHUH16pMuvpQ0l5INDPSFKu5LWcYFdS9knK4cHLp1bD1OznSEKX6SXXGKvEEWccqdLMhPkDY1Q==","salt":"2iQvpDqU/qxcd5fftR1vOg==","additionalParameters":{}}', '{"hashIterations":27500,"algorithm":"pbkdf2-sha256","additionalParameters":{}}', 10);

-- Exportiere Struktur von Tabelle keycloak.databasechangelog
CREATE TABLE IF NOT EXISTS `databasechangelog` (
  `ID` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `AUTHOR` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `FILENAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) COLLATE utf8mb4_bin NOT NULL,
  `MD5SUM` varchar(35) COLLATE utf8mb4_bin DEFAULT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `COMMENTS` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `TAG` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `LIQUIBASE` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL,
  `CONTEXTS` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `LABELS` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.databasechangelog: ~110 rows (ungefähr)
DELETE FROM `databasechangelog`;
INSERT INTO `databasechangelog` (`ID`, `AUTHOR`, `FILENAME`, `DATEEXECUTED`, `ORDEREXECUTED`, `EXECTYPE`, `MD5SUM`, `DESCRIPTION`, `COMMENTS`, `TAG`, `LIQUIBASE`, `CONTEXTS`, `LABELS`, `DEPLOYMENT_ID`) VALUES
	('1.0.0.Final-KEYCLOAK-5461', 'sthorger@redhat.com', 'META-INF/jpa-changelog-1.0.0.Final.xml', '2023-01-28 00:31:33', 1, 'EXECUTED', '8:bda77d94bf90182a1e30c24f1c155ec7', 'createTable tableName=APPLICATION_DEFAULT_ROLES; createTable tableName=CLIENT; createTable tableName=CLIENT_SESSION; createTable tableName=CLIENT_SESSION_ROLE; createTable tableName=COMPOSITE_ROLE; createTable tableName=CREDENTIAL; createTable tab...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('1.0.0.Final-KEYCLOAK-5461', 'sthorger@redhat.com', 'META-INF/db2-jpa-changelog-1.0.0.Final.xml', '2023-01-28 00:31:33', 2, 'MARK_RAN', '8:1ecb330f30986693d1cba9ab579fa219', 'createTable tableName=APPLICATION_DEFAULT_ROLES; createTable tableName=CLIENT; createTable tableName=CLIENT_SESSION; createTable tableName=CLIENT_SESSION_ROLE; createTable tableName=COMPOSITE_ROLE; createTable tableName=CREDENTIAL; createTable tab...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('1.1.0.Beta1', 'sthorger@redhat.com', 'META-INF/jpa-changelog-1.1.0.Beta1.xml', '2023-01-28 00:31:34', 3, 'EXECUTED', '8:cb7ace19bc6d959f305605d255d4c843', 'delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=CLIENT_ATTRIBUTES; createTable tableName=CLIENT_SESSION_NOTE; createTable tableName=APP_NODE_REGISTRATIONS; addColumn table...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('1.1.0.Final', 'sthorger@redhat.com', 'META-INF/jpa-changelog-1.1.0.Final.xml', '2023-01-28 00:31:34', 4, 'EXECUTED', '8:80230013e961310e6872e871be424a63', 'renameColumn newColumnName=EVENT_TIME, oldColumnName=TIME, tableName=EVENT_ENTITY', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('1.2.0.Beta1', 'psilva@redhat.com', 'META-INF/jpa-changelog-1.2.0.Beta1.xml', '2023-01-28 00:31:36', 5, 'EXECUTED', '8:67f4c20929126adc0c8e9bf48279d244', 'delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=PROTOCOL_MAPPER; createTable tableName=PROTOCOL_MAPPER_CONFIG; createTable tableName=...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('1.2.0.Beta1', 'psilva@redhat.com', 'META-INF/db2-jpa-changelog-1.2.0.Beta1.xml', '2023-01-28 00:31:36', 6, 'MARK_RAN', '8:7311018b0b8179ce14628ab412bb6783', 'delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=PROTOCOL_MAPPER; createTable tableName=PROTOCOL_MAPPER_CONFIG; createTable tableName=...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('1.2.0.RC1', 'bburke@redhat.com', 'META-INF/jpa-changelog-1.2.0.CR1.xml', '2023-01-28 00:31:38', 7, 'EXECUTED', '8:037ba1216c3640f8785ee6b8e7c8e3c1', 'delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=MIGRATION_MODEL; createTable tableName=IDENTITY_P...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('1.2.0.RC1', 'bburke@redhat.com', 'META-INF/db2-jpa-changelog-1.2.0.CR1.xml', '2023-01-28 00:31:38', 8, 'MARK_RAN', '8:7fe6ffe4af4df289b3157de32c624263', 'delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=MIGRATION_MODEL; createTable tableName=IDENTITY_P...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('1.2.0.Final', 'keycloak', 'META-INF/jpa-changelog-1.2.0.Final.xml', '2023-01-28 00:31:38', 9, 'EXECUTED', '8:9c136bc3187083a98745c7d03bc8a303', 'update tableName=CLIENT; update tableName=CLIENT; update tableName=CLIENT', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('1.3.0', 'bburke@redhat.com', 'META-INF/jpa-changelog-1.3.0.xml', '2023-01-28 00:31:40', 10, 'EXECUTED', '8:b5f09474dca81fb56a97cf5b6553d331', 'delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=ADMI...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('1.4.0', 'bburke@redhat.com', 'META-INF/jpa-changelog-1.4.0.xml', '2023-01-28 00:31:41', 11, 'EXECUTED', '8:ca924f31bd2a3b219fdcfe78c82dacf4', 'delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('1.4.0', 'bburke@redhat.com', 'META-INF/db2-jpa-changelog-1.4.0.xml', '2023-01-28 00:31:41', 12, 'MARK_RAN', '8:8acad7483e106416bcfa6f3b824a16cd', 'delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('1.5.0', 'bburke@redhat.com', 'META-INF/jpa-changelog-1.5.0.xml', '2023-01-28 00:31:41', 13, 'EXECUTED', '8:9b1266d17f4f87c78226f5055408fd5e', 'delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('1.6.1_from15', 'mposolda@redhat.com', 'META-INF/jpa-changelog-1.6.1.xml', '2023-01-28 00:31:41', 14, 'EXECUTED', '8:d80ec4ab6dbfe573550ff72396c7e910', 'addColumn tableName=REALM; addColumn tableName=KEYCLOAK_ROLE; addColumn tableName=CLIENT; createTable tableName=OFFLINE_USER_SESSION; createTable tableName=OFFLINE_CLIENT_SESSION; addPrimaryKey constraintName=CONSTRAINT_OFFL_US_SES_PK2, tableName=...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('1.6.1_from16-pre', 'mposolda@redhat.com', 'META-INF/jpa-changelog-1.6.1.xml', '2023-01-28 00:31:41', 15, 'MARK_RAN', '8:d86eb172171e7c20b9c849b584d147b2', 'delete tableName=OFFLINE_CLIENT_SESSION; delete tableName=OFFLINE_USER_SESSION', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('1.6.1_from16', 'mposolda@redhat.com', 'META-INF/jpa-changelog-1.6.1.xml', '2023-01-28 00:31:41', 16, 'MARK_RAN', '8:5735f46f0fa60689deb0ecdc2a0dea22', 'dropPrimaryKey constraintName=CONSTRAINT_OFFLINE_US_SES_PK, tableName=OFFLINE_USER_SESSION; dropPrimaryKey constraintName=CONSTRAINT_OFFLINE_CL_SES_PK, tableName=OFFLINE_CLIENT_SESSION; addColumn tableName=OFFLINE_USER_SESSION; update tableName=OF...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('1.6.1', 'mposolda@redhat.com', 'META-INF/jpa-changelog-1.6.1.xml', '2023-01-28 00:31:41', 17, 'EXECUTED', '8:d41d8cd98f00b204e9800998ecf8427e', 'empty', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('1.7.0', 'bburke@redhat.com', 'META-INF/jpa-changelog-1.7.0.xml', '2023-01-28 00:31:42', 18, 'EXECUTED', '8:5c1a8fd2014ac7fc43b90a700f117b23', 'createTable tableName=KEYCLOAK_GROUP; createTable tableName=GROUP_ROLE_MAPPING; createTable tableName=GROUP_ATTRIBUTE; createTable tableName=USER_GROUP_MEMBERSHIP; createTable tableName=REALM_DEFAULT_GROUPS; addColumn tableName=IDENTITY_PROVIDER; ...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('1.8.0', 'mposolda@redhat.com', 'META-INF/jpa-changelog-1.8.0.xml', '2023-01-28 00:31:43', 19, 'EXECUTED', '8:1f6c2c2dfc362aff4ed75b3f0ef6b331', 'addColumn tableName=IDENTITY_PROVIDER; createTable tableName=CLIENT_TEMPLATE; createTable tableName=CLIENT_TEMPLATE_ATTRIBUTES; createTable tableName=TEMPLATE_SCOPE_MAPPING; dropNotNullConstraint columnName=CLIENT_ID, tableName=PROTOCOL_MAPPER; ad...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('1.8.0-2', 'keycloak', 'META-INF/jpa-changelog-1.8.0.xml', '2023-01-28 00:31:43', 20, 'EXECUTED', '8:dee9246280915712591f83a127665107', 'dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; update tableName=CREDENTIAL', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('1.8.0', 'mposolda@redhat.com', 'META-INF/db2-jpa-changelog-1.8.0.xml', '2023-01-28 00:31:43', 21, 'MARK_RAN', '8:9eb2ee1fa8ad1c5e426421a6f8fdfa6a', 'addColumn tableName=IDENTITY_PROVIDER; createTable tableName=CLIENT_TEMPLATE; createTable tableName=CLIENT_TEMPLATE_ATTRIBUTES; createTable tableName=TEMPLATE_SCOPE_MAPPING; dropNotNullConstraint columnName=CLIENT_ID, tableName=PROTOCOL_MAPPER; ad...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('1.8.0-2', 'keycloak', 'META-INF/db2-jpa-changelog-1.8.0.xml', '2023-01-28 00:31:43', 22, 'MARK_RAN', '8:dee9246280915712591f83a127665107', 'dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; update tableName=CREDENTIAL', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('1.9.0', 'mposolda@redhat.com', 'META-INF/jpa-changelog-1.9.0.xml', '2023-01-28 00:31:43', 23, 'EXECUTED', '8:d9fa18ffa355320395b86270680dd4fe', 'update tableName=REALM; update tableName=REALM; update tableName=REALM; update tableName=REALM; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=REALM; update tableName=REALM; customChange; dr...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('1.9.1', 'keycloak', 'META-INF/jpa-changelog-1.9.1.xml', '2023-01-28 00:31:43', 24, 'EXECUTED', '8:90cff506fedb06141ffc1c71c4a1214c', 'modifyDataType columnName=PRIVATE_KEY, tableName=REALM; modifyDataType columnName=PUBLIC_KEY, tableName=REALM; modifyDataType columnName=CERTIFICATE, tableName=REALM', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('1.9.1', 'keycloak', 'META-INF/db2-jpa-changelog-1.9.1.xml', '2023-01-28 00:31:43', 25, 'MARK_RAN', '8:11a788aed4961d6d29c427c063af828c', 'modifyDataType columnName=PRIVATE_KEY, tableName=REALM; modifyDataType columnName=CERTIFICATE, tableName=REALM', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('1.9.2', 'keycloak', 'META-INF/jpa-changelog-1.9.2.xml', '2023-01-28 00:31:44', 26, 'EXECUTED', '8:a4218e51e1faf380518cce2af5d39b43', 'createIndex indexName=IDX_USER_EMAIL, tableName=USER_ENTITY; createIndex indexName=IDX_USER_ROLE_MAPPING, tableName=USER_ROLE_MAPPING; createIndex indexName=IDX_USER_GROUP_MAPPING, tableName=USER_GROUP_MEMBERSHIP; createIndex indexName=IDX_USER_CO...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('authz-2.0.0', 'psilva@redhat.com', 'META-INF/jpa-changelog-authz-2.0.0.xml', '2023-01-28 00:31:45', 27, 'EXECUTED', '8:d9e9a1bfaa644da9952456050f07bbdc', 'createTable tableName=RESOURCE_SERVER; addPrimaryKey constraintName=CONSTRAINT_FARS, tableName=RESOURCE_SERVER; addUniqueConstraint constraintName=UK_AU8TT6T700S9V50BU18WS5HA6, tableName=RESOURCE_SERVER; createTable tableName=RESOURCE_SERVER_RESOU...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('authz-2.5.1', 'psilva@redhat.com', 'META-INF/jpa-changelog-authz-2.5.1.xml', '2023-01-28 00:31:45', 28, 'EXECUTED', '8:d1bf991a6163c0acbfe664b615314505', 'update tableName=RESOURCE_SERVER_POLICY', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('2.1.0-KEYCLOAK-5461', 'bburke@redhat.com', 'META-INF/jpa-changelog-2.1.0.xml', '2023-01-28 00:31:46', 29, 'EXECUTED', '8:88a743a1e87ec5e30bf603da68058a8c', 'createTable tableName=BROKER_LINK; createTable tableName=FED_USER_ATTRIBUTE; createTable tableName=FED_USER_CONSENT; createTable tableName=FED_USER_CONSENT_ROLE; createTable tableName=FED_USER_CONSENT_PROT_MAPPER; createTable tableName=FED_USER_CR...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('2.2.0', 'bburke@redhat.com', 'META-INF/jpa-changelog-2.2.0.xml', '2023-01-28 00:31:47', 30, 'EXECUTED', '8:c5517863c875d325dea463d00ec26d7a', 'addColumn tableName=ADMIN_EVENT_ENTITY; createTable tableName=CREDENTIAL_ATTRIBUTE; createTable tableName=FED_CREDENTIAL_ATTRIBUTE; modifyDataType columnName=VALUE, tableName=CREDENTIAL; addForeignKeyConstraint baseTableName=FED_CREDENTIAL_ATTRIBU...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('2.3.0', 'bburke@redhat.com', 'META-INF/jpa-changelog-2.3.0.xml', '2023-01-28 00:31:47', 31, 'EXECUTED', '8:ada8b4833b74a498f376d7136bc7d327', 'createTable tableName=FEDERATED_USER; addPrimaryKey constraintName=CONSTR_FEDERATED_USER, tableName=FEDERATED_USER; dropDefaultValue columnName=TOTP, tableName=USER_ENTITY; dropColumn columnName=TOTP, tableName=USER_ENTITY; addColumn tableName=IDE...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('2.4.0', 'bburke@redhat.com', 'META-INF/jpa-changelog-2.4.0.xml', '2023-01-28 00:31:47', 32, 'EXECUTED', '8:b9b73c8ea7299457f99fcbb825c263ba', 'customChange', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('2.5.0', 'bburke@redhat.com', 'META-INF/jpa-changelog-2.5.0.xml', '2023-01-28 00:31:47', 33, 'EXECUTED', '8:07724333e625ccfcfc5adc63d57314f3', 'customChange; modifyDataType columnName=USER_ID, tableName=OFFLINE_USER_SESSION', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('2.5.0-unicode-oracle', 'hmlnarik@redhat.com', 'META-INF/jpa-changelog-2.5.0.xml', '2023-01-28 00:31:47', 34, 'MARK_RAN', '8:8b6fd445958882efe55deb26fc541a7b', 'modifyDataType columnName=DESCRIPTION, tableName=AUTHENTICATION_FLOW; modifyDataType columnName=DESCRIPTION, tableName=CLIENT_TEMPLATE; modifyDataType columnName=DESCRIPTION, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=DESCRIPTION,...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('2.5.0-unicode-other-dbs', 'hmlnarik@redhat.com', 'META-INF/jpa-changelog-2.5.0.xml', '2023-01-28 00:31:49', 35, 'EXECUTED', '8:29b29cfebfd12600897680147277a9d7', 'modifyDataType columnName=DESCRIPTION, tableName=AUTHENTICATION_FLOW; modifyDataType columnName=DESCRIPTION, tableName=CLIENT_TEMPLATE; modifyDataType columnName=DESCRIPTION, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=DESCRIPTION,...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('2.5.0-duplicate-email-support', 'slawomir@dabek.name', 'META-INF/jpa-changelog-2.5.0.xml', '2023-01-28 00:31:49', 36, 'EXECUTED', '8:73ad77ca8fd0410c7f9f15a471fa52bc', 'addColumn tableName=REALM', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('2.5.0-unique-group-names', 'hmlnarik@redhat.com', 'META-INF/jpa-changelog-2.5.0.xml', '2023-01-28 00:31:49', 37, 'EXECUTED', '8:64f27a6fdcad57f6f9153210f2ec1bdb', 'addUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('2.5.1', 'bburke@redhat.com', 'META-INF/jpa-changelog-2.5.1.xml', '2023-01-28 00:31:49', 38, 'EXECUTED', '8:27180251182e6c31846c2ddab4bc5781', 'addColumn tableName=FED_USER_CONSENT', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('3.0.0', 'bburke@redhat.com', 'META-INF/jpa-changelog-3.0.0.xml', '2023-01-28 00:31:49', 39, 'EXECUTED', '8:d56f201bfcfa7a1413eb3e9bc02978f9', 'addColumn tableName=IDENTITY_PROVIDER', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('3.2.0-fix', 'keycloak', 'META-INF/jpa-changelog-3.2.0.xml', '2023-01-28 00:31:49', 40, 'MARK_RAN', '8:91f5522bf6afdc2077dfab57fbd3455c', 'addNotNullConstraint columnName=REALM_ID, tableName=CLIENT_INITIAL_ACCESS', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('3.2.0-fix-with-keycloak-5416', 'keycloak', 'META-INF/jpa-changelog-3.2.0.xml', '2023-01-28 00:31:49', 41, 'MARK_RAN', '8:0f01b554f256c22caeb7d8aee3a1cdc8', 'dropIndex indexName=IDX_CLIENT_INIT_ACC_REALM, tableName=CLIENT_INITIAL_ACCESS; addNotNullConstraint columnName=REALM_ID, tableName=CLIENT_INITIAL_ACCESS; createIndex indexName=IDX_CLIENT_INIT_ACC_REALM, tableName=CLIENT_INITIAL_ACCESS', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('3.2.0-fix-offline-sessions', 'hmlnarik', 'META-INF/jpa-changelog-3.2.0.xml', '2023-01-28 00:31:49', 42, 'EXECUTED', '8:ab91cf9cee415867ade0e2df9651a947', 'customChange', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('3.2.0-fixed', 'keycloak', 'META-INF/jpa-changelog-3.2.0.xml', '2023-01-28 00:31:51', 43, 'EXECUTED', '8:ceac9b1889e97d602caf373eadb0d4b7', 'addColumn tableName=REALM; dropPrimaryKey constraintName=CONSTRAINT_OFFL_CL_SES_PK2, tableName=OFFLINE_CLIENT_SESSION; dropColumn columnName=CLIENT_SESSION_ID, tableName=OFFLINE_CLIENT_SESSION; addPrimaryKey constraintName=CONSTRAINT_OFFL_CL_SES_P...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('3.3.0', 'keycloak', 'META-INF/jpa-changelog-3.3.0.xml', '2023-01-28 00:31:51', 44, 'EXECUTED', '8:84b986e628fe8f7fd8fd3c275c5259f2', 'addColumn tableName=USER_ENTITY', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('authz-3.4.0.CR1-resource-server-pk-change-part1', 'glavoie@gmail.com', 'META-INF/jpa-changelog-authz-3.4.0.CR1.xml', '2023-01-28 00:31:51', 45, 'EXECUTED', '8:a164ae073c56ffdbc98a615493609a52', 'addColumn tableName=RESOURCE_SERVER_POLICY; addColumn tableName=RESOURCE_SERVER_RESOURCE; addColumn tableName=RESOURCE_SERVER_SCOPE', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('authz-3.4.0.CR1-resource-server-pk-change-part2-KEYCLOAK-6095', 'hmlnarik@redhat.com', 'META-INF/jpa-changelog-authz-3.4.0.CR1.xml', '2023-01-28 00:31:51', 46, 'EXECUTED', '8:70a2b4f1f4bd4dbf487114bdb1810e64', 'customChange', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('authz-3.4.0.CR1-resource-server-pk-change-part3-fixed', 'glavoie@gmail.com', 'META-INF/jpa-changelog-authz-3.4.0.CR1.xml', '2023-01-28 00:31:51', 47, 'MARK_RAN', '8:7be68b71d2f5b94b8df2e824f2860fa2', 'dropIndex indexName=IDX_RES_SERV_POL_RES_SERV, tableName=RESOURCE_SERVER_POLICY; dropIndex indexName=IDX_RES_SRV_RES_RES_SRV, tableName=RESOURCE_SERVER_RESOURCE; dropIndex indexName=IDX_RES_SRV_SCOPE_RES_SRV, tableName=RESOURCE_SERVER_SCOPE', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('authz-3.4.0.CR1-resource-server-pk-change-part3-fixed-nodropindex', 'glavoie@gmail.com', 'META-INF/jpa-changelog-authz-3.4.0.CR1.xml', '2023-01-28 00:31:52', 48, 'EXECUTED', '8:bab7c631093c3861d6cf6144cd944982', 'addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, tableName=RESOURCE_SERVER_POLICY; addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, tableName=RESOURCE_SERVER_RESOURCE; addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, ...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('authn-3.4.0.CR1-refresh-token-max-reuse', 'glavoie@gmail.com', 'META-INF/jpa-changelog-authz-3.4.0.CR1.xml', '2023-01-28 00:31:52', 49, 'EXECUTED', '8:fa809ac11877d74d76fe40869916daad', 'addColumn tableName=REALM', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('3.4.0', 'keycloak', 'META-INF/jpa-changelog-3.4.0.xml', '2023-01-28 00:31:53', 50, 'EXECUTED', '8:fac23540a40208f5f5e326f6ceb4d291', 'addPrimaryKey constraintName=CONSTRAINT_REALM_DEFAULT_ROLES, tableName=REALM_DEFAULT_ROLES; addPrimaryKey constraintName=CONSTRAINT_COMPOSITE_ROLE, tableName=COMPOSITE_ROLE; addPrimaryKey constraintName=CONSTR_REALM_DEFAULT_GROUPS, tableName=REALM...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('3.4.0-KEYCLOAK-5230', 'hmlnarik@redhat.com', 'META-INF/jpa-changelog-3.4.0.xml', '2023-01-28 00:31:53', 51, 'EXECUTED', '8:2612d1b8a97e2b5588c346e817307593', 'createIndex indexName=IDX_FU_ATTRIBUTE, tableName=FED_USER_ATTRIBUTE; createIndex indexName=IDX_FU_CONSENT, tableName=FED_USER_CONSENT; createIndex indexName=IDX_FU_CONSENT_RU, tableName=FED_USER_CONSENT; createIndex indexName=IDX_FU_CREDENTIAL, t...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('3.4.1', 'psilva@redhat.com', 'META-INF/jpa-changelog-3.4.1.xml', '2023-01-28 00:31:53', 52, 'EXECUTED', '8:9842f155c5db2206c88bcb5d1046e941', 'modifyDataType columnName=VALUE, tableName=CLIENT_ATTRIBUTES', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('3.4.2', 'keycloak', 'META-INF/jpa-changelog-3.4.2.xml', '2023-01-28 00:31:53', 53, 'EXECUTED', '8:2e12e06e45498406db72d5b3da5bbc76', 'update tableName=REALM', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('3.4.2-KEYCLOAK-5172', 'mkanis@redhat.com', 'META-INF/jpa-changelog-3.4.2.xml', '2023-01-28 00:31:53', 54, 'EXECUTED', '8:33560e7c7989250c40da3abdabdc75a4', 'update tableName=CLIENT', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('4.0.0-KEYCLOAK-6335', 'bburke@redhat.com', 'META-INF/jpa-changelog-4.0.0.xml', '2023-01-28 00:31:53', 55, 'EXECUTED', '8:87a8d8542046817a9107c7eb9cbad1cd', 'createTable tableName=CLIENT_AUTH_FLOW_BINDINGS; addPrimaryKey constraintName=C_CLI_FLOW_BIND, tableName=CLIENT_AUTH_FLOW_BINDINGS', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('4.0.0-CLEANUP-UNUSED-TABLE', 'bburke@redhat.com', 'META-INF/jpa-changelog-4.0.0.xml', '2023-01-28 00:31:53', 56, 'EXECUTED', '8:3ea08490a70215ed0088c273d776311e', 'dropTable tableName=CLIENT_IDENTITY_PROV_MAPPING', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('4.0.0-KEYCLOAK-6228', 'bburke@redhat.com', 'META-INF/jpa-changelog-4.0.0.xml', '2023-01-28 00:31:54', 57, 'EXECUTED', '8:2d56697c8723d4592ab608ce14b6ed68', 'dropUniqueConstraint constraintName=UK_JKUWUVD56ONTGSUHOGM8UEWRT, tableName=USER_CONSENT; dropNotNullConstraint columnName=CLIENT_ID, tableName=USER_CONSENT; addColumn tableName=USER_CONSENT; addUniqueConstraint constraintName=UK_JKUWUVD56ONTGSUHO...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('4.0.0-KEYCLOAK-5579-fixed', 'mposolda@redhat.com', 'META-INF/jpa-changelog-4.0.0.xml', '2023-01-28 00:31:56', 58, 'EXECUTED', '8:3e423e249f6068ea2bbe48bf907f9d86', 'dropForeignKeyConstraint baseTableName=CLIENT_TEMPLATE_ATTRIBUTES, constraintName=FK_CL_TEMPL_ATTR_TEMPL; renameTable newTableName=CLIENT_SCOPE_ATTRIBUTES, oldTableName=CLIENT_TEMPLATE_ATTRIBUTES; renameColumn newColumnName=SCOPE_ID, oldColumnName...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('authz-4.0.0.CR1', 'psilva@redhat.com', 'META-INF/jpa-changelog-authz-4.0.0.CR1.xml', '2023-01-28 00:31:57', 59, 'EXECUTED', '8:15cabee5e5df0ff099510a0fc03e4103', 'createTable tableName=RESOURCE_SERVER_PERM_TICKET; addPrimaryKey constraintName=CONSTRAINT_FAPMT, tableName=RESOURCE_SERVER_PERM_TICKET; addForeignKeyConstraint baseTableName=RESOURCE_SERVER_PERM_TICKET, constraintName=FK_FRSRHO213XCX4WNKOG82SSPMT...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('authz-4.0.0.Beta3', 'psilva@redhat.com', 'META-INF/jpa-changelog-authz-4.0.0.Beta3.xml', '2023-01-28 00:31:57', 60, 'EXECUTED', '8:4b80200af916ac54d2ffbfc47918ab0e', 'addColumn tableName=RESOURCE_SERVER_POLICY; addColumn tableName=RESOURCE_SERVER_PERM_TICKET; addForeignKeyConstraint baseTableName=RESOURCE_SERVER_PERM_TICKET, constraintName=FK_FRSRPO2128CX4WNKOG82SSRFY, referencedTableName=RESOURCE_SERVER_POLICY', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('authz-4.2.0.Final', 'mhajas@redhat.com', 'META-INF/jpa-changelog-authz-4.2.0.Final.xml', '2023-01-28 00:31:57', 61, 'EXECUTED', '8:66564cd5e168045d52252c5027485bbb', 'createTable tableName=RESOURCE_URIS; addForeignKeyConstraint baseTableName=RESOURCE_URIS, constraintName=FK_RESOURCE_SERVER_URIS, referencedTableName=RESOURCE_SERVER_RESOURCE; customChange; dropColumn columnName=URI, tableName=RESOURCE_SERVER_RESO...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('authz-4.2.0.Final-KEYCLOAK-9944', 'hmlnarik@redhat.com', 'META-INF/jpa-changelog-authz-4.2.0.Final.xml', '2023-01-28 00:31:57', 62, 'EXECUTED', '8:1c7064fafb030222be2bd16ccf690f6f', 'addPrimaryKey constraintName=CONSTRAINT_RESOUR_URIS_PK, tableName=RESOURCE_URIS', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('4.2.0-KEYCLOAK-6313', 'wadahiro@gmail.com', 'META-INF/jpa-changelog-4.2.0.xml', '2023-01-28 00:31:57', 63, 'EXECUTED', '8:2de18a0dce10cdda5c7e65c9b719b6e5', 'addColumn tableName=REQUIRED_ACTION_PROVIDER', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('4.3.0-KEYCLOAK-7984', 'wadahiro@gmail.com', 'META-INF/jpa-changelog-4.3.0.xml', '2023-01-28 00:31:57', 64, 'EXECUTED', '8:03e413dd182dcbd5c57e41c34d0ef682', 'update tableName=REQUIRED_ACTION_PROVIDER', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('4.6.0-KEYCLOAK-7950', 'psilva@redhat.com', 'META-INF/jpa-changelog-4.6.0.xml', '2023-01-28 00:31:57', 65, 'EXECUTED', '8:d27b42bb2571c18fbe3fe4e4fb7582a7', 'update tableName=RESOURCE_SERVER_RESOURCE', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('4.6.0-KEYCLOAK-8377', 'keycloak', 'META-INF/jpa-changelog-4.6.0.xml', '2023-01-28 00:31:57', 66, 'EXECUTED', '8:698baf84d9fd0027e9192717c2154fb8', 'createTable tableName=ROLE_ATTRIBUTE; addPrimaryKey constraintName=CONSTRAINT_ROLE_ATTRIBUTE_PK, tableName=ROLE_ATTRIBUTE; addForeignKeyConstraint baseTableName=ROLE_ATTRIBUTE, constraintName=FK_ROLE_ATTRIBUTE_ID, referencedTableName=KEYCLOAK_ROLE...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('4.6.0-KEYCLOAK-8555', 'gideonray@gmail.com', 'META-INF/jpa-changelog-4.6.0.xml', '2023-01-28 00:31:57', 67, 'EXECUTED', '8:ced8822edf0f75ef26eb51582f9a821a', 'createIndex indexName=IDX_COMPONENT_PROVIDER_TYPE, tableName=COMPONENT', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('4.7.0-KEYCLOAK-1267', 'sguilhen@redhat.com', 'META-INF/jpa-changelog-4.7.0.xml', '2023-01-28 00:31:57', 68, 'EXECUTED', '8:f0abba004cf429e8afc43056df06487d', 'addColumn tableName=REALM', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('4.7.0-KEYCLOAK-7275', 'keycloak', 'META-INF/jpa-changelog-4.7.0.xml', '2023-01-28 00:31:57', 69, 'EXECUTED', '8:6662f8b0b611caa359fcf13bf63b4e24', 'renameColumn newColumnName=CREATED_ON, oldColumnName=LAST_SESSION_REFRESH, tableName=OFFLINE_USER_SESSION; addNotNullConstraint columnName=CREATED_ON, tableName=OFFLINE_USER_SESSION; addColumn tableName=OFFLINE_USER_SESSION; customChange; createIn...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('4.8.0-KEYCLOAK-8835', 'sguilhen@redhat.com', 'META-INF/jpa-changelog-4.8.0.xml', '2023-01-28 00:31:57', 70, 'EXECUTED', '8:9e6b8009560f684250bdbdf97670d39e', 'addNotNullConstraint columnName=SSO_MAX_LIFESPAN_REMEMBER_ME, tableName=REALM; addNotNullConstraint columnName=SSO_IDLE_TIMEOUT_REMEMBER_ME, tableName=REALM', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('authz-7.0.0-KEYCLOAK-10443', 'psilva@redhat.com', 'META-INF/jpa-changelog-authz-7.0.0.xml', '2023-01-28 00:31:57', 71, 'EXECUTED', '8:4223f561f3b8dc655846562b57bb502e', 'addColumn tableName=RESOURCE_SERVER', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('8.0.0-adding-credential-columns', 'keycloak', 'META-INF/jpa-changelog-8.0.0.xml', '2023-01-28 00:31:57', 72, 'EXECUTED', '8:215a31c398b363ce383a2b301202f29e', 'addColumn tableName=CREDENTIAL; addColumn tableName=FED_USER_CREDENTIAL', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('8.0.0-updating-credential-data-not-oracle-fixed', 'keycloak', 'META-INF/jpa-changelog-8.0.0.xml', '2023-01-28 00:31:58', 73, 'EXECUTED', '8:83f7a671792ca98b3cbd3a1a34862d3d', 'update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('8.0.0-updating-credential-data-oracle-fixed', 'keycloak', 'META-INF/jpa-changelog-8.0.0.xml', '2023-01-28 00:31:58', 74, 'MARK_RAN', '8:f58ad148698cf30707a6efbdf8061aa7', 'update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('8.0.0-credential-cleanup-fixed', 'keycloak', 'META-INF/jpa-changelog-8.0.0.xml', '2023-01-28 00:31:58', 75, 'EXECUTED', '8:79e4fd6c6442980e58d52ffc3ee7b19c', 'dropDefaultValue columnName=COUNTER, tableName=CREDENTIAL; dropDefaultValue columnName=DIGITS, tableName=CREDENTIAL; dropDefaultValue columnName=PERIOD, tableName=CREDENTIAL; dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; dropColumn ...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('8.0.0-resource-tag-support', 'keycloak', 'META-INF/jpa-changelog-8.0.0.xml', '2023-01-28 00:31:58', 76, 'EXECUTED', '8:87af6a1e6d241ca4b15801d1f86a297d', 'addColumn tableName=MIGRATION_MODEL; createIndex indexName=IDX_UPDATE_TIME, tableName=MIGRATION_MODEL', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('9.0.0-always-display-client', 'keycloak', 'META-INF/jpa-changelog-9.0.0.xml', '2023-01-28 00:31:58', 77, 'EXECUTED', '8:b44f8d9b7b6ea455305a6d72a200ed15', 'addColumn tableName=CLIENT', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('9.0.0-drop-constraints-for-column-increase', 'keycloak', 'META-INF/jpa-changelog-9.0.0.xml', '2023-01-28 00:31:58', 78, 'MARK_RAN', '8:2d8ed5aaaeffd0cb004c046b4a903ac5', 'dropUniqueConstraint constraintName=UK_FRSR6T700S9V50BU18WS5PMT, tableName=RESOURCE_SERVER_PERM_TICKET; dropUniqueConstraint constraintName=UK_FRSR6T700S9V50BU18WS5HA6, tableName=RESOURCE_SERVER_RESOURCE; dropPrimaryKey constraintName=CONSTRAINT_O...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('9.0.0-increase-column-size-federated-fk', 'keycloak', 'META-INF/jpa-changelog-9.0.0.xml', '2023-01-28 00:31:59', 79, 'EXECUTED', '8:e290c01fcbc275326c511633f6e2acde', 'modifyDataType columnName=CLIENT_ID, tableName=FED_USER_CONSENT; modifyDataType columnName=CLIENT_REALM_CONSTRAINT, tableName=KEYCLOAK_ROLE; modifyDataType columnName=OWNER, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=CLIENT_ID, ta...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('9.0.0-recreate-constraints-after-column-increase', 'keycloak', 'META-INF/jpa-changelog-9.0.0.xml', '2023-01-28 00:31:59', 80, 'MARK_RAN', '8:c9db8784c33cea210872ac2d805439f8', 'addNotNullConstraint columnName=CLIENT_ID, tableName=OFFLINE_CLIENT_SESSION; addNotNullConstraint columnName=OWNER, tableName=RESOURCE_SERVER_PERM_TICKET; addNotNullConstraint columnName=REQUESTER, tableName=RESOURCE_SERVER_PERM_TICKET; addNotNull...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('9.0.1-add-index-to-client.client_id', 'keycloak', 'META-INF/jpa-changelog-9.0.1.xml', '2023-01-28 00:31:59', 81, 'EXECUTED', '8:95b676ce8fc546a1fcfb4c92fae4add5', 'createIndex indexName=IDX_CLIENT_ID, tableName=CLIENT', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('9.0.1-KEYCLOAK-12579-drop-constraints', 'keycloak', 'META-INF/jpa-changelog-9.0.1.xml', '2023-01-28 00:31:59', 82, 'MARK_RAN', '8:38a6b2a41f5651018b1aca93a41401e5', 'dropUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('9.0.1-KEYCLOAK-12579-add-not-null-constraint', 'keycloak', 'META-INF/jpa-changelog-9.0.1.xml', '2023-01-28 00:31:59', 83, 'EXECUTED', '8:3fb99bcad86a0229783123ac52f7609c', 'addNotNullConstraint columnName=PARENT_GROUP, tableName=KEYCLOAK_GROUP', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('9.0.1-KEYCLOAK-12579-recreate-constraints', 'keycloak', 'META-INF/jpa-changelog-9.0.1.xml', '2023-01-28 00:31:59', 84, 'MARK_RAN', '8:64f27a6fdcad57f6f9153210f2ec1bdb', 'addUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('9.0.1-add-index-to-events', 'keycloak', 'META-INF/jpa-changelog-9.0.1.xml', '2023-01-28 00:31:59', 85, 'EXECUTED', '8:ab4f863f39adafd4c862f7ec01890abc', 'createIndex indexName=IDX_EVENT_TIME, tableName=EVENT_ENTITY', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('map-remove-ri', 'keycloak', 'META-INF/jpa-changelog-11.0.0.xml', '2023-01-28 00:31:59', 86, 'EXECUTED', '8:13c419a0eb336e91ee3a3bf8fda6e2a7', 'dropForeignKeyConstraint baseTableName=REALM, constraintName=FK_TRAF444KK6QRKMS7N56AIWQ5Y; dropForeignKeyConstraint baseTableName=KEYCLOAK_ROLE, constraintName=FK_KJHO5LE2C0RAL09FL8CM9WFW9', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('map-remove-ri', 'keycloak', 'META-INF/jpa-changelog-12.0.0.xml', '2023-01-28 00:31:59', 87, 'EXECUTED', '8:e3fb1e698e0471487f51af1ed80fe3ac', 'dropForeignKeyConstraint baseTableName=REALM_DEFAULT_GROUPS, constraintName=FK_DEF_GROUPS_GROUP; dropForeignKeyConstraint baseTableName=REALM_DEFAULT_ROLES, constraintName=FK_H4WPD7W4HSOOLNI3H0SW7BTJE; dropForeignKeyConstraint baseTableName=CLIENT...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('12.1.0-add-realm-localization-table', 'keycloak', 'META-INF/jpa-changelog-12.0.0.xml', '2023-01-28 00:31:59', 88, 'EXECUTED', '8:babadb686aab7b56562817e60bf0abd0', 'createTable tableName=REALM_LOCALIZATIONS; addPrimaryKey tableName=REALM_LOCALIZATIONS', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('default-roles', 'keycloak', 'META-INF/jpa-changelog-13.0.0.xml', '2023-01-28 00:31:59', 89, 'EXECUTED', '8:72d03345fda8e2f17093d08801947773', 'addColumn tableName=REALM; customChange', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('default-roles-cleanup', 'keycloak', 'META-INF/jpa-changelog-13.0.0.xml', '2023-01-28 00:31:59', 90, 'EXECUTED', '8:61c9233951bd96ffecd9ba75f7d978a4', 'dropTable tableName=REALM_DEFAULT_ROLES; dropTable tableName=CLIENT_DEFAULT_ROLES', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('13.0.0-KEYCLOAK-16844', 'keycloak', 'META-INF/jpa-changelog-13.0.0.xml', '2023-01-28 00:31:59', 91, 'EXECUTED', '8:ea82e6ad945cec250af6372767b25525', 'createIndex indexName=IDX_OFFLINE_USS_PRELOAD, tableName=OFFLINE_USER_SESSION', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('map-remove-ri-13.0.0', 'keycloak', 'META-INF/jpa-changelog-13.0.0.xml', '2023-01-28 00:32:00', 92, 'EXECUTED', '8:d3f4a33f41d960ddacd7e2ef30d126b3', 'dropForeignKeyConstraint baseTableName=DEFAULT_CLIENT_SCOPE, constraintName=FK_R_DEF_CLI_SCOPE_SCOPE; dropForeignKeyConstraint baseTableName=CLIENT_SCOPE_CLIENT, constraintName=FK_C_CLI_SCOPE_SCOPE; dropForeignKeyConstraint baseTableName=CLIENT_SC...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('13.0.0-KEYCLOAK-17992-drop-constraints', 'keycloak', 'META-INF/jpa-changelog-13.0.0.xml', '2023-01-28 00:32:00', 93, 'MARK_RAN', '8:1284a27fbd049d65831cb6fc07c8a783', 'dropPrimaryKey constraintName=C_CLI_SCOPE_BIND, tableName=CLIENT_SCOPE_CLIENT; dropIndex indexName=IDX_CLSCOPE_CL, tableName=CLIENT_SCOPE_CLIENT; dropIndex indexName=IDX_CL_CLSCOPE, tableName=CLIENT_SCOPE_CLIENT', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('13.0.0-increase-column-size-federated', 'keycloak', 'META-INF/jpa-changelog-13.0.0.xml', '2023-01-28 00:32:00', 94, 'EXECUTED', '8:9d11b619db2ae27c25853b8a37cd0dea', 'modifyDataType columnName=CLIENT_ID, tableName=CLIENT_SCOPE_CLIENT; modifyDataType columnName=SCOPE_ID, tableName=CLIENT_SCOPE_CLIENT', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('13.0.0-KEYCLOAK-17992-recreate-constraints', 'keycloak', 'META-INF/jpa-changelog-13.0.0.xml', '2023-01-28 00:32:00', 95, 'MARK_RAN', '8:3002bb3997451bb9e8bac5c5cd8d6327', 'addNotNullConstraint columnName=CLIENT_ID, tableName=CLIENT_SCOPE_CLIENT; addNotNullConstraint columnName=SCOPE_ID, tableName=CLIENT_SCOPE_CLIENT; addPrimaryKey constraintName=C_CLI_SCOPE_BIND, tableName=CLIENT_SCOPE_CLIENT; createIndex indexName=...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('json-string-accomodation-fixed', 'keycloak', 'META-INF/jpa-changelog-13.0.0.xml', '2023-01-28 00:32:00', 96, 'EXECUTED', '8:dfbee0d6237a23ef4ccbb7a4e063c163', 'addColumn tableName=REALM_ATTRIBUTE; update tableName=REALM_ATTRIBUTE; dropColumn columnName=VALUE, tableName=REALM_ATTRIBUTE; renameColumn newColumnName=VALUE, oldColumnName=VALUE_NEW, tableName=REALM_ATTRIBUTE', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('14.0.0-KEYCLOAK-11019', 'keycloak', 'META-INF/jpa-changelog-14.0.0.xml', '2023-01-28 00:32:00', 97, 'EXECUTED', '8:75f3e372df18d38c62734eebb986b960', 'createIndex indexName=IDX_OFFLINE_CSS_PRELOAD, tableName=OFFLINE_CLIENT_SESSION; createIndex indexName=IDX_OFFLINE_USS_BY_USER, tableName=OFFLINE_USER_SESSION; createIndex indexName=IDX_OFFLINE_USS_BY_USERSESS, tableName=OFFLINE_USER_SESSION', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('14.0.0-KEYCLOAK-18286', 'keycloak', 'META-INF/jpa-changelog-14.0.0.xml', '2023-01-28 00:32:00', 98, 'MARK_RAN', '8:7fee73eddf84a6035691512c85637eef', 'createIndex indexName=IDX_CLIENT_ATT_BY_NAME_VALUE, tableName=CLIENT_ATTRIBUTES', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('14.0.0-KEYCLOAK-18286-revert', 'keycloak', 'META-INF/jpa-changelog-14.0.0.xml', '2023-01-28 00:32:00', 99, 'MARK_RAN', '8:7a11134ab12820f999fbf3bb13c3adc8', 'dropIndex indexName=IDX_CLIENT_ATT_BY_NAME_VALUE, tableName=CLIENT_ATTRIBUTES', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('14.0.0-KEYCLOAK-18286-supported-dbs', 'keycloak', 'META-INF/jpa-changelog-14.0.0.xml', '2023-01-28 00:32:00', 100, 'EXECUTED', '8:f43dfba07ba249d5d932dc489fd2b886', 'createIndex indexName=IDX_CLIENT_ATT_BY_NAME_VALUE, tableName=CLIENT_ATTRIBUTES', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('14.0.0-KEYCLOAK-18286-unsupported-dbs', 'keycloak', 'META-INF/jpa-changelog-14.0.0.xml', '2023-01-28 00:32:00', 101, 'MARK_RAN', '8:18186f0008b86e0f0f49b0c4d0e842ac', 'createIndex indexName=IDX_CLIENT_ATT_BY_NAME_VALUE, tableName=CLIENT_ATTRIBUTES', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('KEYCLOAK-17267-add-index-to-user-attributes', 'keycloak', 'META-INF/jpa-changelog-14.0.0.xml', '2023-01-28 00:32:00', 102, 'EXECUTED', '8:09c2780bcb23b310a7019d217dc7b433', 'createIndex indexName=IDX_USER_ATTRIBUTE_NAME, tableName=USER_ATTRIBUTE', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('KEYCLOAK-18146-add-saml-art-binding-identifier', 'keycloak', 'META-INF/jpa-changelog-14.0.0.xml', '2023-01-28 00:32:00', 103, 'EXECUTED', '8:276a44955eab693c970a42880197fff2', 'customChange', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('15.0.0-KEYCLOAK-18467', 'keycloak', 'META-INF/jpa-changelog-15.0.0.xml', '2023-01-28 00:32:00', 104, 'EXECUTED', '8:ba8ee3b694d043f2bfc1a1079d0760d7', 'addColumn tableName=REALM_LOCALIZATIONS; update tableName=REALM_LOCALIZATIONS; dropColumn columnName=TEXTS, tableName=REALM_LOCALIZATIONS; renameColumn newColumnName=TEXTS, oldColumnName=TEXTS_NEW, tableName=REALM_LOCALIZATIONS; addNotNullConstrai...', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('17.0.0-9562', 'keycloak', 'META-INF/jpa-changelog-17.0.0.xml', '2023-01-28 00:32:00', 105, 'EXECUTED', '8:5e06b1d75f5d17685485e610c2851b17', 'createIndex indexName=IDX_USER_SERVICE_ACCOUNT, tableName=USER_ENTITY', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('18.0.0-10625-IDX_ADMIN_EVENT_TIME', 'keycloak', 'META-INF/jpa-changelog-18.0.0.xml', '2023-01-28 00:32:00', 106, 'EXECUTED', '8:4b80546c1dc550ac552ee7b24a4ab7c0', 'createIndex indexName=IDX_ADMIN_EVENT_TIME, tableName=ADMIN_EVENT_ENTITY', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('19.0.0-10135', 'keycloak', 'META-INF/jpa-changelog-19.0.0.xml', '2023-01-28 00:32:00', 107, 'EXECUTED', '8:af510cd1bb2ab6339c45372f3e491696', 'customChange', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('20.0.0-12964-supported-dbs', 'keycloak', 'META-INF/jpa-changelog-20.0.0.xml', '2023-01-28 00:32:00', 108, 'EXECUTED', '8:d00f99ed899c4d2ae9117e282badbef5', 'createIndex indexName=IDX_GROUP_ATT_BY_NAME_VALUE, tableName=GROUP_ATTRIBUTE', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('20.0.0-12964-unsupported-dbs', 'keycloak', 'META-INF/jpa-changelog-20.0.0.xml', '2023-01-28 00:32:00', 109, 'MARK_RAN', '8:314e803baf2f1ec315b3464e398b8247', 'createIndex indexName=IDX_GROUP_ATT_BY_NAME_VALUE, tableName=GROUP_ATTRIBUTE', '', NULL, '4.8.0', NULL, NULL, '4862288956'),
	('client-attributes-string-accomodation-fixed', 'keycloak', 'META-INF/jpa-changelog-20.0.0.xml', '2023-01-28 00:32:00', 110, 'EXECUTED', '8:56e4677e7e12556f70b604c573840100', 'addColumn tableName=CLIENT_ATTRIBUTES; update tableName=CLIENT_ATTRIBUTES; dropColumn columnName=VALUE, tableName=CLIENT_ATTRIBUTES; renameColumn newColumnName=VALUE, oldColumnName=VALUE_NEW, tableName=CLIENT_ATTRIBUTES', '', NULL, '4.8.0', NULL, NULL, '4862288956');

-- Exportiere Struktur von Tabelle keycloak.databasechangeloglock
CREATE TABLE IF NOT EXISTS `databasechangeloglock` (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.databasechangeloglock: ~3 rows (ungefähr)
DELETE FROM `databasechangeloglock`;
INSERT INTO `databasechangeloglock` (`ID`, `LOCKED`, `LOCKGRANTED`, `LOCKEDBY`) VALUES
	(1, b'0', NULL, NULL),
	(1000, b'0', NULL, NULL),
	(1001, b'0', NULL, NULL);

-- Exportiere Struktur von Tabelle keycloak.default_client_scope
CREATE TABLE IF NOT EXISTS `default_client_scope` (
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `SCOPE_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `DEFAULT_SCOPE` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`REALM_ID`,`SCOPE_ID`),
  KEY `IDX_DEFCLS_REALM` (`REALM_ID`),
  KEY `IDX_DEFCLS_SCOPE` (`SCOPE_ID`),
  CONSTRAINT `FK_R_DEF_CLI_SCOPE_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `realm` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.default_client_scope: ~20 rows (ungefähr)
DELETE FROM `default_client_scope`;
INSERT INTO `default_client_scope` (`REALM_ID`, `SCOPE_ID`, `DEFAULT_SCOPE`) VALUES
	('23d8cb0f-53b6-4402-a9f9-072c402d5927', '1e109bdc-2125-41fc-a9cd-988e4926ebba', b'1'),
	('23d8cb0f-53b6-4402-a9f9-072c402d5927', '4e92b233-7271-4ccf-a9b7-bd5b43c9a573', b'1'),
	('23d8cb0f-53b6-4402-a9f9-072c402d5927', '51444a9d-afea-400e-b37a-8624ac52ff3d', b'0'),
	('23d8cb0f-53b6-4402-a9f9-072c402d5927', '6d92611e-48b4-4641-9191-de1cd49b50cd', b'0'),
	('23d8cb0f-53b6-4402-a9f9-072c402d5927', '79ae1850-0614-45c7-beed-41698ef668ee', b'1'),
	('23d8cb0f-53b6-4402-a9f9-072c402d5927', '8afddcb9-4bba-4dc3-a54f-fa313a2fcdf7', b'0'),
	('23d8cb0f-53b6-4402-a9f9-072c402d5927', 'ac77881e-2cf5-4db1-8ee9-bef2ac8dcb52', b'1'),
	('23d8cb0f-53b6-4402-a9f9-072c402d5927', 'ae70759a-d8f4-4c19-9ed4-0fa8bdb796fd', b'1'),
	('23d8cb0f-53b6-4402-a9f9-072c402d5927', 'b15a69e0-6f2e-4948-8feb-b7fbd0e4e36c', b'1'),
	('23d8cb0f-53b6-4402-a9f9-072c402d5927', 'c74d2f87-1571-40ec-b831-04c17af5ec75', b'0'),
	('47958335-5c70-4603-abe7-b887490d7b1c', '1326cc85-b925-4654-a935-74af8ef758eb', b'1'),
	('47958335-5c70-4603-abe7-b887490d7b1c', '272fc934-f3a7-4268-83ac-2acc745d87f1', b'0'),
	('47958335-5c70-4603-abe7-b887490d7b1c', '381dfb0c-222f-4c9a-b3bb-336e4987f292', b'1'),
	('47958335-5c70-4603-abe7-b887490d7b1c', '5e5faf95-a740-4ecd-b7ff-757299ed729b', b'1'),
	('47958335-5c70-4603-abe7-b887490d7b1c', '7d79994b-6c92-452b-9e2f-83840e62c837', b'0'),
	('47958335-5c70-4603-abe7-b887490d7b1c', '81fb9c52-5475-4410-86d5-55dee623e213', b'1'),
	('47958335-5c70-4603-abe7-b887490d7b1c', 'b032d9af-b13c-4a76-9069-6b687cc7ac6c', b'1'),
	('47958335-5c70-4603-abe7-b887490d7b1c', 'b880b5f7-bb9c-4fbf-930b-db668b9907ab', b'0'),
	('47958335-5c70-4603-abe7-b887490d7b1c', 'cd2bd538-424e-4b02-884e-26620e48ce31', b'1'),
	('47958335-5c70-4603-abe7-b887490d7b1c', 'f558df31-2459-4a39-b429-fc9e8d1edfb6', b'0');

-- Exportiere Struktur von Tabelle keycloak.event_entity
CREATE TABLE IF NOT EXISTS `event_entity` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `CLIENT_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `DETAILS_JSON` text COLLATE utf8mb4_bin DEFAULT NULL,
  `ERROR` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `IP_ADDRESS` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `REALM_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `SESSION_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `EVENT_TIME` bigint(20) DEFAULT NULL,
  `TYPE` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `USER_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_EVENT_TIME` (`REALM_ID`,`EVENT_TIME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.event_entity: ~0 rows (ungefähr)
DELETE FROM `event_entity`;

-- Exportiere Struktur von Tabelle keycloak.federated_identity
CREATE TABLE IF NOT EXISTS `federated_identity` (
  `IDENTITY_PROVIDER` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `FEDERATED_USER_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `FEDERATED_USERNAME` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `TOKEN` text COLLATE utf8mb4_bin DEFAULT NULL,
  `USER_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`IDENTITY_PROVIDER`,`USER_ID`),
  KEY `IDX_FEDIDENTITY_USER` (`USER_ID`),
  KEY `IDX_FEDIDENTITY_FEDUSER` (`FEDERATED_USER_ID`),
  CONSTRAINT `FK404288B92EF007A6` FOREIGN KEY (`USER_ID`) REFERENCES `user_entity` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.federated_identity: ~0 rows (ungefähr)
DELETE FROM `federated_identity`;

-- Exportiere Struktur von Tabelle keycloak.federated_user
CREATE TABLE IF NOT EXISTS `federated_user` (
  `ID` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `STORAGE_PROVIDER_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.federated_user: ~0 rows (ungefähr)
DELETE FROM `federated_user`;

-- Exportiere Struktur von Tabelle keycloak.fed_user_attribute
CREATE TABLE IF NOT EXISTS `fed_user_attribute` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `USER_ID` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `STORAGE_PROVIDER_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `VALUE` text COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_FU_ATTRIBUTE` (`USER_ID`,`REALM_ID`,`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.fed_user_attribute: ~0 rows (ungefähr)
DELETE FROM `fed_user_attribute`;

-- Exportiere Struktur von Tabelle keycloak.fed_user_consent
CREATE TABLE IF NOT EXISTS `fed_user_consent` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `CLIENT_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `USER_ID` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `STORAGE_PROVIDER_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `CREATED_DATE` bigint(20) DEFAULT NULL,
  `LAST_UPDATED_DATE` bigint(20) DEFAULT NULL,
  `CLIENT_STORAGE_PROVIDER` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `EXTERNAL_CLIENT_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_FU_CONSENT` (`USER_ID`,`CLIENT_ID`),
  KEY `IDX_FU_CONSENT_RU` (`REALM_ID`,`USER_ID`),
  KEY `IDX_FU_CNSNT_EXT` (`USER_ID`,`CLIENT_STORAGE_PROVIDER`,`EXTERNAL_CLIENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.fed_user_consent: ~0 rows (ungefähr)
DELETE FROM `fed_user_consent`;

-- Exportiere Struktur von Tabelle keycloak.fed_user_consent_cl_scope
CREATE TABLE IF NOT EXISTS `fed_user_consent_cl_scope` (
  `USER_CONSENT_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `SCOPE_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`USER_CONSENT_ID`,`SCOPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.fed_user_consent_cl_scope: ~0 rows (ungefähr)
DELETE FROM `fed_user_consent_cl_scope`;

-- Exportiere Struktur von Tabelle keycloak.fed_user_credential
CREATE TABLE IF NOT EXISTS `fed_user_credential` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `SALT` tinyblob DEFAULT NULL,
  `TYPE` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `CREATED_DATE` bigint(20) DEFAULT NULL,
  `USER_ID` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `STORAGE_PROVIDER_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `USER_LABEL` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `SECRET_DATA` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `CREDENTIAL_DATA` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_FU_CREDENTIAL` (`USER_ID`,`TYPE`),
  KEY `IDX_FU_CREDENTIAL_RU` (`REALM_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.fed_user_credential: ~0 rows (ungefähr)
DELETE FROM `fed_user_credential`;

-- Exportiere Struktur von Tabelle keycloak.fed_user_group_membership
CREATE TABLE IF NOT EXISTS `fed_user_group_membership` (
  `GROUP_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `USER_ID` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `STORAGE_PROVIDER_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`GROUP_ID`,`USER_ID`),
  KEY `IDX_FU_GROUP_MEMBERSHIP` (`USER_ID`,`GROUP_ID`),
  KEY `IDX_FU_GROUP_MEMBERSHIP_RU` (`REALM_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.fed_user_group_membership: ~0 rows (ungefähr)
DELETE FROM `fed_user_group_membership`;

-- Exportiere Struktur von Tabelle keycloak.fed_user_required_action
CREATE TABLE IF NOT EXISTS `fed_user_required_action` (
  `REQUIRED_ACTION` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT ' ',
  `USER_ID` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `STORAGE_PROVIDER_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`REQUIRED_ACTION`,`USER_ID`),
  KEY `IDX_FU_REQUIRED_ACTION` (`USER_ID`,`REQUIRED_ACTION`),
  KEY `IDX_FU_REQUIRED_ACTION_RU` (`REALM_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.fed_user_required_action: ~0 rows (ungefähr)
DELETE FROM `fed_user_required_action`;

-- Exportiere Struktur von Tabelle keycloak.fed_user_role_mapping
CREATE TABLE IF NOT EXISTS `fed_user_role_mapping` (
  `ROLE_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `USER_ID` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `STORAGE_PROVIDER_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`ROLE_ID`,`USER_ID`),
  KEY `IDX_FU_ROLE_MAPPING` (`USER_ID`,`ROLE_ID`),
  KEY `IDX_FU_ROLE_MAPPING_RU` (`REALM_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.fed_user_role_mapping: ~0 rows (ungefähr)
DELETE FROM `fed_user_role_mapping`;

-- Exportiere Struktur von Tabelle keycloak.group_attribute
CREATE TABLE IF NOT EXISTS `group_attribute` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL DEFAULT 'sybase-needs-something-here',
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `GROUP_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_GROUP_ATTR_GROUP` (`GROUP_ID`),
  KEY `IDX_GROUP_ATT_BY_NAME_VALUE` (`NAME`,`VALUE`),
  CONSTRAINT `FK_GROUP_ATTRIBUTE_GROUP` FOREIGN KEY (`GROUP_ID`) REFERENCES `keycloak_group` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.group_attribute: ~0 rows (ungefähr)
DELETE FROM `group_attribute`;

-- Exportiere Struktur von Tabelle keycloak.group_role_mapping
CREATE TABLE IF NOT EXISTS `group_role_mapping` (
  `ROLE_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `GROUP_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`ROLE_ID`,`GROUP_ID`),
  KEY `IDX_GROUP_ROLE_MAPP_GROUP` (`GROUP_ID`),
  CONSTRAINT `FK_GROUP_ROLE_GROUP` FOREIGN KEY (`GROUP_ID`) REFERENCES `keycloak_group` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.group_role_mapping: ~0 rows (ungefähr)
DELETE FROM `group_role_mapping`;

-- Exportiere Struktur von Tabelle keycloak.identity_provider
CREATE TABLE IF NOT EXISTS `identity_provider` (
  `INTERNAL_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `PROVIDER_ALIAS` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `PROVIDER_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `STORE_TOKEN` bit(1) NOT NULL DEFAULT b'0',
  `AUTHENTICATE_BY_DEFAULT` bit(1) NOT NULL DEFAULT b'0',
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `ADD_TOKEN_ROLE` bit(1) NOT NULL DEFAULT b'1',
  `TRUST_EMAIL` bit(1) NOT NULL DEFAULT b'0',
  `FIRST_BROKER_LOGIN_FLOW_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `POST_BROKER_LOGIN_FLOW_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `PROVIDER_DISPLAY_NAME` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `LINK_ONLY` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`INTERNAL_ID`),
  UNIQUE KEY `UK_2DAELWNIBJI49AVXSRTUF6XJ33` (`PROVIDER_ALIAS`,`REALM_ID`),
  KEY `IDX_IDENT_PROV_REALM` (`REALM_ID`),
  CONSTRAINT `FK2B4EBC52AE5C3B34` FOREIGN KEY (`REALM_ID`) REFERENCES `realm` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.identity_provider: ~0 rows (ungefähr)
DELETE FROM `identity_provider`;

-- Exportiere Struktur von Tabelle keycloak.identity_provider_config
CREATE TABLE IF NOT EXISTS `identity_provider_config` (
  `IDENTITY_PROVIDER_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`IDENTITY_PROVIDER_ID`,`NAME`),
  CONSTRAINT `FKDC4897CF864C4E43` FOREIGN KEY (`IDENTITY_PROVIDER_ID`) REFERENCES `identity_provider` (`INTERNAL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.identity_provider_config: ~0 rows (ungefähr)
DELETE FROM `identity_provider_config`;

-- Exportiere Struktur von Tabelle keycloak.identity_provider_mapper
CREATE TABLE IF NOT EXISTS `identity_provider_mapper` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `IDP_ALIAS` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `IDP_MAPPER_NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_ID_PROV_MAPP_REALM` (`REALM_ID`),
  CONSTRAINT `FK_IDPM_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `realm` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.identity_provider_mapper: ~0 rows (ungefähr)
DELETE FROM `identity_provider_mapper`;

-- Exportiere Struktur von Tabelle keycloak.idp_mapper_config
CREATE TABLE IF NOT EXISTS `idp_mapper_config` (
  `IDP_MAPPER_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`IDP_MAPPER_ID`,`NAME`),
  CONSTRAINT `FK_IDPMCONFIG` FOREIGN KEY (`IDP_MAPPER_ID`) REFERENCES `identity_provider_mapper` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.idp_mapper_config: ~0 rows (ungefähr)
DELETE FROM `idp_mapper_config`;

-- Exportiere Struktur von Tabelle keycloak.keycloak_group
CREATE TABLE IF NOT EXISTS `keycloak_group` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `PARENT_GROUP` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SIBLING_NAMES` (`REALM_ID`,`PARENT_GROUP`,`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.keycloak_group: ~0 rows (ungefähr)
DELETE FROM `keycloak_group`;

-- Exportiere Struktur von Tabelle keycloak.keycloak_role
CREATE TABLE IF NOT EXISTS `keycloak_role` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `CLIENT_REALM_CONSTRAINT` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `CLIENT_ROLE` bit(1) DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `REALM_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `CLIENT` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `REALM` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_J3RWUVD56ONTGSUHOGM184WW2-2` (`NAME`,`CLIENT_REALM_CONSTRAINT`),
  KEY `IDX_KEYCLOAK_ROLE_CLIENT` (`CLIENT`),
  KEY `IDX_KEYCLOAK_ROLE_REALM` (`REALM`),
  CONSTRAINT `FK_6VYQFE4CN4WLQ8R6KT5VDSJ5C` FOREIGN KEY (`REALM`) REFERENCES `realm` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.keycloak_role: ~87 rows (ungefähr)
DELETE FROM `keycloak_role`;
INSERT INTO `keycloak_role` (`ID`, `CLIENT_REALM_CONSTRAINT`, `CLIENT_ROLE`, `DESCRIPTION`, `NAME`, `REALM_ID`, `CLIENT`, `REALM`) VALUES
	('03e4737e-0972-42f8-96f0-f9754cff01a9', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', b'1', '${role_impersonation}', 'impersonation', '47958335-5c70-4603-abe7-b887490d7b1c', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', NULL),
	('07004d4f-f090-405f-9165-7c4a78cf9948', '2c77769b-4aab-4622-9e44-3a8433637f36', b'1', NULL, 'admin', '47958335-5c70-4603-abe7-b887490d7b1c', '2c77769b-4aab-4622-9e44-3a8433637f36', NULL),
	('081fe6ab-f9c8-4068-a0a0-402a09d1f3b8', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', b'1', '${role_view-identity-providers}', 'view-identity-providers', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', NULL),
	('0b008376-b0d1-4241-9452-de351cd88717', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', b'1', '${role_query-groups}', 'query-groups', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', NULL),
	('0d070596-f17e-4097-982a-f6321ab1fe9e', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', b'1', '${role_manage-events}', 'manage-events', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', NULL),
	('0d39cea7-99be-4bae-9b6c-441c62409f95', '2ef3bfdd-049a-4228-af7b-de4403fe6d04', b'1', '${role_view-profile}', 'view-profile', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '2ef3bfdd-049a-4228-af7b-de4403fe6d04', NULL),
	('0e117675-56ad-4573-ae32-e041aeaa563e', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', b'1', '${role_query-users}', 'query-users', '47958335-5c70-4603-abe7-b887490d7b1c', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', NULL),
	('0e27cff3-02fa-456c-a077-5ad6c89a0c3f', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', b'1', '${role_view-events}', 'view-events', '47958335-5c70-4603-abe7-b887490d7b1c', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', NULL),
	('13d7527e-3038-47ad-95b7-07bea4469d5d', '2ef3bfdd-049a-4228-af7b-de4403fe6d04', b'1', '${role_view-consent}', 'view-consent', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '2ef3bfdd-049a-4228-af7b-de4403fe6d04', NULL),
	('1421012b-7fdd-4ca9-a9b2-4b453246a06c', '47958335-5c70-4603-abe7-b887490d7b1c', b'0', '', 'app-admin', '47958335-5c70-4603-abe7-b887490d7b1c', NULL, NULL),
	('183a780a-478c-40f9-b639-63bb2f9fa188', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', b'1', '${role_manage-users}', 'manage-users', '47958335-5c70-4603-abe7-b887490d7b1c', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', NULL),
	('23652cea-4a07-49b1-b4f0-a1196c7ddbbe', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', b'1', '${role_query-realms}', 'query-realms', '47958335-5c70-4603-abe7-b887490d7b1c', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', NULL),
	('23eb6694-49b2-4fb4-b1d0-1bce51c5eb74', '23d8cb0f-53b6-4402-a9f9-072c402d5927', b'0', '${role_default-roles}', 'default-roles-master', '23d8cb0f-53b6-4402-a9f9-072c402d5927', NULL, NULL),
	('245c4cde-bbe6-41b4-8068-84ade85b620c', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', b'1', '${role_manage-authorization}', 'manage-authorization', '47958335-5c70-4603-abe7-b887490d7b1c', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', NULL),
	('24d1868a-7671-4224-9d11-41655a7aab27', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', b'1', '${role_query-users}', 'query-users', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', NULL),
	('24ed7f0c-5282-4cb3-88b7-c14e8d3602d2', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', b'1', '${role_realm-admin}', 'realm-admin', '47958335-5c70-4603-abe7-b887490d7b1c', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', NULL),
	('27a4cc8c-bafb-4a93-b174-87e032c421ee', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', b'1', '${role_query-realms}', 'query-realms', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', NULL),
	('29673115-8376-45bb-9e7b-595ddfd3b57e', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', b'1', '${role_manage-events}', 'manage-events', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', NULL),
	('2aea6f01-bb16-493e-bdc0-52b83bae5051', '23d8cb0f-53b6-4402-a9f9-072c402d5927', b'0', '${role_create-realm}', 'create-realm', '23d8cb0f-53b6-4402-a9f9-072c402d5927', NULL, NULL),
	('2afb513c-3028-430a-9b35-f0ce5d11d717', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', b'1', '${role_query-clients}', 'query-clients', '47958335-5c70-4603-abe7-b887490d7b1c', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', NULL),
	('2da535f5-229f-48a7-a939-6f96351f4fd2', '4ea4fda8-0d35-4644-9adf-24236632f114', b'1', '${role_view-consent}', 'view-consent', '47958335-5c70-4603-abe7-b887490d7b1c', '4ea4fda8-0d35-4644-9adf-24236632f114', NULL),
	('2f8f1984-8e52-4cc4-ad84-f6b4d0275042', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', b'1', '${role_manage-clients}', 'manage-clients', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', NULL),
	('3043dd87-953b-4a47-8cfd-d8b5f2a8932c', '4ea4fda8-0d35-4644-9adf-24236632f114', b'1', '${role_manage-account}', 'manage-account', '47958335-5c70-4603-abe7-b887490d7b1c', '4ea4fda8-0d35-4644-9adf-24236632f114', NULL),
	('34e6bc32-5596-4c61-8696-8525ce761e59', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', b'1', '${role_impersonation}', 'impersonation', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', NULL),
	('3583d5e6-1173-432c-981c-b7815cee20b9', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', b'1', '${role_view-users}', 'view-users', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', NULL),
	('36798338-061a-4e0e-ac68-d3afd4534c75', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', b'1', '${role_manage-realm}', 'manage-realm', '47958335-5c70-4603-abe7-b887490d7b1c', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', NULL),
	('3808c986-eab6-4ad6-b05f-2e3866dfb81e', '035989c0-3119-47ee-98ca-db933a226945', b'1', NULL, 'admin', '47958335-5c70-4603-abe7-b887490d7b1c', '035989c0-3119-47ee-98ca-db933a226945', NULL),
	('3d9637f8-77df-4a4c-a38c-6bd9ea7276c1', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', b'1', '${role_manage-events}', 'manage-events', '47958335-5c70-4603-abe7-b887490d7b1c', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', NULL),
	('417b3cc0-e882-4198-bf3a-9ef52104ae13', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', b'1', '${role_manage-identity-providers}', 'manage-identity-providers', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', NULL),
	('477ab853-069f-4331-85f2-74f4ec6ed03e', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', b'1', '${role_view-authorization}', 'view-authorization', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', NULL),
	('4c7dfef5-085d-4dfd-965e-5da042549120', 'e300dbf0-8156-400d-a394-21bc9ca441c3', b'1', '${role_read-token}', 'read-token', '47958335-5c70-4603-abe7-b887490d7b1c', 'e300dbf0-8156-400d-a394-21bc9ca441c3', NULL),
	('514e1be8-470f-4a41-8787-3fa664cd7910', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', b'1', '${role_create-client}', 'create-client', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', NULL),
	('51e31922-bb2a-4129-8a82-c17a48a793f0', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', b'1', '${role_impersonation}', 'impersonation', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', NULL),
	('52551043-a306-46ca-9e9b-5dcc8e1ff935', '4ea4fda8-0d35-4644-9adf-24236632f114', b'1', '${role_view-groups}', 'view-groups', '47958335-5c70-4603-abe7-b887490d7b1c', '4ea4fda8-0d35-4644-9adf-24236632f114', NULL),
	('536620cd-042d-4c61-b1ce-e779f8d00b28', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', b'1', '${role_query-realms}', 'query-realms', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', NULL),
	('542b0798-98eb-4ce0-be4d-ccf5cef75513', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', b'1', '${role_view-realm}', 'view-realm', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', NULL),
	('54abcfa3-4770-4bbe-a5d1-d2d68c04d7e4', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', b'1', '${role_manage-realm}', 'manage-realm', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', NULL),
	('55f5e036-e10c-41d3-a3fa-a033e92b251b', '4ea4fda8-0d35-4644-9adf-24236632f114', b'1', '${role_view-profile}', 'view-profile', '47958335-5c70-4603-abe7-b887490d7b1c', '4ea4fda8-0d35-4644-9adf-24236632f114', NULL),
	('58a329c0-e493-4554-8a16-bb10b0305c28', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', b'1', '${role_query-groups}', 'query-groups', '47958335-5c70-4603-abe7-b887490d7b1c', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', NULL),
	('5afd0771-9254-44c6-9132-2e0892992254', '2ef3bfdd-049a-4228-af7b-de4403fe6d04', b'1', '${role_manage-account}', 'manage-account', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '2ef3bfdd-049a-4228-af7b-de4403fe6d04', NULL),
	('5c409cdb-63da-422d-8fd3-befc950cb1ba', '2ef3bfdd-049a-4228-af7b-de4403fe6d04', b'1', '${role_delete-account}', 'delete-account', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '2ef3bfdd-049a-4228-af7b-de4403fe6d04', NULL),
	('606ad618-f0a8-4543-b526-971af5b5448e', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', b'1', '${role_manage-users}', 'manage-users', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', NULL),
	('63bcb921-b0b6-4df5-aabf-0aa91b11596a', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', b'1', '${role_manage-authorization}', 'manage-authorization', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', NULL),
	('79093fb1-0093-47d2-97f4-572ff8cc2420', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', b'1', '${role_manage-users}', 'manage-users', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', NULL),
	('79710760-aac2-4971-acc8-8d28154b28c7', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', b'1', '${role_view-realm}', 'view-realm', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', NULL),
	('7ae2783e-3c71-4ae0-b75c-e452680b5c30', '4ea4fda8-0d35-4644-9adf-24236632f114', b'1', '${role_view-applications}', 'view-applications', '47958335-5c70-4603-abe7-b887490d7b1c', '4ea4fda8-0d35-4644-9adf-24236632f114', NULL),
	('7e7ea688-c340-4493-a173-5a56727114e8', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', b'1', '${role_query-clients}', 'query-clients', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', NULL),
	('840527f7-7414-4336-a285-0fcf84f0300a', '4ea4fda8-0d35-4644-9adf-24236632f114', b'1', '${role_delete-account}', 'delete-account', '47958335-5c70-4603-abe7-b887490d7b1c', '4ea4fda8-0d35-4644-9adf-24236632f114', NULL),
	('8883719d-2359-4401-97c7-376e7aa6aa45', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', b'1', '${role_manage-realm}', 'manage-realm', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', NULL),
	('89a002c1-82f9-4999-8ea9-8a084fc5b406', '23d8cb0f-53b6-4402-a9f9-072c402d5927', b'0', '${role_uma_authorization}', 'uma_authorization', '23d8cb0f-53b6-4402-a9f9-072c402d5927', NULL, NULL),
	('89b619dc-2ed1-494a-84d2-e1510d9ab22e', '2c77769b-4aab-4622-9e44-3a8433637f36', b'1', NULL, 'user', '47958335-5c70-4603-abe7-b887490d7b1c', '2c77769b-4aab-4622-9e44-3a8433637f36', NULL),
	('8f230571-7d8b-4885-a9ac-4ec5da2b7979', '23d8cb0f-53b6-4402-a9f9-072c402d5927', b'0', '${role_offline-access}', 'offline_access', '23d8cb0f-53b6-4402-a9f9-072c402d5927', NULL, NULL),
	('925b7df8-67ba-415b-8d3c-e07f0f4cb06e', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', b'1', '${role_view-users}', 'view-users', '47958335-5c70-4603-abe7-b887490d7b1c', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', NULL),
	('93d5e01a-236c-4aa0-80d2-735ce5235fb9', '4ea4fda8-0d35-4644-9adf-24236632f114', b'1', '${role_manage-consent}', 'manage-consent', '47958335-5c70-4603-abe7-b887490d7b1c', '4ea4fda8-0d35-4644-9adf-24236632f114', NULL),
	('985095f2-9e54-43cd-9e3c-c507c7218479', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', b'1', '${role_view-realm}', 'view-realm', '47958335-5c70-4603-abe7-b887490d7b1c', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', NULL),
	('9a75a1c2-56c7-427d-82da-5d25228fae81', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', b'1', '${role_query-groups}', 'query-groups', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', NULL),
	('9d7fc4b7-71da-4b4d-a364-b869218bbd88', '4ea4fda8-0d35-4644-9adf-24236632f114', b'1', '${role_manage-account-links}', 'manage-account-links', '47958335-5c70-4603-abe7-b887490d7b1c', '4ea4fda8-0d35-4644-9adf-24236632f114', NULL),
	('a64a81ce-b5d2-4275-854b-e3679edef93a', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', b'1', '${role_query-clients}', 'query-clients', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', NULL),
	('ac823ecf-f797-437c-9600-2c1adf768f86', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', b'1', '${role_create-client}', 'create-client', '47958335-5c70-4603-abe7-b887490d7b1c', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', NULL),
	('ace7c196-2f0c-4a12-9b17-88f4a21b918d', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', b'1', '${role_view-clients}', 'view-clients', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', NULL),
	('ad565401-754f-4f0e-bf5f-e307c36783a5', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', b'1', '${role_view-events}', 'view-events', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', NULL),
	('b1c58ee6-4ce1-4e99-8eb2-031740c8e54e', '47958335-5c70-4603-abe7-b887490d7b1c', b'0', '${role_offline-access}', 'offline_access', '47958335-5c70-4603-abe7-b887490d7b1c', NULL, NULL),
	('b3e09f35-8853-4276-b194-ec28c2531fcb', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', b'1', '${role_view-identity-providers}', 'view-identity-providers', '47958335-5c70-4603-abe7-b887490d7b1c', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', NULL),
	('b3e6fd4f-5175-42a4-b7c7-f7aa4fab418a', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', b'1', '${role_manage-clients}', 'manage-clients', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', NULL),
	('b3eddf3d-250b-482a-88d4-20add1ad0002', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', b'1', '${role_view-clients}', 'view-clients', '47958335-5c70-4603-abe7-b887490d7b1c', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', NULL),
	('b715f2d0-3222-4ecd-a0ee-19ad64a204b1', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', b'1', '${role_manage-identity-providers}', 'manage-identity-providers', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', NULL),
	('c0a95923-ba7a-4457-9e86-c054bd15016a', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', b'1', '${role_create-client}', 'create-client', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', NULL),
	('c222fc2d-aa3a-4fbd-9e3c-27894f821c16', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', b'1', '${role_view-clients}', 'view-clients', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', NULL),
	('c295fe04-6fea-4e31-ada8-7cac547508bc', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', b'1', '${role_view-authorization}', 'view-authorization', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', NULL),
	('d05f01e5-497e-4724-8169-13ba9de8915c', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', b'1', '${role_query-users}', 'query-users', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', NULL),
	('d0b80bc7-6aaf-4c2f-906e-0deede603ef8', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', b'1', '${role_view-identity-providers}', 'view-identity-providers', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', NULL),
	('ddad37c9-58a3-4aa4-bdf6-509c4065822f', '47958335-5c70-4603-abe7-b887490d7b1c', b'0', '${role_default-roles}', 'default-roles-wurstwasser', '47958335-5c70-4603-abe7-b887490d7b1c', NULL, NULL),
	('de0b2758-4063-41aa-9f51-7e0114080e3a', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', b'1', '${role_view-users}', 'view-users', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', NULL),
	('dece6e54-8855-4c72-b66c-16872fcccb3e', '2ef3bfdd-049a-4228-af7b-de4403fe6d04', b'1', '${role_view-applications}', 'view-applications', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '2ef3bfdd-049a-4228-af7b-de4403fe6d04', NULL),
	('dfe03b0b-d4b1-427b-b451-38eb681ee0ed', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', b'1', '${role_manage-clients}', 'manage-clients', '47958335-5c70-4603-abe7-b887490d7b1c', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', NULL),
	('e0733acb-438f-4ee9-8adc-20304869393b', '2ef3bfdd-049a-4228-af7b-de4403fe6d04', b'1', '${role_manage-consent}', 'manage-consent', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '2ef3bfdd-049a-4228-af7b-de4403fe6d04', NULL),
	('ea4cfb54-d2cd-4ff7-a046-61241163cfae', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', b'1', '${role_view-events}', 'view-events', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', NULL),
	('ec0c3c3a-6bf5-4258-9db6-3b39975c2bb6', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', b'1', '${role_view-authorization}', 'view-authorization', '47958335-5c70-4603-abe7-b887490d7b1c', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', NULL),
	('f037a6b3-bab8-44a9-bcf4-16ad5aeab248', '035989c0-3119-47ee-98ca-db933a226945', b'1', NULL, 'user', '47958335-5c70-4603-abe7-b887490d7b1c', '035989c0-3119-47ee-98ca-db933a226945', NULL),
	('f2c9e482-411f-41f7-8c55-8bc5da6f1ab3', '47958335-5c70-4603-abe7-b887490d7b1c', b'0', '', 'app-user', '47958335-5c70-4603-abe7-b887490d7b1c', NULL, NULL),
	('f2e7ece9-6fe3-4af0-941b-b16af4e32e53', '2ef3bfdd-049a-4228-af7b-de4403fe6d04', b'1', '${role_view-groups}', 'view-groups', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '2ef3bfdd-049a-4228-af7b-de4403fe6d04', NULL),
	('f5b6cbfd-7a24-4020-a51b-2f36c0bdd0d9', '47958335-5c70-4603-abe7-b887490d7b1c', b'0', '${role_uma_authorization}', 'uma_authorization', '47958335-5c70-4603-abe7-b887490d7b1c', NULL, NULL),
	('f65cc882-6e5d-43e1-a8de-6d7d8b3543b5', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', b'1', '${role_manage-authorization}', 'manage-authorization', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', NULL),
	('f6ea3bab-9428-4573-946c-2fc101660be1', '2ef3bfdd-049a-4228-af7b-de4403fe6d04', b'1', '${role_manage-account-links}', 'manage-account-links', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '2ef3bfdd-049a-4228-af7b-de4403fe6d04', NULL),
	('fa2af4b5-e199-44d2-a4ad-0d7c9ce2cbf5', '66506671-3f2a-4b67-8f53-f2752ca989fd', b'1', '${role_read-token}', 'read-token', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '66506671-3f2a-4b67-8f53-f2752ca989fd', NULL),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', '23d8cb0f-53b6-4402-a9f9-072c402d5927', b'0', '${role_admin}', 'admin', '23d8cb0f-53b6-4402-a9f9-072c402d5927', NULL, NULL),
	('fdc98d84-25a5-4975-bba3-17540c9e99a2', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', b'1', '${role_manage-identity-providers}', 'manage-identity-providers', '47958335-5c70-4603-abe7-b887490d7b1c', 'c3e3ce52-d510-4517-bc39-7a8d0e300bd9', NULL);

-- Exportiere Struktur von Tabelle keycloak.migration_model
CREATE TABLE IF NOT EXISTS `migration_model` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `VERSION` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `UPDATE_TIME` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID`),
  KEY `IDX_UPDATE_TIME` (`UPDATE_TIME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.migration_model: ~1 rows (ungefähr)
DELETE FROM `migration_model`;
INSERT INTO `migration_model` (`ID`, `VERSION`, `UPDATE_TIME`) VALUES
	('6hz6y', '20.0.3', 1674862321);

-- Exportiere Struktur von Tabelle keycloak.offline_client_session
CREATE TABLE IF NOT EXISTS `offline_client_session` (
  `USER_SESSION_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `CLIENT_ID` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `OFFLINE_FLAG` varchar(4) COLLATE utf8mb4_bin NOT NULL,
  `TIMESTAMP` int(11) DEFAULT NULL,
  `DATA` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `CLIENT_STORAGE_PROVIDER` varchar(36) COLLATE utf8mb4_bin NOT NULL DEFAULT 'local',
  `EXTERNAL_CLIENT_ID` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT 'local',
  PRIMARY KEY (`USER_SESSION_ID`,`CLIENT_ID`,`CLIENT_STORAGE_PROVIDER`,`EXTERNAL_CLIENT_ID`,`OFFLINE_FLAG`),
  KEY `IDX_US_SESS_ID_ON_CL_SESS` (`USER_SESSION_ID`),
  KEY `IDX_OFFLINE_CSS_PRELOAD` (`CLIENT_ID`,`OFFLINE_FLAG`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.offline_client_session: ~0 rows (ungefähr)
DELETE FROM `offline_client_session`;

-- Exportiere Struktur von Tabelle keycloak.offline_user_session
CREATE TABLE IF NOT EXISTS `offline_user_session` (
  `USER_SESSION_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `USER_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `CREATED_ON` int(11) NOT NULL,
  `OFFLINE_FLAG` varchar(4) COLLATE utf8mb4_bin NOT NULL,
  `DATA` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `LAST_SESSION_REFRESH` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`USER_SESSION_ID`,`OFFLINE_FLAG`),
  KEY `IDX_OFFLINE_USS_CREATEDON` (`CREATED_ON`),
  KEY `IDX_OFFLINE_USS_PRELOAD` (`OFFLINE_FLAG`,`CREATED_ON`,`USER_SESSION_ID`),
  KEY `IDX_OFFLINE_USS_BY_USER` (`USER_ID`,`REALM_ID`,`OFFLINE_FLAG`),
  KEY `IDX_OFFLINE_USS_BY_USERSESS` (`REALM_ID`,`OFFLINE_FLAG`,`USER_SESSION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.offline_user_session: ~0 rows (ungefähr)
DELETE FROM `offline_user_session`;

-- Exportiere Struktur von Tabelle keycloak.policy_config
CREATE TABLE IF NOT EXISTS `policy_config` (
  `POLICY_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`POLICY_ID`,`NAME`),
  CONSTRAINT `FKDC34197CF864C4E43` FOREIGN KEY (`POLICY_ID`) REFERENCES `resource_server_policy` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.policy_config: ~0 rows (ungefähr)
DELETE FROM `policy_config`;

-- Exportiere Struktur von Tabelle keycloak.protocol_mapper
CREATE TABLE IF NOT EXISTS `protocol_mapper` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `PROTOCOL` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `PROTOCOL_MAPPER_NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `CLIENT_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `CLIENT_SCOPE_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_PROTOCOL_MAPPER_CLIENT` (`CLIENT_ID`),
  KEY `IDX_CLSCOPE_PROTMAP` (`CLIENT_SCOPE_ID`),
  CONSTRAINT `FK_CLI_SCOPE_MAPPER` FOREIGN KEY (`CLIENT_SCOPE_ID`) REFERENCES `client_scope` (`ID`),
  CONSTRAINT `FK_PCM_REALM` FOREIGN KEY (`CLIENT_ID`) REFERENCES `client` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.protocol_mapper: ~58 rows (ungefähr)
DELETE FROM `protocol_mapper`;
INSERT INTO `protocol_mapper` (`ID`, `NAME`, `PROTOCOL`, `PROTOCOL_MAPPER_NAME`, `CLIENT_ID`, `CLIENT_SCOPE_ID`) VALUES
	('07669fa5-273e-4087-9bf4-6f662f984d67', 'address', 'openid-connect', 'oidc-address-mapper', NULL, '6d92611e-48b4-4641-9191-de1cd49b50cd'),
	('0adc0be4-61ad-4300-8ff1-089879de6f05', 'email', 'openid-connect', 'oidc-usermodel-property-mapper', NULL, '1e109bdc-2125-41fc-a9cd-988e4926ebba'),
	('0ed570d5-5f81-4e66-84f5-a041fb0cd2e4', 'phone number', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, 'b880b5f7-bb9c-4fbf-930b-db668b9907ab'),
	('13bfa904-6490-40a1-8b67-6598e4369f45', 'locale', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '381dfb0c-222f-4c9a-b3bb-336e4987f292'),
	('1452aa38-3bc5-4dd6-9d68-cbeedb405749', 'address', 'openid-connect', 'oidc-address-mapper', NULL, '272fc934-f3a7-4268-83ac-2acc745d87f1'),
	('1cda163f-9d80-4e44-9344-a3dfbf33ae89', 'acr loa level', 'openid-connect', 'oidc-acr-mapper', NULL, 'cd2bd538-424e-4b02-884e-26620e48ce31'),
	('225a8815-c908-4b36-af35-299130171d4e', 'audience resolve', 'openid-connect', 'oidc-audience-resolve-mapper', NULL, 'ac77881e-2cf5-4db1-8ee9-bef2ac8dcb52'),
	('230a4309-46a1-43be-9a30-da83f90a8086', 'nickname', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '381dfb0c-222f-4c9a-b3bb-336e4987f292'),
	('25545e0d-6b13-41c5-8ab5-002909f14a6f', 'email verified', 'openid-connect', 'oidc-usermodel-property-mapper', NULL, '5e5faf95-a740-4ecd-b7ff-757299ed729b'),
	('260cb88f-511d-4b7d-ad22-6483c1f74a3b', 'audience resolve', 'openid-connect', 'oidc-audience-resolve-mapper', NULL, '1326cc85-b925-4654-a935-74af8ef758eb'),
	('27fa3057-bc5f-4a4b-bc1a-76cd6b340d8a', 'family name', 'openid-connect', 'oidc-usermodel-property-mapper', NULL, '381dfb0c-222f-4c9a-b3bb-336e4987f292'),
	('2d252301-fece-42fe-97b5-c24f989688db', 'allowed web origins', 'openid-connect', 'oidc-allowed-origins-mapper', NULL, '81fb9c52-5475-4410-86d5-55dee623e213'),
	('3b6c2d1d-4378-4442-885d-89d46f93e56e', 'locale', 'openid-connect', 'oidc-usermodel-attribute-mapper', '808458f5-33ba-4e81-9601-019ae159abb3', NULL),
	('3e880cb6-e858-45fc-88eb-d9b9dc8b55a9', 'full name', 'openid-connect', 'oidc-full-name-mapper', NULL, '381dfb0c-222f-4c9a-b3bb-336e4987f292'),
	('40e0fd4c-25b9-4baf-9782-684fb3d9df3f', 'gender', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '381dfb0c-222f-4c9a-b3bb-336e4987f292'),
	('42519b72-fd1f-45f7-b58e-7af677c4a9ef', 'middle name', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '4e92b233-7271-4ccf-a9b7-bd5b43c9a573'),
	('49b51130-c60f-41a0-b70d-012e5daa616a', 'zoneinfo', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '381dfb0c-222f-4c9a-b3bb-336e4987f292'),
	('4aee1543-571d-4009-bb8a-1b0db52afb7b', 'picture', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '4e92b233-7271-4ccf-a9b7-bd5b43c9a573'),
	('4cc57dcd-8825-43a9-9a17-4ef96d7acf01', 'website', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '4e92b233-7271-4ccf-a9b7-bd5b43c9a573'),
	('5424501c-dc54-4a07-ad5d-54727f47e103', 'family name', 'openid-connect', 'oidc-usermodel-property-mapper', NULL, '4e92b233-7271-4ccf-a9b7-bd5b43c9a573'),
	('63688f9a-15d2-42cf-ab9a-66e54794b154', 'phone number', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '51444a9d-afea-400e-b37a-8624ac52ff3d'),
	('66698f98-18a6-45bb-b37f-d960b2b09551', 'website', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '381dfb0c-222f-4c9a-b3bb-336e4987f292'),
	('67787520-7745-4203-8d6c-062972007b13', 'updated at', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '381dfb0c-222f-4c9a-b3bb-336e4987f292'),
	('686843cb-d0a5-4829-828a-5ab3ff6c3077', 'audience resolve', 'openid-connect', 'oidc-audience-resolve-mapper', '0c372b1f-3961-4907-83de-1153d2af2d57', NULL),
	('692cde83-3d63-4844-a15b-c6af9a8f9bb3', 'updated at', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '4e92b233-7271-4ccf-a9b7-bd5b43c9a573'),
	('7091736e-b0ae-4c02-bffa-eedf51ad3be0', 'given name', 'openid-connect', 'oidc-usermodel-property-mapper', NULL, '4e92b233-7271-4ccf-a9b7-bd5b43c9a573'),
	('778292ab-821e-4a06-8855-4f8172b3a1e4', 'given name', 'openid-connect', 'oidc-usermodel-property-mapper', NULL, '381dfb0c-222f-4c9a-b3bb-336e4987f292'),
	('80fba5f7-1ef5-49bd-8f06-a4332f384ac8', 'acr loa level', 'openid-connect', 'oidc-acr-mapper', NULL, '79ae1850-0614-45c7-beed-41698ef668ee'),
	('81f66368-badd-4b19-be6f-ddb665bde492', 'locale', 'openid-connect', 'oidc-usermodel-attribute-mapper', '8d03d66e-c122-4357-b8ac-647ff79ac3be', NULL),
	('82ade246-b424-4674-bc9c-8f6d907bd3d4', 'email', 'openid-connect', 'oidc-usermodel-property-mapper', NULL, '5e5faf95-a740-4ecd-b7ff-757299ed729b'),
	('85ef208e-f75d-4496-bb99-5b2cc3aae98a', 'username', 'openid-connect', 'oidc-usermodel-property-mapper', NULL, '4e92b233-7271-4ccf-a9b7-bd5b43c9a573'),
	('86742682-9275-4e17-93d3-84857b09eadf', 'phone number verified', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, 'b880b5f7-bb9c-4fbf-930b-db668b9907ab'),
	('898b093c-66e7-4741-b9f7-5df4555208dc', 'role list', 'saml', 'saml-role-list-mapper', NULL, 'b032d9af-b13c-4a76-9069-6b687cc7ac6c'),
	('8c120346-5e91-4d08-866e-89fb2b4be705', 'profile', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '4e92b233-7271-4ccf-a9b7-bd5b43c9a573'),
	('977984ee-81de-4da3-9bb4-72ffa9585495', 'locale', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '4e92b233-7271-4ccf-a9b7-bd5b43c9a573'),
	('980f21f3-30d3-4d9a-95c6-62754e037e7e', 'groups', 'openid-connect', 'oidc-usermodel-realm-role-mapper', NULL, 'f558df31-2459-4a39-b429-fc9e8d1edfb6'),
	('9893b4a0-24dc-4a84-8d16-16f4fd90b0ce', 'birthdate', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '4e92b233-7271-4ccf-a9b7-bd5b43c9a573'),
	('9d7065a2-eed9-43ef-9355-c5dd0a929d75', 'middle name', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '381dfb0c-222f-4c9a-b3bb-336e4987f292'),
	('a4d908af-e3cb-470e-93ea-3c14323d6084', 'realm roles', 'openid-connect', 'oidc-usermodel-realm-role-mapper', NULL, 'ac77881e-2cf5-4db1-8ee9-bef2ac8dcb52'),
	('aede3ecc-74ee-4260-b599-e63761628e65', 'role list', 'saml', 'saml-role-list-mapper', NULL, 'b15a69e0-6f2e-4948-8feb-b7fbd0e4e36c'),
	('b080e04f-b0c9-44e0-bd29-d319bbf24006', 'full name', 'openid-connect', 'oidc-full-name-mapper', NULL, '4e92b233-7271-4ccf-a9b7-bd5b43c9a573'),
	('b8b94227-ddf5-4992-b7d0-c8bb696a7c79', 'realm roles', 'openid-connect', 'oidc-usermodel-realm-role-mapper', NULL, '1326cc85-b925-4654-a935-74af8ef758eb'),
	('bde524c2-3f09-403f-8a6f-2f62ecb9bd8c', 'client roles', 'openid-connect', 'oidc-usermodel-client-role-mapper', NULL, '1326cc85-b925-4654-a935-74af8ef758eb'),
	('c81009ee-e976-4344-b1b5-52af80fe04b2', 'upn', 'openid-connect', 'oidc-usermodel-property-mapper', NULL, 'c74d2f87-1571-40ec-b831-04c17af5ec75'),
	('cf833979-19a8-42a6-903a-9ca8f980e592', 'picture', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '381dfb0c-222f-4c9a-b3bb-336e4987f292'),
	('de73e919-404e-49c2-a1ea-79fe10a30501', 'profile', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '381dfb0c-222f-4c9a-b3bb-336e4987f292'),
	('e5c8ebd9-b0ac-4c95-af3e-daa297cdde3b', 'nickname', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '4e92b233-7271-4ccf-a9b7-bd5b43c9a573'),
	('e7e9a51c-712c-4107-92b8-5fdaadde6093', 'allowed web origins', 'openid-connect', 'oidc-allowed-origins-mapper', NULL, 'ae70759a-d8f4-4c19-9ed4-0fa8bdb796fd'),
	('e811726f-12e6-40d6-9ef1-b08cf9242c94', 'zoneinfo', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '4e92b233-7271-4ccf-a9b7-bd5b43c9a573'),
	('e9859deb-80e0-4cfd-b9dd-c7bad797bb88', 'gender', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '4e92b233-7271-4ccf-a9b7-bd5b43c9a573'),
	('eb0fca1b-8134-47bb-aa32-ed2939e7b89c', 'birthdate', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '381dfb0c-222f-4c9a-b3bb-336e4987f292'),
	('eec27305-9814-4bc7-80e5-c6db533d67c2', 'username', 'openid-connect', 'oidc-usermodel-property-mapper', NULL, '381dfb0c-222f-4c9a-b3bb-336e4987f292'),
	('ef1b8b37-574f-42b3-be35-f0aae21adf94', 'groups', 'openid-connect', 'oidc-usermodel-realm-role-mapper', NULL, 'c74d2f87-1571-40ec-b831-04c17af5ec75'),
	('f020c210-bbd8-4210-9e30-749ea818ddc7', 'client roles', 'openid-connect', 'oidc-usermodel-client-role-mapper', NULL, 'ac77881e-2cf5-4db1-8ee9-bef2ac8dcb52'),
	('f344f839-de00-44f2-b664-c07367d19a8b', 'email verified', 'openid-connect', 'oidc-usermodel-property-mapper', NULL, '1e109bdc-2125-41fc-a9cd-988e4926ebba'),
	('fa2e1c07-bb0f-474b-9db4-7dacc88d7d8a', 'audience resolve', 'openid-connect', 'oidc-audience-resolve-mapper', 'cc4616ba-1805-413c-8edb-1f5315bb8df5', NULL),
	('fbb59320-a064-4c21-af44-1fbd5599a5be', 'upn', 'openid-connect', 'oidc-usermodel-property-mapper', NULL, 'f558df31-2459-4a39-b429-fc9e8d1edfb6'),
	('fd137ddd-44d1-49c4-a990-3c42c5fe0e21', 'phone number verified', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '51444a9d-afea-400e-b37a-8624ac52ff3d');

-- Exportiere Struktur von Tabelle keycloak.protocol_mapper_config
CREATE TABLE IF NOT EXISTS `protocol_mapper_config` (
  `PROTOCOL_MAPPER_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`PROTOCOL_MAPPER_ID`,`NAME`),
  CONSTRAINT `FK_PMCONFIG` FOREIGN KEY (`PROTOCOL_MAPPER_ID`) REFERENCES `protocol_mapper` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.protocol_mapper_config: ~294 rows (ungefähr)
DELETE FROM `protocol_mapper_config`;
INSERT INTO `protocol_mapper_config` (`PROTOCOL_MAPPER_ID`, `VALUE`, `NAME`) VALUES
	('07669fa5-273e-4087-9bf4-6f662f984d67', 'true', 'access.token.claim'),
	('07669fa5-273e-4087-9bf4-6f662f984d67', 'true', 'id.token.claim'),
	('07669fa5-273e-4087-9bf4-6f662f984d67', 'country', 'user.attribute.country'),
	('07669fa5-273e-4087-9bf4-6f662f984d67', 'formatted', 'user.attribute.formatted'),
	('07669fa5-273e-4087-9bf4-6f662f984d67', 'locality', 'user.attribute.locality'),
	('07669fa5-273e-4087-9bf4-6f662f984d67', 'postal_code', 'user.attribute.postal_code'),
	('07669fa5-273e-4087-9bf4-6f662f984d67', 'region', 'user.attribute.region'),
	('07669fa5-273e-4087-9bf4-6f662f984d67', 'street', 'user.attribute.street'),
	('07669fa5-273e-4087-9bf4-6f662f984d67', 'true', 'userinfo.token.claim'),
	('0adc0be4-61ad-4300-8ff1-089879de6f05', 'true', 'access.token.claim'),
	('0adc0be4-61ad-4300-8ff1-089879de6f05', 'email', 'claim.name'),
	('0adc0be4-61ad-4300-8ff1-089879de6f05', 'true', 'id.token.claim'),
	('0adc0be4-61ad-4300-8ff1-089879de6f05', 'String', 'jsonType.label'),
	('0adc0be4-61ad-4300-8ff1-089879de6f05', 'email', 'user.attribute'),
	('0adc0be4-61ad-4300-8ff1-089879de6f05', 'true', 'userinfo.token.claim'),
	('0ed570d5-5f81-4e66-84f5-a041fb0cd2e4', 'true', 'access.token.claim'),
	('0ed570d5-5f81-4e66-84f5-a041fb0cd2e4', 'phone_number', 'claim.name'),
	('0ed570d5-5f81-4e66-84f5-a041fb0cd2e4', 'true', 'id.token.claim'),
	('0ed570d5-5f81-4e66-84f5-a041fb0cd2e4', 'String', 'jsonType.label'),
	('0ed570d5-5f81-4e66-84f5-a041fb0cd2e4', 'phoneNumber', 'user.attribute'),
	('0ed570d5-5f81-4e66-84f5-a041fb0cd2e4', 'true', 'userinfo.token.claim'),
	('13bfa904-6490-40a1-8b67-6598e4369f45', 'true', 'access.token.claim'),
	('13bfa904-6490-40a1-8b67-6598e4369f45', 'locale', 'claim.name'),
	('13bfa904-6490-40a1-8b67-6598e4369f45', 'true', 'id.token.claim'),
	('13bfa904-6490-40a1-8b67-6598e4369f45', 'String', 'jsonType.label'),
	('13bfa904-6490-40a1-8b67-6598e4369f45', 'locale', 'user.attribute'),
	('13bfa904-6490-40a1-8b67-6598e4369f45', 'true', 'userinfo.token.claim'),
	('1452aa38-3bc5-4dd6-9d68-cbeedb405749', 'true', 'access.token.claim'),
	('1452aa38-3bc5-4dd6-9d68-cbeedb405749', 'true', 'id.token.claim'),
	('1452aa38-3bc5-4dd6-9d68-cbeedb405749', 'country', 'user.attribute.country'),
	('1452aa38-3bc5-4dd6-9d68-cbeedb405749', 'formatted', 'user.attribute.formatted'),
	('1452aa38-3bc5-4dd6-9d68-cbeedb405749', 'locality', 'user.attribute.locality'),
	('1452aa38-3bc5-4dd6-9d68-cbeedb405749', 'postal_code', 'user.attribute.postal_code'),
	('1452aa38-3bc5-4dd6-9d68-cbeedb405749', 'region', 'user.attribute.region'),
	('1452aa38-3bc5-4dd6-9d68-cbeedb405749', 'street', 'user.attribute.street'),
	('1452aa38-3bc5-4dd6-9d68-cbeedb405749', 'true', 'userinfo.token.claim'),
	('1cda163f-9d80-4e44-9344-a3dfbf33ae89', 'true', 'access.token.claim'),
	('1cda163f-9d80-4e44-9344-a3dfbf33ae89', 'true', 'id.token.claim'),
	('230a4309-46a1-43be-9a30-da83f90a8086', 'true', 'access.token.claim'),
	('230a4309-46a1-43be-9a30-da83f90a8086', 'nickname', 'claim.name'),
	('230a4309-46a1-43be-9a30-da83f90a8086', 'true', 'id.token.claim'),
	('230a4309-46a1-43be-9a30-da83f90a8086', 'String', 'jsonType.label'),
	('230a4309-46a1-43be-9a30-da83f90a8086', 'nickname', 'user.attribute'),
	('230a4309-46a1-43be-9a30-da83f90a8086', 'true', 'userinfo.token.claim'),
	('25545e0d-6b13-41c5-8ab5-002909f14a6f', 'true', 'access.token.claim'),
	('25545e0d-6b13-41c5-8ab5-002909f14a6f', 'email_verified', 'claim.name'),
	('25545e0d-6b13-41c5-8ab5-002909f14a6f', 'true', 'id.token.claim'),
	('25545e0d-6b13-41c5-8ab5-002909f14a6f', 'boolean', 'jsonType.label'),
	('25545e0d-6b13-41c5-8ab5-002909f14a6f', 'emailVerified', 'user.attribute'),
	('25545e0d-6b13-41c5-8ab5-002909f14a6f', 'true', 'userinfo.token.claim'),
	('27fa3057-bc5f-4a4b-bc1a-76cd6b340d8a', 'true', 'access.token.claim'),
	('27fa3057-bc5f-4a4b-bc1a-76cd6b340d8a', 'family_name', 'claim.name'),
	('27fa3057-bc5f-4a4b-bc1a-76cd6b340d8a', 'true', 'id.token.claim'),
	('27fa3057-bc5f-4a4b-bc1a-76cd6b340d8a', 'String', 'jsonType.label'),
	('27fa3057-bc5f-4a4b-bc1a-76cd6b340d8a', 'lastName', 'user.attribute'),
	('27fa3057-bc5f-4a4b-bc1a-76cd6b340d8a', 'true', 'userinfo.token.claim'),
	('3b6c2d1d-4378-4442-885d-89d46f93e56e', 'true', 'access.token.claim'),
	('3b6c2d1d-4378-4442-885d-89d46f93e56e', 'locale', 'claim.name'),
	('3b6c2d1d-4378-4442-885d-89d46f93e56e', 'true', 'id.token.claim'),
	('3b6c2d1d-4378-4442-885d-89d46f93e56e', 'String', 'jsonType.label'),
	('3b6c2d1d-4378-4442-885d-89d46f93e56e', 'locale', 'user.attribute'),
	('3b6c2d1d-4378-4442-885d-89d46f93e56e', 'true', 'userinfo.token.claim'),
	('3e880cb6-e858-45fc-88eb-d9b9dc8b55a9', 'true', 'access.token.claim'),
	('3e880cb6-e858-45fc-88eb-d9b9dc8b55a9', 'true', 'id.token.claim'),
	('3e880cb6-e858-45fc-88eb-d9b9dc8b55a9', 'true', 'userinfo.token.claim'),
	('40e0fd4c-25b9-4baf-9782-684fb3d9df3f', 'true', 'access.token.claim'),
	('40e0fd4c-25b9-4baf-9782-684fb3d9df3f', 'gender', 'claim.name'),
	('40e0fd4c-25b9-4baf-9782-684fb3d9df3f', 'true', 'id.token.claim'),
	('40e0fd4c-25b9-4baf-9782-684fb3d9df3f', 'String', 'jsonType.label'),
	('40e0fd4c-25b9-4baf-9782-684fb3d9df3f', 'gender', 'user.attribute'),
	('40e0fd4c-25b9-4baf-9782-684fb3d9df3f', 'true', 'userinfo.token.claim'),
	('42519b72-fd1f-45f7-b58e-7af677c4a9ef', 'true', 'access.token.claim'),
	('42519b72-fd1f-45f7-b58e-7af677c4a9ef', 'middle_name', 'claim.name'),
	('42519b72-fd1f-45f7-b58e-7af677c4a9ef', 'true', 'id.token.claim'),
	('42519b72-fd1f-45f7-b58e-7af677c4a9ef', 'String', 'jsonType.label'),
	('42519b72-fd1f-45f7-b58e-7af677c4a9ef', 'middleName', 'user.attribute'),
	('42519b72-fd1f-45f7-b58e-7af677c4a9ef', 'true', 'userinfo.token.claim'),
	('49b51130-c60f-41a0-b70d-012e5daa616a', 'true', 'access.token.claim'),
	('49b51130-c60f-41a0-b70d-012e5daa616a', 'zoneinfo', 'claim.name'),
	('49b51130-c60f-41a0-b70d-012e5daa616a', 'true', 'id.token.claim'),
	('49b51130-c60f-41a0-b70d-012e5daa616a', 'String', 'jsonType.label'),
	('49b51130-c60f-41a0-b70d-012e5daa616a', 'zoneinfo', 'user.attribute'),
	('49b51130-c60f-41a0-b70d-012e5daa616a', 'true', 'userinfo.token.claim'),
	('4aee1543-571d-4009-bb8a-1b0db52afb7b', 'true', 'access.token.claim'),
	('4aee1543-571d-4009-bb8a-1b0db52afb7b', 'picture', 'claim.name'),
	('4aee1543-571d-4009-bb8a-1b0db52afb7b', 'true', 'id.token.claim'),
	('4aee1543-571d-4009-bb8a-1b0db52afb7b', 'String', 'jsonType.label'),
	('4aee1543-571d-4009-bb8a-1b0db52afb7b', 'picture', 'user.attribute'),
	('4aee1543-571d-4009-bb8a-1b0db52afb7b', 'true', 'userinfo.token.claim'),
	('4cc57dcd-8825-43a9-9a17-4ef96d7acf01', 'true', 'access.token.claim'),
	('4cc57dcd-8825-43a9-9a17-4ef96d7acf01', 'website', 'claim.name'),
	('4cc57dcd-8825-43a9-9a17-4ef96d7acf01', 'true', 'id.token.claim'),
	('4cc57dcd-8825-43a9-9a17-4ef96d7acf01', 'String', 'jsonType.label'),
	('4cc57dcd-8825-43a9-9a17-4ef96d7acf01', 'website', 'user.attribute'),
	('4cc57dcd-8825-43a9-9a17-4ef96d7acf01', 'true', 'userinfo.token.claim'),
	('5424501c-dc54-4a07-ad5d-54727f47e103', 'true', 'access.token.claim'),
	('5424501c-dc54-4a07-ad5d-54727f47e103', 'family_name', 'claim.name'),
	('5424501c-dc54-4a07-ad5d-54727f47e103', 'true', 'id.token.claim'),
	('5424501c-dc54-4a07-ad5d-54727f47e103', 'String', 'jsonType.label'),
	('5424501c-dc54-4a07-ad5d-54727f47e103', 'lastName', 'user.attribute'),
	('5424501c-dc54-4a07-ad5d-54727f47e103', 'true', 'userinfo.token.claim'),
	('63688f9a-15d2-42cf-ab9a-66e54794b154', 'true', 'access.token.claim'),
	('63688f9a-15d2-42cf-ab9a-66e54794b154', 'phone_number', 'claim.name'),
	('63688f9a-15d2-42cf-ab9a-66e54794b154', 'true', 'id.token.claim'),
	('63688f9a-15d2-42cf-ab9a-66e54794b154', 'String', 'jsonType.label'),
	('63688f9a-15d2-42cf-ab9a-66e54794b154', 'phoneNumber', 'user.attribute'),
	('63688f9a-15d2-42cf-ab9a-66e54794b154', 'true', 'userinfo.token.claim'),
	('66698f98-18a6-45bb-b37f-d960b2b09551', 'true', 'access.token.claim'),
	('66698f98-18a6-45bb-b37f-d960b2b09551', 'website', 'claim.name'),
	('66698f98-18a6-45bb-b37f-d960b2b09551', 'true', 'id.token.claim'),
	('66698f98-18a6-45bb-b37f-d960b2b09551', 'String', 'jsonType.label'),
	('66698f98-18a6-45bb-b37f-d960b2b09551', 'website', 'user.attribute'),
	('66698f98-18a6-45bb-b37f-d960b2b09551', 'true', 'userinfo.token.claim'),
	('67787520-7745-4203-8d6c-062972007b13', 'true', 'access.token.claim'),
	('67787520-7745-4203-8d6c-062972007b13', 'updated_at', 'claim.name'),
	('67787520-7745-4203-8d6c-062972007b13', 'true', 'id.token.claim'),
	('67787520-7745-4203-8d6c-062972007b13', 'long', 'jsonType.label'),
	('67787520-7745-4203-8d6c-062972007b13', 'updatedAt', 'user.attribute'),
	('67787520-7745-4203-8d6c-062972007b13', 'true', 'userinfo.token.claim'),
	('692cde83-3d63-4844-a15b-c6af9a8f9bb3', 'true', 'access.token.claim'),
	('692cde83-3d63-4844-a15b-c6af9a8f9bb3', 'updated_at', 'claim.name'),
	('692cde83-3d63-4844-a15b-c6af9a8f9bb3', 'true', 'id.token.claim'),
	('692cde83-3d63-4844-a15b-c6af9a8f9bb3', 'long', 'jsonType.label'),
	('692cde83-3d63-4844-a15b-c6af9a8f9bb3', 'updatedAt', 'user.attribute'),
	('692cde83-3d63-4844-a15b-c6af9a8f9bb3', 'true', 'userinfo.token.claim'),
	('7091736e-b0ae-4c02-bffa-eedf51ad3be0', 'true', 'access.token.claim'),
	('7091736e-b0ae-4c02-bffa-eedf51ad3be0', 'given_name', 'claim.name'),
	('7091736e-b0ae-4c02-bffa-eedf51ad3be0', 'true', 'id.token.claim'),
	('7091736e-b0ae-4c02-bffa-eedf51ad3be0', 'String', 'jsonType.label'),
	('7091736e-b0ae-4c02-bffa-eedf51ad3be0', 'firstName', 'user.attribute'),
	('7091736e-b0ae-4c02-bffa-eedf51ad3be0', 'true', 'userinfo.token.claim'),
	('778292ab-821e-4a06-8855-4f8172b3a1e4', 'true', 'access.token.claim'),
	('778292ab-821e-4a06-8855-4f8172b3a1e4', 'given_name', 'claim.name'),
	('778292ab-821e-4a06-8855-4f8172b3a1e4', 'true', 'id.token.claim'),
	('778292ab-821e-4a06-8855-4f8172b3a1e4', 'String', 'jsonType.label'),
	('778292ab-821e-4a06-8855-4f8172b3a1e4', 'firstName', 'user.attribute'),
	('778292ab-821e-4a06-8855-4f8172b3a1e4', 'true', 'userinfo.token.claim'),
	('80fba5f7-1ef5-49bd-8f06-a4332f384ac8', 'true', 'access.token.claim'),
	('80fba5f7-1ef5-49bd-8f06-a4332f384ac8', 'true', 'id.token.claim'),
	('81f66368-badd-4b19-be6f-ddb665bde492', 'true', 'access.token.claim'),
	('81f66368-badd-4b19-be6f-ddb665bde492', 'locale', 'claim.name'),
	('81f66368-badd-4b19-be6f-ddb665bde492', 'true', 'id.token.claim'),
	('81f66368-badd-4b19-be6f-ddb665bde492', 'String', 'jsonType.label'),
	('81f66368-badd-4b19-be6f-ddb665bde492', 'locale', 'user.attribute'),
	('81f66368-badd-4b19-be6f-ddb665bde492', 'true', 'userinfo.token.claim'),
	('82ade246-b424-4674-bc9c-8f6d907bd3d4', 'true', 'access.token.claim'),
	('82ade246-b424-4674-bc9c-8f6d907bd3d4', 'email', 'claim.name'),
	('82ade246-b424-4674-bc9c-8f6d907bd3d4', 'true', 'id.token.claim'),
	('82ade246-b424-4674-bc9c-8f6d907bd3d4', 'String', 'jsonType.label'),
	('82ade246-b424-4674-bc9c-8f6d907bd3d4', 'email', 'user.attribute'),
	('82ade246-b424-4674-bc9c-8f6d907bd3d4', 'true', 'userinfo.token.claim'),
	('85ef208e-f75d-4496-bb99-5b2cc3aae98a', 'true', 'access.token.claim'),
	('85ef208e-f75d-4496-bb99-5b2cc3aae98a', 'preferred_username', 'claim.name'),
	('85ef208e-f75d-4496-bb99-5b2cc3aae98a', 'true', 'id.token.claim'),
	('85ef208e-f75d-4496-bb99-5b2cc3aae98a', 'String', 'jsonType.label'),
	('85ef208e-f75d-4496-bb99-5b2cc3aae98a', 'username', 'user.attribute'),
	('85ef208e-f75d-4496-bb99-5b2cc3aae98a', 'true', 'userinfo.token.claim'),
	('86742682-9275-4e17-93d3-84857b09eadf', 'true', 'access.token.claim'),
	('86742682-9275-4e17-93d3-84857b09eadf', 'phone_number_verified', 'claim.name'),
	('86742682-9275-4e17-93d3-84857b09eadf', 'true', 'id.token.claim'),
	('86742682-9275-4e17-93d3-84857b09eadf', 'boolean', 'jsonType.label'),
	('86742682-9275-4e17-93d3-84857b09eadf', 'phoneNumberVerified', 'user.attribute'),
	('86742682-9275-4e17-93d3-84857b09eadf', 'true', 'userinfo.token.claim'),
	('898b093c-66e7-4741-b9f7-5df4555208dc', 'Role', 'attribute.name'),
	('898b093c-66e7-4741-b9f7-5df4555208dc', 'Basic', 'attribute.nameformat'),
	('898b093c-66e7-4741-b9f7-5df4555208dc', 'false', 'single'),
	('8c120346-5e91-4d08-866e-89fb2b4be705', 'true', 'access.token.claim'),
	('8c120346-5e91-4d08-866e-89fb2b4be705', 'profile', 'claim.name'),
	('8c120346-5e91-4d08-866e-89fb2b4be705', 'true', 'id.token.claim'),
	('8c120346-5e91-4d08-866e-89fb2b4be705', 'String', 'jsonType.label'),
	('8c120346-5e91-4d08-866e-89fb2b4be705', 'profile', 'user.attribute'),
	('8c120346-5e91-4d08-866e-89fb2b4be705', 'true', 'userinfo.token.claim'),
	('977984ee-81de-4da3-9bb4-72ffa9585495', 'true', 'access.token.claim'),
	('977984ee-81de-4da3-9bb4-72ffa9585495', 'locale', 'claim.name'),
	('977984ee-81de-4da3-9bb4-72ffa9585495', 'true', 'id.token.claim'),
	('977984ee-81de-4da3-9bb4-72ffa9585495', 'String', 'jsonType.label'),
	('977984ee-81de-4da3-9bb4-72ffa9585495', 'locale', 'user.attribute'),
	('977984ee-81de-4da3-9bb4-72ffa9585495', 'true', 'userinfo.token.claim'),
	('980f21f3-30d3-4d9a-95c6-62754e037e7e', 'true', 'access.token.claim'),
	('980f21f3-30d3-4d9a-95c6-62754e037e7e', 'groups', 'claim.name'),
	('980f21f3-30d3-4d9a-95c6-62754e037e7e', 'true', 'id.token.claim'),
	('980f21f3-30d3-4d9a-95c6-62754e037e7e', 'String', 'jsonType.label'),
	('980f21f3-30d3-4d9a-95c6-62754e037e7e', 'true', 'multivalued'),
	('980f21f3-30d3-4d9a-95c6-62754e037e7e', 'foo', 'user.attribute'),
	('9893b4a0-24dc-4a84-8d16-16f4fd90b0ce', 'true', 'access.token.claim'),
	('9893b4a0-24dc-4a84-8d16-16f4fd90b0ce', 'birthdate', 'claim.name'),
	('9893b4a0-24dc-4a84-8d16-16f4fd90b0ce', 'true', 'id.token.claim'),
	('9893b4a0-24dc-4a84-8d16-16f4fd90b0ce', 'String', 'jsonType.label'),
	('9893b4a0-24dc-4a84-8d16-16f4fd90b0ce', 'birthdate', 'user.attribute'),
	('9893b4a0-24dc-4a84-8d16-16f4fd90b0ce', 'true', 'userinfo.token.claim'),
	('9d7065a2-eed9-43ef-9355-c5dd0a929d75', 'true', 'access.token.claim'),
	('9d7065a2-eed9-43ef-9355-c5dd0a929d75', 'middle_name', 'claim.name'),
	('9d7065a2-eed9-43ef-9355-c5dd0a929d75', 'true', 'id.token.claim'),
	('9d7065a2-eed9-43ef-9355-c5dd0a929d75', 'String', 'jsonType.label'),
	('9d7065a2-eed9-43ef-9355-c5dd0a929d75', 'middleName', 'user.attribute'),
	('9d7065a2-eed9-43ef-9355-c5dd0a929d75', 'true', 'userinfo.token.claim'),
	('a4d908af-e3cb-470e-93ea-3c14323d6084', 'true', 'access.token.claim'),
	('a4d908af-e3cb-470e-93ea-3c14323d6084', 'realm_access.roles', 'claim.name'),
	('a4d908af-e3cb-470e-93ea-3c14323d6084', 'String', 'jsonType.label'),
	('a4d908af-e3cb-470e-93ea-3c14323d6084', 'true', 'multivalued'),
	('a4d908af-e3cb-470e-93ea-3c14323d6084', 'foo', 'user.attribute'),
	('aede3ecc-74ee-4260-b599-e63761628e65', 'Role', 'attribute.name'),
	('aede3ecc-74ee-4260-b599-e63761628e65', 'Basic', 'attribute.nameformat'),
	('aede3ecc-74ee-4260-b599-e63761628e65', 'false', 'single'),
	('b080e04f-b0c9-44e0-bd29-d319bbf24006', 'true', 'access.token.claim'),
	('b080e04f-b0c9-44e0-bd29-d319bbf24006', 'true', 'id.token.claim'),
	('b080e04f-b0c9-44e0-bd29-d319bbf24006', 'true', 'userinfo.token.claim'),
	('b8b94227-ddf5-4992-b7d0-c8bb696a7c79', 'true', 'access.token.claim'),
	('b8b94227-ddf5-4992-b7d0-c8bb696a7c79', 'realm_access.roles', 'claim.name'),
	('b8b94227-ddf5-4992-b7d0-c8bb696a7c79', 'String', 'jsonType.label'),
	('b8b94227-ddf5-4992-b7d0-c8bb696a7c79', 'true', 'multivalued'),
	('b8b94227-ddf5-4992-b7d0-c8bb696a7c79', 'foo', 'user.attribute'),
	('bde524c2-3f09-403f-8a6f-2f62ecb9bd8c', 'true', 'access.token.claim'),
	('bde524c2-3f09-403f-8a6f-2f62ecb9bd8c', 'resource_access.${client_id}.roles', 'claim.name'),
	('bde524c2-3f09-403f-8a6f-2f62ecb9bd8c', 'String', 'jsonType.label'),
	('bde524c2-3f09-403f-8a6f-2f62ecb9bd8c', 'true', 'multivalued'),
	('bde524c2-3f09-403f-8a6f-2f62ecb9bd8c', 'foo', 'user.attribute'),
	('c81009ee-e976-4344-b1b5-52af80fe04b2', 'true', 'access.token.claim'),
	('c81009ee-e976-4344-b1b5-52af80fe04b2', 'upn', 'claim.name'),
	('c81009ee-e976-4344-b1b5-52af80fe04b2', 'true', 'id.token.claim'),
	('c81009ee-e976-4344-b1b5-52af80fe04b2', 'String', 'jsonType.label'),
	('c81009ee-e976-4344-b1b5-52af80fe04b2', 'username', 'user.attribute'),
	('c81009ee-e976-4344-b1b5-52af80fe04b2', 'true', 'userinfo.token.claim'),
	('cf833979-19a8-42a6-903a-9ca8f980e592', 'true', 'access.token.claim'),
	('cf833979-19a8-42a6-903a-9ca8f980e592', 'picture', 'claim.name'),
	('cf833979-19a8-42a6-903a-9ca8f980e592', 'true', 'id.token.claim'),
	('cf833979-19a8-42a6-903a-9ca8f980e592', 'String', 'jsonType.label'),
	('cf833979-19a8-42a6-903a-9ca8f980e592', 'picture', 'user.attribute'),
	('cf833979-19a8-42a6-903a-9ca8f980e592', 'true', 'userinfo.token.claim'),
	('de73e919-404e-49c2-a1ea-79fe10a30501', 'true', 'access.token.claim'),
	('de73e919-404e-49c2-a1ea-79fe10a30501', 'profile', 'claim.name'),
	('de73e919-404e-49c2-a1ea-79fe10a30501', 'true', 'id.token.claim'),
	('de73e919-404e-49c2-a1ea-79fe10a30501', 'String', 'jsonType.label'),
	('de73e919-404e-49c2-a1ea-79fe10a30501', 'profile', 'user.attribute'),
	('de73e919-404e-49c2-a1ea-79fe10a30501', 'true', 'userinfo.token.claim'),
	('e5c8ebd9-b0ac-4c95-af3e-daa297cdde3b', 'true', 'access.token.claim'),
	('e5c8ebd9-b0ac-4c95-af3e-daa297cdde3b', 'nickname', 'claim.name'),
	('e5c8ebd9-b0ac-4c95-af3e-daa297cdde3b', 'true', 'id.token.claim'),
	('e5c8ebd9-b0ac-4c95-af3e-daa297cdde3b', 'String', 'jsonType.label'),
	('e5c8ebd9-b0ac-4c95-af3e-daa297cdde3b', 'nickname', 'user.attribute'),
	('e5c8ebd9-b0ac-4c95-af3e-daa297cdde3b', 'true', 'userinfo.token.claim'),
	('e811726f-12e6-40d6-9ef1-b08cf9242c94', 'true', 'access.token.claim'),
	('e811726f-12e6-40d6-9ef1-b08cf9242c94', 'zoneinfo', 'claim.name'),
	('e811726f-12e6-40d6-9ef1-b08cf9242c94', 'true', 'id.token.claim'),
	('e811726f-12e6-40d6-9ef1-b08cf9242c94', 'String', 'jsonType.label'),
	('e811726f-12e6-40d6-9ef1-b08cf9242c94', 'zoneinfo', 'user.attribute'),
	('e811726f-12e6-40d6-9ef1-b08cf9242c94', 'true', 'userinfo.token.claim'),
	('e9859deb-80e0-4cfd-b9dd-c7bad797bb88', 'true', 'access.token.claim'),
	('e9859deb-80e0-4cfd-b9dd-c7bad797bb88', 'gender', 'claim.name'),
	('e9859deb-80e0-4cfd-b9dd-c7bad797bb88', 'true', 'id.token.claim'),
	('e9859deb-80e0-4cfd-b9dd-c7bad797bb88', 'String', 'jsonType.label'),
	('e9859deb-80e0-4cfd-b9dd-c7bad797bb88', 'gender', 'user.attribute'),
	('e9859deb-80e0-4cfd-b9dd-c7bad797bb88', 'true', 'userinfo.token.claim'),
	('eb0fca1b-8134-47bb-aa32-ed2939e7b89c', 'true', 'access.token.claim'),
	('eb0fca1b-8134-47bb-aa32-ed2939e7b89c', 'birthdate', 'claim.name'),
	('eb0fca1b-8134-47bb-aa32-ed2939e7b89c', 'true', 'id.token.claim'),
	('eb0fca1b-8134-47bb-aa32-ed2939e7b89c', 'String', 'jsonType.label'),
	('eb0fca1b-8134-47bb-aa32-ed2939e7b89c', 'birthdate', 'user.attribute'),
	('eb0fca1b-8134-47bb-aa32-ed2939e7b89c', 'true', 'userinfo.token.claim'),
	('eec27305-9814-4bc7-80e5-c6db533d67c2', 'true', 'access.token.claim'),
	('eec27305-9814-4bc7-80e5-c6db533d67c2', 'preferred_username', 'claim.name'),
	('eec27305-9814-4bc7-80e5-c6db533d67c2', 'true', 'id.token.claim'),
	('eec27305-9814-4bc7-80e5-c6db533d67c2', 'String', 'jsonType.label'),
	('eec27305-9814-4bc7-80e5-c6db533d67c2', 'username', 'user.attribute'),
	('eec27305-9814-4bc7-80e5-c6db533d67c2', 'true', 'userinfo.token.claim'),
	('ef1b8b37-574f-42b3-be35-f0aae21adf94', 'true', 'access.token.claim'),
	('ef1b8b37-574f-42b3-be35-f0aae21adf94', 'groups', 'claim.name'),
	('ef1b8b37-574f-42b3-be35-f0aae21adf94', 'true', 'id.token.claim'),
	('ef1b8b37-574f-42b3-be35-f0aae21adf94', 'String', 'jsonType.label'),
	('ef1b8b37-574f-42b3-be35-f0aae21adf94', 'true', 'multivalued'),
	('ef1b8b37-574f-42b3-be35-f0aae21adf94', 'foo', 'user.attribute'),
	('f020c210-bbd8-4210-9e30-749ea818ddc7', 'true', 'access.token.claim'),
	('f020c210-bbd8-4210-9e30-749ea818ddc7', 'resource_access.${client_id}.roles', 'claim.name'),
	('f020c210-bbd8-4210-9e30-749ea818ddc7', 'String', 'jsonType.label'),
	('f020c210-bbd8-4210-9e30-749ea818ddc7', 'true', 'multivalued'),
	('f020c210-bbd8-4210-9e30-749ea818ddc7', 'foo', 'user.attribute'),
	('f344f839-de00-44f2-b664-c07367d19a8b', 'true', 'access.token.claim'),
	('f344f839-de00-44f2-b664-c07367d19a8b', 'email_verified', 'claim.name'),
	('f344f839-de00-44f2-b664-c07367d19a8b', 'true', 'id.token.claim'),
	('f344f839-de00-44f2-b664-c07367d19a8b', 'boolean', 'jsonType.label'),
	('f344f839-de00-44f2-b664-c07367d19a8b', 'emailVerified', 'user.attribute'),
	('f344f839-de00-44f2-b664-c07367d19a8b', 'true', 'userinfo.token.claim'),
	('fbb59320-a064-4c21-af44-1fbd5599a5be', 'true', 'access.token.claim'),
	('fbb59320-a064-4c21-af44-1fbd5599a5be', 'upn', 'claim.name'),
	('fbb59320-a064-4c21-af44-1fbd5599a5be', 'true', 'id.token.claim'),
	('fbb59320-a064-4c21-af44-1fbd5599a5be', 'String', 'jsonType.label'),
	('fbb59320-a064-4c21-af44-1fbd5599a5be', 'username', 'user.attribute'),
	('fbb59320-a064-4c21-af44-1fbd5599a5be', 'true', 'userinfo.token.claim'),
	('fd137ddd-44d1-49c4-a990-3c42c5fe0e21', 'true', 'access.token.claim'),
	('fd137ddd-44d1-49c4-a990-3c42c5fe0e21', 'phone_number_verified', 'claim.name'),
	('fd137ddd-44d1-49c4-a990-3c42c5fe0e21', 'true', 'id.token.claim'),
	('fd137ddd-44d1-49c4-a990-3c42c5fe0e21', 'boolean', 'jsonType.label'),
	('fd137ddd-44d1-49c4-a990-3c42c5fe0e21', 'phoneNumberVerified', 'user.attribute'),
	('fd137ddd-44d1-49c4-a990-3c42c5fe0e21', 'true', 'userinfo.token.claim');

-- Exportiere Struktur von Tabelle keycloak.realm
CREATE TABLE IF NOT EXISTS `realm` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `ACCESS_CODE_LIFESPAN` int(11) DEFAULT NULL,
  `USER_ACTION_LIFESPAN` int(11) DEFAULT NULL,
  `ACCESS_TOKEN_LIFESPAN` int(11) DEFAULT NULL,
  `ACCOUNT_THEME` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `ADMIN_THEME` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `EMAIL_THEME` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `EVENTS_ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `EVENTS_EXPIRATION` bigint(20) DEFAULT NULL,
  `LOGIN_THEME` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `NOT_BEFORE` int(11) DEFAULT NULL,
  `PASSWORD_POLICY` text COLLATE utf8mb4_bin DEFAULT NULL,
  `REGISTRATION_ALLOWED` bit(1) NOT NULL DEFAULT b'0',
  `REMEMBER_ME` bit(1) NOT NULL DEFAULT b'0',
  `RESET_PASSWORD_ALLOWED` bit(1) NOT NULL DEFAULT b'0',
  `SOCIAL` bit(1) NOT NULL DEFAULT b'0',
  `SSL_REQUIRED` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `SSO_IDLE_TIMEOUT` int(11) DEFAULT NULL,
  `SSO_MAX_LIFESPAN` int(11) DEFAULT NULL,
  `UPDATE_PROFILE_ON_SOC_LOGIN` bit(1) NOT NULL DEFAULT b'0',
  `VERIFY_EMAIL` bit(1) NOT NULL DEFAULT b'0',
  `MASTER_ADMIN_CLIENT` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `LOGIN_LIFESPAN` int(11) DEFAULT NULL,
  `INTERNATIONALIZATION_ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `DEFAULT_LOCALE` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `REG_EMAIL_AS_USERNAME` bit(1) NOT NULL DEFAULT b'0',
  `ADMIN_EVENTS_ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `ADMIN_EVENTS_DETAILS_ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `EDIT_USERNAME_ALLOWED` bit(1) NOT NULL DEFAULT b'0',
  `OTP_POLICY_COUNTER` int(11) DEFAULT 0,
  `OTP_POLICY_WINDOW` int(11) DEFAULT 1,
  `OTP_POLICY_PERIOD` int(11) DEFAULT 30,
  `OTP_POLICY_DIGITS` int(11) DEFAULT 6,
  `OTP_POLICY_ALG` varchar(36) COLLATE utf8mb4_bin DEFAULT 'HmacSHA1',
  `OTP_POLICY_TYPE` varchar(36) COLLATE utf8mb4_bin DEFAULT 'totp',
  `BROWSER_FLOW` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `REGISTRATION_FLOW` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `DIRECT_GRANT_FLOW` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `RESET_CREDENTIALS_FLOW` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `CLIENT_AUTH_FLOW` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `OFFLINE_SESSION_IDLE_TIMEOUT` int(11) DEFAULT 0,
  `REVOKE_REFRESH_TOKEN` bit(1) NOT NULL DEFAULT b'0',
  `ACCESS_TOKEN_LIFE_IMPLICIT` int(11) DEFAULT 0,
  `LOGIN_WITH_EMAIL_ALLOWED` bit(1) NOT NULL DEFAULT b'1',
  `DUPLICATE_EMAILS_ALLOWED` bit(1) NOT NULL DEFAULT b'0',
  `DOCKER_AUTH_FLOW` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `REFRESH_TOKEN_MAX_REUSE` int(11) DEFAULT 0,
  `ALLOW_USER_MANAGED_ACCESS` bit(1) NOT NULL DEFAULT b'0',
  `SSO_MAX_LIFESPAN_REMEMBER_ME` int(11) NOT NULL,
  `SSO_IDLE_TIMEOUT_REMEMBER_ME` int(11) NOT NULL,
  `DEFAULT_ROLE` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_ORVSDMLA56612EAEFIQ6WL5OI` (`NAME`),
  KEY `IDX_REALM_MASTER_ADM_CLI` (`MASTER_ADMIN_CLIENT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.realm: ~2 rows (ungefähr)
DELETE FROM `realm`;
INSERT INTO `realm` (`ID`, `ACCESS_CODE_LIFESPAN`, `USER_ACTION_LIFESPAN`, `ACCESS_TOKEN_LIFESPAN`, `ACCOUNT_THEME`, `ADMIN_THEME`, `EMAIL_THEME`, `ENABLED`, `EVENTS_ENABLED`, `EVENTS_EXPIRATION`, `LOGIN_THEME`, `NAME`, `NOT_BEFORE`, `PASSWORD_POLICY`, `REGISTRATION_ALLOWED`, `REMEMBER_ME`, `RESET_PASSWORD_ALLOWED`, `SOCIAL`, `SSL_REQUIRED`, `SSO_IDLE_TIMEOUT`, `SSO_MAX_LIFESPAN`, `UPDATE_PROFILE_ON_SOC_LOGIN`, `VERIFY_EMAIL`, `MASTER_ADMIN_CLIENT`, `LOGIN_LIFESPAN`, `INTERNATIONALIZATION_ENABLED`, `DEFAULT_LOCALE`, `REG_EMAIL_AS_USERNAME`, `ADMIN_EVENTS_ENABLED`, `ADMIN_EVENTS_DETAILS_ENABLED`, `EDIT_USERNAME_ALLOWED`, `OTP_POLICY_COUNTER`, `OTP_POLICY_WINDOW`, `OTP_POLICY_PERIOD`, `OTP_POLICY_DIGITS`, `OTP_POLICY_ALG`, `OTP_POLICY_TYPE`, `BROWSER_FLOW`, `REGISTRATION_FLOW`, `DIRECT_GRANT_FLOW`, `RESET_CREDENTIALS_FLOW`, `CLIENT_AUTH_FLOW`, `OFFLINE_SESSION_IDLE_TIMEOUT`, `REVOKE_REFRESH_TOKEN`, `ACCESS_TOKEN_LIFE_IMPLICIT`, `LOGIN_WITH_EMAIL_ALLOWED`, `DUPLICATE_EMAILS_ALLOWED`, `DOCKER_AUTH_FLOW`, `REFRESH_TOKEN_MAX_REUSE`, `ALLOW_USER_MANAGED_ACCESS`, `SSO_MAX_LIFESPAN_REMEMBER_ME`, `SSO_IDLE_TIMEOUT_REMEMBER_ME`, `DEFAULT_ROLE`) VALUES
	('23d8cb0f-53b6-4402-a9f9-072c402d5927', 60, 300, 60, NULL, NULL, NULL, b'1', b'0', 0, NULL, 'master', 0, NULL, b'0', b'0', b'0', b'0', 'EXTERNAL', 1800, 36000, b'0', b'0', '47ed0cd6-85c7-4377-bb6f-4d4c9b62c900', 1800, b'0', NULL, b'0', b'0', b'0', b'0', 0, 1, 30, 6, 'HmacSHA1', 'totp', '47b8216a-ea64-46a0-b859-e3fac25a40b4', '59d2dcfc-f1cc-4b71-90d0-e4708adfc1b8', '3d666503-5c58-4640-b2ac-1afc4e2492b6', '3876b1cf-8d8b-4e46-b7e6-b155694e07d4', '86ea6c61-8a23-4508-bfd8-aee426bb5202', 2592000, b'0', 900, b'1', b'0', '321b21ab-42cc-4507-84f9-5e3e5de52bb1', 0, b'0', 0, 0, '23eb6694-49b2-4fb4-b1d0-1bce51c5eb74'),
	('47958335-5c70-4603-abe7-b887490d7b1c', 60, 300, 300, NULL, NULL, NULL, b'1', b'0', 0, NULL, 'wurstwasser', 0, NULL, b'0', b'0', b'0', b'0', 'EXTERNAL', 1800, 36000, b'0', b'0', '58be2cf6-f202-4a7e-9cc9-a94bb28cec01', 1800, b'0', NULL, b'0', b'0', b'0', b'0', 0, 1, 30, 6, 'HmacSHA1', 'totp', '6c9cf822-831b-48f4-a8f3-5c0db558db8e', '51e69285-0fde-4d24-8c52-7f5394e04f24', 'e647ed12-8f0e-4095-ab6c-53ca78508944', '5bd4b3d8-c015-4fd6-9252-0ce4f1695222', '75beb864-f5e1-468a-a008-ebbd4999acdc', 2592000, b'0', 900, b'1', b'0', '4cd27323-0e9c-4156-b355-46e3dbe96609', 0, b'0', 0, 0, 'ddad37c9-58a3-4aa4-bdf6-509c4065822f');

-- Exportiere Struktur von Tabelle keycloak.realm_attribute
CREATE TABLE IF NOT EXISTS `realm_attribute` (
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` longtext CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`NAME`,`REALM_ID`),
  KEY `IDX_REALM_ATTR_REALM` (`REALM_ID`),
  CONSTRAINT `FK_8SHXD6L3E9ATQUKACXGPFFPTW` FOREIGN KEY (`REALM_ID`) REFERENCES `realm` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.realm_attribute: ~67 rows (ungefähr)
DELETE FROM `realm_attribute`;
INSERT INTO `realm_attribute` (`NAME`, `REALM_ID`, `VALUE`) VALUES
	('_browser_header.contentSecurityPolicy', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'frame-src \'self\'; frame-ancestors \'self\'; object-src \'none\';'),
	('_browser_header.contentSecurityPolicy', '47958335-5c70-4603-abe7-b887490d7b1c', 'frame-src \'self\'; frame-ancestors \'self\'; object-src \'none\';'),
	('_browser_header.contentSecurityPolicyReportOnly', '23d8cb0f-53b6-4402-a9f9-072c402d5927', ''),
	('_browser_header.contentSecurityPolicyReportOnly', '47958335-5c70-4603-abe7-b887490d7b1c', ''),
	('_browser_header.strictTransportSecurity', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'max-age=31536000; includeSubDomains'),
	('_browser_header.strictTransportSecurity', '47958335-5c70-4603-abe7-b887490d7b1c', 'max-age=31536000; includeSubDomains'),
	('_browser_header.xContentTypeOptions', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'nosniff'),
	('_browser_header.xContentTypeOptions', '47958335-5c70-4603-abe7-b887490d7b1c', 'nosniff'),
	('_browser_header.xFrameOptions', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'SAMEORIGIN'),
	('_browser_header.xFrameOptions', '47958335-5c70-4603-abe7-b887490d7b1c', 'SAMEORIGIN'),
	('_browser_header.xRobotsTag', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'none'),
	('_browser_header.xRobotsTag', '47958335-5c70-4603-abe7-b887490d7b1c', 'none'),
	('_browser_header.xXSSProtection', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '1; mode=block'),
	('_browser_header.xXSSProtection', '47958335-5c70-4603-abe7-b887490d7b1c', '1; mode=block'),
	('actionTokenGeneratedByAdminLifespan', '47958335-5c70-4603-abe7-b887490d7b1c', '43200'),
	('actionTokenGeneratedByUserLifespan', '47958335-5c70-4603-abe7-b887490d7b1c', '300'),
	('bruteForceProtected', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'false'),
	('bruteForceProtected', '47958335-5c70-4603-abe7-b887490d7b1c', 'false'),
	('cibaAuthRequestedUserHint', '47958335-5c70-4603-abe7-b887490d7b1c', 'login_hint'),
	('cibaBackchannelTokenDeliveryMode', '47958335-5c70-4603-abe7-b887490d7b1c', 'poll'),
	('cibaExpiresIn', '47958335-5c70-4603-abe7-b887490d7b1c', '120'),
	('cibaInterval', '47958335-5c70-4603-abe7-b887490d7b1c', '5'),
	('defaultSignatureAlgorithm', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'RS256'),
	('defaultSignatureAlgorithm', '47958335-5c70-4603-abe7-b887490d7b1c', 'RS256'),
	('displayName', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'Keycloak'),
	('displayNameHtml', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '<div class="kc-logo-text"><span>Keycloak</span></div>'),
	('failureFactor', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '30'),
	('failureFactor', '47958335-5c70-4603-abe7-b887490d7b1c', '30'),
	('maxDeltaTimeSeconds', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '43200'),
	('maxDeltaTimeSeconds', '47958335-5c70-4603-abe7-b887490d7b1c', '43200'),
	('maxFailureWaitSeconds', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '900'),
	('maxFailureWaitSeconds', '47958335-5c70-4603-abe7-b887490d7b1c', '900'),
	('minimumQuickLoginWaitSeconds', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '60'),
	('minimumQuickLoginWaitSeconds', '47958335-5c70-4603-abe7-b887490d7b1c', '60'),
	('oauth2DeviceCodeLifespan', '47958335-5c70-4603-abe7-b887490d7b1c', '600'),
	('oauth2DevicePollingInterval', '47958335-5c70-4603-abe7-b887490d7b1c', '5'),
	('offlineSessionMaxLifespan', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '5184000'),
	('offlineSessionMaxLifespan', '47958335-5c70-4603-abe7-b887490d7b1c', '5184000'),
	('offlineSessionMaxLifespanEnabled', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'false'),
	('offlineSessionMaxLifespanEnabled', '47958335-5c70-4603-abe7-b887490d7b1c', 'false'),
	('parRequestUriLifespan', '47958335-5c70-4603-abe7-b887490d7b1c', '60'),
	('permanentLockout', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'false'),
	('permanentLockout', '47958335-5c70-4603-abe7-b887490d7b1c', 'false'),
	('quickLoginCheckMilliSeconds', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '1000'),
	('quickLoginCheckMilliSeconds', '47958335-5c70-4603-abe7-b887490d7b1c', '1000'),
	('realmReusableOtpCode', '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'false'),
	('realmReusableOtpCode', '47958335-5c70-4603-abe7-b887490d7b1c', 'false'),
	('waitIncrementSeconds', '23d8cb0f-53b6-4402-a9f9-072c402d5927', '60'),
	('waitIncrementSeconds', '47958335-5c70-4603-abe7-b887490d7b1c', '60'),
	('webAuthnPolicyAttestationConveyancePreference', '47958335-5c70-4603-abe7-b887490d7b1c', 'not specified'),
	('webAuthnPolicyAttestationConveyancePreferencePasswordless', '47958335-5c70-4603-abe7-b887490d7b1c', 'not specified'),
	('webAuthnPolicyAuthenticatorAttachment', '47958335-5c70-4603-abe7-b887490d7b1c', 'not specified'),
	('webAuthnPolicyAuthenticatorAttachmentPasswordless', '47958335-5c70-4603-abe7-b887490d7b1c', 'not specified'),
	('webAuthnPolicyAvoidSameAuthenticatorRegister', '47958335-5c70-4603-abe7-b887490d7b1c', 'false'),
	('webAuthnPolicyAvoidSameAuthenticatorRegisterPasswordless', '47958335-5c70-4603-abe7-b887490d7b1c', 'false'),
	('webAuthnPolicyCreateTimeout', '47958335-5c70-4603-abe7-b887490d7b1c', '0'),
	('webAuthnPolicyCreateTimeoutPasswordless', '47958335-5c70-4603-abe7-b887490d7b1c', '0'),
	('webAuthnPolicyRequireResidentKey', '47958335-5c70-4603-abe7-b887490d7b1c', 'not specified'),
	('webAuthnPolicyRequireResidentKeyPasswordless', '47958335-5c70-4603-abe7-b887490d7b1c', 'not specified'),
	('webAuthnPolicyRpEntityName', '47958335-5c70-4603-abe7-b887490d7b1c', 'keycloak'),
	('webAuthnPolicyRpEntityNamePasswordless', '47958335-5c70-4603-abe7-b887490d7b1c', 'keycloak'),
	('webAuthnPolicyRpId', '47958335-5c70-4603-abe7-b887490d7b1c', ''),
	('webAuthnPolicyRpIdPasswordless', '47958335-5c70-4603-abe7-b887490d7b1c', ''),
	('webAuthnPolicySignatureAlgorithms', '47958335-5c70-4603-abe7-b887490d7b1c', 'ES256'),
	('webAuthnPolicySignatureAlgorithmsPasswordless', '47958335-5c70-4603-abe7-b887490d7b1c', 'ES256'),
	('webAuthnPolicyUserVerificationRequirement', '47958335-5c70-4603-abe7-b887490d7b1c', 'not specified'),
	('webAuthnPolicyUserVerificationRequirementPasswordless', '47958335-5c70-4603-abe7-b887490d7b1c', 'not specified');

-- Exportiere Struktur von Tabelle keycloak.realm_default_groups
CREATE TABLE IF NOT EXISTS `realm_default_groups` (
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `GROUP_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`REALM_ID`,`GROUP_ID`),
  UNIQUE KEY `CON_GROUP_ID_DEF_GROUPS` (`GROUP_ID`),
  KEY `IDX_REALM_DEF_GRP_REALM` (`REALM_ID`),
  CONSTRAINT `FK_DEF_GROUPS_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `realm` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.realm_default_groups: ~0 rows (ungefähr)
DELETE FROM `realm_default_groups`;

-- Exportiere Struktur von Tabelle keycloak.realm_enabled_event_types
CREATE TABLE IF NOT EXISTS `realm_enabled_event_types` (
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`REALM_ID`,`VALUE`),
  KEY `IDX_REALM_EVT_TYPES_REALM` (`REALM_ID`),
  CONSTRAINT `FK_H846O4H0W8EPX5NWEDRF5Y69J` FOREIGN KEY (`REALM_ID`) REFERENCES `realm` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.realm_enabled_event_types: ~0 rows (ungefähr)
DELETE FROM `realm_enabled_event_types`;

-- Exportiere Struktur von Tabelle keycloak.realm_events_listeners
CREATE TABLE IF NOT EXISTS `realm_events_listeners` (
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`REALM_ID`,`VALUE`),
  KEY `IDX_REALM_EVT_LIST_REALM` (`REALM_ID`),
  CONSTRAINT `FK_H846O4H0W8EPX5NXEV9F5Y69J` FOREIGN KEY (`REALM_ID`) REFERENCES `realm` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.realm_events_listeners: ~2 rows (ungefähr)
DELETE FROM `realm_events_listeners`;
INSERT INTO `realm_events_listeners` (`REALM_ID`, `VALUE`) VALUES
	('23d8cb0f-53b6-4402-a9f9-072c402d5927', 'jboss-logging'),
	('47958335-5c70-4603-abe7-b887490d7b1c', 'jboss-logging');

-- Exportiere Struktur von Tabelle keycloak.realm_localizations
CREATE TABLE IF NOT EXISTS `realm_localizations` (
  `REALM_ID` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `LOCALE` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `TEXTS` longtext CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`REALM_ID`,`LOCALE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.realm_localizations: ~0 rows (ungefähr)
DELETE FROM `realm_localizations`;

-- Exportiere Struktur von Tabelle keycloak.realm_required_credential
CREATE TABLE IF NOT EXISTS `realm_required_credential` (
  `TYPE` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `FORM_LABEL` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `INPUT` bit(1) NOT NULL DEFAULT b'0',
  `SECRET` bit(1) NOT NULL DEFAULT b'0',
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`REALM_ID`,`TYPE`),
  CONSTRAINT `FK_5HG65LYBEVAVKQFKI3KPONH9V` FOREIGN KEY (`REALM_ID`) REFERENCES `realm` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.realm_required_credential: ~2 rows (ungefähr)
DELETE FROM `realm_required_credential`;
INSERT INTO `realm_required_credential` (`TYPE`, `FORM_LABEL`, `INPUT`, `SECRET`, `REALM_ID`) VALUES
	('password', 'password', b'1', b'1', '23d8cb0f-53b6-4402-a9f9-072c402d5927'),
	('password', 'password', b'1', b'1', '47958335-5c70-4603-abe7-b887490d7b1c');

-- Exportiere Struktur von Tabelle keycloak.realm_smtp_config
CREATE TABLE IF NOT EXISTS `realm_smtp_config` (
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`REALM_ID`,`NAME`),
  CONSTRAINT `FK_70EJ8XDXGXD0B9HH6180IRR0O` FOREIGN KEY (`REALM_ID`) REFERENCES `realm` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.realm_smtp_config: ~0 rows (ungefähr)
DELETE FROM `realm_smtp_config`;

-- Exportiere Struktur von Tabelle keycloak.realm_supported_locales
CREATE TABLE IF NOT EXISTS `realm_supported_locales` (
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`REALM_ID`,`VALUE`),
  KEY `IDX_REALM_SUPP_LOCAL_REALM` (`REALM_ID`),
  CONSTRAINT `FK_SUPPORTED_LOCALES_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `realm` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.realm_supported_locales: ~0 rows (ungefähr)
DELETE FROM `realm_supported_locales`;

-- Exportiere Struktur von Tabelle keycloak.redirect_uris
CREATE TABLE IF NOT EXISTS `redirect_uris` (
  `CLIENT_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`CLIENT_ID`,`VALUE`),
  KEY `IDX_REDIR_URI_CLIENT` (`CLIENT_ID`),
  CONSTRAINT `FK_1BURS8PB4OUJ97H5WUPPAHV9F` FOREIGN KEY (`CLIENT_ID`) REFERENCES `client` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.redirect_uris: ~8 rows (ungefähr)
DELETE FROM `redirect_uris`;
INSERT INTO `redirect_uris` (`CLIENT_ID`, `VALUE`) VALUES
	('035989c0-3119-47ee-98ca-db933a226945', 'http://localhost:4200/*'),
	('0c372b1f-3961-4907-83de-1153d2af2d57', '/realms/wurstwasser/account/*'),
	('2c77769b-4aab-4622-9e44-3a8433637f36', 'http://localhost:3333/*'),
	('2ef3bfdd-049a-4228-af7b-de4403fe6d04', '/realms/master/account/*'),
	('4ea4fda8-0d35-4644-9adf-24236632f114', '/realms/wurstwasser/account/*'),
	('808458f5-33ba-4e81-9601-019ae159abb3', '/admin/master/console/*'),
	('8d03d66e-c122-4357-b8ac-647ff79ac3be', '/admin/wurstwasser/console/*'),
	('cc4616ba-1805-413c-8edb-1f5315bb8df5', '/realms/master/account/*');

-- Exportiere Struktur von Tabelle keycloak.required_action_config
CREATE TABLE IF NOT EXISTS `required_action_config` (
  `REQUIRED_ACTION_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`REQUIRED_ACTION_ID`,`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.required_action_config: ~0 rows (ungefähr)
DELETE FROM `required_action_config`;

-- Exportiere Struktur von Tabelle keycloak.required_action_provider
CREATE TABLE IF NOT EXISTS `required_action_provider` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `ALIAS` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `DEFAULT_ACTION` bit(1) NOT NULL DEFAULT b'0',
  `PROVIDER_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_REQ_ACT_PROV_REALM` (`REALM_ID`),
  CONSTRAINT `FK_REQ_ACT_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `realm` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.required_action_provider: ~18 rows (ungefähr)
DELETE FROM `required_action_provider`;
INSERT INTO `required_action_provider` (`ID`, `ALIAS`, `NAME`, `REALM_ID`, `ENABLED`, `DEFAULT_ACTION`, `PROVIDER_ID`, `PRIORITY`) VALUES
	('0740e374-8916-4db3-be50-c3167cad747f', 'update_user_locale', 'Update User Locale', '23d8cb0f-53b6-4402-a9f9-072c402d5927', b'1', b'0', 'update_user_locale', 1000),
	('0c4db83a-054c-4727-a91d-d5a0aa353e12', 'CONFIGURE_TOTP', 'Configure OTP', '47958335-5c70-4603-abe7-b887490d7b1c', b'1', b'0', 'CONFIGURE_TOTP', 10),
	('26831d2c-5711-4946-a0d2-586de85046ec', 'webauthn-register', 'Webauthn Register', '23d8cb0f-53b6-4402-a9f9-072c402d5927', b'1', b'0', 'webauthn-register', 70),
	('2d62ac98-498f-464c-a4ef-26630d1f5d19', 'update_user_locale', 'Update User Locale', '47958335-5c70-4603-abe7-b887490d7b1c', b'1', b'0', 'update_user_locale', 1000),
	('48365acc-8b44-4c52-8169-f25ce3a070bb', 'delete_account', 'Delete Account', '23d8cb0f-53b6-4402-a9f9-072c402d5927', b'0', b'0', 'delete_account', 60),
	('48a7d843-5ffa-4d6f-9176-23556e9e23f6', 'webauthn-register-passwordless', 'Webauthn Register Passwordless', '23d8cb0f-53b6-4402-a9f9-072c402d5927', b'1', b'0', 'webauthn-register-passwordless', 80),
	('49d2ab8c-7941-45f0-a9b7-398debb5a612', 'VERIFY_EMAIL', 'Verify Email', '23d8cb0f-53b6-4402-a9f9-072c402d5927', b'1', b'0', 'VERIFY_EMAIL', 50),
	('517a1951-6cd0-4f11-b2ef-5e6d6bafbb9f', 'UPDATE_PROFILE', 'Update Profile', '47958335-5c70-4603-abe7-b887490d7b1c', b'1', b'0', 'UPDATE_PROFILE', 40),
	('5600af9c-c4ef-44e1-aba1-3a448eef40e4', 'UPDATE_PASSWORD', 'Update Password', '47958335-5c70-4603-abe7-b887490d7b1c', b'1', b'0', 'UPDATE_PASSWORD', 30),
	('59ea4ee4-508d-4e54-b13b-c2452ca4a7b4', 'webauthn-register-passwordless', 'Webauthn Register Passwordless', '47958335-5c70-4603-abe7-b887490d7b1c', b'1', b'0', 'webauthn-register-passwordless', 80),
	('671fbac9-b3b2-4ac2-be94-0b2b88a61033', 'CONFIGURE_TOTP', 'Configure OTP', '23d8cb0f-53b6-4402-a9f9-072c402d5927', b'1', b'0', 'CONFIGURE_TOTP', 10),
	('724a488e-6622-4b56-a5e3-724d6661880b', 'terms_and_conditions', 'Terms and Conditions', '23d8cb0f-53b6-4402-a9f9-072c402d5927', b'0', b'0', 'terms_and_conditions', 20),
	('85c1125f-02df-4b7f-8ba7-b38c90234faf', 'webauthn-register', 'Webauthn Register', '47958335-5c70-4603-abe7-b887490d7b1c', b'1', b'0', 'webauthn-register', 70),
	('acb3d5b7-79f3-458d-9ba5-c6523659ad3f', 'UPDATE_PROFILE', 'Update Profile', '23d8cb0f-53b6-4402-a9f9-072c402d5927', b'1', b'0', 'UPDATE_PROFILE', 40),
	('d7358e96-b668-45bf-91f1-c12306625f6b', 'VERIFY_EMAIL', 'Verify Email', '47958335-5c70-4603-abe7-b887490d7b1c', b'1', b'0', 'VERIFY_EMAIL', 50),
	('e11435d6-ad55-4b37-b6df-ff72bdb1fc89', 'UPDATE_PASSWORD', 'Update Password', '23d8cb0f-53b6-4402-a9f9-072c402d5927', b'1', b'0', 'UPDATE_PASSWORD', 30),
	('e90782d0-dbea-4a8a-bb7f-c85260897d46', 'terms_and_conditions', 'Terms and Conditions', '47958335-5c70-4603-abe7-b887490d7b1c', b'0', b'0', 'terms_and_conditions', 20),
	('f25985da-f143-496a-bc28-db80012c579f', 'delete_account', 'Delete Account', '47958335-5c70-4603-abe7-b887490d7b1c', b'0', b'0', 'delete_account', 60);

-- Exportiere Struktur von Tabelle keycloak.resource_attribute
CREATE TABLE IF NOT EXISTS `resource_attribute` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL DEFAULT 'sybase-needs-something-here',
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `RESOURCE_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_5HRM2VLF9QL5FU022KQEPOVBR` (`RESOURCE_ID`),
  CONSTRAINT `FK_5HRM2VLF9QL5FU022KQEPOVBR` FOREIGN KEY (`RESOURCE_ID`) REFERENCES `resource_server_resource` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.resource_attribute: ~0 rows (ungefähr)
DELETE FROM `resource_attribute`;

-- Exportiere Struktur von Tabelle keycloak.resource_policy
CREATE TABLE IF NOT EXISTS `resource_policy` (
  `RESOURCE_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `POLICY_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`RESOURCE_ID`,`POLICY_ID`),
  KEY `IDX_RES_POLICY_POLICY` (`POLICY_ID`),
  CONSTRAINT `FK_FRSRPOS53XCX4WNKOG82SSRFY` FOREIGN KEY (`RESOURCE_ID`) REFERENCES `resource_server_resource` (`ID`),
  CONSTRAINT `FK_FRSRPP213XCX4WNKOG82SSRFY` FOREIGN KEY (`POLICY_ID`) REFERENCES `resource_server_policy` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.resource_policy: ~0 rows (ungefähr)
DELETE FROM `resource_policy`;

-- Exportiere Struktur von Tabelle keycloak.resource_scope
CREATE TABLE IF NOT EXISTS `resource_scope` (
  `RESOURCE_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `SCOPE_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`RESOURCE_ID`,`SCOPE_ID`),
  KEY `IDX_RES_SCOPE_SCOPE` (`SCOPE_ID`),
  CONSTRAINT `FK_FRSRPOS13XCX4WNKOG82SSRFY` FOREIGN KEY (`RESOURCE_ID`) REFERENCES `resource_server_resource` (`ID`),
  CONSTRAINT `FK_FRSRPS213XCX4WNKOG82SSRFY` FOREIGN KEY (`SCOPE_ID`) REFERENCES `resource_server_scope` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.resource_scope: ~0 rows (ungefähr)
DELETE FROM `resource_scope`;

-- Exportiere Struktur von Tabelle keycloak.resource_server
CREATE TABLE IF NOT EXISTS `resource_server` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `ALLOW_RS_REMOTE_MGMT` bit(1) NOT NULL DEFAULT b'0',
  `POLICY_ENFORCE_MODE` varchar(15) COLLATE utf8mb4_bin NOT NULL,
  `DECISION_STRATEGY` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.resource_server: ~0 rows (ungefähr)
DELETE FROM `resource_server`;

-- Exportiere Struktur von Tabelle keycloak.resource_server_perm_ticket
CREATE TABLE IF NOT EXISTS `resource_server_perm_ticket` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `OWNER` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `REQUESTER` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `CREATED_TIMESTAMP` bigint(20) NOT NULL,
  `GRANTED_TIMESTAMP` bigint(20) DEFAULT NULL,
  `RESOURCE_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `SCOPE_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `RESOURCE_SERVER_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `POLICY_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_FRSR6T700S9V50BU18WS5PMT` (`OWNER`,`REQUESTER`,`RESOURCE_SERVER_ID`,`RESOURCE_ID`,`SCOPE_ID`),
  KEY `FK_FRSRHO213XCX4WNKOG82SSPMT` (`RESOURCE_SERVER_ID`),
  KEY `FK_FRSRHO213XCX4WNKOG83SSPMT` (`RESOURCE_ID`),
  KEY `FK_FRSRHO213XCX4WNKOG84SSPMT` (`SCOPE_ID`),
  KEY `FK_FRSRPO2128CX4WNKOG82SSRFY` (`POLICY_ID`),
  CONSTRAINT `FK_FRSRHO213XCX4WNKOG82SSPMT` FOREIGN KEY (`RESOURCE_SERVER_ID`) REFERENCES `resource_server` (`ID`),
  CONSTRAINT `FK_FRSRHO213XCX4WNKOG83SSPMT` FOREIGN KEY (`RESOURCE_ID`) REFERENCES `resource_server_resource` (`ID`),
  CONSTRAINT `FK_FRSRHO213XCX4WNKOG84SSPMT` FOREIGN KEY (`SCOPE_ID`) REFERENCES `resource_server_scope` (`ID`),
  CONSTRAINT `FK_FRSRPO2128CX4WNKOG82SSRFY` FOREIGN KEY (`POLICY_ID`) REFERENCES `resource_server_policy` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.resource_server_perm_ticket: ~0 rows (ungefähr)
DELETE FROM `resource_server_perm_ticket`;

-- Exportiere Struktur von Tabelle keycloak.resource_server_policy
CREATE TABLE IF NOT EXISTS `resource_server_policy` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TYPE` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `DECISION_STRATEGY` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL,
  `LOGIC` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL,
  `RESOURCE_SERVER_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `OWNER` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_FRSRPT700S9V50BU18WS5HA6` (`NAME`,`RESOURCE_SERVER_ID`),
  KEY `IDX_RES_SERV_POL_RES_SERV` (`RESOURCE_SERVER_ID`),
  CONSTRAINT `FK_FRSRPO213XCX4WNKOG82SSRFY` FOREIGN KEY (`RESOURCE_SERVER_ID`) REFERENCES `resource_server` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.resource_server_policy: ~0 rows (ungefähr)
DELETE FROM `resource_server_policy`;

-- Exportiere Struktur von Tabelle keycloak.resource_server_resource
CREATE TABLE IF NOT EXISTS `resource_server_resource` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `TYPE` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `ICON_URI` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `OWNER` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `RESOURCE_SERVER_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `OWNER_MANAGED_ACCESS` bit(1) NOT NULL DEFAULT b'0',
  `DISPLAY_NAME` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_FRSR6T700S9V50BU18WS5HA6` (`NAME`,`OWNER`,`RESOURCE_SERVER_ID`),
  KEY `IDX_RES_SRV_RES_RES_SRV` (`RESOURCE_SERVER_ID`),
  CONSTRAINT `FK_FRSRHO213XCX4WNKOG82SSRFY` FOREIGN KEY (`RESOURCE_SERVER_ID`) REFERENCES `resource_server` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.resource_server_resource: ~0 rows (ungefähr)
DELETE FROM `resource_server_resource`;

-- Exportiere Struktur von Tabelle keycloak.resource_server_scope
CREATE TABLE IF NOT EXISTS `resource_server_scope` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `ICON_URI` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `RESOURCE_SERVER_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `DISPLAY_NAME` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_FRSRST700S9V50BU18WS5HA6` (`NAME`,`RESOURCE_SERVER_ID`),
  KEY `IDX_RES_SRV_SCOPE_RES_SRV` (`RESOURCE_SERVER_ID`),
  CONSTRAINT `FK_FRSRSO213XCX4WNKOG82SSRFY` FOREIGN KEY (`RESOURCE_SERVER_ID`) REFERENCES `resource_server` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.resource_server_scope: ~0 rows (ungefähr)
DELETE FROM `resource_server_scope`;

-- Exportiere Struktur von Tabelle keycloak.resource_uris
CREATE TABLE IF NOT EXISTS `resource_uris` (
  `RESOURCE_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`RESOURCE_ID`,`VALUE`),
  CONSTRAINT `FK_RESOURCE_SERVER_URIS` FOREIGN KEY (`RESOURCE_ID`) REFERENCES `resource_server_resource` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.resource_uris: ~0 rows (ungefähr)
DELETE FROM `resource_uris`;

-- Exportiere Struktur von Tabelle keycloak.role_attribute
CREATE TABLE IF NOT EXISTS `role_attribute` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `ROLE_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_ROLE_ATTRIBUTE` (`ROLE_ID`),
  CONSTRAINT `FK_ROLE_ATTRIBUTE_ID` FOREIGN KEY (`ROLE_ID`) REFERENCES `keycloak_role` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.role_attribute: ~0 rows (ungefähr)
DELETE FROM `role_attribute`;

-- Exportiere Struktur von Tabelle keycloak.scope_mapping
CREATE TABLE IF NOT EXISTS `scope_mapping` (
  `CLIENT_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `ROLE_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`CLIENT_ID`,`ROLE_ID`),
  KEY `IDX_SCOPE_MAPPING_ROLE` (`ROLE_ID`),
  CONSTRAINT `FK_OUSE064PLMLR732LXJCN1Q5F1` FOREIGN KEY (`CLIENT_ID`) REFERENCES `client` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.scope_mapping: ~4 rows (ungefähr)
DELETE FROM `scope_mapping`;
INSERT INTO `scope_mapping` (`CLIENT_ID`, `ROLE_ID`) VALUES
	('0c372b1f-3961-4907-83de-1153d2af2d57', '3043dd87-953b-4a47-8cfd-d8b5f2a8932c'),
	('0c372b1f-3961-4907-83de-1153d2af2d57', '52551043-a306-46ca-9e9b-5dcc8e1ff935'),
	('cc4616ba-1805-413c-8edb-1f5315bb8df5', '5afd0771-9254-44c6-9132-2e0892992254'),
	('cc4616ba-1805-413c-8edb-1f5315bb8df5', 'f2e7ece9-6fe3-4af0-941b-b16af4e32e53');

-- Exportiere Struktur von Tabelle keycloak.scope_policy
CREATE TABLE IF NOT EXISTS `scope_policy` (
  `SCOPE_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `POLICY_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`SCOPE_ID`,`POLICY_ID`),
  KEY `IDX_SCOPE_POLICY_POLICY` (`POLICY_ID`),
  CONSTRAINT `FK_FRSRASP13XCX4WNKOG82SSRFY` FOREIGN KEY (`POLICY_ID`) REFERENCES `resource_server_policy` (`ID`),
  CONSTRAINT `FK_FRSRPASS3XCX4WNKOG82SSRFY` FOREIGN KEY (`SCOPE_ID`) REFERENCES `resource_server_scope` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.scope_policy: ~0 rows (ungefähr)
DELETE FROM `scope_policy`;

-- Exportiere Struktur von Tabelle keycloak.username_login_failure
CREATE TABLE IF NOT EXISTS `username_login_failure` (
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `USERNAME` varchar(255) CHARACTER SET utf8 NOT NULL,
  `FAILED_LOGIN_NOT_BEFORE` int(11) DEFAULT NULL,
  `LAST_FAILURE` bigint(20) DEFAULT NULL,
  `LAST_IP_FAILURE` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `NUM_FAILURES` int(11) DEFAULT NULL,
  PRIMARY KEY (`REALM_ID`,`USERNAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.username_login_failure: ~0 rows (ungefähr)
DELETE FROM `username_login_failure`;

-- Exportiere Struktur von Tabelle keycloak.user_attribute
CREATE TABLE IF NOT EXISTS `user_attribute` (
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `USER_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL DEFAULT 'sybase-needs-something-here',
  PRIMARY KEY (`ID`),
  KEY `IDX_USER_ATTRIBUTE` (`USER_ID`),
  KEY `IDX_USER_ATTRIBUTE_NAME` (`NAME`,`VALUE`),
  CONSTRAINT `FK_5HRM2VLF9QL5FU043KQEPOVBR` FOREIGN KEY (`USER_ID`) REFERENCES `user_entity` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.user_attribute: ~0 rows (ungefähr)
DELETE FROM `user_attribute`;

-- Exportiere Struktur von Tabelle keycloak.user_consent
CREATE TABLE IF NOT EXISTS `user_consent` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `CLIENT_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `USER_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `CREATED_DATE` bigint(20) DEFAULT NULL,
  `LAST_UPDATED_DATE` bigint(20) DEFAULT NULL,
  `CLIENT_STORAGE_PROVIDER` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `EXTERNAL_CLIENT_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_JKUWUVD56ONTGSUHOGM8UEWRT` (`CLIENT_ID`,`CLIENT_STORAGE_PROVIDER`,`EXTERNAL_CLIENT_ID`,`USER_ID`),
  KEY `IDX_USER_CONSENT` (`USER_ID`),
  CONSTRAINT `FK_GRNTCSNT_USER` FOREIGN KEY (`USER_ID`) REFERENCES `user_entity` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.user_consent: ~0 rows (ungefähr)
DELETE FROM `user_consent`;

-- Exportiere Struktur von Tabelle keycloak.user_consent_client_scope
CREATE TABLE IF NOT EXISTS `user_consent_client_scope` (
  `USER_CONSENT_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `SCOPE_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`USER_CONSENT_ID`,`SCOPE_ID`),
  KEY `IDX_USCONSENT_CLSCOPE` (`USER_CONSENT_ID`),
  CONSTRAINT `FK_GRNTCSNT_CLSC_USC` FOREIGN KEY (`USER_CONSENT_ID`) REFERENCES `user_consent` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.user_consent_client_scope: ~0 rows (ungefähr)
DELETE FROM `user_consent_client_scope`;

-- Exportiere Struktur von Tabelle keycloak.user_entity
CREATE TABLE IF NOT EXISTS `user_entity` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `EMAIL` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `EMAIL_CONSTRAINT` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `EMAIL_VERIFIED` bit(1) NOT NULL DEFAULT b'0',
  `ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `FEDERATION_LINK` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `FIRST_NAME` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `LAST_NAME` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `REALM_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `USERNAME` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `CREATED_TIMESTAMP` bigint(20) DEFAULT NULL,
  `SERVICE_ACCOUNT_CLIENT_LINK` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `NOT_BEFORE` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_DYKN684SL8UP1CRFEI6ECKHD7` (`REALM_ID`,`EMAIL_CONSTRAINT`),
  UNIQUE KEY `UK_RU8TT6T700S9V50BU18WS5HA6` (`REALM_ID`,`USERNAME`),
  KEY `IDX_USER_EMAIL` (`EMAIL`),
  KEY `IDX_USER_SERVICE_ACCOUNT` (`REALM_ID`,`SERVICE_ACCOUNT_CLIENT_LINK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.user_entity: ~4 rows (ungefähr)
DELETE FROM `user_entity`;
INSERT INTO `user_entity` (`ID`, `EMAIL`, `EMAIL_CONSTRAINT`, `EMAIL_VERIFIED`, `ENABLED`, `FEDERATION_LINK`, `FIRST_NAME`, `LAST_NAME`, `REALM_ID`, `USERNAME`, `CREATED_TIMESTAMP`, `SERVICE_ACCOUNT_CLIENT_LINK`, `NOT_BEFORE`) VALUES
	('305b4f46-733f-4d60-8fb3-c46731ffcb31', NULL, '23bb8f66-83a5-4a35-93ee-611eadee9c0f', b'0', b'1', NULL, '', '', '47958335-5c70-4603-abe7-b887490d7b1c', 'user2', 1674993515018, NULL, 0),
	('4ba00864-98ee-4a4f-a52c-f975ed7a4fd7', NULL, '4687daad-7936-4119-a323-fd2ff4eca74d', b'0', b'1', NULL, '', '', '47958335-5c70-4603-abe7-b887490d7b1c', 'user1', 1674993449805, NULL, 0),
	('7356dc37-ad48-405e-8285-7fad444f8274', NULL, 'e2bc78df-3abd-4ac7-9202-951dcf277205', b'0', b'1', NULL, '', '', '47958335-5c70-4603-abe7-b887490d7b1c', 'user3', 1674993557088, NULL, 0),
	('e27e025c-ec23-4c15-9594-c6cee010598f', NULL, '8d216a60-c206-41b4-9311-531293cbf1c0', b'0', b'1', NULL, NULL, NULL, '23d8cb0f-53b6-4402-a9f9-072c402d5927', 'admin', 1674862326169, NULL, 0);

-- Exportiere Struktur von Tabelle keycloak.user_federation_config
CREATE TABLE IF NOT EXISTS `user_federation_config` (
  `USER_FEDERATION_PROVIDER_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`USER_FEDERATION_PROVIDER_ID`,`NAME`),
  CONSTRAINT `FK_T13HPU1J94R2EBPEKR39X5EU5` FOREIGN KEY (`USER_FEDERATION_PROVIDER_ID`) REFERENCES `user_federation_provider` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.user_federation_config: ~0 rows (ungefähr)
DELETE FROM `user_federation_config`;

-- Exportiere Struktur von Tabelle keycloak.user_federation_mapper
CREATE TABLE IF NOT EXISTS `user_federation_mapper` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `FEDERATION_PROVIDER_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `FEDERATION_MAPPER_TYPE` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_USR_FED_MAP_FED_PRV` (`FEDERATION_PROVIDER_ID`),
  KEY `IDX_USR_FED_MAP_REALM` (`REALM_ID`),
  CONSTRAINT `FK_FEDMAPPERPM_FEDPRV` FOREIGN KEY (`FEDERATION_PROVIDER_ID`) REFERENCES `user_federation_provider` (`ID`),
  CONSTRAINT `FK_FEDMAPPERPM_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `realm` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.user_federation_mapper: ~0 rows (ungefähr)
DELETE FROM `user_federation_mapper`;

-- Exportiere Struktur von Tabelle keycloak.user_federation_mapper_config
CREATE TABLE IF NOT EXISTS `user_federation_mapper_config` (
  `USER_FEDERATION_MAPPER_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`USER_FEDERATION_MAPPER_ID`,`NAME`),
  CONSTRAINT `FK_FEDMAPPER_CFG` FOREIGN KEY (`USER_FEDERATION_MAPPER_ID`) REFERENCES `user_federation_mapper` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.user_federation_mapper_config: ~0 rows (ungefähr)
DELETE FROM `user_federation_mapper_config`;

-- Exportiere Struktur von Tabelle keycloak.user_federation_provider
CREATE TABLE IF NOT EXISTS `user_federation_provider` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `CHANGED_SYNC_PERIOD` int(11) DEFAULT NULL,
  `DISPLAY_NAME` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `FULL_SYNC_PERIOD` int(11) DEFAULT NULL,
  `LAST_SYNC` int(11) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `PROVIDER_NAME` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `REALM_ID` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_USR_FED_PRV_REALM` (`REALM_ID`),
  CONSTRAINT `FK_1FJ32F6PTOLW2QY60CD8N01E8` FOREIGN KEY (`REALM_ID`) REFERENCES `realm` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.user_federation_provider: ~0 rows (ungefähr)
DELETE FROM `user_federation_provider`;

-- Exportiere Struktur von Tabelle keycloak.user_group_membership
CREATE TABLE IF NOT EXISTS `user_group_membership` (
  `GROUP_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `USER_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`GROUP_ID`,`USER_ID`),
  KEY `IDX_USER_GROUP_MAPPING` (`USER_ID`),
  CONSTRAINT `FK_USER_GROUP_USER` FOREIGN KEY (`USER_ID`) REFERENCES `user_entity` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.user_group_membership: ~0 rows (ungefähr)
DELETE FROM `user_group_membership`;

-- Exportiere Struktur von Tabelle keycloak.user_required_action
CREATE TABLE IF NOT EXISTS `user_required_action` (
  `USER_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `REQUIRED_ACTION` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT ' ',
  PRIMARY KEY (`REQUIRED_ACTION`,`USER_ID`),
  KEY `IDX_USER_REQACTIONS` (`USER_ID`),
  CONSTRAINT `FK_6QJ3W1JW9CVAFHE19BWSIUVMD` FOREIGN KEY (`USER_ID`) REFERENCES `user_entity` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.user_required_action: ~0 rows (ungefähr)
DELETE FROM `user_required_action`;

-- Exportiere Struktur von Tabelle keycloak.user_role_mapping
CREATE TABLE IF NOT EXISTS `user_role_mapping` (
  `ROLE_ID` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `USER_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`ROLE_ID`,`USER_ID`),
  KEY `IDX_USER_ROLE_MAPPING` (`USER_ID`),
  CONSTRAINT `FK_C4FQV34P1MBYLLOXANG7B1Q3L` FOREIGN KEY (`USER_ID`) REFERENCES `user_entity` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.user_role_mapping: ~26 rows (ungefähr)
DELETE FROM `user_role_mapping`;
INSERT INTO `user_role_mapping` (`ROLE_ID`, `USER_ID`) VALUES
	('0b008376-b0d1-4241-9452-de351cd88717', 'e27e025c-ec23-4c15-9594-c6cee010598f'),
	('0d070596-f17e-4097-982a-f6321ab1fe9e', 'e27e025c-ec23-4c15-9594-c6cee010598f'),
	('1421012b-7fdd-4ca9-a9b2-4b453246a06c', '305b4f46-733f-4d60-8fb3-c46731ffcb31'),
	('1421012b-7fdd-4ca9-a9b2-4b453246a06c', '7356dc37-ad48-405e-8285-7fad444f8274'),
	('23eb6694-49b2-4fb4-b1d0-1bce51c5eb74', 'e27e025c-ec23-4c15-9594-c6cee010598f'),
	('24d1868a-7671-4224-9d11-41655a7aab27', 'e27e025c-ec23-4c15-9594-c6cee010598f'),
	('27a4cc8c-bafb-4a93-b174-87e032c421ee', 'e27e025c-ec23-4c15-9594-c6cee010598f'),
	('3583d5e6-1173-432c-981c-b7815cee20b9', 'e27e025c-ec23-4c15-9594-c6cee010598f'),
	('417b3cc0-e882-4198-bf3a-9ef52104ae13', 'e27e025c-ec23-4c15-9594-c6cee010598f'),
	('477ab853-069f-4331-85f2-74f4ec6ed03e', 'e27e025c-ec23-4c15-9594-c6cee010598f'),
	('514e1be8-470f-4a41-8787-3fa664cd7910', 'e27e025c-ec23-4c15-9594-c6cee010598f'),
	('542b0798-98eb-4ce0-be4d-ccf5cef75513', 'e27e025c-ec23-4c15-9594-c6cee010598f'),
	('606ad618-f0a8-4543-b526-971af5b5448e', 'e27e025c-ec23-4c15-9594-c6cee010598f'),
	('63bcb921-b0b6-4df5-aabf-0aa91b11596a', 'e27e025c-ec23-4c15-9594-c6cee010598f'),
	('7e7ea688-c340-4493-a173-5a56727114e8', 'e27e025c-ec23-4c15-9594-c6cee010598f'),
	('8883719d-2359-4401-97c7-376e7aa6aa45', 'e27e025c-ec23-4c15-9594-c6cee010598f'),
	('ad565401-754f-4f0e-bf5f-e307c36783a5', 'e27e025c-ec23-4c15-9594-c6cee010598f'),
	('b3e6fd4f-5175-42a4-b7c7-f7aa4fab418a', 'e27e025c-ec23-4c15-9594-c6cee010598f'),
	('c222fc2d-aa3a-4fbd-9e3c-27894f821c16', 'e27e025c-ec23-4c15-9594-c6cee010598f'),
	('d0b80bc7-6aaf-4c2f-906e-0deede603ef8', 'e27e025c-ec23-4c15-9594-c6cee010598f'),
	('ddad37c9-58a3-4aa4-bdf6-509c4065822f', '305b4f46-733f-4d60-8fb3-c46731ffcb31'),
	('ddad37c9-58a3-4aa4-bdf6-509c4065822f', '4ba00864-98ee-4a4f-a52c-f975ed7a4fd7'),
	('ddad37c9-58a3-4aa4-bdf6-509c4065822f', '7356dc37-ad48-405e-8285-7fad444f8274'),
	('f2c9e482-411f-41f7-8c55-8bc5da6f1ab3', '4ba00864-98ee-4a4f-a52c-f975ed7a4fd7'),
	('f2c9e482-411f-41f7-8c55-8bc5da6f1ab3', '7356dc37-ad48-405e-8285-7fad444f8274'),
	('fbe3d808-d48b-4394-b69c-181fd9fac26a', 'e27e025c-ec23-4c15-9594-c6cee010598f');

-- Exportiere Struktur von Tabelle keycloak.user_session
CREATE TABLE IF NOT EXISTS `user_session` (
  `ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `AUTH_METHOD` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `IP_ADDRESS` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `LAST_SESSION_REFRESH` int(11) DEFAULT NULL,
  `LOGIN_USERNAME` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `REALM_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `REMEMBER_ME` bit(1) NOT NULL DEFAULT b'0',
  `STARTED` int(11) DEFAULT NULL,
  `USER_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `USER_SESSION_STATE` int(11) DEFAULT NULL,
  `BROKER_SESSION_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `BROKER_USER_ID` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.user_session: ~0 rows (ungefähr)
DELETE FROM `user_session`;

-- Exportiere Struktur von Tabelle keycloak.user_session_note
CREATE TABLE IF NOT EXISTS `user_session_note` (
  `USER_SESSION` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` text COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`USER_SESSION`,`NAME`),
  CONSTRAINT `FK5EDFB00FF51D3472` FOREIGN KEY (`USER_SESSION`) REFERENCES `user_session` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.user_session_note: ~0 rows (ungefähr)
DELETE FROM `user_session_note`;

-- Exportiere Struktur von Tabelle keycloak.web_origins
CREATE TABLE IF NOT EXISTS `web_origins` (
  `CLIENT_ID` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `VALUE` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`CLIENT_ID`,`VALUE`),
  KEY `IDX_WEB_ORIG_CLIENT` (`CLIENT_ID`),
  CONSTRAINT `FK_LOJPHO213XCX4WNKOG82SSRFY` FOREIGN KEY (`CLIENT_ID`) REFERENCES `client` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Exportiere Daten aus Tabelle keycloak.web_origins: ~4 rows (ungefähr)
DELETE FROM `web_origins`;
INSERT INTO `web_origins` (`CLIENT_ID`, `VALUE`) VALUES
	('035989c0-3119-47ee-98ca-db933a226945', '*'),
	('2c77769b-4aab-4622-9e44-3a8433637f36', 'http://localhost:3333/*'),
	('808458f5-33ba-4e81-9601-019ae159abb3', '+'),
	('8d03d66e-c122-4357-b8ac-647ff79ac3be', '+');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
