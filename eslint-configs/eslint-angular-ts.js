/**
 * the activated Ruleset is found here: https://github.com/angular-eslint/angular-eslint#functionality
 */

/**
 * -----------------------------------------------------
 * TYPESCRIPT FILES (COMPONENTS, SERVICES ETC) (.ts)
 * -----------------------------------------------------
 */
module.exports = {
    /*
    extends: ['plugin:@angular-eslint/template/process-inline-templates'],
    // ... other config specific to TypeScript files
    rules: {
        // functionality
        '@angular-eslint/contextual-decorator': ['error'],
        '@angular-eslint/contextual-lifecycle': ['error'],
        '@angular-eslint/no-attribute-decorator': ['error'],
        '@angular-eslint/no-lifecycle-call': ['error'],
        '@angular-eslint/no-output-native': ['error'],
        '@angular-eslint/no-pipe-impure': ['error'],
        '@angular-eslint/prefer-on-push-component-change-detection': ['error'],
        '@angular-eslint/use-injectable-provided-in': ['error'],
        '@angular-eslint/use-lifecycle-interface': ['error'],
        // maintainability
        '@angular-eslint/component-max-inline-declarations': [
            'error',
            {
                animations: 15,
                styles: 3,
                template: 3
            }
        ],
        '@angular-eslint/no-conflicting-lifecycle': ['error'],
        '@angular-eslint/no-forward-ref': ['error'],
        '@angular-eslint/no-input-prefix': ['error'],
        '@angular-eslint/no-input-rename': ['error'],
        '@angular-eslint/no-output-on-prefix': ['error'],
        '@angular-eslint/no-output-rename': ['error'],
        '@angular-eslint/prefer-output-readonly': ['error'],
        '@angular-eslint/relative-url-prefix': ['error'],
        '@angular-eslint/use-component-selector': ['error'],
        '@angular-eslint/use-component-view-encapsulation': ['error'],
        '@angular-eslint/use-pipe-transform-interface': ['error'],
        // style
        '@angular-eslint/component-class-suffix': [
            'error',
            {
                suffixes: ['Component', 'View', 'NgCom']
            }
        ],
        '@angular-eslint/component-selector': [
            'error',
            {
                prefix: [''],
                style: 'kebab-case',
                type: 'element'
            }
        ],
        '@angular-eslint/directive-class-suffix': [
            'error',
            {
                suffixes: ['Directive', 'NgDir']
            }
        ],
        '@angular-eslint/directive-selector': [
            'error',
            {
                prefix: [''],
                style: 'camelCase',
                type: 'attribut'
            }
        ],
        '@angular-eslint/no-host-metadata-property': ['error'],
        '@angular-eslint/no-inputs-metadata-property': ['error'],
        '@angular-eslint/no-outputs-metadata-property': ['error'],
        '@angular-eslint/no-queries-metadata-property': ['error'],
        '@angular-eslint/pipe-prefix': [
            'error',
            {
                prefixes: []
            }
        ],
        // other
        '@angular-eslint/no-empty-lifecycle-method': ['error'],
        '@angular-eslint/sort-ngmodule-metadata-arrays': ['off']
    }
    */
};
