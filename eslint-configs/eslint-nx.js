/**
 * the activated Ruleset is found here: https://nx.dev/linter/eslint-plugin-nx
 */
module.exports = {
    rules: {
        '@nrwl/nx/nx-plugin-checks': ['error'],
        '@nrwl/nx/enforce-module-boundaries': [
            'error',
            {
                enforceBuildableLibDependency: true,
                allow: [],
                depConstraints: [
                    // applications
                    {
                        sourceTag: '*',
                        onlyDependOnLibsWithTags: ['*']
                    }
                ]
            }
        ]
    }
};
