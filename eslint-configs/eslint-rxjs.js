/**
 * the activated Ruleset is found here: https://github.com/cartant/eslint-plugin-rxjs
 * The activated Ruleset is found here: https://github.com/cartant/eslint-plugin-rxjs-angular
 */

module.exports = {
    /*
    // plugins: ["rxjs", "rxjs-angular"],
    rules: {
        // eslint-plugin-rxjs
        'rxjs/ban-observables': [
            'error',
            {
                partition: true,
                of: false,
                onErrorResumeNext: 'What is this? Visual Basic?'
            }
        ],
        'rxjs/ban-operators': [
            'error',
            {
                partition: true,
                map: false,
                onErrorResumeNext: 'What is this? Visual Basic?'
            }
        ],
        'rxjs/finnish': [
            'error',
            {
                functions: true,
                methods: true,
                names: {
                    '^(canActivate|canActivateChild|canDeactivate|canLoad|intercept|resolve|validate)$': false
                },
                parameters: true,
                properties: true,
                strict: false,
                types: {
                    '^EventEmitter$': false
                },
                variables: true
            }
        ],
        'rxjs/just': ['off'],
        'rxjs/no-async-subscribe': ['error'],
        'rxjs/no-compat': ['error'],
        'rxjs/no-connectable': ['error'],
        'rxjs/no-create': ['error'],
        'rxjs/no-cyclic-action': [
            'error',
            {
                observable: '[Aa]ction(s|s\\$|\\$)$'
            }
        ],
        'rxjs/no-explicit-generics': ['error'],
        'rxjs/no-exposed-subjects': [
            'error',
            {
                allowProtected: true
            }
        ],
        'rxjs/no-finnish': ['off'],
        'rxjs/no-ignored-error': ['error'],
        'rxjs/no-ignored-notifier': ['error'],
        'rxjs/no-ignored-observable': ['error'],
        'rxjs/no-ignored-replay-buffer': ['error'],
        'rxjs/no-ignored-subscribe': ['error'],
        'rxjs/no-ignored-subscription': ['error'],
        'rxjs/no-ignored-takewhile-value': ['error'],
        'rxjs/no-implicit-any-catch': [
            'error',
            {
                allowExplicitAny: true
            }
        ],
        'rxjs/no-index': ['error'],
        'rxjs/no-internal': ['error'],
        'rxjs/no-nested-subscribe': ['error'],
        'rxjs/no-redundant-notify': ['error'],
        'rxjs/no-sharereplay': [
            'error',
            {
                allowConfig: true
            }
        ],
        'rxjs/no-subclass': ['error'],
        'rxjs/no-subject-unsubscribe': ['error'],
        'rxjs/no-subject-value': ['error'],
        'rxjs/no-subscribe-handlers': ['error'],
        'rxjs/no-topromise': ['error'],
        'rxjs/no-unbound-methods': ['error'],
        'rxjs/no-unsafe-catch': [
            'error',
            {
                observable: '[Aa]ction(s|s\\$|\\$)$'
            }
        ],
        'rxjs/no-unsafe-first': [
            'error',
            {
                observable: '[Aa]ction(s|s\\$|\\$)$'
            }
        ],
        'rxjs/no-unsafe-subject-next': ['error'],
        'rxjs/no-unsafe-switchmap': [
            'error',
            {
                disallow: ['add', 'create', 'delete', 'post', 'put', 'remove', 'set', 'update'],
                observable: '[Aa]ction(s|s\\$|\\$)$'
            }
        ],
        'rxjs/no-unsafe-takeuntil': [
            'error',
            {
                alias: ['untilDestroyed']
            }
        ],
        'rxjs/prefer-observer': [
            'error',
            {
                allowNext: false
            }
        ],
        'rxjs/suffix-subjects': [
            'error',
            {
                parameters: true,
                properties: true,
                suffix: 'Subject',
                types: {
                    '^EventEmitter$': false
                },
                variables: true
            }
        ],
        'rxjs/throw-error': ['error'],
        // eslint-plugin-rxjs-angular
        'rxjs-angular/prefer-async-pipe': ['error'],
        'rxjs-angular/prefer-composition': [
            'error',
            {
                checkDecorators: ['Component']
            }
        ],
        'rxjs-angular/prefer-takeuntil': [
            'error',
            {
                alias: ['untilDestroyed'],
                checkComplete: true,
                checkDecorators: ['Component'],
                checkDestroy: true
            }
        ]
    }
    */
};
