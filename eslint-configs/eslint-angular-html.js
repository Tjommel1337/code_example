/**
 * the activated Ruleset is found here: https://github.com/angular-eslint/angular-eslint#functionality
 */

/**
 * -----------------------------------------------------
 * COMPONENT TEMPLATES
 * -----------------------------------------------------
 */
module.exports = {/*
    parser: '@angular-eslint/template-parser',
    plugins: ['@angular-eslint/template'],
    rules: {
        // functionality
        '@angular-eslint/template/accessibility-alt-text': ['error'],
        '@angular-eslint/template/accessibility-elements-content': ['error'],
        '@angular-eslint/template/accessibility-label-for': ['error'],
        '@angular-eslint/template/no-positive-tabindex': ['error'],
        '@angular-eslint/template/accessibility-table-scope': ['error'],
        '@angular-eslint/template/accessibility-valid-aria': ['error'],
        '@angular-eslint/template/banana-in-box': ['error'],
        '@angular-eslint/template/click-events-have-key-events': ['error'],
        '@angular-eslint/template/mouse-events-have-key-events': ['error'],
        '@angular-eslint/template/no-any': ['error'],
        '@angular-eslint/template/no-autofocus': ['error'],
        '@angular-eslint/template/no-distracting-elements': ['error'],
        '@angular-eslint/template/no-negated-async': ['error'],
        // maintainability
        '@angular-eslint/template/conditional-complexity': [
            'error',
            {
                maxComplexity: 5
            }
        ],
        '@angular-eslint/template/cyclomatic-complexity': [
            'error',
            {
                maxComplexity: 5
            }
        ],
        '@angular-eslint/template/i18n': ['off'],
        '@angular-eslint/template/no-call-expression': ['error'],
        '@angular-eslint/template/use-track-by-function': ['error'],
        // other
        '@angular-eslint/template/accessibility-label-has-associated-control': ['error'],
        '@angular-eslint/template/eqeqeq': ['error'],
        '@angular-eslint/template/no-duplicate-attributes': [
            'error',
            {
                allowTwoWayDataBinding: false,
                ignore: []
            }
        ],
        '@angular-eslint/template/button-has-type': ['error'],

        '@angular-eslint/template/accessibility-interactive-supports-focus': ['error'],
        '@angular-eslint/template/accessibility-role-has-required-aria': ['error'],
        '@angular-eslint/template/attributes-order': [
            'error',
            {
                alphabetical: false,
                order: [
                    'TEMPLATE_REFERENCE',
                    'STRUCTURAL_DIRECTIVE',
                    'ATTRIBUTE_BINDING',
                    'INPUT_BINDING',
                    'TWO_WAY_BINDING',
                    'OUTPUT_BINDING'
                ]
            }
        ],
        '@angular-eslint/template/no-inline-styles': [
            'error',
            {
                allowNgStyle: false,
                allowBindToStyle: false
            }
        ]
    }*/
};
