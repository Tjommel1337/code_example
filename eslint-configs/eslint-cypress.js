/**
 * the activated Ruleset is found here: https://github.com/cypress-io/eslint-plugin-cypress#rules
 */

module.exports = {
    /*
     * plugin: ["cypress"],
     * env: {
     *     "cypress/globals": true,
     * },
     */
    rules: {
        'cypress/no-assigning-return-values': 'error',
        'cypress/no-unnecessary-waiting': 'error',
        'cypress/no-async-tests': 'error',
        'cypress/no-force': 'warn',
        'cypress/assertion-before-screenshot': 'warn',
        'cypress/require-data-selectors': 'error',
        'cypress/no-pause': 'error'
    }
};
