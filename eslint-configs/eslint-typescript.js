/**
 * the activated Ruleset is found here: https://typescript-eslint.io/rules/
 */

module.exports = {
    /*
    rules: {
        // supported Rules
        '@typescript-eslint/adjacent-overload-signatures': ['error'],
        '@typescript-eslint/array-type': [
            'error',
            {
                default: 'array-simple'
            }
        ],
        '@typescript-eslint/await-thenable': ['error'],
        '@typescript-eslint/ban-ts-comment': ['error'],
        '@typescript-eslint/ban-tslint-comment': ['error'],
        '@typescript-eslint/ban-types': ['error'],
        '@typescript-eslint/class-literal-property-style': ['error', 'fields'],
        '@typescript-eslint/consistent-generic-constructors': ['error', 'constructor'],
        '@typescript-eslint/consistent-indexed-object-style': ['error', 'record'],
        '@typescript-eslint/consistent-type-assertions': [
            'error',
            {
                assertionStyle: 'as',
                objectLiteralTypeAssertions: 'never'
            }
        ],
        '@typescript-eslint/consistent-type-definitions': ['error', 'interface'],
        '@typescript-eslint/consistent-type-exports': [
            'error',
            {
                fixMixedExportsWithInlineTypeSpecifier: false
            }
        ],
        '@typescript-eslint/consistent-type-imports': [
            'error',
            {
                prefer: 'no-type-imports',
                disallowTypeAnnotations: true
            }
        ],
        '@typescript-eslint/method-signature-style': ['error', 'method'],
        '@typescript-eslint/naming-convention': [
            'error',
            // default configurations
            {
                selector: 'default',
                format: ['camelCase', 'UPPER_CASE'],
                leadingUnderscore: 'allow',
                trailingUnderscore: 'allow',
                filter: {
                    regex: '\\d+',
                    match: false
                }
            },
            {
                selector: 'variable',
                format: ['camelCase', 'UPPER_CASE'],
                leadingUnderscore: 'allow',
                trailingUnderscore: 'allow',
                filter: {
                    regex: '\\d+',
                    match: false
                }
            },
            // extra specifications
            {
                selector: 'typeLike',
                format: ['PascalCase']
            },
            {
                selector: ['enum'],
                format: ['PascalCase'],
                leadingUnderscore: 'forbid',
                trailingUnderscore: 'forbid'
            },
            {
                selector: ['enumMember'],
                format: ['UPPER_CASE', 'PascalCase'],
                leadingUnderscore: 'forbid',
                trailingUnderscore: 'forbid'
            },
            {
                selector: ['interface'],
                format: ['PascalCase'],
                prefix: ['I']
            },
            {
                selector: ['typeAlias'],
                format: ['PascalCase'],
                prefix: ['T']
            }
        ],
        '@typescript-eslint/no-base-to-string': [
            'error',
            {
                ignoredTypeNames: ['RegExp']
            }
        ],
        '@typescript-eslint/no-confusing-non-null-assertion': ['error'],
        '@typescript-eslint/no-confusing-void-expression': [
            'error',
            {
                ignoreArrowShorthand: false,
                ignoreVoidOperator: false
            }
        ],
        '@typescript-eslint/no-duplicate-enum-values': ['error'],
        '@typescript-eslint/no-dynamic-delete': ['error'],
        '@typescript-eslint/no-empty-interface': [
            'error',
            {
                allowSingleExtends: false
            }
        ],
        '@typescript-eslint/no-explicit-any': [
            'error',
            {
                fixToUnknown: false,
                ignoreRestArgs: false
            }
        ],
        '@typescript-eslint/no-extra-non-null-assertion': ['error'],
        '@typescript-eslint/no-extraneous-class': [
            'error',
            {
                allowConstructorOnly: false,
                allowEmpty: false,
                allowStaticOnly: true,
                allowWithDecorator: true
            }
        ],
        '@typescript-eslint/no-floating-promises': [
            'error',
            {
                ignoreVoid: true,
                ignoreIIFE: false
            }
        ],
        '@typescript-eslint/no-for-in-array': ['error'],
        '@typescript-eslint/no-implicit-any-catch': [
            'error',
            {
                allowExplicitAny: false
            }
        ],
        '@typescript-eslint/no-inferrable-types': ['off'],
        '@typescript-eslint/no-invalid-void-type': [
            'error',
            {
                allowInGenericTypeArguments: true,
                allowAsThisParameter: false
            }
        ],
        '@typescript-eslint/no-meaningless-void-operator': [
            'error',
            {
                checkNever: true
            }
        ],
        '@typescript-eslint/no-misused-new': ['error'],
        '@typescript-eslint/no-misused-promises': [
            'error',
            {
                checksConditionals: true,
                checksVoidReturn: true,
                checksSpreads: true
            }
        ],
        '@typescript-eslint/no-namespace': [
            'error',
            {
                allowDeclarations: false,
                allowDefinitionFiles: true
            }
        ],
        '@typescript-eslint/no-non-null-asserted-nullish-coalescing': ['error'],
        '@typescript-eslint/no-non-null-asserted-optional-chain': ['error'],
        '@typescript-eslint/no-non-null-assertion': ['error'],
        '@typescript-eslint/no-parameter-properties': [
            // deprecated: use parameter-properties instead
            'off'
        ],
        '@typescript-eslint/no-redundant-type-constituents': ['error'],
        '@typescript-eslint/no-require-imports': ['error'],
        '@typescript-eslint/no-this-alias': [
            'error',
            {
                allowDestructuring: true,
                allowedNames: []
            }
        ],
        '@typescript-eslint/no-type-alias': [
            'error',
            {
                allowAliases: 'always',
                allowCallbacks: 'never',
                allowConditionalTypes: 'always',
                allowConstructors: 'never',
                allowLiterals: 'never',
                allowMappedTypes: 'always',
                allowTupleTypes: 'always',
                allowGenerics: 'always'
            }
        ],
        '@typescript-eslint/no-unnecessary-boolean-literal-compare': [
            'error',
            {
                allowComparingNullableBooleansToTrue: false,
                allowComparingNullableBooleansToFalse: false
            }
        ],
        '@typescript-eslint/no-unnecessary-condition': [
            'error',
            {
                allowConstantLoopConditions: false,
                allowRuleToRunWithoutStrictNullChecksIKnowWhatIAmDoing: false
            }
        ],
        '@typescript-eslint/no-unnecessary-qualifier': ['error'],
        '@typescript-eslint/no-unnecessary-type-arguments': ['error'],
        '@typescript-eslint/no-unnecessary-type-assertion': [
            'error',
            {
                typesToIgnore: []
            }
        ],
        '@typescript-eslint/no-unnecessary-type-constraint': ['error'],
        '@typescript-eslint/no-unsafe-argument': ['error'],
        '@typescript-eslint/no-unsafe-assignment': ['error'],
        '@typescript-eslint/no-unsafe-call': ['error'],
        '@typescript-eslint/no-unsafe-member-access': ['error'],
        '@typescript-eslint/no-unsafe-return': ['error'],
        '@typescript-eslint/no-useless-empty-export': ['error'],
        '@typescript-eslint/no-var-requires': ['error'],
        '@typescript-eslint/non-nullable-type-assertion-style': ['error'],
        '@typescript-eslint/parameter-properties': [
            'error',
            {
                prefer: 'class-property',
                // TODO: needs angular adjustments
                allow: ['private readonly', 'protected readonly', 'public readonly']
            }
        ],
        '@typescript-eslint/prefer-as-const': ['error'],
        '@typescript-eslint/prefer-enum-initializers': ['error'],
        '@typescript-eslint/prefer-for-of': ['error'],
        '@typescript-eslint/prefer-function-type': ['error'],
        '@typescript-eslint/prefer-includes': ['error'],
        '@typescript-eslint/prefer-literal-enum-member': [
            'error',
            {
                allowBitwiseExpressions: false
            }
        ],
        '@typescript-eslint/prefer-namespace-keyword': ['error'],
        '@typescript-eslint/prefer-nullish-coalescing': [
            'error',
            {
                ignoreTernaryTests: false,
                ignoreConditionalTests: false,
                ignoreMixedLogicalExpressions: false
            }
        ],
        '@typescript-eslint/prefer-optional-chain': ['error'],
        '@typescript-eslint/prefer-readonly': [
            'error',
            {
                onlyInlineLambdas: false
            }
        ],
        '@typescript-eslint/prefer-readonly-parameter-types': ['off'],
        '@typescript-eslint/prefer-reduce-type-parameter': ['error'],
        '@typescript-eslint/prefer-regexp-exec': ['error'],
        '@typescript-eslint/prefer-return-this-type': ['error'],
        '@typescript-eslint/prefer-string-starts-ends-with': ['error'],
        '@typescript-eslint/prefer-ts-expect-error': ['error'],
        '@typescript-eslint/promise-function-async': [
            'error',
            {
                allowAny: false,
                allowedPromiseNames: [],
                checkArrowFunctions: true,
                checkFunctionDeclarations: true,
                checkFunctionExpressions: true,
                checkMethodDeclarations: true
            }
        ],
        '@typescript-eslint/require-array-sort-compare': [
            'error',
            {
                ignoreStringArrays: false
            }
        ],
        '@typescript-eslint/restrict-plus-operands': [
            'error',
            {
                checkCompoundAssignments: true,
                allowAny: false
            }
        ],
        '@typescript-eslint/restrict-template-expressions': [
            'error',
            {
                allowNumber: false,
                allowBoolean: false,
                allowAny: false,
                allowNullish: false,
                allowRegExp: false
            }
        ],
        '@typescript-eslint/sort-type-union-intersection-members': [
            'error',
            {
                checkIntersections: true,
                checkUnions: true,
                groupOrder: [
                    'named',
                    'keyword',
                    'operator',
                    'literal',
                    'function',
                    'import',
                    'conditional',
                    'object',
                    'tuple',
                    'intersection',
                    'union',
                    'nullish'
                ]
            }
        ],
        '@typescript-eslint/strict-boolean-expressions': [
            'error',
            {
                allowString: true,
                allowNumber: true,
                allowNullableObject: false,
                allowNullableBoolean: false,
                allowNullableString: false,
                allowNullableNumber: false,
                allowAny: false,
                allowRuleToRunWithoutStrictNullChecksIKnowWhatIAmDoing: false
            }
        ],
        '@typescript-eslint/switch-exhaustiveness-check': ['error'],
        '@typescript-eslint/triple-slash-reference': [
            'error',
            {
                lib: 'always',
                path: 'never',
                types: 'prefer-import'
            }
        ],
        '@typescript-eslint/type-annotation-spacing': [
            'error',
            {
                before: true,
                after: true,
                overrides: {
                    // TODO: eventually needs further investigation and coding examples
                    colon: {
                        before: false
                    }
                }
            }
        ],
        // TODO: eventually disable this option and use implicit types
        '@typescript-eslint/typedef': [
            'error',
            {
                // TODO: eventually activate more typedefs
                arrayDestructuring: false,
                arrowParameter: false,
                memberVariableDeclaration: true,
                objectDestructuring: false,
                parameter: true,
                propertyDeclaration: true,
                variableDeclaration: true,
                variableDeclarationIgnoreFunction: true
            }
        ],
        '@typescript-eslint/unbound-method': [
            'error',
            {
                ignoreStatic: false
            }
        ],
        '@typescript-eslint/unified-signatures': [
            'error',
            {
                // TODO: eventually disable?
                ignoreDifferentlyNamedParameters: true
            }
        ],
        '@typescript-eslint/explicit-function-return-type': [
            'error',
            {
                allowExpressions: false,
                allowTypedFunctionExpressions: true,
                allowHigherOrderFunctions: false,
                allowDirectConstAssertionInArrowFunctions: true,
                allowConciseArrowFunctionExpressionsStartingWithVoid: false,
                allowedNames: []
            }
        ],
        '@typescript-eslint/explicit-member-accessibility': [
            'error',
            {
                accessibility: 'explicit',
                overrides: {
                    constructors: 'no-public'
                },
                ignoredMethodNames: [
                    // TODO: eventually add Angular lifecycle implementations here?
                ]
            }
        ],
        '@typescript-eslint/explicit-module-boundary-types': [
            'error',
            {
                allowArgumentsExplicitlyTypedAsAny: false,
                allowDirectConstAssertionInArrowFunctions: true,
                allowedNames: [],
                allowHigherOrderFunctions: false,
                allowTypedFunctionExpressions: true
            }
        ],
        '@typescript-eslint/member-delimiter-style': [
            'error',
            {
                multiline: {
                    delimiter: 'semi',
                    requireLast: true
                },
                singleline: {
                    delimiter: 'semi',
                    requireLast: false
                },
                multilineDetection: 'brackets'
            }
        ],
        '@typescript-eslint/member-ordering': [
            // TODO: eventually change to error?
            'warn',
            {
                default: [
                    // index signature
                    'signature',

                    // fields
                    'public-static-field',
                    'protected-static-field',
                    'private-static-field',

                    'public-decorated-field',
                    'protected-decorated-field',
                    'private-decorated-field',

                    'public-instance-field',
                    'protected-instance-field',
                    'private-instance-field',

                    'public-abstract-field',
                    'protected-abstract-field',
                    'private-abstract-field',

                    'public-field',
                    'protected-field',
                    'private-field',

                    'static-field',
                    'instance-field',
                    'abstract-field',

                    'decorated-field',

                    'field',

                    // static initialization
                    'static-initialization',

                    // constructors
                    'public-constructor',
                    'protected-constructor',
                    'private-constructor',

                    'constructor',

                    // getters
                    'public-static-get',
                    'protected-static-get',
                    'private-static-get',

                    'public-decorated-get',
                    'protected-decorated-get',
                    'private-decorated-get',

                    'public-instance-get',
                    'protected-instance-get',
                    'private-instance-get',

                    'public-abstract-get',
                    'protected-abstract-get',
                    'private-abstract-get',

                    'public-get',
                    'protected-get',
                    'private-get',

                    'static-get',
                    'instance-get',
                    'abstract-get',

                    'decorated-get',

                    'get',

                    // setters
                    'public-static-set',
                    'protected-static-set',
                    'private-static-set',

                    'public-decorated-set',
                    'protected-decorated-set',
                    'private-decorated-set',

                    'public-instance-set',
                    'protected-instance-set',
                    'private-instance-set',

                    'public-abstract-set',
                    'protected-abstract-set',
                    'private-abstract-set',

                    'public-set',
                    'protected-set',
                    'private-set',

                    'static-set',
                    'instance-set',
                    'abstract-set',

                    'decorated-set',

                    'set',

                    // methods
                    'public-static-method',
                    'protected-static-method',
                    'private-static-method',

                    'public-decorated-method',
                    'protected-decorated-method',
                    'private-decorated-method',

                    'public-instance-method',
                    'protected-instance-method',
                    'private-instance-method',

                    'public-abstract-method',
                    'protected-abstract-method',
                    'private-abstract-method',

                    'public-method',
                    'protected-method',
                    'private-method',

                    'static-method',
                    'instance-method',
                    'abstract-method',

                    'decorated-method',

                    'method'
                ]
            }
        ],
        // extension Rules
        'brace-style': ['off'],
        '@typescript-eslint/brace-style': [
            'error',
            '1tbs',
            {
                allowSingleLine: false
            }
        ],
        'comma-dangle': ['off'],
        '@typescript-eslint/comma-dangle': ['error', 'always-multiline'],
        'comma-spacing': ['off'],
        '@typescript-eslint/comma-spacing': [
            'error',
            {
                before: false,
                after: true
            }
        ],
        'default-param-last': ['off'],
        '@typescript-eslint/default-param-last': ['error'],
        'dot-notation': ['off'],
        '@typescript-eslint/dot-notation': [
            'error',
            {
                allowPattern: '',
                allowKeywords: true,
                allowPrivateClassPropertyAccess: false,
                allowProtectedClassPropertyAccess: false,
                allowIndexSignaturePropertyAccess: false
            }
        ],
        'func-call-spacing': ['off'],
        '@typescript-eslint/func-call-spacing': ['error', 'never'],
        'indent': ['off'],
        '@typescript-eslint/indent': [
            'error',
            4,
            {
                ignoredNodes: [],
                SwitchCase: 1,
                VariableDeclarator: 'first',
                outerIIFEBody: 1,
                MemberExpression: 1,
                FunctionDeclaration: {
                    body: 1,
                    parameters: 'first'
                },
                FunctionExpression: {
                    body: 1,
                    parameters: 'first'
                },
                StaticBlock: {
                    body: 1
                },
                CallExpression: {
                    arguments: 'first'
                },
                ArrayExpression: 'first',
                ObjectExpression: 'first',
                ImportDeclaration: 'first',
                flatTernaryExpressions: true,
                offsetTernaryExpressions: true,
                ignoreComments: false
            }
        ],
        'init-declarations': ['off'],
        '@typescript-eslint/init-declarations': ['off'],
        'keyword-spacing': ['off'],
        '@typescript-eslint/keyword-spacing': [
            'error',
            {
                before: true,
                after: true
            }
        ],
        'lines-between-class-members': ['off'],
        '@typescript-eslint/lines-between-class-members': [
            'error',
            'always',
            {
                exceptAfterSingleLine: true,
                // TODO: eventually deactivate for overloads?
                exceptAfterOverload: true
            }
        ],
        'no-array-constructor': ['off'],
        '@typescript-eslint/no-array-constructor': ['error'],
        'no-dupe-class-members': ['off'],
        '@typescript-eslint/no-dupe-class-members': ['error'],
        'no-duplicate-imports': ['off'],
        '@typescript-eslint/no-duplicate-imports': [
            'error',
            {
                includeExports: true
            }
        ],
        'no-empty-function': ['off'],
        '@typescript-eslint/no-empty-function': [
            'error',
            {
                allow: []
            }
        ],
        'no-extra-parens': ['off'],
        '@typescript-eslint/no-extra-parens': [
            'error',
            'all',
            {
                conditionalAssign: false,
                returnAssign: false,
                nestedBinaryExpressions: false,
                ignoreJSX: 'all',
                enforceForArrowConditionals: true,
                enforceForSequenceExpressions: true,
                enforceForNewInMemberExpressions: true,
                enforceForFunctionPrototypeMethods: true
            }
        ],
        'no-extra-semi': ['off'],
        '@typescript-eslint/no-extra-semi': ['error'],
        'no-implied-eval': ['off'],
        '@typescript-eslint/no-implied-eval': ['error'],
        'no-invalid-this': ['off'],
        '@typescript-eslint/no-invalid-this': [
            'error',
            {
                capIsConstructor: true
            }
        ],
        'no-loop-func': ['off'],
        '@typescript-eslint/no-loop-func': ['error'],
        'no-loss-of-precision': ['off'],
        '@typescript-eslint/no-loss-of-precision': ['error'],
        'no-magic-numbers': ['off'],
        '@typescript-eslint/no-magic-numbers': [
            'error',
            {
                ignore: [0, 1],
                ignoreArrayIndexes: false,
                ignoreDefaultValues: false,
                ignoreClassFieldInitialValues: false,
                enforceConst: true,
                detectObjects: true,
                ignoreEnums: true,
                ignoreNumericLiteralTypes: false,
                ignoreReadonlyClassProperties: true,
                ignoreTypeIndexes: false
            }
        ],
        'no-redeclare': ['off'],
        '@typescript-eslint/no-redeclare': [
            'error',
            {
                builtinGlobals: true,
                ignoreDeclarationMerge: false
            }
        ],
        'no-restricted-imports': ['off'],
        '@typescript-eslint/no-restricted-imports': ['off'],
        'no-shadow': ['off'],
        '@typescript-eslint/no-shadow': [
            'error',
            {
                builtinGlobals: true,
                hoist: 'all',
                allow: [
                    // express
                    'Request',
                    'Response',
                    'WebSocket'
                ],
                ignoreOnInitialization: false,
                ignoreTypeValueShadow: false,
                ignoreFunctionTypeParameterNameValueShadow: false
            }
        ],
        'no-throw-literal': ['off'],
        '@typescript-eslint/no-throw-literal': [
            'error',
            {
                allowThrowingAny: false,
                allowThrowingUnknown: false
            }
        ],
        'no-unused-expressions': ['off'],
        '@typescript-eslint/no-unused-expressions': [
            'error',
            {
                allowShortCircuit: false,
                allowTernary: false,
                allowTaggedTemplates: false,
                enforceForJSX: true
            }
        ],
        'no-unused-vars': ['off'],
        '@typescript-eslint/no-unused-vars': [
            'error',
            {
                vars: 'all',
                varsIgnorePattern: '',
                args: 'all',
                ignoreRestSiblings: true,
                argsIgnorePattern: '^_',
                destructuredArrayIgnorePattern: '^_',
                // TODO: eventually not needed and change to `none`
                caughtErrors: 'all'
            }
        ],
        'no-use-before-define': ['off'],
        '@typescript-eslint/no-use-before-define': [
            'error',
            {
                functions: true,
                classes: true,
                variables: true,
                // TODO: eventually changed to false according to docs
                allowNamedExports: true,
                enums: true,
                typedefs: true,
                ignoreTypeReferences: false
            }
        ],
        'no-useless-constructor': ['off'],
        '@typescript-eslint/no-useless-constructor': ['error'],
        'object-curly-spacing': ['off'],
        '@typescript-eslint/object-curly-spacing': ['error', 'always'],
        'padding-line-between-statements': ['off'],
        '@typescript-eslint/padding-line-between-statements': ['off'],
        'quotes': ['off'],
        '@typescript-eslint/quotes': [
            'error',
            'single',
            {
                avoidEscape: true,
                allowTemplateLiterals: true
            }
        ],
        'require-await': ['off'],
        '@typescript-eslint/require-await': ['error'],
        'no-return-await': ['off'],
        '@typescript-eslint/return-await': ['error', 'in-try-catch'],
        'semi': ['off'],
        '@typescript-eslint/semi': ['error', 'always'],
        'space-before-blocks': ['off'],
        '@typescript-eslint/space-before-blocks': ['error', 'always'],
        'space-before-function-paren': ['off'],
        '@typescript-eslint/space-before-function-paren': [
            'error',
            {
                anonymous: 'never',
                named: 'never',
                asyncArrow: 'always'
            }
        ],
        'space-infix-ops': ['off'],
        '@typescript-eslint/space-infix-ops': [
            'error',
            {
                int32Hint: false
            }
        ],
        '@typescript-eslint/no-unsafe-declaration-merging': ['error']
    }
    */
};
