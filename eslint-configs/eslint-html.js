/**
 * the activated Ruleset is found here: https://github.com/BenoitZugmeyer/eslint-plugin-html#settings
 */

module.exports = {/*
    /*
     * plugin: ["@html-eslint"],
     * parser: "@html-eslint/parser",
    
    rules: {
        // seo
        '@html-eslint/require-lang': ['error'],
        '@html-eslint/require-title': ['error'],
        '@html-eslint/no-multiple-h1': ['error'],
        '@html-eslint/require-meta-description': ['error'],
        // style
        '@html-eslint/no-extra-spacing-attrs': [
            'error',
            {
                enforceBeforeSelfClose: true
            }
        ],
        '@html-eslint/element-newline': ['error'],
        '@html-eslint/indent': ['error', 4],
        '@html-eslint/quotes': ['error', 'double'],
        '@html-eslint/id-naming-convention': ['error', 'camelCase'],
        '@html-eslint/no-multiple-empty-lines': [
            'error',
            {
                max: 1
            }
        ],
        '@html-eslint/no-trailing-spaces': ['error'],
        // best practice
        '@html-eslint/require-doctype': ['error'],
        '@html-eslint/no-duplicate-id': ['error'],
        '@html-eslint/no-inline-styles': ['error'],
        '@html-eslint/require-li-container': ['error'],
        '@html-eslint/no-obsolete-tags': ['error'],
        '@html-eslint/require-closing-tags': [
            'error',
            {
                selfClosing: 'always',
                allowSelfClosingCustom: false
            }
        ],
        '@html-eslint/require-meta-charset': ['error'],
        '@html-eslint/no-target-blank': ['error'],
        '@html-eslint/no-duplicate-attrs': ['error'],
        '@html-eslint/require-button-type': ['error'],
        '@html-eslint/no-restricted-attrs': [
            'error',
            {
                tagPatterns: ['^img$', '^div$'],
                attrPatterns: ['^data-.*'],
                message: 'Do not use data-* attr'
            }
        ],
        // accessibility
        '@html-eslint/require-img-alt': ['error'],
        '@html-eslint/no-skip-heading-levels': ['error'],
        '@html-eslint/require-frame-title': ['error'],
        '@html-eslint/no-non-scalable-viewport': ['error'],
        '@html-eslint/no-positive-tabindex': ['error'],
        '@html-eslint/require-meta-viewport': ['error'],
        '@html-eslint/no-abstract-roles': ['error'],
        '@html-eslint/no-aria-hidden-body': ['error'],
        '@html-eslint/no-accesskey-attrs': ['error']
    }*/
};
