/**
 * the activated Ruleset is found here: https://eslint.org/docs/latest/rules/
 */

module.exports = {
    /*
    rules: {
        // ----- Possible Problems: These rules relate to possible logic errors in code:
        'array-callback-return': [
            'error',
            {
                allowImplicit: false,
                checkForEach: true
            }
        ],
        'constructor-super': ['error'],
        'for-direction': ['error'],
        'getter-return': [
            'error',
            {
                allowImplicit: false
            }
        ],
        'no-async-promise-executor': ['error'],
        'no-await-in-loop': ['error'],
        'no-class-assign': ['error'],
        'no-compare-neg-zero': ['error'],
        'no-cond-assign': ['error', 'always'],
        'no-const-assign': ['error'],
        'no-constant-binary-expression': ['error'],
        'no-constant-condition': [
            // TODO: eventually error?
            'warn',
            {
                checkLoops: true
            }
        ],
        'no-constructor-return': ['error'],
        'no-control-regex': ['warn'],
        'no-debugger': ['error'],
        'no-dupe-args': ['error'],
        'no-dupe-class-members': ['error'],
        'no-dupe-else-if': ['error'],
        'no-dupe-keys': ['error'],
        'no-duplicate-case': ['error'],
        'no-duplicate-imports': [
            'error',
            {
                includeExports: true
            }
        ],
        'no-empty-character-class': ['error'],
        'no-empty-pattern': ['error'],
        'no-ex-assign': ['error'],
        'no-func-assign': ['error'],
        'no-import-assign': ['error'],
        'no-inner-declarations': ['error', 'both'],
        'no-invalid-regexp': [
            'error',
            {
                allowConstructorFlags: [
                    // with this flag the search is case-insensitive: no difference between A and a (see the example below).
                    'i',
                    // with this flag the search looks for all matches, without it – only the first match is returned.
                    'g',
                    // multiline mode (covered in the chapter Multiline mode of anchors ^ $, flag "m").
                    'm',
                    // enables “dotall” mode, that allows a dot . to match newline character \n (covered in the chapter Character classes).
                    's',
                    // enables full Unicode support. The flag enables correct processing of surrogate pairs. More about that in the chapter Unicode: flag "u" and class \p{...}.
                    'u',
                    // “Sticky” mode: searching at the exact position in the text (covered in the chapter Sticky flag "y", searching at position)
                    'y'
                ]
            }
        ],
        'no-irregular-whitespace': [
            'error',
            // TODO: check difference of options on code examples
            {
                skipStrings: false,
                skipComments: false,
                skipRegExps: false,
                skipTemplates: false
            }
        ],
        'no-loss-of-precision': ['error'],
        'no-misleading-character-class': ['error'],
        'no-new-symbol': ['error'],
        'no-obj-calls': ['error'],
        'no-promise-executor-return': ['error'],
        'no-prototype-builtins': ['off'],
        'no-self-assign': [
            'error',
            {
                props: true
            }
        ],
        'no-self-compare': ['error'],
        'no-setter-return': ['error'],
        'no-sparse-arrays': ['error'],
        'no-template-curly-in-string': ['error'],
        'no-this-before-super': ['error'],
        'no-undef': [
            // currently changed to warn until get a good solution
            'warn',
            {
                typeof: true
            }
        ],
        // TODO: check this rule on code examples
        'no-unexpected-multiline': ['error'],
        'no-unmodified-loop-condition': [
            // TODO: eventually error?
            'warn'
        ],
        'no-unreachable': ['error'],
        'no-unreachable-loop': ['error'],
        'no-unsafe-finally': ['error'],
        'no-unsafe-negation': [
            'error',
            {
                enforceForOrderingRelations: true
            }
        ],
        'no-unsafe-optional-chaining': [
            'error',
            {
                disallowArithmeticOperators: true
            }
        ],
        'no-unused-private-class-members': ['error'],
        'no-unused-vars': [
            'error',
            {
                vars: 'all',
                varsIgnorePattern: '',
                args: 'all',
                ignoreRestSiblings: true,
                argsIgnorePattern: '^_',
                destructuredArrayIgnorePattern: '^_',
                // TODO: eventually not needed and change to `none`
                caughtErrors: 'all'
            }
        ],
        'no-use-before-define': [
            'error',
            {
                functions: true,
                classes: true,
                variables: true
            }
        ],
        'no-useless-backreference': ['error'],
        'require-atomic-updates': [
            'error',
            {
                allowProperties: false
            }
        ],
        'use-isnan': [
            'error',
            {
                enforceForSwitchCase: true,
                enforceForIndexOf: true
            }
        ],
        'valid-typeof': [
            'error',
            {
                requireStringLiterals: true
            }
        ],
        // ----- Suggestions: These rules suggest alternate ways of doing things:
        'accessor-pairs': [
            'error',
            {
                setWithoutGet: true,
                getWithoutSet: false,
                enforceForClassMembers: true
            }
        ],
        'arrow-body-style': [
            'error',
            'as-needed',
            {
                requireReturnForObjectLiteral: false
            }
        ],
        'block-scoped-var': ['error'],
        'camelcase': [
            'error',
            {
                properties: 'always',
                ignoreDestructuring: false,
                ignoreImports: false,
                ignoreGlobals: false,
                allow: []
            }
        ],
        'capitalized-comments': [
            // TODO: eventually error?
            'warn',
            'never',
            {
                ignorePattern: 'TODO:',
                ignoreInlineComments: false,
                ignoreConsecutiveComments: false
            }
        ],
        'class-methods-use-this': ['off'],
        'consistent-return': [
            'error',
            {
                treatUndefinedAsUnspecified: false
            }
        ],
        'consistent-this': ['error', 'that'],
        'curly': ['error', 'all'],
        'default-case': [
            'error',
            {
                commentPattern: ''
            }
        ],
        'default-case-last': ['error'],
        'default-param-last': ['error'],
        'dot-notation': [
            'error',
            {
                allowPattern: '',
                allowKeywords: false
            }
        ],
        'eqeqeq': [
            'error',
            'always',
            {
                null: 'ignore'
            }
        ],
        'func-name-matching': [
            'error',
            'always',
            {
                considerPropertyDescriptor: true,
                // TODO: eventually change to `false`?
                includeCommonJSModuleExports: true
            }
        ],
        'func-names': [
            'error',
            'as-needed',
            {
                generators: 'as-needed'
            }
        ],
        'func-style': [
            'error',
            'declaration',
            {
                allowArrowFunctions: true
            }
        ],
        'grouped-accessor-pairs': ['error', 'getBeforeSet'],
        'guard-for-in': [
            // TODO: decide if warn or off
            'warn'
        ],
        'id-denylist': ['error', 'any', 'Number', 'number', 'String', 'string', 'Boolean', 'boolean', 'Undefined', 'undefined'],
        'id-length': [
            'error',
            {
                min: 3,
                // TODO: find value for maxlength
                max: Infinity,
                properties: 'always',
                exceptions: [
                    '_',
                    // counter
                    'i',
                    // bootstrap grid
                    'xs',
                    'sm',
                    'md',
                    'lg',
                    'xl',
                    // often used options
                    'ws',
                    'id',
                    'db',
                    'in'
                ]
            }
        ],
        'id-match': ['off'],
        'init-declarations': ['off'],
        'max-classes-per-file': [
            'error',
            {
                max: 1,
                // TODO: eventually activate?
                ignoreExpressions: false
            }
        ],
        'max-depth': [
            'error',
            {
                max: 4
            }
        ],
        'max-lines': [
            'error',
            {
                max: 300,
                skipBlankLines: true,
                skipComments: true
            }
        ],
        'max-lines-per-function': [
            'error',
            {
                max: 50,
                skipBlankLines: true,
                skipComments: true,
                IIFEs: false
            }
        ],
        'max-nested-callbacks': [
            'error',
            {
                max: 3
            }
        ],
        'max-params': [
            'error',
            {
                max: 5
            }
        ],
        'max-statements': [
            // disabled due to other explicit rules
            'off'
        ],
        'multiline-comment-style': ['error', 'starred-block'],
        'new-cap': [
            'error',
            {
                newIsCap: true,
                newIsCapExceptions: [],
                newIsCapExceptionPattern: '',
                capIsNew: true,
                capIsNewExceptions: [
                    // decorator: Angular
                    'Component',
                    'NgModule',
                    'Directive',
                    'Input',
                    'ViewChild',
                    'Injectable',
                    // decorator: NestJS
                    'Controller',
                    'Get',
                    'Module',
                    'Injectable',
                    // express Classes
                    'Router'
                ],
                capIsNewExceptionPattern: '',
                properties: true
            }
        ],
        'no-alert': ['error'],
        'no-array-constructor': ['error'],
        'no-bitwise': [
            'error',
            {
                allow: [],
                int32Hint: false
            }
        ],
        'no-caller': ['error'],
        'no-case-declarations': ['error'],
        'no-confusing-arrow': [
            'error',
            {
                allowParens: false,
                onlyOneSimpleParam: false
            }
        ],
        'no-console': ['error'],
        'no-continue': [
            // TODO: test this setting and decide for "error" or "off" if "error" activate rule in sonarqube
            'warn'
        ],
        'no-delete-var': ['error'],
        'no-div-regex': ['error'],
        'no-else-return': [
            'error',
            {
                allowElseIf: false
            }
        ],
        'no-empty': [
            'error',
            {
                allowEmptyCatch: false
            }
        ],
        'no-empty-function': [
            'error',
            {
                allow: []
            }
        ],
        'no-eq-null': [
            // rule `eqeqeq` is more powerfull
            'off'
        ],
        'no-eval': [
            'error',
            {
                allowIndirect: false
            }
        ],
        'no-extend-native': [
            'error',
            {
                exceptions: []
            }
        ],
        'no-extra-bind': ['error'],
        'no-extra-boolean-cast': [
            'error',
            {
                enforceForLogicalOperands: true
            }
        ],
        'no-extra-label': ['error'],
        'no-extra-semi': ['error'],
        'no-floating-decimal': ['error'],
        'no-global-assign': [
            'error',
            {
                exceptions: []
            }
        ],
        'no-implicit-coercion': [
            'error',
            {
                boolean: true,
                number: true,
                string: true,
                disallowTemplateShorthand: true,
                allow: []
            }
        ],
        'no-implied-eval': ['error'],
        'no-inline-comments': [
            'error',
            {
                ignorePattern: ''
            }
        ],
        'no-invalid-this': [
            'error',
            {
                capIsConstructor: true
            }
        ],
        'no-iterator': ['error'],
        'no-label-var': ['error'],
        'no-labels': ['error'],
        'no-lone-blocks': ['error'],
        'no-lonely-if': ['error'],
        'no-loop-func': ['error'],
        'no-magic-numbers': ['error'],
        'no-mixed-operators': [
            'error',
            {
                allowSamePrecedence: true
            }
        ],
        'no-multi-assign': [
            'error',
            {
                ignoreNonDeclaration: false
            }
        ],
        'no-multi-str': ['error'],
        'no-negated-condition': ['error'],
        'no-nested-ternary': ['error'],
        'no-new': ['error'],
        'no-new-func': ['error'],
        'no-new-object': ['error'],
        'no-new-wrappers': ['error'],
        'no-nonoctal-decimal-escape': ['error'],
        'no-octal': ['error'],
        'no-octal-escape': ['error'],
        'no-param-reassign': [
            'error',
            {
                props: true
            }
        ],
        'no-plusplus': [
            'error',
            {
                allowForLoopAfterthoughts: false
            }
        ],
        'no-proto': ['error'],
        'no-redeclare': [
            'error',
            {
                builtinGlobals: true
            }
        ],
        'no-regex-spaces': ['error'],
        'no-restricted-exports': ['off'],
        'no-restricted-globals': ['off'],
        'no-restricted-imports': ['off'],
        'no-restricted-properties': ['off'],
        'no-restricted-syntax': ['off'],
        'no-return-assign': ['error', 'always'],
        'no-return-await': ['error'],
        'no-script-url': ['error'],
        'no-sequences': [
            'error',
            {
                allowInParentheses: false
            }
        ],
        'no-shadow': [
            'error',
            {
                builtinGlobals: true,
                hoist: 'all',
                allow: [],
                ignoreOnInitialization: false
            }
        ],
        'no-shadow-restricted-names': ['error'],
        'no-ternary': ['off'],
        'no-throw-literal': ['error'],
        'no-undef-init': ['error'],
        'no-undefined': ['error'],
        'no-underscore-dangle': [
            'error',
            {
                allowAfterThis: true
            }
        ],
        'no-unneeded-ternary': [
            'error',
            {
                defaultAssignment: true
            }
        ],
        'no-unused-expressions': [
            'error',
            {
                allowShortCircuit: false,
                allowTernary: false,
                allowTaggedTemplates: false,
                enforceForJSX: true
            }
        ],
        'no-unused-labels': ['error'],
        'no-useless-call': ['error'],
        'no-useless-catch': [
            // TODO: check on code examples. eventuelally disable?
            'error'
        ],
        'no-useless-computed-key': [
            'error',
            {
                enforceForClassMembers: false
            }
        ],
        'no-useless-concat': ['error'],
        'no-useless-constructor': ['error'],
        'no-useless-escape': ['error'],
        'no-useless-rename': [
            'error',
            {
                ignoreImport: false,
                ignoreExport: false,
                ignoreDestructuring: false
            }
        ],
        'no-useless-return': ['error'],
        'no-var': ['error'],
        'no-void': [
            'error',
            {
                allowAsStatement: true
            }
        ],
        'no-with': ['error'],
        'object-shorthand': ['error', 'never'],
        'one-var': ['error', 'never'],
        'one-var-declaration-per-line': ['error', 'always'],
        'operator-assignment': ['error', 'always'],
        'prefer-arrow-callback': [
            'error',
            {
                allowNamedFunctions: false,
                allowUnboundThis: false
            }
        ],
        'prefer-const': [
            'error',
            {
                destructuring: 'any',
                ignoreReadBeforeAssign: false
            }
        ],
        'prefer-destructuring': [
            // TODO: deside if this should be activated (read docs again)
            'off'
        ],
        'prefer-exponentiation-operator': ['error'],
        'prefer-named-capture-group': ['off'],
        'prefer-numeric-literals': ['off'],
        'prefer-object-has-own': ['error'],
        'prefer-object-spread': ['error'],
        'prefer-promise-reject-errors': [
            'error',
            {
                allowEmptyReject: true
            }
        ],
        'prefer-regex-literals': [
            'error',
            {
                disallowRedundantWrapping: true
            }
        ],
        'prefer-rest-params': ['error'],
        'prefer-spread': ['error'],
        'prefer-template': ['error'],
        'quote-props': [
            'error',
            'consistent-as-needed',
            {
                keywords: false,
                unnecessary: true,
                numbers: true
            }
        ],
        'radix': ['error', 'always'],
        'require-await': ['error'],
        'require-unicode-regexp': ['error'],
        'require-yield': ['error'],
        'sort-imports': ['off'],
        'sort-keys': ['off'],
        'sort-vars': ['off'],
        'spaced-comment': [
            'error',
            'always',
            {
                markers: ['/'],
                exceptions: ['-', '+']
            }
        ],
        'strict': ['off'],
        'symbol-description': ['error'],
        'vars-on-top': ['error'],
        'yoda': [
            'error',
            'never',
            {
                exceptRange: true,
                onlyEquality: false
            }
        ],
        // ----- Layout & Formatting: These rules care about how the code looks rather than how it executes:
        'array-bracket-newline': ['error', 'consistent'],
        'array-bracket-spacing': [
            'error',
            'never',
            {
                singleValue: false,
                objectsInArrays: false,
                arraysInArrays: false
            }
        ],
        'array-element-newline': ['error', 'consistent'],
        'arrow-parens': ['error', 'always'],
        'arrow-spacing': [
            'error',
            {
                before: true,
                after: true
            }
        ],
        'block-spacing': ['error', 'always'],
        'brace-style': [
            'error',
            '1tbs',
            {
                allowSingleLine: false
            }
        ],
        'comma-dangle': ['error', 'always-multiline'],
        'comma-spacing': [
            'error',
            {
                before: false,
                after: true
            }
        ],
        'comma-style': ['error', 'last'],
        'computed-property-spacing': [
            'error',
            'never',
            {
                enforceForClassMembers: true
            }
        ],
        'dot-location': ['error', 'property'],
        'eol-last': ['error', 'always'],
        'func-call-spacing': ['error', 'never'],
        'function-call-argument-newline': ['error', 'consistent'],
        'function-paren-newline': ['off'],
        'generator-star-spacing': [
            'error',
            {
                before: false,
                after: true
            }
        ],
        'implicit-arrow-linebreak': ['off'],
        'jsx-quotes': ['error', 'prefer-double'],
        'key-spacing': [
            'error',
            {
                beforeColon: false,
                afterColon: true
            }
        ],
        'keyword-spacing': [
            'error',
            {
                before: true,
                after: true
            }
        ],
        'line-comment-position': [
            // TODO: change to error?
            'warn',
            'above'
        ],
        'linebreak-style': ['warn', 'unix'],
        'lines-around-comment': [
            'error',
            {
                beforeBlockComment: false,
                afterBlockComment: false,
                beforeLineComment: false,
                afterLineComment: false,
                allowBlockStart: true,
                allowBlockEnd: true,
                allowClassStart: false,
                allowClassEnd: false,
                allowObjectStart: true,
                allowObjectEnd: false,
                allowArrayStart: true,
                allowArrayEnd: false,
                ignorePattern: '',
                applyDefaultIgnorePatterns: true
            }
        ],
        'lines-between-class-members': [
            'error',
            'always',
            {
                exceptAfterSingleLine: false
            }
        ],
        'max-len': [
            'error',
            {
                code: 100,
                tabWidth: 4,
                comments: 60,
                ignoreComments: true,
                ignoreTrailingComments: true,
                ignoreUrls: true,
                ignoreStrings: true,
                ignoreTemplateLiterals: true,
                ignoreRegExpLiterals: true,
                ignorePattern: 'import'
            }
        ],
        'max-statements-per-line': [
            'error',
            {
                max: 1
            }
        ],
        'multiline-ternary': ['error', 'always-multiline'],
        'new-parens': ['error', 'always'],
        'newline-per-chained-call': [
            'error',
            {
                ignoreChainWithDepth: 2
            }
        ],
        'no-extra-parens': [
            'error',
            'all',
            {
                conditionalAssign: false,
                returnAssign: false,
                nestedBinaryExpressions: false,
                ignoreJSX: 'all',
                enforceForArrowConditionals: true,
                enforceForSequenceExpressions: true,
                enforceForNewInMemberExpressions: true,
                enforceForFunctionPrototypeMethods: true
            }
        ],
        'no-mixed-spaces-and-tabs': ['error'],
        'no-multi-spaces': [
            'error',
            {
                ignoreEOLComments: false,
                exceptions: {}
            }
        ],
        'no-multiple-empty-lines': [
            'error',
            {
                max: 2,
                maxEOF: 1,
                maxBOF: 0
            }
        ],
        'no-tabs': [
            'error',
            {
                allowIndentationTabs: true
            }
        ],
        'no-whitespace-before-property': ['error'],
        'nonblock-statement-body-position': ['error', 'beside'],
        'object-curly-newline': [
            'error',
            {
                ObjectExpression: {
                    minProperties: 1
                },
                ObjectPattern: {
                    multiline: true
                },
                ImportDeclaration: {
                    consistent: true
                },
                ExportDeclaration: {
                    multiline: true,
                    minProperties: 3
                }
            }
        ],
        'object-curly-spacing': ['error', 'always'],
        'object-property-newline': [
            'error',
            {
                allowAllPropertiesOnSameLine: false
            }
        ],
        'operator-linebreak': [
            'error',
            'after',
            {
                overrides: {
                    '?': 'before',
                    ':': 'before'
                }
            }
        ],
        'padded-blocks': ['error', 'never'],
        'padding-line-between-statements': ['off'],
        'quotes': [
            'error',
            'single',
            {
                avoidEscape: true,
                allowTemplateLiterals: true
            }
        ],
        'rest-spread-spacing': ['error', 'never'],
        'semi': ['error', 'always'],
        'semi-spacing': [
            'error',
            {
                before: false,
                after: true
            }
        ],
        'semi-style': ['error', 'last'],
        'space-before-blocks': ['error', 'always'],
        'space-before-function-paren': [
            'error',
            {
                anonymous: 'never',
                named: 'always',
                asyncArrow: 'always'
            }
        ],
        'space-in-parens': ['error', 'never'],
        'space-infix-ops': [
            'error',
            {
                int32Hint: false
            }
        ],
        'space-unary-ops': ['error'],
        'switch-colon-spacing': [
            'error',
            {
                after: true,
                before: false
            }
        ],
        'template-curly-spacing': ['error', 'never'],
        'template-tag-spacing': ['error', 'never'],
        'unicode-bom': ['error', 'never'],
        'wrap-iife': [
            'error',
            'inside',
            {
                functionPrototypeMethods: true
            }
        ],
        'wrap-regex': ['error'],
        'yield-star-spacing': ['error', 'after']
    },
    overrides: [
        {
            files: ['*.ts', '*.tsx', '*.js', '*.jsx'],
            rules: {
                // ----- Possible Problems: These rules relate to possible logic errors in code:

                // ----- Suggestions: These rules suggest alternate ways of doing things:
                'no-implicit-globals': [
                    'error',
                    {
                        lexicalBindings: true
                    }
                ],
                // ----- Layout & Formatting: These rules care about how the code looks rather than how it executes:
                'indent': [
                    'error',
                    4,
                    {
                        ignoredNodes: [],
                        SwitchCase: 0,
                        VariableDeclarator: 'first',
                        outerIIFEBody: 1,
                        MemberExpression: 1,
                        FunctionDeclaration: {
                            body: 1,
                            parameters: 'first'
                        },
                        FunctionExpression: {
                            body: 1,
                            parameters: 'first'
                        },
                        StaticBlock: {
                            body: 1
                        },
                        CallExpression: {
                            arguments: 'first'
                        },
                        ArrayExpression: 'first',
                        ObjectExpression: 'first',
                        ImportDeclaration: 'first',
                        flatTernaryExpressions: true,
                        offsetTernaryExpressions: true,
                        ignoreComments: false
                    }
                ],
                'no-trailing-spaces': [
                    'error',
                    {
                        skipBlankLines: false,
                        ignoreComments: false
                    }
                ]
            }
        }
    ]
    */
};
